<?
if(!Yii::app()->request->isAjaxRequest)
	echo CHtml::openTag('div', $this->elementsAttributes['commentsList']);

if($this->paginationType == CommentsWidget::PAGES_TOGGLE)
	 $this->commentsPage();
else $this->commentsOffset();

if(!Yii::app()->request->isAjaxRequest)
	echo CHtml::closeTag('div');
?>