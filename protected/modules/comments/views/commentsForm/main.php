<?
// подгрузка страница в AJAX-режиме
if(Yii::app()->request->isAjaxRequest)
	$this->render($this->views['list']);

// обычный режим
else
{
	?>
	<div id="messages">
	<?
	// сообщения системы комментирования

	if($commentsSuccess = Yii::app()->user->getFlash('commentsSuccess'))
	{
		?><div class="alert alert-block alert-success fade in"><?
			echo $commentsSuccess;
		?></div><?
	}

	if($commentsError = Yii::app()->user->getFlash('commentsError'))
	{
		?><div class="alert alert-block alert-error fade in"><?
			echo $commentsError;
		?></div><?
	}
	?>
	</div>
	<?

	// основная форма (комментарии первого уровня)
	$this->render($this->views['form'], array(
		'owner_id' => $this->owner->id,
		'model' => get_class($this->owner),
		'timestamp' => time(),
		'parent_id' => NULL,
	));

	// кнопка "оставить комментарий" (показывает форму отправки)
	echo CHtml::tag('a',
		$this->elementsAttributes['addCommentButton'],
		Yii::t('comments', 'Add comment')
	);

	// список уже добавленных комментариев
	$this->render($this->views['list']);

}