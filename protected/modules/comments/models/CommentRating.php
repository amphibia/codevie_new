<?php

/**
 * @author koshevy
 * 
 * Модель отметки рейтинга комментария. Связана с моеделью комментариев Comment.
 * При сохранениии производит индексацию аттрибутов рейтинга (likes/dislikes)
 * в связанной модели.
 * 
 * Для манипуляциий с рейтингом, в классе описанны статические методы-хелперы
 * добавления и удаления отметки:
 *	CommentRating::addRating($comment, $value = self::RATING_LIKE)
 *	CommentRating::removeRating($comment)
 * 
 * Эти методы используются в при реализации функционала работы с рейтингом в
 * связанной модели:
 *	Comment::like()
 *	Comment::dislike()
 * 
 * Чтобы узнать текущее значени рейтинга, используйте индекируемые свойства
 * связанной модели
 *	Comment::$likes
 *	Comment::$dislikes
 *	
 */
class CommentRating extends ActiveRecord
{
	const RATING_LIKE = '1';
	const RATING_DISLIKE = '-1';

	/**
	 * Голосование за комментарий от имени текущего пользователя.
	 * Если пользователь проголосовал до этого, предыдущая отметка отменяется.
	 *
	 * @param mixed $comment объект или ID комментария
	 * @param integer $value self::RATING_LIKE или self::RATING_DISLIKE
	 * @return mixed созданая запись, или NULL в случае ошибки
	 */
	public static function addRating($comment, $value = self::RATING_LIKE)
	{
		$comment_id = ($comment instanceof Comment)
			? $comment->id : (int)$comment;

		// ссылка на проголосовавшего пользователя
		if(	Yii::app()->hasComponent('users') && Yii::app()->user &&
			!Yii::app()->user->isGuest)
			 $user_id = Yii::app()->user->model->id;
		else return NULL;

		// удаление предыдущей отметки
		self::model()->deleteAllByAttributes(array(
			compact('comment_id', 'user_id')
		));

		$newRating = new self;
		$newRating->setAttributes(compact(
			'comment_id', 'value', 'user_id'
		), false);

		if(!$newRating->save()) return false;
		else return $newRating;
	}

	/**
	 * Удаление отметки рейтинга для указанного коментария и текущего
	 * пользователя.
	 * @param boolean
	 */
	public static function removeRating($comment)
	{
		$comment_id = ($comment instanceof Comment)
			? $comment->id : (int)$comment;
		
		// ссылка на проголосовавшего пользователя
		if(	Yii::app()->hasComponent('users') && Yii::app()->user &&
			!Yii::app()->user->isGuest)
			 $user_id = Yii::app()->user->model->id;
		else return NULL;

		return self::model()->deleteAllByAttributes(array(
			compact('comment_id', 'user_id')
		));
	}

    public function behaviors()
	{
		return array
		(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			),

			// Работа с POST-ом для MANY_TO_MANY.
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
		);
    }

    public function relations(){
		return array('comment' => array(self::BELONGS_TO, 'Comment', 'comment_id'));
	}

    public function tableName()
    {
		static $tableName = NULL;

		if(!$tableName)
		{
			$originalName = 'comments_rating';
			$postfix = NULL;
			if(($lang = Yii::app()->language) != 'ru') $postfix = "_$lang";
			$tableName = "{{".$originalName."$postfix}}";
		}

		return $tableName;
    }

    public function scopes()
    {
        return array
        (
            'likes' => array('condition' => "t.value=".self::RATING_LIKE),
            'dislikes' => array('condition' => "t.value=".self::RATING_DISLIKE),
        );
    }

	/**
	 * Фильтр по конкретному комментарию.
	 * @param mixed $comment ID или объект комментария
	 */
	public function comment(mixed $comment)
	{
		$comment_id = ($comment instanceof Comment)
			? (int)$comment->id : (int)$comment;

		$this->dbCriteria->addColumnCondition(compact('comment_id'));
		return $this;
	}


	protected function afterSave() {
		$result = parent::afterSave();

		// индексирование результата для комментария
		$this->comment->setAttributes(array(
			'index_likes' => self::model()->comment($this->comment)->likes()->count(),
			'index_dislikes' => self::model()->comment($this->comment)->dislikes()->count()
		), false);

		$this->comment->save();
	}

	public function rules()
	{
		return array
        (
			array('comment_id, user_id', 'numerical', 'integerOnly' => true),
			array('value', 'in', 'range' => array(self::RATING_LIKE, self::RATING_DISLIKE)),
			array('comment_id, user_id, value', 'unsafe'),

			array('comment_id, user_id, value', 'required'),
		);
	}

}