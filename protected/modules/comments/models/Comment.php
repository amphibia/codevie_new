<?php

/**
 * @author koshevy
 * 
 * Модель для работы с комментариями. Может быть привязана к любой модели по
 * имени класса и первичному ключу объекта. Комментарий не может быть создан
 * до записи модели-хозяина в базу.
 * 
 * 
 * Система комментирования устроена с учетом вложенности. Используется механизм
 * NESTED-SETS с множеством корней. Для комментариев привязанных к каждому новому
 * элементу создается новая ветка с собственным корнем - чтобы при просчете
 * дерева, происходящим при записи комментария, не приходлось проходиться по
 * всем комментариям в базе.
 * 
 * При сохранении комментария, его предок также индексируется в
 * свойстве Comment::$parent_id.
 * 
 * 
 * Имеет связи с моделью CommentsRating (система оценки комментариев) и с автором
 * комментария (если используется модуль Users), а также методы для обращения
 * к модели-хозяину, родительскому комментарию и т.п.
 * 
 * Реализован функционал работы с рейтингом, фильтры по пользователю и модели
 * хозяина.
 * 
 * 
 * Для создания нового комментария используйте статический метод - хелпер
 * Comment::createComment(CActiveRecord $owner, $parent, $subject = NULL, $text).
 */
class Comment extends ExtCoreNestedAR
{
	/**
     * @var boolean 
     */
	public $hasManyRoots = true;
	
	/**
	 * Создание комментария.
	 * Создает корень ветки для конкретного элемента модели, если он не создан,
	 * и привязывает к нему комментарий.
	 * 
	 * Если указан предок, добавляет его к подветке указанного комментария.
	 * 
	 * @param CActiveRecorde $owner
	 * @param mixed $parent Comment-объект родителcкого коментария, либо его ID
	 * @param type $subject
	 * @param type $text
	 * @param CActiveRecord созданный комментарий
	 */
	public static function createComment(CActiveRecord $owner, $parent, $subject = NULL, $text)
	{
		if($owner->isNewRecord){
			throw new CDbException(Yii::t('comments', 'Owner model can`t be new!'));
		}

		$newComment = new self;
		$newComment->setAttributes(array(
			'subject' => $subject,
			'text' => $text,
			'owner_id' => $owner->id,
			'model' => get_class($owner)
		), false);

		// добавление подкомментария к существующему
		if($parent)
		{
			if(!is_object($parent)) $parent = self::cachedInstance($parent);
			if(!$parent) throw new CDbException(Yii::t('comments',
				'Parent comment is not exists!'));

			$newComment->parent_id = $parent->id;
			$newComment->appendTo($parent, true);
		}

		// добавление комментария первого уровня
		else
		{
			// поиск корня для этого элемента
			$rootLevel = self::model()->roots()->findByAttributes(array(
				'owner_id' => $owner->id,
				'model' => get_class($owner),
			));

			// создание корня при первом добавлении
			if(!$rootLevel)
			{
				$rootLevel = new self;
				$rootLevel->setAttributes(array(
					'model'=>get_class($owner),
					'owner_id'=>$owner->id,
					'text'=>'-'
				), false);

				if(!$rootLevel->save()){
					throw new CDbException(Yii::t('comments',
						'Can`t create root comment item!'));
				}
			}

			$newComment->parent_id = $rootLevel->id;
			$newComment->appendTo($rootLevel, true);
		}

		return $newComment;
	}

	/**
	 * Поставить отметку "нравится" для этого комментария от имени текущего
	 * пользователя.
	 * @return boolean
	 */
	public function like(){
		if($this->isNewRecord) return false;
		return CommentRating::addRating($comment, self::RATING_LIKE);
	}

	/**
	 * Поставить отметку "не нравится" для этого комментария от имени текущего
	 * пользователя.
	 * @return boolean
	 */
	public function dislike(){
		if($this->isNewRecord) return false;
		return CommentRating::addRating($comment, self::RATING_DISLIKE);
	}



	/** ** ** ** ** ** ** ** ** ** ** 
	 * Внутренние технические методы */

    public function behaviors()
	{
		return array
		(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			),

			// Работа с POST-ом для MANY_TO_MANY.
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
		);
    }

    public function init()
	{
		parent::init();
		Yii::loadExtConfig($this, __FILE__);
    }

    public function tableName()
    {
		static $tableName = NULL;

		if(!$tableName)
		{
			$originalName = 'comments';
			$postfix = NULL;
			if(($lang = Yii::app()->language) != 'ru') $postfix = "_$lang";
			$tableName = "{{".$originalName."$postfix}}";
		}

		return $tableName;
    }

	public function relations()
	{
		$relations = array(
			'ratings' => array(self::HAS_MANY, 'CommentRating', 'comment_id'),
		);

		// связь с автором
		if(Yii::app()->hasComponent('users')){
			$relations['user'] = array(
				self::BELONGS_TO, Yii::app()->users->modelClass, 'user_id');
		}

		return $relations;
	}


	/**
	 * Производил ли пользователь голосование за этот комментарий.
	 *
	 * @param mixed $user ID/объект пользователя или NULL - для текущего пользователя
	 * @return boolean
	 */
	public function isVoted($user = NULL)
	{
		if($this->isNewRecord) return false;

		if(!Yii::app()->hasComponent('users'))
			throw new CException(Yii::t('comments', 'Users module not installed!'));

		// текущий пользователь
		if(is_null($user) && !Yii::app()->user->isGuest)
			$user = Yii::app()->user->model;

		if($user_id = is_object($user) ? $user->id : (int)$user)
		{
			// запрос на существующие записи рейтинга
			return (bool)CommentRating::model()
				->comment($this->id)->user($user_id)
				->count();
		}
		else return false;

	}


	/**
	 * Непосредственный предок (обращение по индексу).
	 * @return Comment
	 */
	public function getParent(){
		return $this->parent_id
			? self::cachedInstance($this->parent_id) : NULL;
	}

	/**
	 * Обращение к модели хозяина комментария.
	 * @return CActiveRecord
	 */
	public function getOwner(){
		if($this->isNewRecord) return NULL;
		$ownerClass = $this->model;
		return $ownerClass::cachedInstance($this->owner_id);
	}

	/**
	 * Общее количество отметок рейтинга.
	 * @return integer
	 */
	public function getTotalRatings(){
		return $this->index_likes + $this->index_dislikes;
	}

	/**
	 * Сумма рейтинга.
	 * @return integer
	 */
	public function getRatingsResult(){
		return $this->index_likes - $this->index_dislikes;
	}
	
	/**
	 * Отсортировка по пользователю.
	 * @param mixed $user объект или ID пользователя
	 * @return Comment
	 */
	public function user($user)
	{
		// должна быть установлена пользовательская система
		if(!Yii::app()->hasComponent('users')){
			throw new CException(Yii::t('comments', 'Users module not installed!'));
		}

		$modelClass = Yii::app()->users->modelClass;
		$user_id = ($user instanceof $modelClass) ? (int)$user->id : (int)$user;

		$this->dbCriteria->addColumnCondition(compact('user_id'));

		return $this;
	}
	
	/**
	 * Отсортировка по хозяину (объект модели).
	 * @param CActiveRecord $model
	 * @return Comment
	 */
	public function owner(CActiveRecord $model){
		if(!$model->isNewRecord)
		return $this->ownerByID(get_class($model), $model->id);
	}
	
	/**
	 * Отсортировка по классу модели и ID хозяина.
	 * @param type $modelClass
	 * @param type $ownerID
	 */
	public function ownerByID($modelClass, $ownerID)
	{
		$this->dbCriteria->addColumnCondition(array(
			'model' => $modelClass,
			'owner_id' => $ownerID
		));

		return $this;
	}

	public function scopes(){
		return array(
			'deleted' => array('condition' => 't.deleted=1'),
			'published' => array('condition' => 'NOT t.deleted'),

			// только корни (технические записи)
			'roots' => array('condition' => "t.{$this->levelAttribute}=1"),

			// только комментарии первого уровня
			'firstLevel' => array('condition' => "t.{$this->levelAttribute}=2"),

			// только непосредственно комментарии (без корней)
			'withoutRoots' => array('condition' => "t.{$this->levelAttribute}>1"),
		);
	}


	/** ** ** ** ** ** ** ** ** ** ** 
	 * Методы и настройки валидации */
	
	public function rules()
	{
		$rules = array
		(
            array('parent_id, owner_id, user_id, deleted, edited, date_created, date_changed', 'numerical', 'integerOnly'=>true),
			array('model', 'match', 'pattern'=>'/^[A-Z][a-zA-Z0-9_]{1,63}$/'),
			array('parent_id, owner_id, user_id, deleted, model, edited, index_likes, index_dislikes', 'unsafe'),

			array('text', 'length', 'max' => Yii::app()->commentsagent->maxCommentLength),
			array('owner_id, model, text', 'required'),
			array('subject', 'safe'),
			array('deleted, edited', 'default', 'value' => 0)
		);

		return $rules;
	}

	public function beforeValidate() 
	{
		if($this->isNewRecord)
		{
			// информация об авторе
			$this->ip = $_SERVER['REMOTE_ADDR'];
			$this->user_agent = $_SERVER['HTTP_USER_AGENT'];
			if(Yii::app()->hasComponent('users') && Yii::app()->user){
				if(!Yii::app()->user->isGuest){
					$this->user_id = Yii::app()->user->model->id;
				}
			}
		}
		else{
			unset($this->ip, $this->user_agent);
			$this->edited = 1;
		}

		$this->subject = htmlspecialchars($this->subject);
		$this->text = htmlspecialchars($this->text);

		return parent::beforeValidate();
	}

	/**
	 * Удаление связей вслед за объектом.
	 */
	public function afterDelete() {
		parent::afterDelete();
		CommentRating::model()->deleteAll("comment_id=:comment_id",
			array(':comment_id'=>$this->id));
	}


	public function __toString(){ return $this->text; }

}