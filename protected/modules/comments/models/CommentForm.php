<?php

/**
 * @author koshevy
 * 
 * Модель формы отправки комментария.
 * Предназначена для получения промежуточных данных, проверки и последующей
 * записи в модель Comment.
 * 
 * После сохранения (или попытки сохранения, при условии, что успешно пройдена
 * валидация формы) хранит объект модели Comment, в которую производилась запись
 * в свойстве CommentForm::$commentModel. Если на этапе сохранения в базу
 * возникли ошибки, они хранятся в этой модели.
 */
class CommentForm extends CFormModel
{
    // данные формы
	public	$parent_id, $owner_id, $model, $subject, $text,
			$signature, $timestamp, $captcha;

	/**
	 * Вновь созданная модель комментария (после сохранения).
	 * @var Comment
	 */
	public $commentModel = NULL;

	/**
	 * Модель-хозяин комментария.
	 * @var CActiveRecord
	 */
	public $owner = NULL;

	/**
	 * Модель-предок комментария.
	 * @var Comment
	 */
	public $parent = NULL;



	public function init()
	{
		parent::init();
		Yii::loadExtConfig($this, __FILE__);
	}

	/**
	 * Генерация подписи для формы.
	 * @param integer $parent_id
	 * @param integer $owner_id
	 * @param string $model
	 * @param integer $timeStamp
	 * @return string
	 */
	public static function createSignature($parent_id, $owner_id, $model, $timeStamp){
		return md5(
			Yii::app()->commentsagent->formSignatureSalt . session_id() .
			$parent_id . $owner_id . $model . $timeStamp
		);
	}

    public function rules()
    {
		$purifier = new CHtmlPurifier();
		$purifier->options = array(
			'Attr.EnableID' => true,
			'Attr.AllowedFrameTargets'=> array('_blank','_top'),
		);

		$rules = array
		(
			array('text', 'length', 'max' => Yii::app()->commentsagent->maxCommentLength),

			array('signature, owner_id, model, text, timestamp', 'required'),
			array('parent_id, owner_id, timestamp', 'numerical', 'integerOnly' => true),
			array('subject, text', 'filter', 'filter'=>array($purifier, 'purify')),
			array('model', 'match', 'pattern'=>'/^[a-zA-Z0-9_]{1,64}$/'),
			array('signature', 'signatureCheck')
		);

		if(Yii::app()->commentsagent->useCaptcha){
			$rules[] = array('captcha', 'required');
			$rules[] = array('captcha', 'captcha');
		}

		return $rules;
    }

	/**
	 * Проверка подписи
	 */
	public function signatureCheck()
	{
		if(isset($_POST['token'])) throw new CHttpException(404);
		$trueSignature = $this->createSignature(
			$this->parent_id, $this->owner_id, $this->model, $this->timestamp);

		if($this->signature != $trueSignature)
			$this->addError('signature', 'Invalid form signature.');
	}

	/**
	 * Проверка предка комментария (для вложенных комментариев).
	 */
	public function parentCheck()
	{
		if($this->parent_id)
		{
			$this->parent = Comment::cachedInstance($this->parent_id);
			if(!$this->parent)
				$this->addError('parent_id', Yii::t(
					'comments', 'Can`t find parent comment!'));

			else if(
				($this->parent->model != $this->model) ||
				($this->parent->owner_id != $this->owner_id) )

				$this->addError('parent_id', Yii::t(
					'comments', 'Wrong parent comment!'));

			else if($this->parent->{$this->parent->levelAttribute} >=
					(Yii::app()->commentsagent->maxNestingLevel + 1))

				$this->addError('parent_id', Yii::t(
					'comments', 'The maximum level of nesting is reached.'));		
		}
	}

	/**
	 * Проверка модели - хозяина.
	 */
	public function ownerCheck()
	{
		if($this->owner_id && $this->model)
		{
			$modelClass = $this->model;
			$this->owner = $modelClass::cachedInstance($this->owner_id);

			if(!$this->owner)
				$this->addError('parent_id', Yii::t(
					'comments', 'Can`t owner model!'));
		}
	}
	
    public function relations(){
		return array();
    }
	
	public function attributeLabels() {
		return array(
			'username' => Yii::t('attributes', 'Username'),
			'password' => Yii::t('attributes', 'Password'),
		);
	}

	protected function beforeValidate() {
		$result = parent::beforeValidate();
		$this->signatureCheck();
		$this->ownerCheck();
		$this->parentCheck();
		return $result;
	}

	public function  save()
	{
		if($this->validate())
		{
			$this->commentModel = Comment::createComment(
				$this->owner, $this->parent, $this->subject, $this->text);
			
			/**
			 * @todo: подумать над защитой от брутфорса
			 */

			return !$this->commentModel->hasErrors();
		}
		else return false;
	}
			
}

?>
