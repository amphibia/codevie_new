<?php
/**
 * @title Comments system
 * @description {Comments manage and interfaces}
 * @vendor koshevy
 * @preload yes
 * @package comments
 * @requirements{component:users}
 */
class CommentsModule extends ExtCoreModule
{
}
