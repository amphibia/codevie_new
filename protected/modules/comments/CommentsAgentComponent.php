<?php

/**
 * @title Comments agent agent
 * @description { Register admin services. }
 * @preload yes
 * @vendor koshevy
 * @package comments
 * @requirements{component:users, module:comments}
 */
class CommentsAgentComponent extends CApplicationComponent
{

	/**
	 * Сообщение об успешной отправке комментария.
	 * @var string
	 */
	public $successMessage = 'Comment sent successfully';

	/**
	 * Сообщение о неудавшейся попытке отправки комментария.
	 * @var string
	 */
	public $failMessage = 'Comment sent failed';

	/**
	 * Будет ли испоьзоваться капча в формах отправки комментариев.
	 * @var boolean
	 */
	public $useCaptcha = false;
	
	/**
	 * Настройки капчи в форме отправки комментариев.
	 * @var array
	 */
	public $captchaOptions = array(
		'foreColor' => 0x999999,
		'class'=>'CCaptchaAction',
		'transparent' => true,
		'testLimit' => 1
	);

	/**
	 * Предполагается ли использовать ответы на комментарии.
	 */
	public $useAnswers = false;

	/**
	 * Размер одной страницы выводимых комментариев.
	 * @var int
	 */
	public $commentsPageSize = 10;

	/**
	 * Соль для формирования подписи к форме комментария.
	 * @var string
	 */
	public $formSignatureSalt = 'COMMENTS_SIGNATURE_';

	/**
	 * Максимальный уровень вложенности для комментариев.
	 * @var integer
	 */
	public $maxNestingLevel = 10;

	/**
	 * Максимальная длина комментария.
	 * @var integer
	 */
	public $maxCommentLength = 512;

	/**
	 * Количество комментариев на одной странице (значение по умолчанию, если
	 * не используется CommentsWidget::$pageSize).
	 * @var integer
	 */
	public $pageSize = 10;


	/**
	 * Формирование DB-зависимости при работе с комментариями.
	 * Может создавать привязку ко всем комментариям, либо только к комментариям
	 * одного хозяина.
	 * 
	 * @param string $modelClass
	 * @param int $ownerID
	 */
	public function getCommentsCacheDepency($modelClass = NULL, $ownerID = NULL){
		/* @TODO реализовать привязку к системе комментирования. */
	}

 	public function init()
    {
        Yii::import('application.modules.comments.*');
        Yii::import('application.modules.comments.models.*');
        Yii::import('application.modules.comments.services.*');
		Yii::loadExtConfig($this, __FILE__);

		Yii::app()->registerService('comments', 'CommentsAdminService', 'admin',
            array(
                'title' => 'Site structure management',
                'menuGroup' => 'comments'
            )
        );
	}

}
