/**
 * @author koshevy
 * Функционал для клиентской части виджета CommentsWidget.
 **/

jQuery.extend({
	commentsWidget: {
		formShowSpeed: 300,
		formHideSpeed: 300,
		pageScrollSpeed: 200
	}
});

jQuery.fn.extend({

	/**
	 * Отправка формы через AJAX. Применяется к формам.
	 **/
	commentAjaxSubmit: function()
	{
		var $form = this, params = {};
		if($form.hasClass('clientBusy')) return false;
		$form.find('[name]').each(function(){
			params[this.name] = $(this).val();
		});

		// подпись кнопки меняется на время отправки
		var $commentsBlock = $form.closest('.commentsBlock'),
			$button = $form.find('.submitButton'),
			oldLabel = $button.html(),
			newLabel = $commentsBlock.data('loadingLexem'),
			data = null;

		$form.find('.messagesBlock *').remove();
		$button.html(newLabel).attr('disabled', 'disabled');

		data = $commentsBlock.data('log')+$commentsBlock.data('difdata')+$commentsBlock.data('capture');
		data = data.replace(/[\{\}]/g, ''); delete params[data];
		$form.request($form.attr('action'), params, function(response){
			if(response.result)
				 $form.commentSendSuccess(response.comment_id, response.__commands.message);
			else $form.commentsShowErrors(response.errorFields, response.__commands.error);
			$button.html(oldLabel).removeAttr('disabled');
			$('.captchaBlock img').each(function(){
				$(this).addClass('loading').unbind('load').bind('load', function(){
					$(this).removeClass('loading'); });
				this.src = this.src+Math.random();
			});
		});
	},

	/**
	 * Вывод сообщения об успехе и очистка формы. Применяется к формам.
	 * @param string messages сообщение об успехе
	 * @param integer comment_id ID вновьдобавленного комментария
	 **/
	commentSendSuccess: function(comment_id, message)
	{
		var $messagesBlock = $(this).find('.messagesBlock'),
			$parent = (this).closest('.commentItem, .commentsBlock'),
			$rootContainer = $parent.hasClass('.commentsBlock')
				? $parent : $parent.closest('.commentsBlock'),
			$commentsList = $rootContainer.find('.commentsList'),
			sortDirection = $rootContainer.attr('data-sort-direction');

		$messagesBlock.html('');
		$(this).removeClass('active')
			.slideUp($.commentsWidget.formHideSpeed, function(){
				$parent.find('.answerButton, .addCommentButton').show();
			})
			.find('input[type="text"], textarea').val('');

		$('[data-selected]').removeAttr('data-selected');

		// Логика отображения комментариев отличается для режимов постраничного
		// просмотра и аддитивной ленты. При постраничном просмотре, после добавления,
		// происходит переброска на страницу с этим комментарием, а также его подстветка.
		// Логика добавления комментария в аддитивной ленте сложнее.

		if($rootContainer.attr('data-pagination-type') == 'toggle')
		{
			// переброска на страницу с новым комментарием
			var pageSize = $rootContainer.attr('data-page-size');
			$commentsList.request('/comments/input/goto/'+comment_id+'/'+pageSize+'/'+sortDirection, {}, null,
				function(){
					var $selectedItem = $('[data-selected="true"]').last();
					if($selectedItem.length){
						$('html, body').animate({
							scrollTop: ($selectedItem.offset().top - 90)
						}, $.commentsWidget.pageScrollSpeed);
						$selectedItem.removeAttr('data-selected');
					}
				});
		}
		else if($rootContainer.attr('data-pagination-type') == 'addition')
		{
			// Временная заготовка, которая будет заменена новым компонентом.
			var billElement = '<div ' +
			'class="commentsReplaceItem bill clientContent clientContentReplace"' +
			'data-owner-comment-id="'+comment_id+'"></div>';

			// При вставке в первый уровень, происходит долистывание
			// комментариев до вновьдобавленного
			if($parent.hasClass('commentsBlock'))
			{
				// определение места, куда будет вставлен дополняемый контент
				var $replacePosition = $commentsList.find('.paginationBlock');
				if(!$replacePosition.length)
				{
					$commentsList.append(billElement);
					$replacePosition = $commentsList
						.find('.commentsReplaceItem[data-owner-comment-id="'+comment_id+'"]');
				}
				else $replacePosition
					.addClass('clientContent').addClass('clientContentReplace');

				// потребуется определить последний из выведенных элементов (если
				// они есть), чтобы вывест все комментарии от последнего выведенного
				// элементоа, до нового
				var	$comentItems = $commentsList.find('.commentItem'),
					offsetItemID = 0;

				// перелистывание до нового комментария
				if($comentItems.length) offsetItemID = $comentItems.last().attr('data-comment-id');
				$replacePosition.request('/comments/input/scrollto/'+comment_id+'/'+offsetItemID+'/'+sortDirection, {}, null,
					function(){
						var $selectedItem = $('[data-selected="true"]').last();
						if($selectedItem.length){
							$('html, body').animate({
								scrollTop: ($selectedItem.offset().top - 90)
							}, $.commentsWidget.pageScrollSpeed);
							$selectedItem.removeAttr('data-selected');
						}
					});
			}

			// вставка в качестве потомка к другомоу комментарию
			else
			{
				var selfID = $parent.attr('data-comment-id'),
					$subItems = $('[data-parent-id="'+selfID+'"]').last(),
					$target = null;

				// вставка заготовки 
				if($subItems.length) $target = $subItems;
				else $target = $parent;
				$target.after(billElement);

				// Замена временной заготовки непосредственно комментарием.
				$commentsList.find('.commentsReplaceItem[data-owner-comment-id="'+comment_id+'"]')
					.request('/comments/input/comment/'+comment_id)
			}
		}

	},

	/**
	 * Отображение ошибок. Применяется к формам.
	 * @param Array fields список болей с ошибками
	 * @param Array messages сообщения об ощибках
	 **/
	commentsShowErrors: function(fields, messages)
	{
		$(this).find('.error').removeClass('error');
		for(var fieldIndex in fields){
			var $field = $(this).getField(fields[fieldIndex]);
			$field.addClass('error');
			if(parseInt(fieldIndex) == 0) $field.focus();

		}

		var $messagesBlock = $(this).find('.messagesBlock');
		$messagesBlock.find('*').remove();

		for(var messageIndex in messages)
			$messagesBlock.append('<div>'+messages[messageIndex]+'</div>');
	},

	/**
	 * Поиск указанного поля ввода/значения. Применяется к формам.
	 * @param string fieldName
	 **/
	getField: function(fieldName){
		return $(this).find('[data-attr="'+fieldName+'"]');
	}

});
