/**
 * @author koshevy
 * Клиентская часть виджета CommentsWidget - инициализации и рутинные операции.
 * Использует плагин commentsWidget.js.
 **/
$(document).ready(function()
{
	$('.commentsBlock form textarea').autosize();

	// отправки формы по AJAX
	$('.commentsBlock form').live('submit', function(){
		$(this).commentAjaxSubmit(); return false;
	});

	$('.commentsBlock form input, .commentsBlock form textarea').live(
		'keydown', function(){ $(this).removeClass('error') }
	);

	// отображение основной формы отправки комментария
	$('.answerButton').live('click', function(){
		$('.commentsBlock form.active').removeClass('active').stop(true)
			.slideUp($.commentsWidget.formHideSpeed, function(){
				$(this).closest('.commentItem').find('.answerButton').show();
			});

		$(this).hide();
		$(this).closest('.commentItem').find('form').addClass('active')
			.slideDown($.commentsWidget.formShowSpeed);
	});

	// кнопка "отправить комментарий"
	$('.commentsBlock .addCommentButton').live('click', function(){
		$('.commentsBlock .answerButton, .commentsBlock .addCommentButton').show();
		$('.commentsBlock form.active').removeClass('active').stop(true)
			.slideUp($.commentsWidget.formHideSpeed);

		$(this).fadeOut($.commentsWidget.formShowSpeed);
		$(this).closest('.commentsBlock').find('> form').addClass('active')
			.slideDown($.commentsWidget.formShowSpeed, function(){
				$(this).css('height', 'auto');
			});
	});

	// отображение формы ответа на комментарий
	$('.answerButton').live('click', function(){
		$('.commentsBlock .answerButton, .commentsBlock .addCommentButton').show();
		$('.commentsBlock form.active').removeClass('active').stop(true)
			.slideUp($.commentsWidget.formHideSpeed, function(){
			});

		$(this).fadeOut($.commentsWidget.formShowSpeed);
		$(this).closest('.commentItem').find('form').addClass('sliding').addClass('active')
			.slideDown($.commentsWidget.formShowSpeed, function(){
				$(this).removeClass('sliding');
				$(this).css('height', 'auto');
				$(this).css('overflow', 'visible');
			});
	});

	// кнопка "отменить"
	$('.cancelButton').live('click', function(){
		var $container = $(this).closest('.commentItem, .commentsBlock'),
			$addCommentButton = $container.find('.addCommentButton, .answerButton');

		$container.find('form.active').addClass('sliding').removeClass('active').stop(true)
			.slideUp($.commentsWidget.formHideSpeed, function(){
				$addCommentButton.show();
			});
	})

	// переключение страниц по AJAX (если используется нумерованный список
	// страниц)
	$('.commentsBlock #commentsPagination a').live('click', function(){
		var	$element = $(this),
			$commentsList = $element.closest('.commentsList'),
			href = $element.attr('href');

		if(	$commentsList.hasClass('clientBusy') ||
			$element.closest('li').hasClass('selected'))
			return false;
		$element.addClass('loading');
		$commentsList.request(href, {},
			function(){ $element.removeClass('loading'); },
			function(){
				$('html, body').animate({
					scrollTop:($commentsList.offset().top)
				}, $.commentsWidget.pageScrollSpeed);
			}
		);

		return false;
	});

	// подгрузка новых страниц по AJAX (если используется подгрузка новых страниц)
	$('.commentsBlock .paginationMoreLink').live('click', function(){
		var $lastCommentItem = $('.commentsBlock .commentItem').last(),
			lastItemID = $lastCommentItem.attr('data-comment-id'),
			href = $(this).attr('href');

		href = href.replace('{offset}', lastItemID);
		$(this).addClass('loading');
		$(this).closest('.paginationBlock').request(href, {}, null, function(){
			
		});

		return false;
	});
});