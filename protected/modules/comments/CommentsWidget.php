<?php

/**
 * @author koshevy
 * Вывод комментариев, привязанных к определенному элементу модели, а так же
 * форм для отправки.
 * 
 * Виджет работает с прилагающимся контроллером InputController - оба они
 * являются частью единной экосистемы модуля Comments.
 * 
 * @todo: ПРИМЕЧАНИЕ! Указывать параметры напрямую в месте вывода виджета
 * нет смысла, т.к. используемые настройки будут в любом случае недоступны
 * в подгружаемых элементах (при перелистывании страниц, добавлении комментариев
 * и тп). Указывать параметры нужно через общие конфиги для класса. Возможно,
 * чтобы не вводить в заблуждение, имеет смысл перенести аргументы виджета
 * в компонент CommentsAgent.
 * 
 * 
 * @todo: ПРИМЕЧАНИЕ: Корректная работа при PAGES_ADDITION и DESC еще не
 * отлажена. Добавление комментариев в этом режим не работает.
 * 
 * Формы реализованы для модели CommentForm. Виджеты CActiveForm и т.п. не используются.
 * Отправка по AJAX (ExtCore-JSON).
 */
class CommentsWidget extends CWidget
{
	// Тип постраничного просмотра содержимого (кнопки с номерами страниц
	// или продолжающаяся лента с кнопкой "добавить").
	const PAGES_TOGGLE = 'toggle';
	const PAGES_ADDITION = 'addition';

	/**
	 * Единственный комментарий, который нужно вывести.
	 * Если этот параметр указан, выводится только запись об указанном комментарии.
	 * @var Comment
	 */
	public $singleComment = NULL;

	/**
	 * ID элемента, который требуется подсветить.
	 * @var integer
	 */
	public $selectedItem = NULL;

	/**
	 * Комментируемый элемент.
	 * @var CActiveRecord
	 */
	public $owner = NULL;


	public $views = array(
		'main' => 'commentsForm/main',
		'form' => 'commentsForm/_form',
		'list' => 'commentsForm/_list',
	);
	
	public $assets = array(
		'js' => array('jquery.autosize.js', 'commentsWidget.js', 'routine.js'),
		'css' => array('common.css'),
	);
	
	/**
	 * Аттрибуты используемых элементов
	 * 
	 * ВНИМАНИЕ! JavaScript, обслуживающий этот виджет жестко привязан к указанным
	 * аттрибутам "data-attr" и классам некоторых элементов (в частности, блока
	 * для вывода сообщений).
	 * 
	 * @var array
	 */
	public $elementsAttributes = array
	(
		'commentsBlock' => array('class' => 'commentsBlock'),

		'subjectBlock' => array('type' => 'text', 'class' => 'subjectBlock'),
		'subjectField' => array('class' => 'subjectField', 'data-attr'=>'subject', 'placeholder' => 'Enter subject...'),

		'textBlock' => array('class'=>'textBlock'),
		'textField' => array('class'=>'textField', 'data-attr'=>'text', 'required'=>'required', 'placeholder'=>'Enter text...'),

		'rateBlock' => array('class'=>'rateBlock'),

		'captchaBlock' => array('class'=>'captchaBlock'),
		'captchaField' => array('class'=>'captchaField', 'data-attr'=>'captcha', 'placeholder'=>'Captcha code', 'required'=>'required'),

		'addCommentButton' => array('class'=>'addCommentButton', 'href'=>'javascript:;'),
		'answerButton' => array('class'=>'answerButton', 'href'=>'javascript:;'),
		'buttonsBlock' => array('class'=>'buttonsBlock'),
		'messagesBlock' => array('class'=>'messagesBlock'),
		'submitButton' => array('class'=>'submitButton', 'type'=>'submit'),
		'cancelButton' => array('class'=>'cancelButton', 'type'=>'button'),

		'ownerField' => array('data-attr'=>'owner'),
		'modelField' => array('data-attr' => 'model'),
		'parentField' => array('data-attr' => 'parent'),
		'timestampField' => array('data-attr' => 'timestamp'),
		'signatureField' => array('data-attr' => 'signature'),

		'commentsList' => array('class' => 'commentsList clientContent'),
		'commentItem' => array('class' => 'commentItem'),
		'commentSubject' => array('class' => 'commentSubject'),
		'commentText' => array('class' => 'commentText'),
		
		'deletedComment' => array('class' => 'deletedComment'),

		'paginationBlock' => array('class' => 'paginationBlock clientContent clientContentReplace'),
		'paginationMoreLink' => array('class' => 'paginationMoreLink'),

		'userInfo' => array('class' => 'userInfo'),
	);

	/**
	 * Количество комментариев на одной странице.
	 * Если NULL, значение берется из CommentsAgent::$pageSize.
	 * 
	 * ПРИМЕЧАНИЕ: это значение говорит о том, сколько комментариев нужно вывести
	 * первоначально - при формировании страницы. При дальнейшем листании и
	 * AJAX-подгрузках, размер страницы, также, будет равен CommentsAgent::$pageSize.
	 * @var integer
	 */
	public $pageSize = NULL;

	/**
	 * Номер отображаемой страницы комментариев.
	 * Если указать "last", выведет последнюю страницу.
	 * 
	 * Свойство используется при указании постраничного вывода.
	 * Если указан режим дополняемой ленты, используется 'offset'.
	 * @var integer
	 */
	public $currentPage = 1;

	/**
	 * Стартовая позиция, с которой следует выводить комментарии.
	 * 
	 * Свойство используется при указании $paginationType как self::PAGES_ADDITION.
	 * В режиме постраничного вывода используется значение $currentPage.
	 * @var integer
	 */
	public $offset = 0;

	/**
	 * Использование рейтинга (нравится/не нравится)
	 * @var boolean
	 */
	public $useRating = NULL;

	/**
	 * Возможность отвечать на комментарии.
	 * @var boolean
	 */
	public $useAnswers = NULL;

	/**
	 * Будет ли использоваться тема комментария.
	 * @var boolean
	 */
	public $useSubject = false;

	/**
	 * Надпись на кнопке отправки.
	 * @var string
	 */
	public $submitTitle = 'Send comment';

	/**
	 * Надпись на кнопке отмены.
	 * @var string
	 */
	public $cancelTitle = 'Cancel';
	
	/**
	 * Подпись к полю темы комментария.
	 * @var string
	 */
	public $subjectLabel = NULL;

	/**
	 * Подпись к полю текста комментария.
	 * @var string
	 */
	public $textLabel = NULL;

	/**
	 * Подпись к полю ввода капчи.
	 * @var string
	 */
	public $captchaLabel = NULL;
	
	/**
	 * Подпись анонимного пользователя.
	 * @var string
	 */
	public $anonymUserLabel = 'Anonymus';

	/**
	 * Свойство, переопределяющее метод виджета, выводящий информацию об авторе
	 * комментария (будет использоваться вместо CommentsWidget::_showUserInfo($comment)).
	 * @var callback
	 */
	public $userInfoMethod = NULL;

	/**
	 * Роут для постраничного списка.
	 * @var string
	 */
	public $paginationRoute = '/comments/input/page/';

	/**
	 * Роут на следующую страницу (если $paginationType = self::PAGES_ADDITION).
	 * @var string
	 */
	public $additionRoute = '/comments/input/offset/';
	
	/**
	 * Тип листания страниц с комментариями.
	 * @var string
	 */
	public $paginationType = self::PAGES_ADDITION;

	/**
	 * Количество кнопок в переключателе страниц.
	 * @var integer
	 */
	public $paginationMaxButtonCount = 5;

	/**
	 * Направление сортировки комментариев при выводе (ASC/DESC).
	 * @var string
	 */
	public $sortDirection = 'DESC';



	public function init()
	{
		Yii::loadExtConfig($this, __FILE__);

		// Виджет может работать только с установленным модулем 'comments'
		// и прилагающимся к нему компонентом 'commentsAgent'.
		if(!Yii::app()->hasModule('comments')) throw new CException(
			Yii::t('comments', 'CommentsWidget require CommentsModule, installed as "comments".'));
		if(!Yii::app()->hasComponent('commentsagent')) throw new CException(
			Yii::t('comments', 'CommentsWidget require CommentsAgentComponent, installed as "commentsagent".'));
		

		// работа в режиме единственного комментария
		if($this->singleComment){
			$this->_showComment($this->singleComment);
			return;
		}

		Yii::applyAssets($this, __FILE__);
		if(!$this->pageSize) $this->pageSize = Yii::app()->commentsagent->pageSize;

		if($this->owner->isNewRecord){
			throw new CException(Yii::t('comments',
				'Comments owner cant be new record!'));
		}
		
		$this->sortDirection = strtoupper($this->sortDirection);
		if($this->sortDirection != 'ASC')
			$this->sortDirection = 'DESC';

		// блок с комментариями
		$this->elementsAttributes['commentsBlock'] = array_merge(
			$this->elementsAttributes['commentsBlock'], array(
				'id' => $this->id,
				'data-pagination-type' => $this->paginationType,
				'data-page-number' => $this->currentPage,
				'data-page-size' => $this->pageSize,
				'data-sort-direction' => $this->sortDirection,
				'data-timestamp' => time(),
			)
		);

		if(!Yii::app()->request->isAjaxRequest)
			echo CHtml::openTag('div', $this->elementsAttributes['commentsBlock']);
		
		// данные для клиентской части
		$loadingLexem = addslashes(Yii::t('comments', 'Comment sent'));
		Yii::app()->clientScript->registerScript('loadingLexem',
			"$('#{$this->id}').data('loadingLexem', '{$loadingLexem}')",
			CClientScript::POS_READY);

		// часть механизма защиты от подделок формы
		Yii::app()->clientScript->registerScript('loadingLexem',
		"$('#{$this->id}').data('difdata', '{ok}');" .
		"$('#{$this->id}').data('log', '{t}');" .
		"$('#{$this->id}').data('capture', '{en}');" ,
		CClientScript::POS_READY);
		
	}

	public function run()
	{
		if($this->singleComment) return;
		$this->render($this->views['main']);

		// закрытие тега с комментариями
		if(!Yii::app()->request->isAjaxRequest)
			echo CHtml::closeTag('div');
	}

	/**
	 * Вывод страницы с комментариями. Используется для отображения комментариев,
	 * если $this->paginationType == self::PAGES_TOGGLE.
	 */
	public function commentsPage()
	{
		$commentsModel = Comment::model()->withoutRoots()->owner($this->owner);
		$commentsModel->dbCriteria->order = "t.{$commentsModel->leftAttribute} {$this->sortDirection}";
		$activeDataProvider = $commentsModel->getActiveDataProvider();
		$activeDataProvider->pagination->pageSize = $this->pageSize;
		$activeDataProvider->pagination->setItemCount($commentsModel->count);
		$activeDataProvider->pagination->route = $this->paginationRoute;
		$activeDataProvider->pagination->params = array(
			'model' => get_class($this->owner),
			'owner_id' => $this->owner->id,
			'pageSize' => $this->pageSize,
			'sortDirection' => $this->sortDirection,
		);

		if($this->currentPage == 'last')
			$this->currentPage = $activeDataProvider->pagination->pageCount;
		$activeDataProvider->pagination->currentPage = $this->currentPage
			? $this->currentPage-1 : 0;

		// вывод комментариев
		foreach($activeDataProvider->data as $comment)
			$this->_showComment($comment);

		// переключение страниц
		
		echo CHtml::openTag('div', $this->elementsAttributes['paginationBlock']);

		?><div class="pagination"><?
		$this->widget('CLinkPager',array(
			'id' => 'commentsPagination',
			'pages' => $activeDataProvider->pagination,
			'maxButtonCount' => $this->paginationMaxButtonCount,
			'cssFile' => false,
			'firstPageLabel' => false,
			'lastPageLabel' => false,
			'header' => false,
			'prevPageLabel' => Yii::t('comments', '← Previous'),
			'nextPageLabel' => Yii::t('comments', 'Next →'),
		));
		?></div><?

		echo CHtml::closeTag('div');
	}

	/**
	 * Вывод списка комментариев начиная со стартовой позиции.
	 * Используется, если $this->paginationType == self::PAGES_ADDITION.
	 */
	public function commentsOffset()
	{
		$commentsModel = Comment::model()->withoutRoots()->owner($this->owner);
		$totalCount = $commentsModel->count;
		$commentsModel->dbCriteria->order = "t.{$commentsModel->leftAttribute} {$this->sortDirection}";
		$commentsModel->dbCriteria->offset = $this->offset;
		$commentsModel->dbCriteria->limit = $this->pageSize;

		foreach($commentsModel->findAll() as $comment)
			$this->_showComment($comment);
		
		echo CHtml::openTag('div', $this->elementsAttributes['paginationBlock']);

		if($totalCount > ($this->offset + $this->pageSize))
		echo CHtml::link(Yii::t('comments', 'More comments'),
			Yii::app()->urlManager->createUrl(
				$this->additionRoute, array(
					'model' => get_class($this->owner),
					'owner_id' => $this->owner->id,
					'offsetItemID' => '{offset}',
					'pageSize' => $this->pageSize,
					'sortDirection' => $this->sortDirection,
				)
			),
			$this->elementsAttributes['paginationMoreLink']
		);

		echo CHtml::closeTag('div');
	} 

	/**
	 * Вывод одного комментария.
	 * @param Comment $comment
	 */
	protected function _showComment(Comment $comment)
	{
		$attributes = $this->elementsAttributes['commentItem'];
		$attributes['data-comment-id'] = $comment->id;
		$attributes['data-parent-id'] = $comment->parent_id;
		$attributes['data-show-timestamp'] = time(); // время отображения комментария
		$attributes['level'] = $comment->{$comment->levelAttribute} - 1;
		if($comment->deleted) $attributes['data-deleted'] = 'true';
		if($comment->edited) $attributes['data-edited'] = 'true';
		if(($comment->id == $this->selectedItem) || $this->singleComment)
			$attributes['data-selected'] = 'true';

		echo CHtml::openTag('div', $attributes);

		$this->_showUserInfo($comment);
		echo CHtml::tag('div', array('class'=>'date'),
			Yii::app()->dateFormatter->formatDateTime($comment->date_created, 'long')
		);
		
		// комментарий не был удален
		if(!$comment->deleted)
		{
			if($comment->subject)
				echo CHtml::tag('h4',
					$this->elementsAttributes['commentSubject'],
					$comment->subject);

			echo CHtml::tag('div',
				$this->elementsAttributes['commentText'],
				CHtml::tag('span', array(), $comment->text));

			if(Yii::app()->commentsagent->useAnswers)
			{
				$this->render($this->views['form'], array(
					'owner_id' => $comment->owner_id,
					'model' => $comment->model,
					'timestamp' => time(),
					'parent_id' => $comment->id,
				));

				echo CHtml::tag('a',
					$this->elementsAttributes['answerButton'],
					Yii::t('comments', 'Answer') );
			}
		
		}
		// удаленный комментарий
		else
		{
			if($comment->subject)
				echo CHtml::tag('div',
					$this->elementsAttributes['deletedComment'],
					Yii::t('comments', 'Comment was deleted by moderator!'));
		}

		echo CHtml::closeTag('div');
	}

	/**
	 * Вывод информации о пользователе.
	 * @param Comment $comment
	 */
	protected function _showUserInfo($comment = NULL)
	{
		if($comment)
		{
			if(	$comment->user_id &&
				Yii::app()->hasComponent('users') &&
				($user = User::cachedInstance($comment->user_id))
			)	$userName = $user->username;
		}
		else if(Yii::app()->hasComponent('users') && Yii::app()->user){
			$user = Yii::app()->user->model;
			if($user) $userName = $user->username;
		}

		if(!isset($userName)) $userName = $this->anonymUserLabel;
		echo CHtml::tag('div', $this->elementsAttributes['userInfo'], $userName);
	}
}

?>
