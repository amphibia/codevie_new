<?php

/*
 * Административный сервис - управление переводами
 */

class TranslateAdminService extends ExtCoreService
{

    public $modelClass = 'TranslateForm';
    /**
     * @var string the base path for all translated messages. Defaults to null, meaning
     * the "messages" subdirectory of the application directory (e.g. "protected/messages").
     */
    public $basePath;

    public $saved = false;

    public $assets = array(
        'css' => array('translate.css'),
        'js' => array('translate.js'),
    );

    public $views = array(
        'index' => 'index',
    );

    public function getRbacOperation()
    {
        return 'Translation management';
    }

    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.translate.views');
        Yii::import('application.modules.users.models.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        return parent::beforeAction($action);
    }

    public function actionIndex($categoryCurrent = null)
    {
        if ($this->basePath === null)
            $this->basePath = Yii::getPathOfAlias('application.messages');

        $categories = array();
        $categories = array_filter(glob($this->basePath . '/' . Yii::app()->language . '/*.php'), 'is_file');
        $categories = array_map(function ($value) {
            $pathinfo = pathinfo($value);
            return $pathinfo['filename'];
        }, $categories);

        if (!$categoryCurrent) {
            $categoryCurrent = $categories[0];
        } elseif (!is_file($this->basePath . '/' . Yii::app()->language . '/' . $categoryCurrent . '.php')) {
            Yii::app()->request->redirect($this->createUrl('index', array($categories[0])));
        }

        if (Yii::app()->request->isPostRequest) {
            $filename = $this->basePath . '/' . Yii::app()->language . '/' . $categoryCurrent . '.php';
            $tArray = array();
            $tString = "<?php\n\nreturn array(\n";
            if (is_array($_POST['names']) && is_array($_POST['values']) &&
                (count($_POST['names']) == count($_POST['values']))
            ) {

                foreach ($_POST['names'] as $key => $value) {
                    if (trim($value)) {
                        $value = str_replace("'", "\'", stripslashes(htmlspecialchars_decode($value, ENT_QUOTES)));
                        $tString .= "	'" . $value . "'			=> '" . htmlspecialchars($_POST['values'][$key], ENT_QUOTES) . "',\n";
                    }
                }
                $tString .= ");";
                // Обновляем файл с новыми записями
                file_put_contents($filename, $tString);
                $this->saved = true;
            }
        }

        $messages = include $this->basePath . '/' . Yii::app()->language . '/' . $categoryCurrent . '.php';

        $this->render($this->views['index'], array(
            'categoryCurrent' => $categoryCurrent,
            'categories' => $categories,
            'messages' => $messages,
        ));
    }

}