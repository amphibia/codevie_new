<?php

return array
(
	"views" => array('form' => 'options/form',),
    "modelClass" => 'AuthOptionsForm',

	"formConfig" => array(
		
		'elements' => array(
			'from_email' => array('type'=>'text', 'class'=>'span3'),
			'return_path' => array('type'=>'text', 'class'=>'span3',
				'placeholder'=>Yii::t('users', 'Using "From" field')),
			'from_title' => array('type'=>'text', 'class'=>'span4'),
			'subject_verify' => array('type'=>'text', 'class'=>'span4'),
			'subject_forgot_password' => array('type'=>'text', 'class'=>'span4'),
			
			'<br/>',
			
			'bcc' => array('type'=>'text', 'class'=>'span3',
				'placeholder'=>Yii::t('users', 'No copies')),
			'type' => array('type'=>'default', 'class'=>'span2'),
			'<div class="accountData">',
			'username' => array('type'=>'text', 'class'=>'span2'),
			'host' => array('type'=>'text', 'class'=>'span3'),
			'port' => array('type'=>'text', 'class'=>'span1'),
			'password' => array('type'=>'password', 'class'=>'span2',
				'placeholder'=> Yii::t('users', 'Enter new password...')),
			'</div>',

			'<br/>',
		),
		
		'buttons' => array(
			'save'=>array(
				'label' => Yii::t('users', 'Save changes'),
				'type' => 'submit',
				'icon' => 'icon-check',
			)
		)
	),
);