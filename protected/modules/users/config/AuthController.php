<?php
return array(
    'views' => array(
        'loginForm' => 'auth/loginForm'
    ),

    'assets' => array(
        'css' => array('auth.css')
    ),
);