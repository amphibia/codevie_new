<?php

return array
(
	/**
	 * Модель пользователя, с которой работает компонент.
	 * @var string
	 */
	"modelClass" => 'User',

	/**
	 * Класс модели роли пользователя.
	 * @var string
	 */
	"roleModelClass" => 'AuthRole',

	/**
	 * Класс модели операции, требующей доступа.
	 * @var string
	 */
	"operationModelClass" => 'AuthOperation',

	/**
	 * Класс модели записи о входе.
	 * @var string
	 */
	"loginModelClass" => 'AuthLogin',
	
	/**
	 * Класс модели профайла.
	 * @var string
	 */
	"profileModelClass" => 'UserProfile',

	/**
	 * Класс нового компонента "user", которым подменяется родной компонент Yii.
	 * @var string
	 */
	"userComponentClass" => 'UserComponent',

	/**
	 * Класс модели формы авторизации.
	 * @var string
	 */
	"formModelClass" => 'LoginForm',

	/**
	 * Класс модели формы авторизации.
	 * @var string
	 */
	"registrationModelClass" => 'RegistrationForm',

	/**
	 * Класс модели формы запроса на восстановление пароля.
	 * @var string
	 */
	'recoveryModelClass' => 'PasswordRecoveryForm',

	/**
	 * Класс модели формы смены пароля.
	 * @var string
	 */
	'changeModelClass' => 'PasswordChangeForm',
	
	/**
	 * CUserIdentity - элемент, используемый для входа.
	 * @var string
	 */
	"userIdentityClass" => 'UserIdentity',

	/**
	 * Требуется подтверждение зарегистрированного пользователя,
	 * без которого он вспринимается как гость.
	 *
	 * @var boolean
	 */
	'needVerification' => true,

	/**
	 * RBAC - операция по умолчанию.
	 * Проименяется для контроллеров, к которым не указана RBAC - операция.
	 * @var string
	 */
	"rbacDefaultOperation" => 'Controllers',

	/**
	 * URL формы входа. Значение будет указано в одноименное свойство компонента
	 * 'user'. Здесь указано, чтобы было удобней конфигурировать из одного места.
	 * @var string
	 */
	"loginUrl" => '/auth/login',

	/**
	 * Адрес выхода.
	 * @var string
	 */
	"logoutUrl" => '/auth/logout',

	/**
	 * Длительность сессии входа. Будет передано аругентом в метод
	 * CWebUser::login(IUserIdentity $identity, integer $duration=>0) 
	 * @var int
	 */
	"loginDuration" => (60 * 60) * 24,

	/**
	 * Сообщение, что требуется авторизация.
	 * @var strung
	 */
	"loginRequiredAjaxResponse" => Yii::t('users', 
		'Need authorization. Please, {a}log in{/a}.',
		array('{a}'=>'<a href="/auth/login">', '{/a}'=>'</a>')
	),


	/**
	 * Аккаунт для PhpMailerComponent.
	 * @var array
	 */
	"mailerAccount" => array
	(
		'type' => 'smtp',
		'html' => true,
		'SMTPDebug' => 'true',

		'options' => array(
			'CharSet' => 'utf-8',
		),
	),

	// события по умолчанию
	"onRegistration" => array($component, '_onRegistration'),
	"onPasswordRecovery" => array($component, '_onPasswordRecovery'),
	"onBeforeActions" => function(){
		// по умолчанию, используется Bootstrap, чтобы не использовать его,
		// достаточно переписать это свойство в конфиге
		//Yii::import(Yii::localExtension( "bootstrap", "components.*" ));
		//Yii::app()->setComponent("bootstrap", new Bootstrap);
	},

);

