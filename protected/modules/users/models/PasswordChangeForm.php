<?php

/**
 * Модель формы регистрации.
 * Проверяет вводимые данные и занимается непосредственной сменой пароля.
 * 
 * Для работы с пользователем, необходимо указать модель конкретного пользователя.
 * Данная модель не осуществляет поиск пользователя.
 */
class PasswordChangeForm extends CFormModel
{
	/**
	 * Пользователь, с которым работает форма.
	 * @var User
	 */
	public $user = NULL;


    public $password;
	public $retype_password;


    public function rules()
	{
        return array
		(
			array('password', 'required', 'message' => Yii::t('users', 'Enter your new password')),
			array('retype_password', 'required', 'message' => Yii::t('users', 'Retype your password')),
			array('retype_password', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t('users', 'Passwords does`t match'))
		);
    }

    public function relations(){
        return array();
    }

	public function beforeValidate() {
		$this->user->setAttributes($this->attributes);

		if(!$result = $this->user->validate(array_keys($this->attributes), true))
			foreach($this->user->errors as $attribute => $errors)
				foreach($errors as $error) $this->addError($attribute, $error);

		return parent::beforeValidate();
	}

	/**
	 * Смена пароля.
	 */
	public function save(){
		if(!$this->validate()) return false;

		if(!$result = $this->user->save(false, $this->getSafeAttributeNames()))
			foreach($this->user->errors as $attribute => $errors)
				foreach($errors as $error) $this->addError($attribute, $error);
		
		// сброс блокировки формы входа
		else{
			if(class_exists('LoginForm') && Yii::app()->hasComponent('cache'))
				Yii::app()->cache->offsetUnset(LoginForm::_getBlockCacheID());
		}

		return $result;
	}

	public function attributeLabels() {
		return array(
			'password' => Yii::t('attributes', 'Enter your new password'),
			'retype_password' => Yii::t('attributes', 'Retype password'),
		);
	}

}



?>
