<?php

/**
 * Модель формы смены е-мейла.
 * Проверяет вводимые данные и занимается непосредственной сменой емейла.
 * 
 * Для работы с пользователем, необходимо указать модель конкретного пользователя.
 * Данная модель не осуществляет поиск пользователя.
 */
class EmailChangeForm extends CFormModel
{
	/**
	 * Пользователь, с которым работает форма.
	 * @var User
	 */
	public $user = NULL;


    public $email;


    public function rules()
	{
        return array
		(
			array('email', 'required', 'message' => Yii::t('users', 'Enter your new email')),
            array('email', 'email'),
		);
    }

    public function relations(){
        return array();
    }

	public function beforeValidate() {
		$this->user->setAttributes($this->attributes);

		if(!$result = $this->user->validate(array_keys($this->attributes), true))
			foreach($this->user->errors as $attribute => $errors)
				foreach($errors as $error) $this->addError($attribute, $error);

		return parent::beforeValidate();
	}

	/**
	 * Смена е-мейла.
	 */
	public function save(){
		if(!$this->validate()) return false;

		if(!$result = $this->user->save(false, $this->getSafeAttributeNames()))
			foreach($this->user->errors as $attribute => $errors)
				foreach($errors as $error) $this->addError($attribute, $error);
		
		// сброс блокировки формы входа
		else{
			if(class_exists('LoginForm') && Yii::app()->hasComponent('cache'))
				Yii::app()->cache->offsetUnset(LoginForm::_getBlockCacheID());
		}

		return $result;
	}

	public function attributeLabels() {
		return array(
			'email' => Yii::t('users', 'Enter your new email'),
		);
	}

}
