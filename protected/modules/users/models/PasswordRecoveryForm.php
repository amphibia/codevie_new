<?php

/**
 * Модель формы регистрации.
 * Производит проверку корректности параметров, поиск соответствия пользователь/пароль
 * (для этого нужно указать $userModelClass - класс модели пользователя),
 * а также блокирует на время форму для этого пользователя при успешной отправке.
 */
class PasswordRecoveryForm extends CFormModel
{
    public $username;
	public $email;


	/**
	 * Время блокирования 
	 * @var type 
	 */
	public $blockTime = 29200; // на 8 часов блокирует, чтобы не хулиганили

	/**
	 * Модель заблокирована.
	 * @param boolean
	 */
	public $blocked = false;

	/**
	 * Класс модели пользователя, c которой работает модель регистрации.
	 * @var string
	 */
	public $userModelClass = NULL;

	/**
	 * Пользователь, с которым будет ассоциирована модель после
	 * успешной отправки формы.
	 * @var CModel
	 */
	protected $_user = NULL;
	public function getUser(){
		return $this->_user;
	}

	public function validate($attributes = null, $clearErrors = true){
		if(!parent::validate($attributes, $clearErrors))
			return false;

		// проверка уникальности мыла и имени
		$usersModel = new $this->userModelClass;
		$this->_user = $usersModel->findByAttributes(
			array('username'=>$this->username, 'email'=>$this->email)
		);

		if(!$this->_user){
			$this->addError('username', Yii::t('users', 'Username or email incorrect.'));
			return false;
		}

		return true;
	}

	/**
	 * Выдает разрешение на отправку.
	 * Сначала валидирует форму, а потом проверяет, не было ли недавно отправок
	 * с этого IP. Если да - также блокирует форму.
	 *
	 * @return boolean
	 */
	public function resolve()
	{
		if(!$this->validate()) return false;
		
		if(Yii::app()->hasComponent('cache'))
		{
			$cacheID = get_class($this)."_{$this->email}_recoveryBlocked";
			if(Yii::app()->cache->get($cacheID)){
				$this->blocked = true;
				return false;
			}

			Yii::app()->cache->set($cacheID, true, $this->blockTime);
		}
		
		return true;
	}

    public function rules(){
		return array(
			array('username', 'required', 'message' => Yii::t('users', 'Enter username')),
			array('email', 'required', 'message' => Yii::t('users', 'Enter email')),
			array('email', 'email', 'message' => Yii::t('users', 'Invalid email')),
		);
		
    }

    public function relations(){
        return array();
    }

	public function attributeLabels() {
		return array(
			'username' => Yii::t('attributes', 'Username'),
			'email' => Yii::t('attributes', 'Email'),
		);
	}
}



?>
