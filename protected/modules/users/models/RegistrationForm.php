<?php

/**
 * Модель формы регистрации.
 */
class RegistrationForm extends CFormModel
{
	/**
	 * Класс модели пользователя, c которой работает модель регистрации.
	 * @var string
	 */
	public $userModelClass = NULL;

	/**
	 * Класс модели роли, c которой работает модель регистрации.
	 * @var string
	 */
	public $roleModelClass = NULL;

	/**
	 * Класс модели профайла, c которой работает модель регистрации.
	 * @var string
	 */
	public $profileModelClass = NULL;
	
	/**
	 * Пользователь, с которым будет ассоциирована модель после
	 * успешной отправки формы.
	 * @var CModel
	 */
	protected $_user = NULL;
	public function getUser(){
		return $this->_user;
	}

    public $username;
	public $email;
    public $password;
	public $retype_password;
	public $status = 'registered';

    public function rules()
    {
        return array
        (
            array('username, email, password, retype_password', 'required'),
			array('password', 'length', 'min'=>6, 'max'=>32),
			array('retype_password', 'compare', 'compareAttribute'=>'password',
				'message'=>Yii::t('users', 'Passwords not match')
			),
			array('email', 'email'),
        );
    }

	public function validate($attributes = null, $clearErrors = true) {
		if(!parent::validate($attributes, $clearErrors))
			return false;

		// проверка уникальности мыла и имени
		$usersModel = new $this->userModelClass;
		if($usersModel->exists('username=:username',
			array(':username'=>$this->username) ))
			$this->addError('username', Yii::t('users', 'User with this username already exists.'));
		if($usersModel->exists('email=:email',
			array(':email'=>$this->email) ))
			$this->addError('email', Yii::t('users', 'User with this email already exists.'));

		return !$this->hasErrors();
	}

	public function attributeLabels() {
		return array(
			'username' => Yii::t('attributes', 'Username'),
			'email' => Yii::t('attributes', 'User email'),
			'password' => Yii::t('attributes', 'Password'),
			'retype_password' => Yii::t('attributes', 'Retype password'),
			'status' => Yii::t('attributes', 'User status'),
		);
	}
	
	/**
	 * Сохранение нового пользователя.
	 */
	public function save()
	{
		// роль по умолчанию
		$defaultRoleModel = new $this->roleModelClass;
		$defaultRole = $defaultRoleModel->find('default_role>0');
		if(!$defaultRole) throw new CException(
			Yii::t('Registration require default role!'), 500);

		$user = new $this->userModelClass;
		$user->setAttributes(array(
			'username' => $this->username,
			'email' => $this->email,
			'password' => $this->password,
			'retype_password' => $this->retype_password,
			'role_id' => $defaultRole->id,
			'status' => $this->status,
		));

		if(!$user->save()){
			$errorMessage = Yii::t('users', 'Errors with registration:');
			foreach($user->getErrors() as $attr => $errors)
				foreach($errors as $error)
					$errorMessage .= "$attr: $error; ";

			throw new CException($errorMessage, 500);
		}
		else{
			$this->_user = $user;
			return true;
		}
	}

    public function relations(){
        return array();
    }

}



?>
