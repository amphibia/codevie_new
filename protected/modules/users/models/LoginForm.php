<?php

/**
 * Модель формы входа в систему.
 * В модели реализована пристейшая защита от брутфорса. В случае, если с этой формы
 * слишком часто приходили запросы, не проходящие проверку, форма блокируется.
 * 
 * Учет попыток ведется с использованием системы кеширования. Настоятельно рекомендуется
 * подключать MEMCACHE.
 */
class LoginForm extends CFormModel
{
	/**
	 * Компонент входа,с которым работает модель.
	 * Если указывается, должен указываться вместе с компонентом UsersComponent,
	 * или его потомком - свойство $usersCompoonent.
	 * @var CUserIdendidty
	 */
	public $userIdentity = NULL;

	/**
	 * Компонент управления пользовательской системой.
	 * @var UsersComponent
	 */
	public $usersComponent = NULL;
	
	/**
	 * Заблокирована ли расса.
	 * @var 
	 */
	public $blocked = false;

	
	/**
	 * Время, на которое блокируется форма (в секундах).
	 * @var integer
	 */
	public $blockTime = 1200;

	/**
	 * Минимальный интервал между попытками.
	 * @var integer
	 */
	public $minRequestInteval = 4;

	/**
	 * Максимальное количество идущих подряд нудачных попыток.
	 * @var integer
	 */
	public $maxAttempts = 10;


    // данные формы
    public $username;
    public $password;
    public $rememberMe = false;


	/**
	 * ID кэша, в котором хранится количество попыток входа для этой формы.
	 * @return string
	 */
	public static function _getCountsCacheID(){
		return get_called_class()."_".Yii::app()->request->csrfToken."_counts";
	}

	public static function _getBlockCacheID(){
		return get_called_class()."_".Yii::app()->request->csrfToken."_block";
	}

	public function init()
	{
		// Проверка, была ли запись заблокирована.
		if(Yii::app()->cache->get($this->_getBlockCacheID()))
			$this->blocked = true;
	}

	/**
	 * Если указано свойство $this->userIdentity, производится вход.
	 */
	public function login()
	{
		if($this->blocked) return false;

		if($this->validate())
			if($this->usersComponent && $this->userIdentity && $this->username && $this->password){
				$this->userIdentity->usersComponent = $this->usersComponent;
				if(!$this->usersComponent->login($this->userIdentity))
					$this->addError('password', $this->userIdentity->errorMessage);
			}

		// учет неудачной попытки
		if($this->hasErrors() && Yii::app()->hasComponent('cache')){
			$cacheId = self::_getCountsCacheID();
			$count = intval(Yii::app()->cache->get($cacheId))+1;
			if($count >= $this->maxAttempts){
				 Yii::app()->cache->set(self::_getBlockCacheID(), true, $this->blockTime);
				 Yii::app()->cache->offsetUnset($cacheId);
			}
			else Yii::app()->cache->set($cacheId, $count, ($count*$this->minRequestInteval));
		}
	}
	
    public function rules()
    {
        return array
        (
            array('username', 'required', 'message' => Yii::t('users', 'Enter username')),
			array('password', 'required', 'message' => Yii::t('users', 'Enter password')),
            array('rememberMe', 'unsafe'),
        );
    }


    public function relations(){
        return array();
    }
	
	public function attributeLabels() {
		return array(
			'username' => Yii::t('attributes', 'Username'),
			'password' => Yii::t('attributes', 'Password'),
		);
	}

}

?>
