<?php

/*
 * Административный сервис - управление ролями
 */
class UsersRolesAdminService extends ExtCoreAdminService
{
    public $modelClass = 'AuthRole';

    public $assets = array(
		'css' => array('admin.css'),
		'js' => array('admin/roles.js'),
    );

    public $views = array(
        'create' => 'roles/create',
        'update' => 'roles/update',
        'index' => 'roles/index',
		'_form' => 'roles/_form',
    );

    public $dictionary = array(
        'actions' => array(
            'index' => 'Roles management',
            'create' => 'Add new role',
        )
    );
	
	public $_operationsFilter = array();


    public function getRbacOperation(){
        return 'Users roles management';
    }


    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.users.views');
        Yii::import('application.modules.users.models.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);
		
		// отправка клиенту адреса actionSetAttributes
		$setAttributesUrl = $this->createUrl('setattributes', false);
		$setAttributesScript = "$(document).data('setAttributesUrl', '$setAttributesUrl')";
		Yii::app()->clientScript->registerScript(
			'setAttributesUrl', $setAttributesScript, CClientScript::POS_HEAD);
		
        return parent::beforeAction($action);
	}

	public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
	{
        // приёмка внешнего фильтра - по категориям
        if(isset($_POST['operationsFilter'])){
            if(is_array($_POST['operationsFilter'])
                && sizeof($_POST['operationsFilter']))
                $this->_operationsFilter = $_POST['operationsFilter'];
        }

		// применение фильтра по категориям
		if(sizeof($this->_operationsFilter))
		{
			$this->model->dbCriteria->mergeWith(array(
				'with' => 'operations',
				'together' => true,
			));

			//$this->model->dbCriteria->addInCondition('operation_id', $this->_operationsFilter);
			foreach($this->_operationsFilter as $operationId)
				$this->model->dbCriteria->addCondition('operation_id='.intval($operationId));
			$this->model->dbCriteria->addCondition('super_user>0', 'OR');
		}

		parent::actionIndex($orderBy, $scope, $search, $page);
	}

    /**
     * Отдача страницы для работы виджета MTMSelect (при перелистывании)
     * @param integer $pageNumber
     * @param mixed $pageSize 
     */
	public function actionOperationsMTMPage($pageNumber, $pageSize, $search = NULL){
		Yii::import(Yii::localExtension('MTMSelect', '*'));
		MTMSelect::getPage($this->model, 'operations', $pageNumber, $pageSize, 'name', $search);
	}

}