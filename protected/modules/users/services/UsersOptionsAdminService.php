<?php

/*
 * Административный сервис - управление ролями
 */
class UsersOptionsAdminService extends ExtCoreService
{
    public $modelClass = NULL;
	public $formConfig = array();
	public $views = array();
	
	public $assets = array(
		'js' => array('admin/options.js')
	);


	public function getRbacOperation() {
		return 'Email notifications setup';
	}

	protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.users.views');
        Yii::import('application.modules.users.models.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);
		
        return parent::beforeAction($action);
	}

	/**
	 * Форма редактирования настроек.
	 */
	public function actionForm()
	{
		$form = new CForm($this->formConfig, $this->model);

        if(isset($_POST[$this->modelClass])){
            $form->loadData();
            if($form->model->save())
			{
				Yii::app()->user->setFlash('success',
					Yii::t('users', 'Options has been saved.'));
			}
			else Yii::app()->user->setFlash('error',
				 Yii::t('users', 'Have some errors.'));
		}

		$this->render($this->views['form'], compact('form'));
	}

}