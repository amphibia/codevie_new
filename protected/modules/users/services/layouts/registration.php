<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?= Yii::t('users', 'Error {error}', array('{error}'=>$code)); ?></title>
	<link rel="stylesheet" type="text/css" media="all" href="/css/error.css" />
</head>


<body lang="<?= Yii::app()->language ?>" error_code="<?= $code ?>">

	<div class="container root-container">

		<?= $content; ?>

	</div>

</body>

</html>