<?php

/**
 * Компонент, которым подменяется встроенный компонент 'user'.
 * Изменяется процедура проверки доступа (checkAccess) и принцип хранения данных
 * пользователя (в UserComponent::$model).
 * 
 * Компонент не требует подключения  в конфиге или панеои управления.
 * Подклчается автоматически компонентом UsersComponent.
 * 
 * @title User component
 * @description { Auto component of UsersComponent. <strong>Do not turn on!</strong> }
 * @preload no
 * @vendor koshevy
 * @requirements{component:users}
 * @package users
 */
class UserComponent extends CWebUser
{
	public $allowAutoLogin = true;
	public $autoRenewCookie = true;

    /**
     * Компонент, через который происходит работа с пользовательской системой.
     * @var UsersComponent
     */
    public $usersComponent = NULL;

    /**
     * Модель с данными этого пользователя. 
     * @var CModel
     */
    protected $_model = NULL;


	public function init(){
		Yii::loadExtConfig($this, __FILE__);
		return parent::init();
	}

	/**
	 * Инициализация/проверка установленной авторизации, после того как
	 * будут инициализированны UserComponent и UsersComponent.
	 */
	public function initAutorization()
	{
		/**
		 * @todo: прежде чем делать восстановление по кукисам, нужно убедиться,
		 * что подцепится модель! И сохраняется пароль.
		 */
		// пытается восстановить предудущую сессию автоматически
		//$this->restoreFromCookie();

		// проверка, не сменился ли пароль
		if(!$this->isGuest)
		{
			if(!$this->model || ($this->getUsedPassword() != $this->model->password))
				$this->usersComponent->logout();
		}
	}

    /**
     * Обращение к модели пользователя, ID которой должна храниться в сессии.
     * @return mixed
     */
    public function getModel()
	{
        if ($this->_model)
            return $this->_model;
        else {
            if ($user_id = ($this->getState('user_id')
				? $this->getState('user_id') : $this->id) )
			{
                $userModel = $this->usersComponent->modelClass;
                return $userModel::model()->findByPk($user_id);
            }
        }

        return NULL;
    }

    /**
     * Сохранение модели (в сессии сохраняется ID пользователя, и в дальнейшем,
     * по ней можно будет обратиться к этой модели).
     * @param CModel $value
     */
    public function setModel(CModel $value) {
        $this->_model = $value;
        $this->setState('user_id', $value->id);
    }

	/**
	 * Обращение к паролю, который использовался при входе.
	 * Каждый раз при инициализации компонента, происходит проверка, не сменился
	 * ли пароль. Если да - производится выход.
	 */
	public function getUsedPassword(){
		if($this->hasState('usedPassword'))
			 return $this->getState('usedPassword');
		else return NULL;
	}

	/**
	 * Вход в систему по идентификационной карточке.
	 * В случае успеха, сохраняет используемый при входе пароль в сессию.
	 * @param CUserIdentity $identity
	 * @param int $duration
	 * @return string
	 */
	public function login($identity, $duration = 0) {
		$result = parent::login($identity, $duration);
		if($result) $this->setState('usedPassword', $identity->password);
		return $result;
	}

    /**
     * Проверка доступа к действию.
	 * Использует модельную инфрасттруктура модуля 'users' (хранит данные в БД),
	 * для поиска и сравнения привиллегий пользователя.
     * 
     * @param type $operation
     * @param type $params
     * @param type $allowCaching
     * @return boolean
     * @throws CHttpException
     */
    public function checkAccess($operation, $params = array(), $allowCaching = true)
	{
        // проверка доступа для гостей
        if($this->isGuest || !$this->model
		|| (($this->model->status == 'registered') && $this->usersComponent->userIdentityClass))
		{
            // гостевая роль ещё не инициализирована
            $roleClass = $this->usersComponent->roleModelClass;
            $guestRole = $roleClass::model()->find('guest_role AND enabled');
            if (!$guestRole)
                return true;

            // проверка настроек доступа для гостей
            $operationsClass = $this->usersComponent->operationModelClass;
            $operations = $operationsClass::model();
			$operations->dbCriteria->mergeWith(array(
				'condition' => '(role_id=:role_id) AND (name=:operation)',
				'with' => 'roles',
				'together' => true,
				'params' => array(':role_id' => $guestRole->id, ':operation' => $operation),
			));

			return $operations->exists();
        }

        // проверка доступа для пользователя
        else
		{
			if(!$this->model)
				throw new CHttpException(500,
					Yii::t('users', 'Can`t find user model!'));

			return $this->model->operationCheckAccess($operation);
        }
    }

}

?>
