$(document).ready(function()
{
	/**
	 * Переключение видимости блока с учетными данными
	 * email-аккаунта (для SMTP).
	 */
	function toggleAccountData(scoore){
		if($("#AuthOptionsForm_type").val()=='mail')
			 $('.accountData').slideUp(scoore);
		else $('.accountData').slideDown(scoore);
	}

	toggleAccountData(0);
	$('#AuthOptionsForm_type').change(function(){
		toggleAccountData(100); });
	$('#AuthOptionsForm_type').keypress(function(){
		toggleAccountData(100); });
});