$(document).ready(function()
{
	// при перелистывании страниц в таблице изменяется URL
	if($('#ActiveTable').length)
		$.client.config.workPlace = 'ActiveTable';

	// переключение super_user (в форме редактирования роли)
	var superUserOnToggle = function(){
		var $superUserElement = $('#AuthRole-form #AuthRole_super_user');
		if(!$superUserElement.length) return;

		if($superUserElement.attr('checked')){
			$('.control-group[attr="operations"]').hide();
			$('.super_user_info').show();
		}
		else{
			$('.control-group[attr="operations"]').show();
			$('.super_user_info').hide();
		}
	}

	$('#AuthRole-form #AuthRole_super_user').live('change', superUserOnToggle);
	superUserOnToggle.apply(this, []);

	// выключатель "enabled" в таблице
	$('.ActiveTable .enabledToggle').live('click', function(){
		var checked = $(this).attr('checked') ? 1 : 0,
			id = $(this).attr('data-id'),
			url = $(document).data('setAttributesUrl');
		$(this).request(url, {
			id:id, values:{enabled:checked}
		});
	});

	// выключатель "super_user" в таблице
	$('.ActiveTable .superUserToggle').live('click', function(){
		var checked = $(this).attr('checked') ? 1 : 0,
			id = $(this).attr('data-id'),
			url = $(document).data('setAttributesUrl');
		$(this).request(url, {
			id:id, values:{super_user:checked}
		});
	});

	// выключатель "guest_role" в таблице
	$('.ActiveTable .guestRoleToggle').live('click', function(){
		var checked = $(this).attr('checked') ? 1 : 0,
			id = $(this).attr('data-id'),
			url = $(document).data('setAttributesUrl'),
			$checkbox = $(this);
		$(this).request(url, {
			id:id, values:{guest_role:checked}
			}, null,
			function(request_result, requestOptions){
				if(!request_result.result) $checkbox.removeAttr('checked');
			}
		);
	});

	// выключатель "default_role" в таблице
	$('.ActiveTable .defaultRoleToggle').live('click', function(){
		var checked = $(this).attr('checked') ? 1 : 0,
			id = $(this).attr('data-id'),
			url = $(document).data('setAttributesUrl'),
			$checkbox = $(this);
		$(this).request(url, {
			id:id, values:{default_role:checked}
			}, null,
			function(request_result, requestOptions){
				if(!request_result.result) $checkbox.removeAttr('checked');
			}
		);
	});

	// удаление роли JSON-запросом
	$('.ActiveTable .deleteButton a').live('click', function(){
		var $ActveTable = $(this).closest('.ActiveTable'),
			$table = $ActveTable.find('.table');
		$ActveTable.request($(this).attr('href'), {
			json_operation_id: $.client.nextOperationId(function(){
				$ActveTable.request($table.attr('uri'));
			})
		});
		return false;
	});

	/**
	 * Обработчик события - выборки операций.
	 * @var callback
	 */
	var onMTMSelectChange = function(){    
		var $ActveTable = $('#ActiveTable'),
			$table = $ActveTable.find('.table');
		// перезагрузка таблицы
		$ActveTable.request($table.attr('uri'));
	};

	// каждый раз при отправке запроса от имени таблицы, к нему добавляются
	// данные о фильтре категорий
	$('.ActiveTable').unbind('onBeforeRequest');
	$('.ActiveTable').bind('onBeforeRequest', function(evt, data){                
		var operations = [];
		// информация о выбранных категориях
		$('.selectedOperations .mtmselect').find('.mtmitem[id]').each(function(){
			operations.push($(this).attr('id')); });
		data.post.operationsFilter = operations;
	});

	// привязка обработчика - выборка по категориям
	$('.mtmselect').unbind('OnMTMSelectAdd');
	$('.mtmselect').unbind('OnMTMSelectRemove');
	$('.mtmselect').bind('OnMTMSelectAdd', onMTMSelectChange);
	$('.mtmselect').bind('OnMTMSelectRemove', onMTMSelectChange);

	return $(this);
});