$(document).ready(function()
{
	// при перелистывании страниц в таблице изменяется URL
	if($('#ActiveTable').length)
		$.client.config.workPlace = 'ActiveTable';

	// переключатель "status" (выпадающий список) в таблице
	$('.ActiveTable .statusToggle').live('change', function(){
		var id = $(this).attr('data-id'),
			url = $(document).data('setAttributesUrl');
		$(this).request(url, {
			id:id, values:{status:$(this).val()}
		});
	});

	// удаление пользователся JSON-запросом
	$('.ActiveTable .deleteButton a').live('click', function(){
		var $ActveTable = $(this).closest('.ActiveTable'),
			$table = $ActveTable.find('.table');
		$ActveTable.request($(this).attr('href'), {
			json_operation_id: $.client.nextOperationId(function(){
				$ActveTable.request($table.attr('uri'));
			})
		});
		return false;
	});

	/**
	 * Обработчик события - выборки ролей.
	 * @var callback
	 */
	var onMTMSelectChange = function(){    
		var $ActveTable = $('#ActiveTable'),
			$table = $ActveTable.find('.table');
		// перезагрузка таблицы
		$ActveTable.request($table.attr('uri'));
	};

	// каждый раз при отправке запроса от имени таблицы, к нему добавляются
	// данные о фильтре категорий
	$('.ActiveTable').unbind('onBeforeRequest');
	$('.ActiveTable').bind('onBeforeRequest', function(evt, data){                
		var roles = [];
		// информация о выбранных категориях
		$('.selectedRoles .mtmselect').find('.mtmitem[id]').each(function(){
			roles.push($(this).attr('id')); });
		data.post.rolesFilter = roles;
	});

	// привязка обработчика - выборка по категориям
	$('.mtmselect').unbind('OnMTMSelectAdd');
	$('.mtmselect').unbind('OnMTMSelectRemove');
	$('.mtmselect').bind('OnMTMSelectAdd', onMTMSelectChange);
	$('.mtmselect').bind('OnMTMSelectRemove', onMTMSelectChange);

	return $(this);
});