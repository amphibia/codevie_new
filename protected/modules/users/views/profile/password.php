<?php
echo $this->renderPartial($this->views['nav']);

if($success === true){
    echo CHtml::tag('div', array('class' => 'alert alert-success'), Yii::t('users', 'Password successfully changed'). '<button type="button" class="close" data-dismiss="alert">&times;</button>');
}

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'password_change_form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
));

echo CHtml::hiddenField(Yii::app()->request->csrfTokenName, Yii::app()->request->csrfToken);

echo $form->passwordField($this->modelPassword, 'old_password', array(
    'placeholder' => $this->modelPassword->getAttributeLabel('old_password'),
    'class' => 'input-block-level'
));
echo $form->error($this->modelPassword, 'old_password');

echo $form->passwordField($this->modelPassword, 'password', array(
    'placeholder' => $this->modelPassword->getAttributeLabel('password'),
    'class' => 'input-block-level'
));
echo $form->error($this->modelPassword, 'password');

echo $form->passwordField($this->modelPassword, 'retype_password', array(
    'placeholder' => $this->modelPassword->getAttributeLabel('retype_password'),
    'class' => 'input-block-level'
));
echo $form->error($this->modelPassword, 'retype_password');


$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('users', 'Change password')));

$this->endWidget();
