<?php $this->renderPartial($this->views['nav']) ?>

<?php
if($success === true){
    echo CHtml::tag('div', array('class' => 'alert alert-success'), Yii::t('users', 'E-mail successfully changed'). '<button type="button" class="close" data-dismiss="alert">&times;</button>');
}
?>

    <p><?= Yii::t('users', 'Current e-mail') ?>: <strong><?= Yii::app()->user->model->email ?></strong></p>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email_change_form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
));

echo CHtml::hiddenField(Yii::app()->request->csrfTokenName, Yii::app()->request->csrfToken);

echo $form->textField($this->modelEmail, 'email', array(
    'placeholder' => $this->modelEmail->getAttributeLabel('email'),
    'class' => 'input-block-level'
));
echo $form->error($this->modelEmail, 'email');

$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('users', 'Change email')));

$this->endWidget();