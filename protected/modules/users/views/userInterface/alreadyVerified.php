<h2><?= Yii::t('users', 'Your account already verified'); ?></h2>

<p><?= Yii::t('users', 'You have already verified account, so you don\'t need to verify it again.'); ?></p>
<p>
	<?= Yii::t('users', 'You can go to <a href="{profileUrl}">profile</a>.',
			array('{profileUrl}'=>Yii::app()->controller->createUrl('/users/profile'))); ?>
</p>