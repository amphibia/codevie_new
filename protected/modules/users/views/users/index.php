<? if(!Yii::app()->request->isAjaxRequest){ ?>


    <div class="page-header">

		<h1><?= Yii::t('users', 'Users management'); ?></h1><br/>

		<?

		$this->widget(
			Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__)
		);

		// кнопка "в индекс админки"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
			'size'=>'small',
			'buttonType'=>'link',
			'icon'=>'icon-arrow-left',
			'label'=>Yii::t('admin', 'Administration index'),
			'url'=>'/admin/',
		));

		?>

		<br/><br/>
		<p>
			<span class="icon-user"></span>
			<span class="icon-wrench"></span>
			<?
			echo Yii::t('users',
				'Here you can view/create/edit users of your site. <br/> '.
				'You can change roles of users and privillegies for roles.'
			);
			?>
		</p>
		<p>
			
			<?
			echo Yii::t('users',
				'You can {a}change roles</a>, if you have access to it.',
				array('{a}'=>'<a href="'.(Yii::app()->getService('roles', 'admin')->createUrl('index')).'">')
			);
			?>
		</p>

    </div>

	<div class="selectedRoles"><? require '_rolesSelect.php'; ?></div>
	<br/>
    <?
}

$this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config);

?>
