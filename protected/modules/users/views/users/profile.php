<div class="page-header">
<h1><?= Yii::t('users',
		"User '{username}' profile update",
		array('{username}' => $this->model->user->username)
	); ?>
</h1>
<br/>
<?

// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('users', 'Back to user'),
    'url'=>$this->createUrl('update', array($this->model->user->id)),
));

?>
</div>

<?php

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__));


// вывод автоформы
$this->widget(Yii::localExtension('bootforms', 'BootAutoForm', __FILE__),
    array(
        'form' => $form,
        'autoElements' => false,
    )
);


?>
