<? if(!Yii::app()->request->isAjaxRequest){ ?>
    <div class="page-header">
    
    <h1><?= Yii::t('users', 'User roles management'); ?></h1><br/>

    <?
    $this->widget(
        Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__)
    );

    // кнопка "в индекс админки"
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
        'size'=>'small',
        'buttonType'=>'link',
        'icon'=>'icon-arrow-left',
        'label'=>Yii::t('admin', 'Administration index'),
        'url'=>'http://cms/admin/',
    ));
	?>
	
	<br/><br/>
	<p>
		<span class="icon-user"></span>
		<span class="icon-wrench"></span>
		<?
		echo Yii::t('users',
			'Here you can create/edit roles of users on your site. <br/>'.
			'Roles incapsulate operations, availabled for user with this role.'
		);
		?>
	</p>

	<p>
	<?
		echo Yii::t('users',
			'{a}Users</a> need binding to role, for access to site operations.',
			array('{a}'=>'<a href="'.(Yii::app()->getService('users', 'admin')->createUrl('index')).'">')
		);
	?>
	</p>

    </div>

	<div class="selectedOperations"><? require '_operationsSelect.php'; ?></div>
	<br/>

    <?
}
?>

<? $this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config); ?>
