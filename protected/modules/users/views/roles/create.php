<div class="page-header">
<h1><?= Yii::t('users', 'User role create'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('users', 'User roles management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>

<?php

$this->renderPartial($this->views['_form'], compact('form'));

?>
