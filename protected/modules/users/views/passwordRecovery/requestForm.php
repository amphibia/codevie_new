<?
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'recovery_form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
));
?>

	<?
	if(!$this->model->blocked)
	{
	?>
		<h2 id="prompt"><?= Yii::t('users', 'Password recovery'); ?></h2>
		<input type=hidden name="<?= Yii::app()->request->csrfTokenName ?>" value="<?= Yii::app()->request->csrfToken ?>" />

		<p class="instruction">
			<?= Yii::t('users', 'You must enter your username and email.'); ?>
			<?= Yii::t('users', 'You will receive your referral link to password reset at your email.'); ?>
		</p>

		<?
		echo $form->textField($this->model, 'username', array(
			'placeholder' => $this->model->getAttributeLabel('username'),
			'class' => 'input-block-level'
		));
		echo $form->error($this->model, 'username');

		echo $form->textField($this->model, 'email', array(
			'placeholder' => $this->model->getAttributeLabel('email'),
			'class' => 'input-block-level'
		));
		echo $form->error($this->model, 'email');
		?>

		<button id="submit" class="btn btn-large btn-info" type="submit"><?= Yii::t('users', 'Send recovery request') ?></button>
	<?
	}
	else
	{
	?>
		<div class="errorMessage">
			<?= Yii::t('users', 'Recently you have already sent a request to password recovery. Wait a while.'); ?>
		</div>
	<?
	}
	?>

<?
$this->endWidget();