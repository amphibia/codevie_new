<html>
	<head>
	</head>

	<body>
		<h1><?= Yii::t('users', 'Password recovery');  ?></h1>

		<p><?= Yii::t('users', 'We got request to change you password.'); ?></p>
		<p>
			<?= Yii::t('users',
					'If you realy want to change password, go to: <a href="{link}">{link}</a>.',
					array('{link}' => $actionUrl)
				);
			?>
		</p>

		<p><?= Yii::t('users', 'IP of initiator: {ip}.', array('{ip}' => $_SERVER['REMOTE_ADDR'])); ?></p>
		
	</body>

</html>