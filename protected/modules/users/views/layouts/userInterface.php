<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?= Yii::t('users', 'User account management'); ?></title>
</head>
<body id="userInterface">

	<div id="supremeLogo"></div>
	<div class="container root-container"><?php echo $content; ?></div>

</body>
</html>
