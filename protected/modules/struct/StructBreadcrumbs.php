<?php

/**
 * @author slaik
 *
 * Вывод хлебных крошек текущей категории.
 *
 */
class StructBreadcrumbs extends CWidget
{

    /**
     * Идентификатор
     * @var string
     */
    public $id = null;

    /**
     * Отображение корня
     * @var boolean
     */
    public $showRoot = true;

    /**
     * Отображение текущего раздела
     * @var boolean
     */
    public $showCurrent = true;

    /**
     * Разделитель
     * @var string
     */
    public $divider = '/';
    public $htmlAttributes = array('class' => 'breadcrumb');
    public $view = 'breadcrumbs';

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);
    }

    public function run()
    {
        $level = Yii::app()->struct->getCategoryLevel();
        $ancestors = array();
        while ($level) {
            if (is_object($level[0])) {
                array_unshift($ancestors, $level[0]);
                $level = &$level['parent'];
            } else {
                break;
            }
        }

        // Добавляем корневой раздел
        $root = null;
        if ($this->showRoot && isset($ancestors[0]) && is_object($ancestors[0]->parent)) {
            $root = $ancestors[0]->parent;
            while (is_object($root)) {
                if (is_object($root->parent) && $root->parent->title) {
                    $root = $root->parent;
                } else {
                    break;
                }
            }
            if (is_object($root)) {
                array_unshift($ancestors, $root);
            }
        }

        if ($this->id) {
            $this->htmlAttributes['id'] = $this->id;
        }

        $this->render($this->view, array('ancestors' => $ancestors));
    }

}
