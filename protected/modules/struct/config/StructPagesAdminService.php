<?php

/**
 * Настройки связанных моделей.
 * Используются при формировании формы.
 */
$pageConfig = require(__DIR__.'/StructPage.php');
$categoryOptionsConfig = require(__DIR__.'/StructCategoryOptions.php');


/**
 * Конфигурация административного сервиса управления материалами
 * (StructPagesAdminService).
 */
return array
(
    'assets' => array(
        'css' => array('admin.css'),
        'js' => array(
			'sortble.multupages'=>'admin/ui.sortble.multupages.js',
			'admin/jquery.jscrollpane.min.js',
			'admin/jquery.autosize.js',
			'admin/pages.js'
		),
    ),

	'views' => array(
        'create' => 'pages/create',
        'update' => 'pages/update',
        'index' => 'pages/index',
        '_form' => 'pages/_form',
        '_categoriesSelect' => 'pages/_categoriesSelect',
		'_tagsSelect' => 'pages/_tagsSelect',
		'_groupOperationsBar' => 'pages/_groupOperationsBar',
    ),

    /**
     * Поля, отображаемые в таблице индекса (actionIndex).
     * @var array
     */
    'indexTableOptions' => array
    (
        /**
         * Поле сортировки.
         * @var string 
         */
        'sortField' => 'position',
        
        'htmlAttributes' => array
        (
            "container"         => array(),

            "table"             => array("cellspacing"=>0, "cellpadding"=>0),
            "row"               => array(),
            "headRow"           => array(),
            "cell"              => array("valign"=>"top"),

            // дополнительные поля
            "additionalField"   => array(),

            // настройка формы фильтров по таблице
            "form"          => array("method" => "POST"),
            "searchBlock"   => array(),
            "searchField"   => array(),
            "searchSubmit"  => array(),
            "scopeButton"   => array(),
            "scopeValue"    => array(),

            "info"          => array(),
        ),

        'fieldsOrder' => array('position', 'title', 'alias', 'published', 'update'),
        'route' => 'index',

        'buttons' => array(
            'update' => array(

                'htmlOptions'=>array('class'=>'pageManagement pull-right'),

                'buttons' => array(
                    array(
                        'label' => ' ',
                        'icon'=>'icon-edit',
                        'items' => array(
                            'update' => array('label'=>Yii::t('app', 'Open in edit form'), 'icon'=>'icon-pencil',),
                            'delete' => array('label'=>Yii::t('app', 'Delete item'), 'icon'=>'icon-trash', 'itemOptions'=>array('class'=>'deletePage')),
                        ),
                    )
                )
            )
        ),

		'scopes' => array
		(
			'published'=>function($model, $scopeValue){ $model->published(); },
			'draft'=>function($model, $scopeValue){ $model->draft(); },
			'hidden'=>function($model, $scopeValue){ $model->hidden(); },
			'uncategorized'=>function($model, $scopeValue){ $model->uncategorized(); },
		),
                    
        /**
         * Подписи к фильтрам.
         * @var array
         */
        'scopeLabels' => array(
            'published' => Yii::t('struct', 'Published pages'),
            'draft' => Yii::t('struct', 'Drafts'),
            'hidden' => Yii::t('struct', 'Hidden pages'),
			'uncategorized' => Yii::t('struct', 'Uncategorized pages'),
        ),

                    
        /**
         * Предобработка полей.
         */
        'preConvertors' => array
        (
            /**
             * Вывод элемента управления местоположением страницы.
             */
            'position' => function($fieldName, $fieldValue, $row, $isHeader = FALSE)
            {
                if($isHeader) return $fieldValue;

                return
                CHtml::openTag('div', array('class'=>'moveDropdown dropdown', 'oncontextmenu' => 'return false;'))
                    . CHtml::openTag('div',
                        array(
                            'class'=>'icon-resize-vertical positionHolder',
                            'position' => $fieldValue,
                            'page_id' => $row['id'],
                            'title' => Yii::t('struct', 'Drag this page.'),
                            'data-title' => Yii::t('struct', 'Sort by position'),
                            'data-content' => Yii::t('struct', 'You can change position of page, when table sorted by "position" field.'),
                        )
                    )
                    . CHtml::closeTag('div')
                    . CHtml::openTag('ul', array('class'=>'dropdown-menu'))
                        . CHtml::tag('li', array('class'=>'move_to_first'), '<a href="javascript:;"><span class="icon-fast-backward"></span> &nbsp; ' . Yii::t('struct', 'Move to first') . '</a>')
                        . CHtml::tag('li', array('class'=>'move_to_last'), '<a href="javascript:;"><span class="icon-fast-forward"></span> &nbsp; ' . Yii::t('struct', 'Move to last') . '</a>')
                    . CHtml::closeTag('ul')
                . CHtml::closeTag('div');
            },
            
            /**
             * Переключение статуса опубликованности.
             */
            'published' => function($fieldName, $fieldValue, $row, $isHeader = FALSE, $model)
            {
                if($isHeader) return $fieldValue;
				Yii::app()->controller->widget('ext.bootstrap.widgets.TbEditableField', array(
					'type' => 'select',
					'model' => $row,
					'attribute' => 'published',
					'url' => Yii::app()->controller->createUrl('simpleattr', false),
					'source' => $model->publishedRange,
					'title' => Yii::t('struct', 'Select publish status'),
					//'enabled' => true
				));
				
				return NULL;
            },
					
			'title' => function($fieldName, $fieldValue, $row, $isHeader = FALSE, $model)
			{
				if($isHeader) return $fieldValue;
				return CHtml::link($fieldValue,
					Yii::app()->controller->createUrl('update', array('id'=>$row->id))
				);
			}

        ),

        'pageSize' => 15,
        'sortVar' => 'orderBy',
    ),




	/**
	 * ---
	 * Конфигурация технических полей.
	 * Составная часть конфигурации формы управления разделом.
	 * ---
	 */
	'techSection' => array
	(
	),

	/**
	 * ---
	 * Конфигурация контентных полей.
	 * Составная часть конфигурации формы управления разделом.
	 * ---
	 */
	'contentSection' => array
	(
		'<div id="contentSection">'.

			'<div class="control-group">'.
				'<label class="control-label"> &nbsp;</label>' .
				'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Content') . '</h2></div>' .
			'</div>',

			/***
			 * ---
			 * Эти значения конфигурируются моделью в зависимости от значений
			 * в модели SructCategoryOptions, и изменяются в соответствующем конфиге
			 * (значение StructCategoryOptions::$contentFormElements).
			 * 
			 * В таком виде, как здесь, свойство используется при отсутствии
			 * настроек, однако, во избежание путаницы, значения вынесены
			 * в конфиг с описанием настроек
			 * ---
			 */
			'short_text' => $categoryOptionsConfig['contentFormElements']['html'],
			'full_text' => $categoryOptionsConfig['contentFormElements']['html'],

		'</div>'
	),

	/**
	 * ---
	 * Заголовок, который идет перед расширенными полями.
	 * ---
	 */
	'customFieldsHeader' =>
		'<div class="control-group">'.
			'<label class="control-label"> &nbsp;</label>' .
			'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Additional data') . '</h2></div>' .
		'</div>',
	
	/**
	 * ---
	 * Конфигурация галереи.
	 * ---
	 */
	'gallerySection' => array
	(
		'<div class="control-group">'.
			'<label class="control-label"> &nbsp;</label>' .
			'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Visual material') . '</h2></div>' .
		'</div>',
		
		'gallery' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
	),
	
    /**
	 * ---
	 * Заготовка формы создания (в данном случае, применяется и для редактирования).
	 * ---
	 */

    'createForm' => array
    (
		'attributes' => array(
			'enctype' => 'multipart/form-data',
		),

        'elements' => array
        (
			'<div class="control-group">'.
				'<label class="control-label"> &nbsp;</label>' .
				'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Placement') . '</h2></div>' .
			'</div>',

			'title' => array('type'=>'text', 'class'=>'span6', 'placeholder'=>Yii::t('struct', 'Enter page title')),

			'<div class="hint-top">'.
				Yii::t('struct', 'Use in URL of category and her subitems.').
			'</div>',
			'alias' => array('type'=>'text', 'class'=>'span3', 'placeholder'=>Yii::t('struct', 'Transliterated title')),
			'categories' => array('type'=>Yii::localExtension('mtmselect', 'MTMSelect'), 'route'=>'CategoriesMTMPage'),
			'tags' => array('type'=>Yii::localExtension('mtmselect', 'MTMSelect'), 'route'=>'TagsMTMPage'),

			'<div class="hint-top">'.
				Yii::t('struct', 
					'<strong>draft</strong> - category opening by URL, but don`t displaing in menus;<br/>' .
					'<strong>published</strong> - category visible in menu and allowed by URL;<br/>' .
					'<strong>hidden</strong> - category unvisible and denied.').
			'</div>',
			'published'=>array('type'=>'default', 'items' => $pageConfig['publishedRange']),

            'in_search' => array(
                'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                'attributes' => array(
                    'enabledLabel' => Yii::t('app', 'Yes'),
                    'disabledLabel' => Yii::t('app', 'No'),
                    'enabledStyle' => 'success',
                )
            ),
        ),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),

);