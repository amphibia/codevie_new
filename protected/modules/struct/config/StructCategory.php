<?php

return array
(
	/**
	 * @TODO Перенести запрет на управление разделами в сферу компетенции
	 * StructPageOptions.
	 * 
	 * Уровень, на который/с которого могут перемещать/добавлять категории
	 * только некоторые пользователи (основа сайта).
	 * @var integer
	 */
	'lockedLevel' => 3,


    /**
     * Диапазон значений для типа категории.
     * @var array
     */
    'typeRange' => array(
        'pages' => Yii::t('struct', 'Default structure with static pages'),
		'group' => Yii::t('struct', 'Group of categories'),
        'controller' => Yii::t('struct', 'Programmed controller'),
        'module' => Yii::t('struct', 'Installed full-functional module'),
        'external_reference' => Yii::t('struct', 'External reference'),
    ),

    /**
     * Шаблоны раздела
     * Вид отображения содержимого раздела и его материалов
     * @var array
     */
    'templateRange' => array(
		'default' => Yii::t('stuct', 'Default template'),
	),

    /**
     * Диапазон значений для статуса категории.
     * @var array
     */
    'publishedRange' => array
    (
        'draft' => Yii::t('struct', 'Category as draft'),
        'published' => Yii::t('struct', 'Published category'),
        'hidden' => Yii::t('struct', 'Don`t show'),
    ),
    
    /**
     * Диапазон значений для способа интеграции с лэйоутом.
     * 
     * Способ интеграции с лэйаутом определяет как будет производиться вывод
     * модуля/контроллера, прикремлённого к категории:
     *  - полная передача управления (свой лэйаут);
     *  - частичная передача (вывод в лэйоуте модуля 'struct');
     *  - указать лэйаут (можно указать собственный лэйоут из папки protected/views/layouts).
     * 
     * @var array
     */
	'layoutIntegrationRange' => array
	(
		'content' => Yii::t('struct', 'Content only'),
		'layout' => Yii::t('struct', 'Own layout'),
		'custom' => Yii::t('struct', 'Custom layout'),
    ),

	/**
	 * @var boolean Авто-формирование алиаса, если он не заполнен.
	 */
	'autoAlias' => true,

);