<?php

/**
 * Настройки связанных моделей.
 * Используются при формировании формы.
 */
$categoryConfig = require(__DIR__.'/StructCategory.php');
$categoryOptionsConfig = require(__DIR__.'/StructCategoryOptions.php');


/**
 * Конфиг для сервиса StructCategoriesAdminService
 */
return array
(
    'assets' => array(
		'core' => array('jquery', 'cookie'),
        'css' => array('admin.css', 'jquery.jscrollpane.css'),
        'js' => array(
			'admin/jquery.mousewheel.js',
			'admin/mwheelIntent.js',
			'admin/jquery.jscrollpane.min.js',
			'admin/jquery.autosize.js',
			'admin/categories.js'
		),
    ),

	'lockedLevel' => '3',

    'modelClass' => 'StructCategory',

    'dictionary' => array(
        'actions' => array(
            'index' => 'Categories management',
            'create' => 'Create category',
        )
    ),

	/**
	 * ---
	 * Конфигурация технических полей.
	 * Составная часть конфигурации формы управления разделом.
	 * ---
	 */
	'techSection' => array
	(
		'<div id="logicSection">'.

            '<div class="control-group">'.
                '<label class="control-label"> &nbsp;</label>' .
                '<div class="controls"><h2 class="sectionHeader logic"><i class="icon-wrench"></i>' . Yii::t('struct', 'Advanced logic') . '</h2>' .
				'<p class="description">'.Yii::t('struct', 'Extended options for advanced users.<br/>Contact support for more information.').'</p></div>' .
            '</div>'.


			'<div class="sectionContent">'.

				'<div class="hint-top">'.
				Yii::t('struct', 'You can use category to <strong>contains pages</strong>, attach category to <strong>functional modules</strong> or <strong>task-oriented programmed controllers</strong>.'),
				'</div>',

				'type' => array('type'=>'dropdownlist', 'items' => $categoryConfig['typeRange']),
				'handler' => array('type'=>'StructHandlerSelect'),
				'external_reference' => array(
					'class'=>'input-block-level',
					'type'=>'text',
					'placeholder'=>Yii::t('struct', 'Enter external URL for redirecting...'),
				),

				'template'=>array('type' => 'dropdownlist', 'items' => (StructCategory::singleTone()->templateRange)),
                'parse_phpdoc' => array(
                    'name' => 'sadas',
                    'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                    'attributes' => array(
                        'enabledLabel' => Yii::t('app', 'Yes'),
                        'disabledLabel' => Yii::t('app', 'No'),
                        'enabledStyle' => 'success',
                    )
                ),
				'layout_integration'=>array('type'=>'dropdownlist', 'items' => $categoryConfig['layoutIntegrationRange']),
				'layout_name'=>array('type'=>'dropdownlist',
					'items'=>Yii::app()->struct->getApllicationLayouts()
				),
                'tech_id' => array('type'=>'text'),
                'across' => array(
                    'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                    'attributes' => array(
                        'enabledLabel' => Yii::t('app', 'Yes'),
                        'disabledLabel' => Yii::t('app', 'No'),
                        'enabledStyle' => 'success',
                    )
                ),

                'in_search' => array(
                    'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                    'attributes' => array(
                        'enabledLabel' => Yii::t('app', 'Yes'),
                        'disabledLabel' => Yii::t('app', 'No'),
                        'enabledStyle' => 'success',
                    )
                ),

			'</div>',

		'</div>',

		'<br/><br/><br/>',
	),

	/**
	 * ---
	 * Конфигурация контентных полей.
	 * Составная часть конфигурации формы управления разделом.
	 * ---
	 */
	'contentSection' => array
	(
		'<div id="contentSection">'.

			'<div class="control-group">'.
				'<label class="control-label"> &nbsp;</label>' .
				'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Content') . '</h2></div>' .
			'</div>',

			/***
			 * ---
			 * Эти значения конфигурируются моделью в зависимости от значений
			 * в модели SructCategoryOptions, и изменяются в соответствующем конфиге
			 * (значение StructCategoryOptions::$contentFormElements).
			 *
			 * В таком виде, как здесь, свойство используется при отсутствии
			 * настроек, однако, во избежание путаницы, значения вынесены
			 * в конфиг с описанием настроек
			 * ---
			 */
			'short_text' => $categoryOptionsConfig['contentFormElements']['html'],
			'full_text' => $categoryOptionsConfig['contentFormElements']['html'],

		'</div>'

	),

	/**
	 * ---
	 * Заголовок, который идет перед расширенными полями.
	 * ---
	 */
	'customFieldsHeader' =>
		'<div class="control-group">'.
			'<label class="control-label"> &nbsp;</label>' .
			'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Additional data') . '</h2></div>' .
		'</div>',

	/**
	 * ---
	 * Конфигурация галереи.
	 * ---
	 */
	'gallerySection' => array
	(
		'<div class="control-group">'.
			'<label class="control-label"> &nbsp;</label>' .
			'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Visual material') . '</h2></div>' .
		'</div>',

		'gallery' => array('type' => Yii::localExtension('multiuploader', 'UploadFormElement')),
	),

    /**
	 * ---
	 * Заготовка формы создания (в данном случае, применяется и для редактирования).
	 * ---
	 */
    'createForm' => array
    (
		'attributes' => array(
			'enctype' => 'multipart/form-data',
		),

        'elements' => array
        (
			'head' => '',

			'<div class="control-group">'.
				'<label class="control-label"> &nbsp;</label>' .
				'<div class="controls"><h2 class="sectionHeader">' . Yii::t('struct', 'Placement') . '</h2></div>' .
			'</div>',

            'title' => array('type'=>'text', 'class'=>'span6', 'placeholder'=>Yii::t('struct', 'Enter category title')),

			'<div class="hint-top">'.
				Yii::t('struct', 'Use in URL of category and her subitems.').
			'</div>',
            'alias' => array('type'=>'text', 'class'=>'span3', 'placeholder'=>Yii::t('struct', 'Transliterated title')),

			'<div class="hint-top">'.
				Yii::t('struct', 'Category will be child of select category, or root if not selected.').
			'</div>',

            'parent' => array('type'=>'default'),

			'<div class="hint-top">'.
				Yii::t('struct',
					'<strong>draft</strong> - category opening by URL, but don`t displaing in menus;<br/>' .
					'<strong>published</strong> - category visible in menu and allowed by URL;<br/>' .
					'<strong>hidden</strong> - category unvisible and denied.').
			'</div>',
            'published'=>array('type' => 'default', 'items'=>$categoryConfig['publishedRange']),
        ),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),

    // свойства формы конфигурирования
    'optionsForm' => array
    (
		'attributes' => array(
			'enctype' => 'multipart/form-data',
		),

        'elements' => array
        (
			'<div id="commonSection">',

			// общие нстройки
			'<div class="control-group">' .
				'<div class="controls">' .
					'<h2 class="sectionHeader">'.Yii::t('struct', 'Common').'</h2>' .
				'</div>' .
			'</div>',

			'enabled' => array(
				'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
				'attributes' => array(
					'enabledLabel' => Yii::t('struct', 'On'),
					'disabledLabel' => Yii::t('struct', 'Off'),
					'enabledStyle' => 'success',
				)
			),

			'show_tech_fields' => array(
				'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
				'attributes' => array(
					'enabledLabel' => $categoryOptionsConfig['booleanRange'][1],
					'disabledLabel' => $categoryOptionsConfig['booleanRange'][0],
					'enabledStyle' => 'success',
				)
			),
			'show_gallery' => array(
				'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
				'attributes' => array(
					'enabledLabel' => $categoryOptionsConfig['booleanRange'][1],
					'disabledLabel' => $categoryOptionsConfig['booleanRange'][0],
					'enabledStyle' => 'success',
					'htmlOptions' => array('data-name'=>'show_gallery'),
				)
			),

			'<div class="additionalSection" rel="show_gallery">',
				//'gallery_min' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
				'gallery_max' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
			'</div>',

			'</div>',

			// предисловие
			'<div class="control-group">' .
				'<label class="control-label"> </label>' .
				'<div class="controls">' .
					'<h2 class="sectionHeader">'.Yii::t('struct', 'Foreword').'</h2>' .
				'</div>' .
			'</div>',

			'foreword' => array(
				'type' => 'dropdownlist',
				'items' => $categoryOptionsConfig['forewordRange'],
				'attributes' => array(
					'data-name' => 'foreword'
				),
			),

			'<div class="additionalSection" rel="foreword">',
				'foreword_required' => array(
					'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
					'attributes' => array(
						'enabledLabel' => $categoryOptionsConfig['booleanRange'][1],
						'disabledLabel' => $categoryOptionsConfig['booleanRange'][0],
						'enabledStyle' => 'success'
					)
				),
				'foreword_length_min' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
				'foreword_length_max' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
			'</div>',


			// контент
			'<div class="control-group">' .
				'<label class="control-label"> </label>' .
				'<div class="controls">' .
					'<h2 class="sectionHeader">'.Yii::t('struct', 'Content').'</h2>' .
				'</div>' .
			'</div>',

			'content' => array(
				'type' => 'dropdownlist',
				'items' => $categoryOptionsConfig['contentRange'],
				'attributes' => array(
					'data-name' => 'content'
				),
			),

			'<div class="additionalSection" rel="content">',
				'content_required' => array(
					'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
					'attributes' => array(
						'enabledLabel' => $categoryOptionsConfig['booleanRange'][1],
						'disabledLabel' => $categoryOptionsConfig['booleanRange'][0],
						'enabledStyle' => 'success'
					)
				),

				'content_length_min' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
				'content_length_max' => array('type' => 'text', 'placeholder'=>Yii::t('struct', 'Any')),
			'</div>',

			'<br/><br/>',

			// дополнительные поля
			'<div class="control-group">' .
				'<label class="control-label"> </label>' .
				'<div class="controls">' .
					'<h2 class="sectionHeader">'.Yii::t('struct', 'Custom fields').'</h2>' .
				'</div>' .
			'</div>',

			'custom_fields' => array(
				'type' => 'application.modules.struct.extensions.CustomFields',
			),
		),
        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),


    /**
     * Текст запроса подтверждения на удаление элементов.
     * @var string
     */
    'deleteConfirmationMessage' => 'Wish you delete category {elements}?<br/>Pages of this category will be deleted too.'.
                                   '|Wish you delete categories {elements}?<br/>Pages of this categories will be deleted too.',

);

?>
