<?php

/**
 * Конфигурация административного сервиса управления материалами
 * (StructPagesAdminService).
 */
return array
(
    'assets' => array(
        'css' => array('admin.css'),
        'js' => array('admin/tags.js'),
    ),

	'views' => array
	(
		'create' => 'tags/create',
		'update' => 'tags/update',
		'index' => 'tags/index',
		'_form' => 'tags/_form',
		'_groupOperationsBar' => 'pages/_groupOperationsBar',
    ),

    /**
     * Поля, отображаемые в таблице индекса (actionIndex).
     * @var array
     */
    'indexTableOptions' => array
    (
        /**
         * Поле сортировки.
         * @var string 
         */
        'sortField' => 'title',

		'htmlAttributes' => array
		(
			"container"         => array(),

			"table"             => array("cellspacing"=>0, "cellpadding"=>0),
			"row"               => array(),
			"headRow"           => array(),
			"cell"              => array("valign"=>"top"),

			// дополнительные поля
			"additionalField"   => array(),

			// настройка формы фильтров по таблице
			"form"          => array("method" => "POST"),
			"searchBlock"   => array(),
			"searchField"   => array(),
			"searchSubmit"  => array(),
			"scopeButton"   => array(),
			"scopeValue"    => array(),

			"info"          => array(),
		),

		'fieldsOrder' => array('title', 'pages_count', 'date_created', 'user_id', 'update'),
		'route' => 'index',

		'buttons' => array(
			'update' => array(

                'htmlOptions'=>array('class'=>'pageManagement pull-right'),

                'buttons' => array(
                    array(
                        'label' => ' ',
                        'icon'=>'icon-edit',
                        'items' => array(
                            'update' => array('label'=>Yii::t('app', 'Open in edit form'), 'icon'=>'icon-pencil',),
                            'delete' => array('label'=>Yii::t('app', 'Delete item'), 'icon'=>'icon-trash', 'itemOptions'=>array('class'=>'deletePage')),
                        ),
                    )
                )
            )
        ),

		'scopes' => array(
			'unrelated' => function($model, $scopeValue){ $model->alone(); },
		),

        /**
         * Подписи к фильтрам.
         * @var array
         */
        'scopeLabels' => array(
			'unrelated' => Yii::t('struct', 'Tags without relations'),
        ),

        /**
         * Предобработка полей.
         */
        'preConvertors' => array
		(
			'user_id' => function($fieldName, $fieldValue, $row, $isHeader = FALSE)
			{
				if($isHeader) return $fieldValue;
				if($fieldValue){
					$user = User::cachedInstance($fieldValue);
					if($user) return CHtml::link($user->username, '#');
					else return Yii::t('struct', 'Unknown user');
				}
				else return Yii::t('struct', 'Guest');
			}
		),

        'pageSize' => 15,
        'sortVar' => 'orderBy',
    ),

    'createForm' => array
    (
		'attributes' => array(
			'enctype' => 'multipart/form-data',
		),

        'elements' => array
		(
			'title' => array('type'=>'text', 'class' => 'span6'),
			'description' => array('type'=>'textarea', 'class' => 'span6'),
        ),

        'showErrorSummary'  => true,
        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
        ),
    ),

);