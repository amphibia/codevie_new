<?php

class StructUrlRule extends CBaseUrlRule
{
    /**
     * ID компонента-категоризатора, с которым работает это правило.
     * @var string
     */
    public $structComponentID = NULL;


    /**
     * Компонент-категоризатор, с которым работает правило.
     * @var StructComponent
     */
    protected $_structComponent = NULL;


    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $this->_structComponent = Yii::app()->{$this->structComponentID};

        $category = $this->_structComponent->getCategory(
			$pathInfo, StructComponent::DRAFT, true);

        if($category)
        {
            $arguments = $this->_structComponent->getArguments();

            // поведение для категорий
            if(!$this->_structComponent->currentPage)
            {
                if(!sizeof($arguments)){
                    $action = 'index';
                    $arguments = array();
                }
                else{
                    $action = array_shift($arguments);
                    $arguments = implode($arguments, '/');
                }
            }

            // поведение для материалов
            else
            {
                $action = 'item';
                $arguments = sizeof($arguments)
                    ? implode($arguments, '/')
                    : $arguments;
            }

            // аргументы для функции
            $_GET = array('_actionArguments' => $arguments);

            // route
            return "{$this->_structComponent->currentControllerID}/{$action}";
        }

        else return false;
    }


    public function createUrl($manager,$route,$params,$ampersand)
    {
        /** @todo формировать URL средствами CBaseUrlRule */

        return NULL;
    }
}
