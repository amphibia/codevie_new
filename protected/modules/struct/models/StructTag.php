<?php

/**
 * @author koshevy
 * 
 * Модель тега для материалов сайта.
 */
class StructTag extends ActiveRecord
{
	public function rules()
	{
		return array
		(
			array('user_id', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max'=>255),
			array('title', 'unique'),
			array('description', 'length', 'max' => '1024'),
		);
	}


	/** ** ** ** ** ** ** ** ** ** ** 
	 * Внутренние технические методы */

    public function init() {
        parent::init();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function behaviors()
	{
		return array
		(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			),

			// Работа с POST-ом для MANY_TO_MANY.
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
		);
    }

    public function relations()
	{
		return array
		(
			// категории
			
			// Категории, с которыми связан тэг (явного указывания не происходит,
			// связь используется как индекс, и проставляется автоматически - в
			// зависимости от привязанных материалов.
			'categories' => array(self::MANY_MANY,
				'StructCategory',
				StructTagCategory::model()->tableName().'(tag_id, category_id)',

                // эти параметры указываются, чтобы сортировка происходила
				// в порядке указывания элементов
                'together' => true, 'order'=>'categories_categories.id ASC'
			),

			// только те из привязанных категорий, которые опубликованы
			'publishedCategories' => array(self::MANY_MANY,
				'StructCategory',
				StructTagCategory::model()->tableName().'(tag_id, category_id)',

                // эти параметры указываются, чтобы сортировка происходила
				// в порядке указывания элементов
                'together' => true, 'order'=>'publishedCategories.id ASC',
				'condition' => "(publishedCategories.published = 'published')"
			),

			// промежуточные модели для связи с категориями
			'categoriesBinds' => array(self::HAS_MANY, 'StructTagCategory', 'tag_id', 'joinType' => 'LEFT JOIN'),

			// связь с категориями через промежуточную модель
			'throughCategoriesBinds' => array(self::HAS_MANY, 'StructCategory', array('category_id'=>'id'), 'through'=>'categoriesBinds', 'joinType' => 'LEFT JOIN'),


			// материалы

			// все связанные материалы
			'pages' => array
			(
				self::MANY_MANY, 'StructPage', StructTagPage::model()->tableName().'(tag_id, page_id)',
				'order'=>"position"
			),

			// только опубликованные связанные материалы
			'publishedPages' => array
			(
				self::MANY_MANY, 'StructPage', StructTagPage::model()->tableName().'(tag_id, page_id)',
				'order'=>"position",
				'condition'=>"published='published'"
			),

			// модели промежуточных связей с материалами
			'pagesBinds' => array(self::HAS_MANY, 'StructTagPage', 'tag_id', 'joinType' => 'LEFT JOIN'),

			// связь с материалами через промежуточную связь
			'throughPagesBinds' => array(self::HAS_MANY, 'StructPage', array('page_id'=>'id'), 'through'=>'pagesBinds', 'joinType' => 'LEFT JOIN'),
		);
	}
	
	public function beforeValidate()
	{		
		if($this->isNewRecord && Yii::app()->hasComponent('user') && Yii::app()->user){
			if(!Yii::app()->user->isGuest){
				$this->user_id = Yii::app()->user->model->id;
			}
		}

		// индексирование категорий, с которыми соприкасается тэг
		$this->categories = array();
		foreach($this->pages as $page){

			// привязанные материалы
			if(!$page) continue;
			if(!is_object($page)){
				$page = StructPage::cachedInstance($page);
			}

			// категории этих материалов
			foreach($page->categories as $category){
				if(!$category) continue;
				if(is_object($category))
					 $category_id = $category->id;
				else $category_id = $category;
			}
			$this->categories[] = $category_id;
		}

		return parent::beforeValidate();
	}

    public function tableName()
    {
        return self::commonTableName('struct_tag');
    }


	/** Условия **/


	/**
	 * Отсортировка тэгов по категориям, c которым связаны теги.
	 * В зависимости от параметра $compareType ищет тэги, связанные со всеми, из
	 * перечисленных категорий, либо одной из них.
	 * 
	 * @param array $categories перечисление ID категорий
	 * @param string $compareType тип формирования условия (AND или OR)
	 */
	public function categories(Array $categories, $compareType = 'OR')
	{
		$this->dbCriteria->mergeWith(array(
			'with' => 'throughCategoriesBinds',
			'together' => true,
		));

		if($compareType == 'OR')
			$this->dbCriteria->addInCondition('category_id', $categories);

		else if($compareType == 'AND')
		{
			$bindsValues = array();
			$binds = StructTagCategory::model();
			$binds->dbCriteria->addInCondition('category_id', $categories);
			$binds->dbCriteria->group = 'tag_id';
			$binds->dbCriteria->having = new CDbExpression('COUNT(category_id)='.sizeof($categories));
			$binds->dbCriteria->select = 'id';
			foreach($binds->findAll() as $row) $bindsValues[] = $row->id;

			$this->dbCriteria->addInCondition('categoriesBinds.id', $bindsValues);
		}

		return $this;
	}

	/**
	 * Отсортировка тэгов по материалам, c которым связаны теги.
	 * В зависимости от параметра $compareType ищет тэги, имеющие связи со всеми
	 * перечисленными материалами, либо с одним из них из них.
	 * 
	 * @param array $categories перечисление ID материалов
	 * @param string $compareType тип формирования условия (AND или OR)
	 */
	public function pages(Array $pages, $compareType = 'OR')
	{
		/**
		 * @todo: эта выборка не тестировалась
		 */
		
		$this->dbCriteria->mergeWith(array(
			'with' => 'throughPagesBinds',
			'together' => true,
		));

		if($compareType == 'OR')
			$this->dbCriteria->addInCondition('page_id', $pages);

		else if($compareType == 'AND')
		{
			$bindsValues = array();
			$binds = StructTagPage::model();
			$binds->dbCriteria->addInCondition('page_id', $pages);
			$binds->dbCriteria->group = 'tag_id';
			$binds->dbCriteria->having = new CDbExpression('COUNT(page_id)='.sizeof($pages));
			$binds->dbCriteria->select = 'id';
			foreach($binds->findAll() as $row) $bindsValues[] = $row->id;

			$this->dbCriteria->addInCondition('pagesBinds.id', $bindsValues);
		}

		return $this;
	}

	/**
	 * Фильтр - только тэги, не имеющие связей с материалами.
	 */
	public function alone(){
		$this->dbCriteria->mergeWith(array(
			'with' => 'throughPagesBinds',
			'together' => true,
			'condition' => 'page_id IS NULL'
		));
	}

	/**
	 * Удаление связей вслед за объектом.
	 */
	public function afterDelete() {
		parent::afterDelete();
		StructTagCategory::model()->deleteAll(
			"tag_id=:tag_id", array(':tag_id'=>$this->id) );
		StructTagPage::model()->deleteAll(
			"tag_id=:tag_id", array(':tag_id'=>$this->id) );
	}


}