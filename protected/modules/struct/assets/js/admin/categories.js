$(document).ready( function(){

	// переключение блока "Дополнительные настройки"
	var $logicSectionHeader = $('#logicSection .sectionHeader'),
		$logicSection = $('#logicSection');
	if($logicSection.length)
	{
		if(parseInt($.cookie('logicSection_state'))) $logicSection.addClass('open');
		$logicSectionHeader.click(function(){
			if($logicSection.hasClass('open')){
				 $logicSection.removeClass('open');
				 $.cookie('logicSection_state', 0);
			}
			else{
				$logicSection.addClass('open');
				$.cookie('logicSection_state', 1);
			}
		});
	}

	// переключение блока "Прикрепленные файлы"
	var $attachsSectionHeader = $('#attachsSection .sectionHeader'),
		$attachsSection = $('#attachsSection');
	if($attachsSection.length)
	{
		if(parseInt($.cookie('attachsSection_state'))) $attachsSection.addClass('open');
		$attachsSectionHeader.click(function(){
			if($attachsSection.hasClass('open')){
				 $attachsSection.removeClass('open');
				 $.cookie('attachsSection_state', 0);
			}
			else{
				$attachsSection.addClass('open');
				$.cookie('attachsSection_state', 1);
			}
		});
	}

	// реакция на выделение элементов
    $(document).bind('selectItems', function(){
        var selectedItems = Array();
        $('.treeview .ui-selected').each(function(){
            selectedItems.push(this.id);});
        
        // URL использованного сервиса заблаговременно оставлен
        // при созданиии страницы
        var manageBarActionUrl = $(document).data('manageBarActionUrl');
        $('#manageBar').request(manageBarActionUrl, {selectedItems:selectedItems}, null,
            function()
            {
                // чтобы не развыделялось
                $('#manageBar select').click(function(){
                    return false;
                });

                // публикация/депубликация на месте
                $('#manageBar .publishedControll').change(function()
                {  
                    var id = $(this).attr('data-id'),
                        title = $(this).attr('data-title'),
                        setAttributesActionUrl = $(document).data('setAttributesActionUrl'),
                        $target = $(this),
						value = this.value;

                    if(id)
					{
                        $(this).request(setAttributesActionUrl, {
                                id:id, values:{published:this.value},
                                customMessage: {
                                    message:'Category "{category}" publish status changed to "{value}"',
                                    '{category}':title,
                                    '{value}':value
                                }
                            }, null,
                            
                            // по завершении запроса перерисовывает
                            function(){
								console.log($('.treeField .ui-selected'));
								$('.treeField .ui-selected .title').attr('published', value);
                                $target.closest('.row').find('.treeview').repaint();
                            }
                        );
                    }

                    return false; 
                });
            });
    });

	// выделение всех категорий
	$('#selectRoots').live('click', function(){
		$('.treeview.sortable li').addClass('ui-selected');
		$(document).trigger('selectItems');
	});

    // переход к редактированию записи по двойному нажатию
	$('.treeField li[id]').live('dblclick', function(){
		var updateUrl = $(document).data('updateUrl');
		var id = $(this).attr('id');
		$(this).find('>span').addClass('clientBusy');
		location.href = updateUrl+id;
		return false;
	});

    // удаление категорий
    $('#deleteCategories').live('click', function(){
        var $selects = $('.treeview .ui-selected');
        $(this).request(
            $(this).attr('data-url'),
            {json_operation_id: $.client.nextOperationId(function(){
                $selects.remove();
                // удаление ненужных +/- (свернуть/развернуть)
                var li = $('.treeview ul:not(:has(li))').parent();
                    li.find('.hitarea').remove();
                    li.removeClass('collapsable');
                    li.removeClass('expandable');
                    li.removeClass('lastCollapsable');
                $(document).trigger('selectItems');
            })}
        );
        return false;
    });

	// удаление по нажатию DELETE
	$(document).keydown(function(evt){

		// влево (сворачивание)
		if(((evt.keyCode == 8) && evt.metaKey) || (evt.keyCode == 46)){
			$('#deleteCategories').click();
		}
	});


    // переключает элемент Handler для работы с контроллерами/модулями в
    // зависимости от типа
    function toggleControllersModules()
    {
        $('#StructCategory_type').each(function(){
			var $cg = $(this).closest('form').find('.control-group[attr="handler"]'),
				$extRef = $(this).closest('form').find('[for="StructCategory_external_reference"]').closest('.control-group ');

			if($(this).val() == 'controller'){
				$cg.show();
				$extRef.hide();
				$cg.find('.handlerSelect>.modules').hide().find('.formElement').attr('disabled', true);
				$cg.find('.handlerSelect>.controllers').show().find('.formElement').removeAttr('disabled');
			}
			else if($(this).val() == 'module'){
				$cg.show();
				$extRef.hide();
				$cg.find('.handlerSelect>.modules').show().find('.formElement').removeAttr('disabled');
				$cg.find('.handlerSelect>.controllers').hide().find('.formElement').attr('disabled', true);
			}
			else if($(this).val() == 'external_reference'){
				$cg.hide(); $extRef.show();
			}
			else{
				$cg.hide(); $extRef.hide();
			}

		});
	}
    toggleControllersModules();
    $('#StructCategory_type').change(toggleControllersModules);

    // отображает/прячет поле выбора лайоута в зависимости от значения 'layout_integration'
    function toggleLayoutName()
    {
        var $parent = $(this).closest('.controls');
        var $activeCheckBox = $parent.find('[name="StructCategory[layout_integration]"]');
        var $layoutName = $parent.closest('form').find('#StructCategory_layout_name').closest('.control-group');

        if($activeCheckBox.val() == 'custom')
             $layoutName.show();
        else $layoutName.hide();
    }
    toggleLayoutName.apply($('[name="StructCategory[layout_integration]"]')[0]);
    $('[name="StructCategory[layout_integration]"]').live('change', toggleLayoutName);


});

/**
 * Боковая панель меняет свою позицию при движении страницы.
 **/
function manageBarScrollAgent()
{
	// применяется только если длинный экран
	if(parseInt($(document.body).height()) < (1150)) return;

	var $sidebar = $("#sidebar");
	if(!$sidebar.length || ($sidebar.parent().width()=='100%')) return; // и не в режиме мобильного устройства

	var fixedTop = parseInt($sidebar.attr('data-fixed-top')),
		bodyScroll = $(document).scrollTop(),
		sidebarOffset = $sidebar.attr('data-original-offset');

	if(!sidebarOffset)
		$sidebar.attr('data-original-offset', sidebarOffset = $sidebar.offset().top)


	if(bodyScroll >= (sidebarOffset-fixedTop))
	{
		var $container = $sidebar.closest('.container'),
			barBottom = parseInt(bodyScroll)+parseInt(fixedTop)+parseInt($sidebar.outerHeight()),
			containerBottom = parseInt($container.offset().top) + parseInt($container.outerHeight());

		if(barBottom >= containerBottom){
			$sidebar.css('top', 'auto').css('bottom',
				(parseInt($(document.body).height()) -
					(((parseInt($container.height())+parseInt($container.offset().top))
					+ (containerBottom - barBottom)))
				));
		}
		else{
			$sidebar.css('top', fixedTop).css('bottom', 'auto');
		}

		$sidebar.css('left', $sidebar.parent().offset().left)
				.css('width', $sidebar.parent().width())
				.css('position', 'fixed')
				.parent().css('height', $sidebar.height());
	}
	else $sidebar
			.css('width', '100%').css('position', 'static')
			.parent().css('height', 'auto');
}

/*
 * Переопределение функционала CTreeView для работы с динамическим изменением
 * содержимого.
 */
;(function($)
{
    // переопределение метода из TreeView, делающее привязку collapsable/expandable
    // по $.live()
    $.extend($.fn, {

        /**
         * Добавляется автоматическое открытие всех пунктов - групп при первом открытии.
         */
        loadCookiesState: function()
        {
            var $tree = $($(this)[0]).closest('.treeview.sortable'),
                collapsableIds = $.cookie($tree.attr('id') + '_collapsable'),
                selectedIds = $.cookie($tree.attr('id') + '_selected');

            // Дерево сайта открывается в первый раз для этого браузера.
            if(collapsableIds === null){
                // открывает все группы
                $tree.find('[type="group"]').each(function(){
                    $(this).closest('li').find('>.hitarea').click();
                });

                // открывает первый элемент, если он не открыт (например, если не группа)
                var $firstHitarea = $tree.find('li').first().find('.hitarea');
                if($firstHitarea.hasClass('expandable-hitarea'))
                    $firstHitarea.click();
            }
            else if(collapsableIds){
                var ids = collapsableIds.split(',');
                var selector = 'ol#u';
                for(var id in ids){
                    selector += ', li#'+ids[id]+'>.hitarea'; }
                $(selector).click();
            }

            $('.ui-selected').removeClass('ui-selected');
            if(selectedIds){
                var ids = selectedIds.split(',');
                var selector = 'ol#u';
                for(var id in ids){
                    selector += ', li#'+ids[id]; }
                $(selector).addClass('ui-selected');
            }

            $(this).closest('.treeview.sortable').trigger('selectItems');
        }
    });

})(jQuery);


$(document).scroll(manageBarScrollAgent);
$(window).resize(manageBarScrollAgent);