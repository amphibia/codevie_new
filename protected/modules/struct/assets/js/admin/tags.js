/**
 * @autor koshevy
 * @package struct
 * Клиентская часть системы управления страницами. Реализует сортировку между страницами
 * (клиент/сервер), сортировку по категориям.
 * 
 * @todo: убедиться что UI-SORTABLE ведёт себя адекватно.
 * 
 */
$(document).ready(function(){

	// при перелистывании страниц в таблице изменяется URL
	if($('#ActiveTable').length)
		$.client.config.workPlace = 'ActiveTable';

    jQuery.fn.extend({

        /**
         * Инициализация фильтра по категориям.
         */
        initCategoriesFilter: function()
        {
            /**
             * Обработчик события - выборки категорий.
             * @var callback
             */
            var onMTMSelectChange = function(){    
                var $ActveTable = $('#ActiveTable'),
					$table = $ActveTable.find('.table');

                // перезагрузка таблицы
				if($ActveTable.length && $table.length)
					$ActveTable.request($table.attr('uri'));
            };

            // каждый раз при отправке запроса от имени таблицы, к нему добавляются
            // данные о фильтре категорий
            $('.ActiveTable').unbind('onBeforeRequest');
            $('.ActiveTable').bind('onBeforeRequest', function(evt, data){                
                var categories = Array()

                // информация о выбранных категориях
                $('.categoriesList .mtmselect').find('.mtmitem[id]').each(function(){
                    categories.push($(this).attr('id')); });
                data.post.selectedCategories = categories;
            });

            // привязка обработчика - выборка по категориям
            $('.mtmselect').unbind('OnMTMSelectAdd');
            $('.mtmselect').unbind('OnMTMSelectRemove');
            $('.mtmselect').bind('OnMTMSelectAdd', onMTMSelectChange);
            $('.mtmselect').bind('OnMTMSelectRemove', onMTMSelectChange);
            
            return $(this);
        },

        /**
         * Удаление страницы.
         * @param элемент, на который было произведено нажатие.
         */
        deletePage: function(){
            var $activeTable = $(this).closest('.ActiveTable');

            // запрос на удаление
            $(this).request($(this).attr('href'),
                {json_operation_id: $.client.nextOperationId(function(){
                    var uri = $activeTable.find('.table').attr('uri');
                    // перерисовка таблицы
                    $activeTable.request(uri);
                })
            });
		}
    });

    $('.deletePage a').click(function(){
        $(this).deletePage(); return false;
    });

    $(document).initCategoriesFilter();

    // контекстное меню элемента перетаскивания
    $('.positionHolder').live('mousedown', function(evt){
        if(evt.which>1){
            $parent = $(this).parent();
            if($parent.hasClass('open')) $parent.removeClass('open')
            else $parent.addClass('open');
            return false;
        }
    });

    $(document).live('mousedown', function(){$('.moveDropdown.open').removeClass('open')});

});

// инициализация элементов, подгруженных в AJAX-запросе
$(document).bind('onPartLoad', function(){
    $('.deletePage a').click(function(){
        $(this).deletePage(); return false;
    });

    $(document).initCategoriesFilter();
});

