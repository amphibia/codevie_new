/**
 * @autor koshevy
 * @package struct
 * Клиентская часть системы управления страницами. Реализует сортировку между страницами
 * (клиент/сервер), сортировку по категориям.
 * 
 * @todo: убедиться что UI-SORTABLE ведёт себя адекватно.
 * 
 */
$(document).ready(function(){

	// переключение блока "Прикрепленные файлы"
	var $attachsSectionHeader = $('#attachsSection .sectionHeader'),
		$attachsSection = $('#attachsSection');
	if($attachsSection.length)
	{
		if(parseInt($.cookie('attachsSection_state'))) $attachsSection.addClass('open');
		$attachsSectionHeader.click(function(){
			if($attachsSection.hasClass('open')){
				 $attachsSection.removeClass('open');
				 $.cookie('attachsSection_state', 0);
			}
			else{
				$attachsSection.addClass('open');
				$.cookie('attachsSection_state', 1);
			}
		});
	}

	// при перелистывании страниц в таблице изменяется URL
	if($('#ActiveTable').length)
		$.client.config.workPlace = 'ActiveTable';

    jQuery.fn.extend({

        initSortableHints: function(){
            $('.ActiveTable .table[sortField!="position"] .positionHolder').each(function(){
                var $sortLink = $(this).closest('.ActiveTable')
                    .find('.headRow [field="position"] a');

                $(this).mouseover(function(){
                    $sortLink.popover({
                        title:$(this).attr('data-title'),
                        content:$(this).attr('data-content')
                    });
                    $sortLink.popover('show');
                });
                $(this).mouseout(function(){$sortLink.popover('hide');});
            });

            return $(this);
        },

        /**
         * Отправка элемента в начало/конец.
         */
        moveToLastFirst: function(element, value)
        {
            $(element).request($(document).data('actionDrag_url'), {
                    elementID: element.attr('page_id'),
                    newPosition: value
                }, null,
                function(){
                    var $activeTable = $(element).closest('.ActiveTable'),
                        uri = $activeTable.find('.table').attr('uri');

                    // перерисовка таблицы
                    $activeTable.request(uri);
                }
            );
        },

        initSortable: function(){
            
            // если подгрузка была совершена не во время перетаскивания
            if(!$('.ActiveTable.sort-active').length)
            {
                // инициализация сортируемого списка
                $('.ActiveTable .table[sortField="position"] tbody').sortable({
                    connectWith: '.ActiveTable .table[sortField="position"] tbody',
                    handle: '.positionHolder',
                    placeholder: 'positionPressholder',
                    axis: 'y',
                    appendTo: '.root-container',
                    helper: 'clone',
                    dropOnEmpty: true,
                    start: function(){
                        var $holder = $('.ActiveTable .table[sortField="position"] .positionHolder');

                        setTimeout(function(){ $('.ActiveTable .sidePagination').addClass('visiblePermanent'); }, 300);

                        $(this).closest('.ActiveTable').addClass('sort-active').find('.sidePagination');
                        $holder.tooltip('disable');
                        $holder.attr('disabled-title', $holder.attr('title'));
                        $holder.removeAttr('title');

                        $('body').css('width', $('body').width());
                    },
                    stop: function(){
                        var $holder = $('.ActiveTable .table[sortField="position"] .positionHolder');

                        $(this).closest('.ActiveTable').removeClass('sort-active').find('.sidePagination').removeClass('visiblePermanent');
                        $(document).initSortableHints().initSortable();
                        $('body').css('width', 'auto');
                        
                        $holder.attr('title', $holder.attr('disabled-title'));
                        $holder.removeAttr('disabled-title');
                        
                        $(document).initSortableHints().initSortable();
                        $(this).menuDisables();
                    },
                    update: function(evt, ui)
                    {
						var $firstElement, $lastElement;	
                        var $prev = $(ui.item).prev().find('.positionHolder'),
                            $next = $(ui.item).next().find('.positionHolder'),
                            page_id = $(ui.item).find('.positionHolder').attr('page_id'),
                            newPosition = null;

                        // перетащили в самое начало
                        if(!$prev.length){
                            $firstElement = $(ui.item).closest('.ActiveTable').find('.positionHolder:first-child[page_id!='+page_id+']');
                            newPosition = $firstElement.attr('position');
                        }
                        // перетащили в самый конец
                        else if(!$next.length){
                            $lastElement = $(ui.item).closest('.ActiveTable').find('.positionHolder[page_id!='+page_id+']').last();
                            newPosition = $lastElement.attr('position');
                        }

                        // придётся ориентироваться на месте
                        else
                        {
                            // определение направления (true - вниз, false - вверх)
                            var ascdesc = $(ui.item).closest('.table').attr('sortdirection'),
                                direction = (ui.position.top > ui.originalPosition.top);
                                //&& (ascdesc=='asc');

                            newPosition = direction
                                ? $prev.attr('position') : $next.attr('position');
                        }

                        $(ui.item).request($(document).data('actionDrag_url'), {
                                elementID: page_id,
                                newPosition: newPosition
                            }, null,
                            function(){
                                var $activeTable = $(ui.item).closest('.ActiveTable'),
                                    uri = $activeTable.find('.table').attr('uri');

                                // перерисовка таблицы
                                $activeTable.request(uri);
                            }
                        );
                    }
                }).disableSelection();
            }
            
            else
            {
                // показывает листатели
                setTimeout(function(){
                    $('.ActiveTable .sidePagination')
                        .addClass('visiblePermanent');
                }, 300);

                // изьятие из таблицы перетаскиваемого элемента
                $('.ActiveTable .bodyRow#'+$('.bodyRow.ui-sortable-helper').attr('id')).remove();
            }

            $('.ActiveTable .table[sortField="position"] .positionHolder').tooltip({placement:'right'});
            $('.ActiveTable .table[sortField!="position"] .positionHolder').removeAttr('title');

            return $(this);
        },

        /**
         * Выключение ненужных элементов контекстного меню ручки для перетаскивания
         * (переместить в начало/переместить в конец). Вызывается сразу после
         * подгрузки фрагмента.
         */
        menuDisables: function(){
            $('.ActiveTable .table .cell[field="position"] .disabled').removeClass('disabled');
            $('.ActiveTable .table[pageNumber="0"] .bodyRow:first .cell[field="position"] .move_to_first').addClass('disabled');
            $('.ActiveTable .table[lastpage="true"] .bodyRow:last .cell[field="position"] .move_to_last').addClass('disabled')
        },

        /**
         * Инициализация фильтра по категориям.
         */
        initMTMFilters: function()
        {
            /**
             * Обработчик события - выборки категорий.
             * @var callback
             */
            var onMTMSelectChange = function(evt){
                var $ActveTable = $('#ActiveTable'),
					$table = $ActveTable.find('.table');

                // перезагрузка таблицы
				if($ActveTable.length && $table.length)
					$ActveTable.request($table.attr('uri'));

            };

            // каждый раз при отправке запроса от имени таблицы, к нему добавляются
            // данные о фильтре категорий
            $('.ActiveTable').unbind('onBeforeRequest');
            $('.ActiveTable').bind('onBeforeRequest', function(evt, data){                
                var categories = [],
					tags = [];

				// информация о выбранных категориях
				$('.selectedCategories .mtmselect').find('.mtmitem[id]').each(function(){
					categories.push($(this).attr('id')); });
				data.post.selectedCategories = categories;

				// информация о выбранных тегах
				$('.selectedTags .mtmselect').find('.mtmitem[id]').each(function(){
					tags.push($(this).attr('id')); });
				data.post.selectedTags = tags;
            });

            // привязка обработчика - выборка по категориям
            $('.mtmselect')
				.unbind('OnMTMSelectAdd')
				.unbind('OnMTMSelectRemove')
				.bind('OnMTMSelectAdd', onMTMSelectChange)
				.bind('OnMTMSelectRemove', onMTMSelectChange);
            
            return $(this);
        },

        /**
         * Удаление страницы.
         * @param элемент, на который было произведено нажатие.
         */
        deletePage: function(){
            var $activeTable = $(this).closest('.ActiveTable'),
                deleteUrl = $(document).data('actionDelete_url'),
                id = $(this).closest('.bodyRow').attr('id');

            // запрос на удаление
            $(this).request(deleteUrl+'/'+id,
                {json_operation_id: $.client.nextOperationId(function(){
                    var uri = $activeTable.find('.table').attr('uri');
                    // перерисовка таблицы
                    $activeTable.request(uri);
                })
            });
		}
    });

    $('.deletePage a').click(function(){
        $(this).deletePage(); return false;
    });

    // автоматическое перелистывание страниц при перетаскивании
    $('.sort-active .sidePagination').live('mouseover', function(evt){
        $(this).click();
    });
    
    // изменение статуса публикации "на месте"
    $('.ActiveTable .changePublished').live('change', function(){
        var id = $(this).closest('.bodyRow').attr('id'),
            url = $(document).data('actionSetAttributes_url');

        $(this).request(url, {
            id: id,
            values: {'published':$(this).val()}
        });
    });

    $('.move_to_first a').unbind('mousedown');
    $('.move_to_first a').live('mousedown', function(evt){
        var $positionHolder = $(this).closest('.moveDropdown').find('.positionHolder'),
            sortdirection = $(this).closest('.table').attr('sortdirection');
        $(this).moveToLastFirst($positionHolder, ((sortdirection!='desc')?'first':'last'));
        return false;
    });
    $('.moveDropdown .move_to_last a').unbind('mousedown');
    $('.moveDropdown .move_to_last a').live('mousedown', function(evt){
        var $positionHolder = $(this).closest('.moveDropdown').find('.positionHolder'),
            sortdirection = $(this).closest('.table').attr('sortdirection');
        $(this).moveToLastFirst($positionHolder, ((sortdirection!='desc')?'last':'first'));
        return false;
    });

    $(this).menuDisables();
    $(document).initSortableHints().initSortable().initMTMFilters();

    // контекстное меню элемента перетаскивания
    $('.positionHolder').live('mousedown', function(evt){
        if(evt.which>1){
            $parent = $(this).parent();
            if($parent.hasClass('open')) $parent.removeClass('open')
            else $parent.addClass('open');
            return false;
        }
    });

    $(document).live('mousedown', function(){$('.moveDropdown.open').removeClass('open')});
	
	// смена статуса для группы выделенных элементов в таблице
	$('.ActiveTable .setPublishStatus').live('change', function(){
		var	$ActiveTable = $(this).closest('.ActiveTable'),
			value = $(this).val(),
			url = $(document).data('actionSetPublishStatus_url'),
			ids = [];

		if(value == '0') return;

		$ActiveTable.find('.selectedRow').each(function(){
			ids.push($(this).attr('id'));
		});

		$ActiveTable.request(url, {ids:ids, status:value}, null, function(){
			var $table = $ActiveTable.find('.table'),
				selector = '';
			$ActiveTable.request($table.attr('uri'), {}, null, function(){
				for(id in ids){ selector += ('.bodyRow#'+ids[id]+', '); }
				$rows = $ActiveTable.find(selector);
				$rows.addClass('selectedRow');
				if($rows.length) $($rows[0]).trigger('onSelectRow');
			});
		})
	});
});

// инициализация элементов, подгруженных в AJAX-запросе
$(document).bind('onPartLoad', function(){
    $('.deletePage a').click(function(){
        $(this).deletePage(); return false;
    });

    $(this).menuDisables();
    $(document).initSortableHints().initSortable().initMTMFilters();
});

