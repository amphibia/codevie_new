<?php


/**
 * Задача контроллера получать перенаправления от StructUrlRule в случае
 * если URL ассоциируется с категорией.
 */
class StructController extends ExtCoreExtController
{

    public $layout = 'application.modules.struct.views.face.layouts.struct';

    /**
     * Компонент - хозяин контроллера.
     * @var StructComponent
     */
    public $structComponent = NULL;

    /**
     * Настройки для виджета @see ActiveTable (actionIndex).
     * Аттрибуты 'model', 'scope' и настройки постраничного вывода указывать не
     * нужно.
     * @var array
     */
    public $indexTableOptions = array();

    /**
     * Количество страниц на одной странице страниц ;)
     * @var integer
     */
    public $pageSize = 15;

    /**
     * Текущая страница (страница вывода страниц).
     * @var integer
     */
    public $currentPageNumber = 1;

    /**
     * Количество страниц.
     * @var integer
     */
    public $pagesCount = 0;

    /**
     * Адреса используемых представлений.
     * @var array
     */
    public $views = array
    (
        'category' => 'face/category',
        'page' => 'face/page',
    );

    public $reservedArguments = array(
        'page' => '/^[0-9]{1,5}$/',
        'scope' => '/^\w+$/',
        'scopeValue' => '/^[\w\-]+$/',
    );

    protected $_useAutoArguments = true;

    public function beforeAction($action) {
        Yii::loadExtConfig($this, __FILE__);
        $this->structComponent->structController = &$this;
        return parent::beforeAction($action);
    }

    public function actionIndex($orderBy = 'position', $scope = NULL, $scopeValue = NULL, $search = NULL, $page = NULL)
    {
        $category = &$this->structComponent->currentCategory;
        $this->modelClass = 'StructPage';

        // Переадресация на внешний URL (если указае режим переадресации).
        // Срабатывает только в случае если открывается простомотр содержимого категории;
        // при открытии материала страницы не производится.
        if($category->type == 'external_reference'){
            if($category->external_reference)
                $this->redirect($category->external_reference);
            else throw new CException(Yii::t('struct', 'Redirect URL not setted'));
        }

        // проверка уместности параметров
        if($orderBy && !array_key_exists($orderBy, $this->model->attributes)) throw new CHttpException(404);
        if($page && !is_numeric($page)) throw new CHttpException(404);

        $this->model->categories(array($category->id))->published();
        $this->model->dbCriteria->order = 't.position';

        $this->model->getActiveDataProvider()->pagination->currentPage = $page?$page-1:$page;
        $this->currentPageNumber = $page ? $page : 1;
        $this->model->getActiveDataProvider()->pagination->pageSize = $this->pageSize;
        $this->pagesCount = ceil($this->model->count / $this->pageSize);

        // в $this->model передаётся список страниц
        $this->render($this->views['category'], compact('category'));
    }

    public function actionItem()
    {
        $this->render($this->views['page'], array(
            'category' => Yii::app()->struct->currentCategory,
            'page' => Yii::app()->struct->currentPage
        ));
    }


    /**
     * Содание URL для ссылки на текущую категорию с текущими параметрами.
     *
     * @param string $route
     * @param array $params
     * @param string $ampersand
     * @return mixed|string
     */
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $controllerUrl = parent::createUrl($route, $params, $ampersand);

        return
            ($category = Yii::app()->struct->currentCategory)
                ? preg_replace("|/{$this->id}|", $category->createUrl(), $controllerUrl)
                : $controllerUrl;
    }
}