<?php

class CustomFields extends CInputWidget
{
	public function run()
	{
		$this->value = ($this->model->{$this->attribute})
			? $this->model->{$this->attribute} : $this->value;
		$this->render('customFields/main');
	}
	
	protected function _errors($index, $field)
	{
		if( !isset($this->model->customFieldErrors[$index]) ||
			!isset($this->model->customFieldErrors[$index][$field])
		)
			return;
	
		echo CHtml::openTag('div');
		echo CHtml::tag('div', array('class'=>'help-inline error'),
			$this->model->customFieldErrors[$index][$field]);
		echo CHtml::tag('div', array('class'=>'clear'), ' ');
		echo CHtml::closeTag('div');
	
	}
}

?>