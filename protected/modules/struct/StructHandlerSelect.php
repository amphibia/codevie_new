<?php

/**
 * @package struct
 * @author handler
 * 
 * Элемент формы для выбора модулей/контроллеров.
 */
class StructHandlerSelect extends CInputWidget
{
    /**
     * ID компонента, отвечающего за учёт модулей и компонент.
     * @var string
     */
    public $componentManagerAlias = 'admin';

    /**
     * Имя класса модели компонент.
     * @var string
     */
    public $componentClass = 'Component';

    /**
     * Не используемые категоризатором модули.
     * @var array
     */
    public $excludedModules = array('extcore');

    /**
     * Не используемые категоризатором контроллеры.
     * @var array
     */
    public $excludedControllers = array();



    /**
     * @var ActiveRecord Модель, с которой завязан этот элемент управления.
     */
	public $model;

    /**
     * @var string Поле в используемой модели,
     * к которому привязан элемент управления. 
     */
    public $attribute;

    /**
     * @var array представления, используемые виджетом.
     */
    public $views = array('main'=>'handlerSelect');
    

    public $assets = array();

    
    /**
     * Список модулей системы (собирается с административного модуля, если
     * модуль не присутствует в системе, либо стоит административный модуль,
     * неподдерживающий работу с компонентами и модулями, свойство становится
     * NULL).
     * 
     * Если же в системе нет найденых модулей, свойство хранит значение - пустой
     * массив.
     * @var mixed
     */
    protected $_modules = NULL;
    
    /**
     * Список контроллеров, с которыми может быть ассоциирована категория.
     * @var array
     */
    protected $_controllers = NULL;

    
	public function init()
    {
        $this->_controllers = $this->_getControllersInfo();
        $this->_modules = $this->_getModulesInfo();
	}

    protected function _getModulesInfo()
    {
        $result = NULL;
        if(Yii::app()->hasComponent($this->componentManagerAlias))
            if(class_exists($className = $this->componentClass)){
                $components = $className::model();
                $components->dbCriteria->addCondition(
                    "(t.type = 'module') AND t.exists AND (t.error='') AND t.active");
                if($this->excludedModules && sizeof($this->excludedModules))
                    $components->dbCriteria->addNotInCondition('alias',
                        $this->excludedModules);

                $result = array();
                foreach($components->findAll() as $module)
                    $result[$alias = $module->alias] = ($title = $module->title)
                        ? $title : $alias;
            }
            
        return $result;
    }

    protected function _getControllersInfo()
    {
        $result = array();

        $controllersDir = Yii::getPathOfAlias('application.controllers');
        if($files = glob($controllersDir."/*Controller.php"))
            foreach($files as $fileName)
            {
                $alias = lcfirst(str_replace('Controller.php', NULL, basename($fileName)));
            
                // описания и названия из PHPDOC
                if(preg_match_all("/(\*[\s^\n]+(?>@(?P<property>title)\s+(?P<value>(?>\{[^\{\}]*})|(?>[^\n]*\n))))/",
                        file_get_contents($fileName), $matches, PREG_PATTERN_ORDER)){
                    if(($pos = array_search('title', $matches['property'])) !== FALSE){ $title = trim($matches['value'][$pos],'{}'); }
                    else $title = $alias;
                }

                $result[$alias] = isset($title) ? $title : $alias;
            }

        return $result;
    }
    
	public function run(){
        $this->render($this->views['main']);
	}

}
?>