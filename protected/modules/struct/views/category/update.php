<div class="page-header">
<h1><?= Yii::t('struct', 'Site category update'); ?></h1><br/>
<?

// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('struct', 'Site categories management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>

<?php
$this->renderPartial('category/_form', compact('form'));
?>
