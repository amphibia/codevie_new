<?

/**
 * Класс модели, с которой работает серис.
 * @var string
 */
$modelClass = $this->modelClass;

/**
 * Аргументы для виджета дерева.
 * @var
 */
$pagesIndexUrl = Yii::app()->getService('pages')->createUrl('index', false);
$updateActionUri = $this->createUrl('update', false);

$bindTable = StructBind::model()->tableName();

$treeArguments = array(
	'model' => $modelClass::model(),
	'leftJoin' => array($bindTable, "{$bindTable}.category_id=t1.id"),
	'fields' => array
	(
        'text'=>'title',
        'alt'=> new CDbExpression("ROUND(COUNT({$bindTable}.id)/t1._level)"),
        'id_parent'=>'parent_id',
        'task'=>'type ',
        'icon'=>'id',
        'tooltip'=>false,
        'url'=>false,
        'options'=>'published'
    ),
    'repaintOnChanges' => true,
    'template' => '<span class="wrap" type="{task}"><span class="title" published="{options}" type="{task}">{text}</span><a rel="tooltip" data-title="'.Yii::t('struct', 'Category pages').'" href="'."{$pagesIndexUrl}parent/{icon}".'" class="viewPages" data-count="{alt}"> <i class="icon-list-alt"></i>{alt} </a><a rel="tooltip" data-title="'.Yii::t('struct', 'Edit').'" href="'.$updateActionUri.'{icon}" class="edit icon-edit"></a></span>',
);


// если происходила перерисовка (требуется вывод только таблицы)
if(Yii::app()->request->isAjaxRequest && isset($_POST['MTreeViewRepaint']))
    $this->widget(
        Yii::localExtension('mtreeview', 'MTreeViewSortable', __FILE__),
        $treeArguments
    );

else
{
    ?>
    <div class="page-header">
    <h1><?= Yii::t('struct', 'Site categories list'); ?></h1><br/>

    <?
    // кнопка "в индекс админки"
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
        'size'=>'small',
        'buttonType'=>'link',
        'icon'=>'icon-arrow-left',
        'label'=>Yii::t('admin', 'Administration index'),
        'url'=>'/admin/',
    ));
    ?>
    </div>


    <div class="row">
		<div class="span4" id="leftColumn">
			<div id="sidebar" data-fixed-top="66">
				<div>
					<div id="manageBar" class="well clientContent">
						<? $this->renderPartial('category/_manageBar', array('selectedItems'=>array())) ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="hint span8">
			<p>Здесь производится редактирование структуры сайта: добавление/удаление новых разделов, перемещение подразделов и т.п.</p>
			<p class="alert alert-warning"><strong>Внимание:</strong> прежде чем менять структуру сайта, проконсультируйтесь с разработчиками. Это может нарушить логику Вашего сайта.</p>
			
			<?
			$this->widget(
				Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__)
			);
			?>
		</div>

        <div class="span8 treeField"><?php
		
		// область кэшируется перед выводом
		$cacheId = 'categoriesAdminIndex';
		$cacheOptions = array(
			'duration' => Yii::app()->struct->cacheExpire,
			'dependency' => Yii::app()->struct->getCategoriesCacheDepency(),
		);
		if($this->beginCache($cacheId, $cacheOptions))
		{

			if(!$modelClass::model()->count()){
				?><div class="alert alert-info"><strong><?= Yii::t('struct', 'No categories found.'); ?></strong> <?
				$createLink = CHtml::link('create it', $this->createUrl('create'));
				echo Yii::t('struct', 'But you can {create it}.',
						array('{create it}'=>$createLink)
				);
				?></div><?
			}
			else

			$this->widget(
				Yii::localExtension('mtreeview', 'MTreeViewSortable', __FILE__),
				$treeArguments
			);

		$this->endCache(); }

        ?></div>
    </div><?
}

