<div class="page-header">
<h1><?= Yii::t('struct', 'Site category create'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('struct', 'Site categories management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>
<?php

// Сообщение о том, что неизвестно какие настройки требуется использовать в разделе.
if(!$this->model->parent_id)
{
?>
<div class="alert alert-info">
	<?=
	Yii::t('struct', 'Until you save this category, you can use only base fields, '
			.'because system don`t know what a parent of it.');
	?>
</div>
<?
}
// Куда будет добавлен раздел.
else if($parent)
{
?>
<div class="alert alert-info">
	<?=
	Yii::t('struct', 'This category wil be added to "{category}" category.',
		array("{category}" => $parent->title) );
	?>
</div>
<?
}

$this->renderPartial('category/_form', compact('form'));

?>
