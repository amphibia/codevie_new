<?

if(!Yii::app()->request->isAjaxRequest)
    echo CHtml::tag('div', array('id'=>'categoryContent'), $category->full_text);


// область кэшируется перед выводом
$cacheId = 'STRUCT_CATEGORY_' . md5(serialize($_POST) . $_SERVER['REQUEST_URI']);
$cacheOptions = array(
	'duration' => Yii::app()->struct->cacheExpire,
	'dependency' => Yii::app()->struct->getCategoriesCacheDepency(),
);
if($this->beginCache($cacheId)){

	if($this->model->count)
		$this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), array(
			'model' => $this->model,
			'buttons' => array(),
			'headerButtons' => false,
			'useHeader' => false,
			'fieldsOrder' => array('title', 'short_text'),
			'route' => '/'.$category->alias,
			'useGroupOperations' => false,

			'preConvertors' => array
			(
				'title' => function($fieldName, $fieldValue, $row, $isHeader = FALSE, $model) use($category){
					if($isHeader) return $fieldValue;
					return CHtml::link($fieldValue, $category->createUrl()."/{$row['alias']}");
				}
			),
		));

$this->endCache(); }

?>