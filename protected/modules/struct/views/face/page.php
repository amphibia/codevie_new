<div class="row"><div class="columnWidthFull"><h1><?= $page->title; ?></h1></div></div>

<div class="row">
    <div class="columnWidthFull">
        <div class="pageContent">
            <?
                if(sizeof($gallery = explode(',', $page->gallery))){
                    $imageUrl = Yii::app()->storage->createUrl(
                        current($gallery), 'newsImage');
                    echo CHtml::image($imageUrl, $page->title, array('align'=>'left'));
                }
            ?>
            <p class="newsFullText"><?= $page->full_text; ?></p>
        </div>
    </div>
</div>