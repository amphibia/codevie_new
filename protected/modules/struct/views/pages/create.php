<div class="page-header">
<h1><?= Yii::t('struct', 'Page create'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('struct', 'Page management'),
    'url'=>$this->createUrl('index', $category_id?array('parent'=>$category_id):array()),
));

?>
</div>

<?php

// Сообщение о том, что неизвестно какие настройки требуется использовать в разделе.
if(!$this->model->category)
{
?>
<div class="alert alert-info">
	<?=
	Yii::t('struct', 'Until you save this page, you can use only base fields, '
			.'because system don`t know what a category of this page.');
	?>
</div>
<?
}
// Куда будет добавлен раздел.
else
{
?>
<div class="alert alert-info">
	<?=
	Yii::t('struct', 'This page wil be added to "{category}" category.',
		array("{category}" => $this->model->category->title) );
	?>
</div>
<?
}

// форма
$this->renderPartial($this->views['_form'], compact('form'));

?>