
<div class="categoriesList">
    <h3><?= Yii::t('struct', 'Category filter'); ?></h3>
    <?

    $this->widget(Yii::localExtension('mtmselect', 'MTMSelect'), array(
        'relationClassName' => 'StructCategory',
        'model' => $this->model,
        'value' => $this->_selectedCategories,
        'sortable' => false,
        'route' => 'CategoriesMTMPage',
        'buttonLabel' => Yii::t('struct', 'Select categories'),
        'hintTitle' => Yii::t('struct', 'Category filter'),
        'hintText' => Yii::t('struct', 'You can set category filter by some categories.'),
        'noItemsMessage' => Yii::t('mtmselect', '<strong>Categories!</strong><br/>Please add one or more categories of site content.')
    ));

    ?>
</div>