<div class="page-header">
<h1><?= Yii::t('struct', 'Page create'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('struct', 'Tags management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>

<?php

// форма
$this->renderPartial($this->views['_form'], compact('form'));

?>