<?php

/**
 * @author koshevy
 *
 * Вывод вложенного меню категорий и страниц, с учетом текущей категории.
 * Нерекурсивное, ипользуется Nested-Sets.
 */
class StructMenu extends CWidget
{
    /**
     * Идентификатор меню.
     * @var string
     */
    public $id = NULL;

    /**
     * Требуется ли отображать главную категорию
     * @var boolean
     */
    public $showRoot = NULL;

    /**
     * Максимальная глубина запроса (если 0 или NULL, не указывается)
     * @var int
     */
    public $depth = NULL;

    /**
     * Родительская категория, потомки которой выводятся
     * Может
     * @var mixed
     */
    public $parentCategory = NULL;

    /**
     * Требуется ли выводить материалы разделов в этом же меню.
     * @var boolean
     */
    public $showPages = NULL;


    public $htmlAttributes = array('class' => 'structMenu');


    public $views = array('categories' => 'menu/categories');


    protected $_startLevel = NULL;


    public function init()
    {
        return false;
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);
    }

    public function run()
    {
        if (Yii::app()->struct->currentCategory)
            $curCategoryID = Yii::app()->struct->currentCategory->id;
        else $curCategoryID = NULL;

        // область кэшируется перед выводом
        /*
        $cacheId = md5(serialize($_POST) . $_SERVER['REQUEST_URI']
            . "__structMenu_{$this->showPages}_{$this->parentCategory}_")
            . $this->parentCategory . $curCategoryID;
        $cacheOptions = array(
            'duration' => Yii::app()->struct->cacheExpire,
            'dependency' => Yii::app()->struct->getCategoriesCacheDepency(),
        );
         *
         */

        //if($this->beginCache($cacheId)){

        if (!$this->parentCategory || !($this->parentCategory instanceof StructCategory)) {
            $structModel = StructCategory::model();
            $_level = $structModel->levelAttribute;

            if (!$this->parentCategory)
                $this->parentCategory = $structModel->find(
                    "({$_level} = 2) AND (alias = '" . Yii::app()->struct->mainCategoryAlias . "')");
            else $this->parentCategory = $structModel->findByPk((int)$this->parentCategory);

            if (!$this->parentCategory)
                throw new CException(Yii::t('struct', 'Can`t find parent category'));
        }

        $this->_startLevel = $this->parentCategory->{$this->parentCategory->leftAttribute};
        if ($this->id) $this->htmlAttributes['id'] = $this->id;

        // выодить ли страницы вместе с категориями
        if ($this->showPages)
            $this->_categoriesAndPages();
        else $this->_categories();

        //$this->endCache(); }
    }

    /**
     * Вывод только категорий.
     */
    protected function _categories()
    {
        $_level = $this->parentCategory->levelAttribute;
        $_left = $this->parentCategory->leftAttribute;
        $_right = $this->parentCategory->rightAttribute;

        $categories = new StructCategory;
        $categories->dbCriteria->addCondition(
            "(t.$_left > {$this->parentCategory->$_left}) AND " .
            "(t.$_right < {$this->parentCategory->$_right}) AND " .
            "t.published = 'published' AND " .
            "t.type != 'group'"
        );

        if ($this->depth) $categories->dbCriteria->addCondition(
            "t.$_level <= " . ($this->parentCategory->$_level + $this->depth));

        $categories->dbCriteria->order = 't.' . $categories->leftAttribute . " ASC";
        $this->render($this->views['categories'], compact('categories'));
    }

    /**
     * Вывод категорий и страниц (используется другой принцип, с использованием
     * JOIN-запросов.
     *
     * ПРИМЕЧАНИЕ: не реализовано.
     */
    protected function _categoriesAndPages()
    {
        /** @TODO: метод не реализован */
    }
}
