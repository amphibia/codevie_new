<?php

/**
 * Компонент изменяет порядок интерпретации URL - вначале
 *
 * @title Dynamic site structure component
 * @description { This component provides user-controlled site structure above programming-based structured. }
 * @preload yes
 * @vendor koshevy
 * @package struct
 */
class StructComponent extends CApplicationComponent
{
    // состояния опубликованности категории (для метода getCategory)
    const PUBLISHED = 2;
    const DRAFT = 1;
    const HIDDEN = 0;


    /**
     * ID последнего контроллера, встреченного в цепочке категорий;
     * этот контроллер перехватывает выполнение запроса.
     *
     * Если в цепочке не встречен ни одни контроллер, применяется контроллер
     * по-умолчанию — StructController.
     *
     * @var string
     */
    public $currentControllerID = NULL;

    /**
     * Текущий шаблон.
     *
     * @var string
     */
    public $currentTemplate = NULL;

    /**
     * Псевдоним главного раздела.
     * @var string
     */
    public $mainCategoryAlias = NULL;

    /**
     * Правила интерпретации URL, вставляемые модулем как наиболее приорететные.
     * @var array
     */
    public $rules = array();

    /**
     * ID контроллера-обработчика по умолчанию для модуля Struct.
     * @return string
     */
    public $structControllerID = NULL;

    /**
     * Текущая категория.
     * @var StructCategory
     */
    public $currentCategory = NULL;

    /**
     * Текущая открытая страница
     * @var StructPage
     */
    public $currentPage = NULL;

    /**
     * Срок жизни кэша страниц в секундах (используется, если в таблице не производилось
     * никаких изменений, если данные в таблице категорий менялись, кэш все равно
     * сбрасывается).
     * @var integer
     */
    public $cacheExpire = NULL;

    /**
     * Контроллер, обрабатывающий запросы модуля.
     * Используется для доступа к настрйкам контроллера из вне.
     * @var StructController
     */
    public $structController = NULL;


    /**
     * Путьи к файлам шаблонов.
     * @var array
     */
    public $templatesRules = array();


    /**
     * Алиас главной страницы по умолчанию.
     * @var array
     */
    public $mainPageAlias = NULL;


    /**
     * Ссылка на уровень текущей категории в дереве $_categoryHierarchy.
     * По элементу 'parent' в таблице можно обратиться к родительским уровням
     * категории.
     * @var array
     */
    protected $_categoryLevel = NULL;

    /**
     * Ссылка на уровень текущей категории в дереве $_categoryHierarchy.
     * По элементу 'parent' в таблице можно обратиться к родительским уровням
     * категории.
     * @return array
     */
    public function getCategoryLevel()
    {
        return $this->_categoryLevel;
    }


    /**
     * Аргументы модуля/контроллера, на который ссылается категория.
     * @var array
     */
    protected $_arguments = NULL;

    /**
     * Аргументы обработанного запроса (указывется методом StructComponent::getCategory).
     * @return array
     */
    public function getArguments()
    {
        return $this->_arguments;
    }

    /**
     * Иерархия выбранной категории (указывется методом StructComponent::getCategory).
     * @var array
     */
    protected $_categoryHierarchy = array();

    /**
     * Иерархия выбранной категории (указывется методом StructComponent::getCategory).
     * @return array
     */
    public function getCategoryHierarchy()
    {
        return $this->_categoryHierarchy;
    }


    /**
     * Массив со ссылками на использованные категории (для доступа напрямую).
     * @var array
     */
    protected $_categoriesDirectly = array();

    /**
     * Дерево категорий, к которым происходило обращение.
     * Структура массива: array($category, 'items'=>array(), 'parent'=>&$parent)
     *  $category - объект категории (или NULL - если нулевой уровень)
     *  'items' - массив с подуровнями такой же структуры
     *  'parent' - ссылка на уровень выше (или NULL)
     *
     * @var array
     */
    protected $_categoriesCache = array(NULL, 'items' => array(), 'parent' => NULL);


    /**
     * Очистка кэша внутреннего кэша категорий.
     */
    public function flushCache()
    {
        $this->_categoriesDirectly = array();
        $this->_categoriesCache = array(NULL, 'items' => array(), 'parent' => NULL);
        Yii::app()->cache->delete(get_class($this) . "__categoriesCache");
        Yii::app()->cache->delete(get_class($this) . "__categoriesDirectCache");
    }

    /**
     * Список лэйаутов приложения (только уровень приложения, без учёта модулей и
     * расширений).
     * @return array
     */
    public function getApllicationLayouts()
    {
        $path = Yii::getPathOfAlias('application.views.layouts') . DIRECTORY_SEPARATOR . '*.php';

        $returnItems = array();
        if (is_array($items = glob($path))) {
            foreach ($items as $key => $item) {
                $name = str_replace('.php', '', basename($item));
                $returnItems[$name] = $name;
            }
        }

        return $returnItems;
    }

    /**
     * Формирование DB-зависимости при работе с категориями.
     */
    public function getCategoriesCacheDepency()
    {
        static $depency = NULL;
        if (!$depency) {
            $categoryTable = StructCategory::singleTone()->tableName();
            $pageTable = StructPage::singleTone()->tableName();

            $depency = new CChainedCacheDependency();
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_created`) FROM {$categoryTable}"));
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_changed`) FROM {$categoryTable}"));
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_created`) FROM {$pageTable}"));
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_changed`) FROM {$pageTable}"));
        }

        return $depency;
    }

    /**
     * Формирование DB-зависимости при работе .
     */
    public function getCategoriesOptionsCacheDepency()
    {
        static $depency = NULL;
        if (!$depency) {
            $categoryOptionsTable = StructCategoryOptions::singleTone()->tableName();

            $depency = new CChainedCacheDependency();
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_created`) FROM `{$categoryOptionsTable}`"));
            $depency->dependencies->add(new CDbCacheDependency("SELECT MAX(`date_changed`) FROM `{$categoryOptionsTable}`"));
        }

        return $depency;
    }

    public function init()
    {
        Yii::import('application.modules.struct.*');
        Yii::import('application.modules.struct.models.*');
        Yii::import('application.modules.struct.services.*');
        Yii::import(Yii::localExtension('imagesuploader', '*'));
        Yii::loadExtConfig($this, __FILE__);

        Yii::app()->registerService('categories', 'StructCategoriesAdminService', 'admin',
            array(
                'title' => 'Site structure management',
                'menuGroup' => 'site structure'
            )
        );

        Yii::app()->registerService('pages', 'StructPagesAdminService', 'admin',
            array(
                'title' => 'Pages mangement',
                'menuGroup' => 'site structure'
            )
        );

        Yii::app()->registerService('tags', 'StructTagsAdminService', 'admin',
            array(
                'title' => 'Tags manegement',
                'menuGroup' => 'site structure'
            )
        );

        /** @todo: Доделать сервис
        /*Yii::app()->registerService('templates', 'StructTemplatesListAdminService', 'admin',
        array(
        'title' => 'Site templates management',
        'menuGroup' => 'site structure'
        )
        );*/

        /** @todo: Здесь нужно инициализировать кэш категорий сайта; указать
         * 'depency' - последний момент изменения категории (если нет поля date_modified -
         * сделать). Лучше всего, сделать хранение в memcache.
         */

        // Перехват всех запросов (отправляется в самый конец - непосредственно
        // перед интерпретацией, чтобы правило других компонентов не перебили
        // правило этого компонента).
        // Для остальных компонентов вынесение добавления правила в событие
        // не требуется.
        $structComponent = $this;
        Yii::app()->onBeginRequest = function ($evt) use ($structComponent) {
            Yii::app()->controllerMap[$structComponent->structControllerID] = array(
                'class' => 'application.modules.struct.controllers.StructController',
                'structComponent' => $structComponent
            );
            Yii::app()->urlManager->addRules($structComponent->rules, false);
        };


        // восстановление информации из кэша
        //$categoriesCache = Yii::app()->cache->get(get_class($this)."__categoriesCache");
        //$categoriesDirectCache = Yii::app()->cache->get(get_class($this)."__categoriesDirectCache");

        //if(($categoriesCache !== false) || ($categoriesDirectCache !== false)){
        // во избежание ошибок, все бехавиоры, используемые в StructCategory и
        // StructPage также должны быть импортированы
        //	Yii::import(Yii::localExtension('advancedarbehavior', '*'));
        //	Yii::import(Yii::localExtension('timestamparbehavior', '*'));
        //	Yii::import('zii.behaviors.*');
        //}
        //if($categoriesCache !== false) $this->_categoriesCache = unserialize($categoriesCache);
        //if($categoriesDirectCache !== false) $this->_categoriesDirectly = unserialize($categoriesDirectCache);

    }

    /**
     * При завершении работы, компонент кэширует использованные данные.
     */
    public function __destruct()
    {
        //$depency = $this->getCategoriesCacheDepency();

        //Yii::app()->cache->set(
        //	get_class($this)."__categoriesCache",
        //	serialize($this->_categoriesCache),
        //	$this->cacheExpire, $depency
        //);

        //Yii::app()->cache->set(
        //	get_class($this)."__categoriesDirectCache",
        //	serialize($this->_categoriesDirectly),
        //	$this->cacheExpire, $depency
        //);
    }


    /**
     * Обращение к категории лбого уровня (обращение по иерархическому запросу).
     * Сохраняет результаты обращения в r'it
     *
     * @param mixed $categoryPath путь до категории - может быть строкой
     * alias`ов категорий в порядке иерархии, разделённых '/', либо уже массивом
     * разделённых alias`ов в соответствующем порядке.
     *
     * @param string $published минимально допустимый статус категории
     * (self::PUBLISHED/self::DRAFT/self::HIDDEN) - в порядке убывания.
     *
     * @param bool $markAsCurrent
     * @return bool|null
     *
     * @return array массив с выкладкой по категории
     */
    public function getCategory($categoryPath, $published = self::PUBLISHED, $markAsCurrent = false)
    {
        /**
         * @todo нужно хранить $this->_categoriesCache в кэше для уменьшения
         * нагрузки на БД. Желательно в MEMCACHE.
         */

        switch ($published) {
            case self::HIDDEN:  $publishedRange[] = 'hidden';
            case self::DRAFT:   $publishedRange[] = 'draft';
            default:            $publishedRange[] = 'published';
        }

        if( !count($aliases = explode('/', trim($categoryPath, '/'))) ||
            (!$aliases[0]) ){
            return NULL;
        }

        // Поиск первого предка - главной категории.
        $mainCategory = new StructCategory;
        $mainCategory = $mainCategory->find(
            "(tech_id = :tech_id)",
            array(':tech_id' => 'mainPage')
        );

        // нет главной - и искать негде
        if(!$mainCategory) return NULL;

        // поиск категорий с такими ALIAS
        $categories = new StructCategory;
        $categories->dbCriteria->addInCondition('alias', $aliases);
        $categories->dbCriteria->addInCondition('published', $publishedRange);
        $categories->dbCriteria->addCondition("type <> 'group'");
        $categories = $categories->findAll();

        // индексация категорий
        $virtualTree = array();
        foreach($categories as $index => $category)
        {
            $virtualTree[$category->id] = array(
                'item' => &$categories[$index],
                'children' => array()
            );

            if(!isset($virtualTree[$category->alias]))
                $virtualTree[$category->alias] = array();
            $virtualTree[$category->alias][] = &$virtualTree[$category->id];
        }

        $lastTopPoint = NULL;
        $startCategoryID = NULL;
        $firstAlias = array_shift($aliases);

        // построение виртуального дерева категорий
        foreach($categories as $index => $category)
        {
            if(isset($virtualTree[$category->parent_id])){
                $virtualTree[$category->parent_id]['children'][$category->alias] =
                $virtualTree[$category->parent_id]['children'][$category->parent_id] = &$categories[$index];
            }

            if( ($category->alias == $firstAlias) &&
                (!$lastTopPoint || ($lastTopPoint > $category->_level)) )
            {
                $lastTopPoint = $category->_level;
                $startCategoryID = $category->id;
            }
        }

        // сразу выявилось несоответствие
        if(!$startCategory = &$virtualTree[$startCategoryID]['item'])
            return NULL;

        // проверка соответствия стартовой точки
        if( ($startCategory->parent_id != $mainCategory->id) &&
            !$this->_isTunnel($mainCategory, $startCategory, $publishedRange) )
            return false;

        // установка текущего контроллера (в дочерних категориях может
        // быть перезаписан)
        if($startCategory->type == 'controller')
            $this->currentControllerID = $startCategory->handler;

        // установка шаблона
        $this->currentTemplate = $startCategory->template
            ? $startCategory->template
            : 'default';


        $this->_categoryHierarchy = array($startCategory);
        $currentID = $startCategoryID;
        $parentID = NULL;
        while(true)
        {
            $currentCategory = &$virtualTree[$currentID]['item'];
            $nextAlias = array_shift($aliases);
            $this->_categoryHierarchy[] = $currentCategory;

            // прямое соответствие (в текущей категории найден соответствующий потомок)
            if(isset($virtualTree[$currentID]['children'][$nextAlias])){
                $currentCategory = &$virtualTree[$currentID]['children'][$nextAlias];
                $currentID = $currentCategory->id;
            }

            // поиск соответствия в сквозных группах
            else
            {
                // пометка, что категория оказалась тупиковой,
                // потому что связей через сквозные группы не нашлось
                $stop = true;
                if(isset($virtualTree[$nextAlias])){
                    foreach($virtualTree[$nextAlias] as $child){
                        if($this->_isTunnel($currentCategory, $child['item'], $publishedRange)){
                            $currentCategory = $child['item'];
                            $currentID = $child['item']->id;
                            $stop = false;
                            break;
                        }
                    }
                }

                if($stop)
                    array_unshift($aliases, $nextAlias);

                // Успешный поиск завершен: Остальные аргументы считаются параметрами.
                break;
            }

            // Успешный поиск завершен: категория не имеет параметров.
            if(!sizeof($aliases)) break;

            // перезапись текущего контроллера
            if($currentCategory->type == 'controller')
                $this->currentControllerID = $currentCategory->handler;

            // перезапись шаблона
            if($currentCategory->template && ($currentCategory->template != 'default'))
                $this->currentTemplate = $currentCategory->template;
        }

        if($currentCategory->across && sizeof($currentCategory->children))
            $currentCategory = $currentCategory->children[0];

        // перезапись шаблона для финальной категории
        if($currentCategory->template && ($currentCategory->template != 'default'))
            $this->currentTemplate = $currentCategory->template;

        // перезапись контроллера для финальной категории
        if($currentCategory->type == 'controller')
            $this->currentControllerID = $currentCategory->handler;

        $this->_arguments = $aliases;
        $this->_categoryLevel = &$virtualTree[$currentID];
        if($markAsCurrent)
            $this->currentCategory = $currentCategory;

        // контроллер по-умолчанию
        if(!$this->currentControllerID)
            $this->currentControllerID = $this->structControllerID;

        // определение текущего материала
        if($currentCategory && sizeof($this->_arguments))
        {
            $pageAlias = array_shift($this->_arguments);
            $page = new StructPage;
            $page = $page->published()->categories(array($currentCategory->id));
            $page->dbCriteria->mergeWith(array(
                'condition' => 't.alias = :alias',
                'params' => array(':alias' => $pageAlias)
            ));

            $page = $page->find();

            if($page)
                 $this->currentPage = $page;                    // установка текущего матриала
            else array_unshift($this->_arguments, $pageAlias);  // возврат параметра на место
        }

        return $currentCategory;
    }

    /**
     * Есть ли прямое соединение между категориями
     * через сквозную группу.
     *
     * @param $topCategory
     * @param $bottomCategory
     * @param $publishedRange
     * @return bool
     */
    protected function _isTunnel($topCategory, $bottomCategory, $publishedRange)
    {
        // правильность соотношения по уровням
        if($topCategory->_level >= $bottomCategory->_level)
            return false;

        // нахождение в одной ветке
        if( ($topCategory->_left < $bottomCategory->_left) &&
            ($topCategory->_right > $bottomCategory->_right) )
        {
            $intermediate = new StructCategory;
            $intermediate->dbCriteria->mergeWith(array(
                'condition' => implode(' AND ', array(
                    '(t._left > :topLeft)',
                    '(t._left < :bottomLeft)',
                    '(t._right < :topRight)',
                    '(t._right > :bottomRight)',
                )),
                'params' => array(
                    ':topLeft' => $topCategory->_left,
                    ':bottomLeft' => $bottomCategory->_left,
                    ':topRight' => $topCategory->_right,
                    ':bottomRight' => $bottomCategory->_right
                )
            ));

            $intermediate = $intermediate->findAll();

            if($intermediate && count($intermediate)){
                foreach($intermediate as $category){
                    if( ($category->type != 'group') ||
                        (!in_array($category->published, $publishedRange)) ){

                        return false;
                    }
                }

                return true;
            }

            else return false;
        }
        else return false;
    }

    /**
     * Обращение к категории по ID. Кэширует категории.
     * @param int $id
     * @return CActiveRecord|null|StructCategory
     */
    public function getCategoriesById($id)
    {
        // из кэша
        if (isset($this->_categoriesDirectly[$id]))
            return $this->_categoriesDirectly[$id];

        $category = new StructCategory;
        if (!$category = $category->findByPk($id))
            return NULL;

        // при первом обращении к категории, вся её иерархия сохраняется в кэше
        $aliases = $category->getAncestorsAliases();
        $this->getCategory($aliases, self::HIDDEN); // прогоняется для создания дерева

        return $category;
    }

    /**
     * Получение пути до файла шаблона.
     *
     * @param $type тип шаблона (page, category, assets, previews)
     * @param $name имя шаблона
     * @return string
     */
    public function getTemplatePath($type, $name)
    {
        $rule = Yii::app()->struct->templatesRules[$type];

        $result = preg_replace('/[\/\\\\]/', '', $rule);
        $result = preg_replace('/\./', DIRECTORY_SEPARATOR, $result);
        $result = preg_replace('/\\{name\\}/', $name, $result);
        $result = preg_replace('/\\{viewsPath\\}/', rtrim(Yii::getPathOfAlias('application.views'), '/'), $result);

        if (($type == 'category') || ($type == 'page'))
            $result .= ".php";

        return $result;
    }

    /**
     * Обращение к разделу главной страницы.
     */
    public function getMainPageCategory()
    {
        static $mainPage = NULL;
        if (!$mainPage) {
            $mainPage = $this->getCategories($this->mainPageAlias);
        }
        return $mainPage;
    }

    /**
     * Обращение к необходимым разделам.
     *
     * @param array|string $categories Массив ключей категорий, либо 1 значение
     * @return StructCategory|array из StructCategory
     */
    public function getCategories($categories)
    {
        static $_cachedCategories = array();
        $return = null;
        if (is_array($categories)) {
            foreach ($categories as $category) {
                if (!isset($_cachedCategories[$category]) || !$_cachedCategories[$category]) {
                    $_cachedCategories[$category] = StructCategory::model()
                        ->findByAttributes(array('tech_id' => $category));
                }

                $return[$category] = $_cachedCategories[$category];
            }
        } else {
            if (!isset($_cachedCategories[$categories]) || !$_cachedCategories[$categories]) {
                $_cachedCategories[$categories] = StructCategory::model()
                    ->findByAttributes(array('tech_id' => $categories));
            }
            $return = $_cachedCategories[$categories];
        }

        return $return;
    }

}
