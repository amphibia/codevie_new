<?
class Component extends ActiveRecord
{

	/**
	 * Внесение данных по компоненту (уже существуещему или новому - определяется
	 * по совпадениям).
	 * @param array $attributes
	 * @return Component
	 */
	public static function updateComponent($attributes)
	{
		// обращение к объекту компонента
		$path = isset($attributes["path"]) ? $attributes["path"] : NULL;
		if($path) $component = Component::model()->findByAttributes(array("path"=>$attributes["path"])); else $component = NULL;
		if(!$component) $component = Component::model()->findByAttributes(
			array("class_name"=>$attributes["class_name"], "alias"=>$attributes["alias"]))
			or $component = new Component;
		if(isset($attributes["requirements"])) if(!is_null($attributes["requirements"]))
			$component->requirements = $attributes["requirements"];

		foreach($component->attributeNames() as $attributeName) if(isset($attributes[$attributeName])) 
			$component->$attributeName = $attributes[$attributeName];
		$component->package = isset($attributes["package"]) ? $attributes["package"] : NULL;

		$component->save();
		$component->_prepareRequirements();
		$component->refresh();

		return $component;
	}


	/**
	 * Включение/выключение компонента.
	 * Выполняет проверки и включение/выключение связанных компонент.
	 * @param boolean value
	 * @return array {result:result, enabledRequirements[... массив включенных компонентов]}
	 */
	public function enable($value = true)
	{
		$result = true;
		$enabledRequirements = array(); // список задетых компонентов

		// попытка включить требуемые комаоненты перед включением
		if($value)
		foreach($this->requirementStatus as $alias => $requirement)
		{

		
			if($requirement["component"]->active) continue;

			// нельзя включить компонент - он не существует
			if($requirement["requirement_error"] == self::requirement_ERROR_NOT_EXIST){
				$this->addError("active", Yii::t("admin", 'Nedded related component "{requirement}" not exists!',
					array("{requirement}"=>$alias)));

				$result = false;

			}else

			if($requirement["requirement_error"] == self::requirement_ERROR_DISABLED){
				if(!$requirement["component"]->enable()){
					$this->addError("active", Yii::t("admin", 'Can`t enable related component "{requirement}" automaticaly.',
						array("{requirement}"=>$alias)));

					$result = false;
				}
				else $enabledRequirements[] = &$requirement["component"];
			}
		}

		if($result){
			$this->active = $value;
			$result = $this->save();
		}
        
		return compact('result', 'enabledRequirements');
	}


	const requirement_ERROR_DISABLED = 1;
	const requirement_ERROR_NOT_EXIST = 2;

	/**
	 * Статус по внешним зависимостям: включены ли компоненты, от
	 * которых зависим данный и есть ли они вообще.
	 * @return array массив с переислениями неудовлетворенных
	 * зависимостей вида: {rel1:array(component: Component, requirement_error:"<requirement_ERROR_DISABLED|requirement_ERROR_NOT_EXIST>")}
	 */
	public function getRequirementStatus()
	{
		$result = array();
		foreach($this->requirements as $component)
		{
			if(!is_object($component)) continue;

			if(!$component->exists)
				$result[$component->alias] = array(
					"component" => &$component,
					"requirement_error" => self::requirement_ERROR_NOT_EXIST,
				);else

			if(!$component->active)
				$result[$component->alias] = array(
					"component" => &$component,
					"requirement_error" => self::requirement_ERROR_DISABLED,
				);
		}
		
		return $result;
	}


	/**
	 * Подготовка внешних связей.
	 * Принимает аргументы в строковом виде - {type, alias}, т.к. на момент 
	 * записи идентификатор компонента зависимости не известен.
	 * @param array $requirements
	 * @return void
	 */
	protected function _prepareRequirements()
	{
		$requirementsNew = array();
        if(!is_array($this->requirements)) $this->requirements = array();
		foreach($this->requirements as $index => $requirementArguments)
		{
			if(is_array($requirementArguments) && sizeof($requirementArguments))
			{
				// поиск требуемого компонента: сначала среди активных
				// компонентов, потом (если не найдено) среди существующих,
				// в противном случае создаётся пустая болванка компонента с exists=false
				if(!$requirement = Component::findByAttributes(array_merge($requirementArguments, array("exists"=>true))));
					$requirement = Component::findByAttributes($requirementArguments);
				if(!$requirement){
					$class_name = ucfirst($requirementArguments["alias"]).ucfirst($requirementArguments["type"]);
					$requirement = self::updateComponent(array_merge($requirementArguments, array("exists"=>false, "class_name"=>$class_name)));
				}

				if($requirement)
					$requirementsNew[] = $requirement->id;
			}
		}

		$this->requirements = $requirementsNew;
		$this->writeManyManyTables();
	}

	public function beforeValidate()
	{
		if(!$this->alias) $this->alias = strtolower(preg_replace("/(Component|Module)/", "", $this->class_name));

		if($this->preload === "yes") $this->preload = true;
		if($this->preload === "no") $this->preload = false;


		// прежде чем включить компонент, происходит ряд проверок
		if($this->active)
		{
			// существует ли этот компонент по факту
			if(!$this->exists && !$this->default) $this->addError('active',
				Yii::t('admin', 'Component {alias} not found.', array('{alias}'=>$this->alias)));

			// конфликт одноимённых компонент
			if(Component::model()->exists(
				"active>0 AND (class_name=:class_name OR alias=:alias) AND type=:type AND id<>:id",
				array(":class_name"=>$this->class_name, ":alias"=>$this->alias, ":id"=>$this->id, ":type"=>$this->type))){
				$this->active = false;
				$this->addError('active', Yii::t('admin', 'Cannot enable: ambigious alias or classname.'));
			}

		}
		else
		{
			// выключить компонент нельзя, если включён хоть один
			// от него зависящий
			foreach($this->depencies as $depency){
				if($depency->active) $this->addError('depencies', Yii::t('admin', 'Cannot disable: depency - {type} {alias} must be disabled before.',
					array('{type}'=>$depency->type, '{alias}'=>$depency->alias)));
			}
		}


		return parent::beforeValidate();
	}



	public function defaultScope(){
		return array(
			'order'=>'package, title',);
	}


	public function tableName(){
    	return '{{app_components}}'; }

	public function relations()
	{
		return array(
			'requirements' => array(self::MANY_MANY, 'Component', 'app_relations(child_id, parent_id)'),
			'depencies' => array(self::MANY_MANY, 'Component', 'app_relations(parent_id, child_id)'),
		);
	}

	public function rules()
	{
		return array(
			array('type, class_name', 'required'),
			array('system, active, exists', 'boolean'),
			array('class_name, alias', 'match', 'pattern' => '/^\w+$/'),
			array('type', 'in', 'range'=>array('module', 'component')),
			array('path', 'match', 'pattern' => '/^(?>\w+?\.?)+$/'),
		);
	}

	protected function afterFind() {
		$result = parent::afterFind();
		if($this->arguments && !is_array($this->arguments))
			$this->arguments = unserialize($this->arguments);
		
		return $result;
	}

	protected function beforeSave() {
		$result = parent::beforeSave();
        $this->_prepareRequirements();
		
		if(is_array($this->arguments))
			$this->arguments = serialize($this->arguments);

		return $result;
	}

	public function save($runValidation = true, $attributes = null) {
		$result = parent::save($runValidation, $attributes);

		if($this->arguments && !is_array($this->arguments))
			$this->arguments = unserialize($this->arguments);
		
		return $result;
	}
}

