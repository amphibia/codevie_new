<?

return array(

	/**
	 * @param array список конфигурационных файлов,
	 * применяемых при сопоставлении данных
	 *
	 * ПРИМЕЧАНИЕ: preload должен располагаться в самом конце,
	 * т.к. к моменту расположения идентификаторы компонентов уже
	 * должны быть известны
	 */
 	'configFiles' => array(
		"component" => "components.php",
		"module" => "modules.php",
		"preload" => "preload.php",
	),

	'blankComponent' => array(
		"active" => false,
		"alias" => NULL,
		"arguments" => array(),
		"preload" => false,
		"package" => NULL,
		//"path" => NULL,
		"system" => false,
		"exists" => true,
		"error" => NULL,
		"default" => NULL,
		"title"=>NULL,
	),

	/**
	 * @param array Записи в конфигах по умолчанию. Применяется в случае
	 * отсутствия заполненных аргументов.
	 * Данные для модулей и компонентов хранятся отдельно. Можно указывать
	 * готовый текст  или данные как есть (переменные, массивы и т.п.).
	 */
	'default' => array
	(
		'component' => array
		(
			'output' => "array('class' => 'ext.extcore.ExtCoreOutput')",
			'db' => "require 'database.php'",
			'cache' => "array('class' => 'system.caching.CDummyCache')",
			'errorHandler' => "array('errorAction' => 'site/error')",
			'log' => array(
				'class'  =>'CLogRouter',
				'routes' => array(
					array('class' => 'CProfileLogRoute', 'levels' => 'profile',),
				),
    		),

            'authManager'=>array
            (
                'class'         => 'CDbAuthManager',
                'connectionID'  => 'db',

                'defaultRoles'  => array('guest'),

                'assignmentTable'   => 'auth_assignment',
                'itemChildTable'    => 'auth_item_child',
                'itemTable'         => 'auth_item',
            ),

		),
		'module' => array(),
	),

	/**
	 * @param array ошибки, возникшие при работе с конфигурацией.
	 */
	'errors' => array(),

	/**
	 * @param array массив ссылок на элементы списка $_components,
	 * осуществляемых по алиасу компонента (разделён по типу компонент)
	 */
	'aliases' => array(
		"component" => array(),
		"module" => array()
	),


);