<?php

/**
 * контроллер административной системы
 */
class DefaultController extends Controller
{
    public $assets = array(
        'css' => array('shell.css', 'jquery.jgrowl.css'),
        'js' => array(
			'shell.js',
			'jquery.autosize.js',
			'jquery.jgrowl.js',
			'masonry.pkgd.min.js',
		),
    );

    public function getRbacOperation(){
		return "Administration system";
	}

    /**
     * Индекс административной системы.
     * @return void
     */
	public function actionIndex(){
        Yii::applyAssets($this, __FILE__);
        $this->render('/index');
	}
    

}