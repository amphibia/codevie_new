<?php

/**
* Контроллер - посредник при обращении к сервису.
*/
class ServicesController extends ExtCoreServicesController
{
    public $assets = array(
        'css' => array('shell.css', 'jquery.jgrowl.css', 'ActiveTable.css'),
        'js' => array(
			'shell.js',
			'jquery.autosize.js',
			'jquery.ui.nestedSortable.js',
			'jquery.jgrowl.js'
		),
    );
    
	public function getRbacOperation(){
		return "Administration system"; }
        
    public function serviceInint(){
        Yii::applyAssets($this, __FILE__);
    }

}