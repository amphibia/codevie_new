/**
 * Поведение оболочки административной системы.
 * @author koshevy
 */
$(document).ready(function(){

    $.client._message = function(messageClass, message){
        messageClass = (messageClass=='error')?'icon-exclamation-sign':'icon-info-sign';
        $.jGrowl('<span class="message-icon '+messageClass+' icon-white"></span><div class="message">'+message+'</div>');};

    // замена функции JSON-подтверждения (окно да/нет)
    $('#confirmModal .confirmation-yes').live('click', function(){
        var url = $(this).attr('data-url');
		var operationId = $(this).attr('data-operation-id');
        if(url) $(this).request(url, {json_confirm:true}, null, function(){
			$.client.operationApplied(operationId);});
		});

    $.client.config.serverCommands.confirmation = function(value, requestResult, lastCommands, url){
        $('#confirmModal .modal-body ').html('<p>'+value+'</p>');
        $('#confirmModal .confirmation-yes').attr('data-url', url);
        $('#confirmModal').modal('show');

        // завершение операции
        if(requestResult.json_operation_id)
            $('#confirmModal .confirmation-yes').attr('data-operation-id', requestResult.json_operation_id);
    };

    // блокирование страницы на время выполнения запроса
    $(document).bind('onClientLock', function(){
        var $lockLayer = $('#pageLockLayer');
        $lockLayer.show();
        if(!$(document).data('loadingTimeout')){
            $(document).data('loadingTimeout', setTimeout(function(){
                $lockLayer.hide();
                $lockLayer.addClass('longWhile');
                $(document.body).addClass('longWhile');
                $lockLayer.fadeIn(250);
                $(document).data('loadingTimeout', false);
            }, 2000));
        }
    });

    // разблокирование страницы по окончании
    $(document).bind('onClientUnlock', function(evt){
        var $lockLayer = $('#pageLockLayer');
        if($(document).data('loadingTimeout')){
            clearTimeout($(document).data('loadingTimeout'));
            $(document).data('loadingTimeout', false);
        }
        
        if($lockLayer.hasClass('longWhile')){
            $lockLayer.fadeOut(250);
            $lockLayer.removeClass('longWhile');
            $(document.body).removeClass('longWhile');
        }
        else $lockLayer.hide();
    });

    // подсказки
    $('[rel="popover"]').popover({trigger:'hover'});
    $('[rel="tooltip"]').tooltip({trigger:'hover'});
    $(document).bind('onPartLoad', function(){
        $('[rel="popover"]').popover();
        $('[rel="tooltip"]').tooltip();
    });

    // кнопка "наверх"
    var showScrolTop = function(){
        if(($(this).scrollTop()>($(this).height()/2)) && !$('#toTop').hasClass('visible')){
            $('#toTop').addClass('visible');
            $('#toTop').fadeIn(200);
        }else
        if(($(this).scrollTop()<($(this).height()/2)) && $('#toTop').hasClass('visible')){
            $('#toTop').removeClass('visible');
            $('#toTop').fadeOut(200);
        }
    }
    $('#toTop').click(function(){$("html, body").animate({ scrollTop: 0 }, '500')});
    $(window).scroll(function(){ showScrolTop(); });
    showScrolTop();
    
    $('#pageHints').live('mouseover', function(){
        $('[rel=popover]').popover('show');
        //$('[rel=tooltip]').tooltip('show');
    });
    $('#pageHints').live('mouseout', function(){
        $('[rel=popover]').popover('hide');
        //$('[rel=tooltip]').tooltip('hide');
    });

	$('textarea').autosize().css('resize', 'none');
});