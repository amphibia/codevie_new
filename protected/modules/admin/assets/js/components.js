$(document).ready( function(){
	$(".ajaxControl[action], .btn[action]").live('click', function(){
		var element = $(this);
		if(element.hasClass('clientBusy') || element.hasClass('disabled')) return; // между запросами или неактивна
		element.button('loading');
		var action = element.attr("action").replace(":value", element.hasClass('active'));
		$(this).request(action, [], function(){element.button('reset');})
		return false;
	});
});

