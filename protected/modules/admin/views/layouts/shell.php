<!DOCTYPE html>
<html lang="<?= Yii::app()->language ?>">
<head>
    <title><?= $this->pageTitle; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body lang="<?= Yii::app()->language ?>" id="top" data-spy="scroll" data-target=".subnav" data-offset="50">

<div id="toTop" rel="tooltip" title="<?= Yii::t('admin', 'Scroll to top') ?>" data-placement="top"></div>

<div id="wrapper">

    <!-- Слой блокирования на время выполнения запроса. -->
    <div class="___system__" id="pageLockLayer">
        <span class="labelLoading label label-info"><?= Yii::t('admin', 'Loading'); ?>...</span>
    </div>

    <!-- Подсказки -->
    <div id="pageHints"></div>

    <!-- Диалог с окном подтверждения -->
    <?php $this->beginWidget(Yii::localExtension('bootstrap', 'widgets.BootModal'), array('id' => 'confirmModal')); ?>
    <div class="modal-header"><a class="close" data-dismiss="modal">&times;</a>

        <h3 class="title"><?= Yii::t('admin', 'Confirmation'); ?></h3></div>
    <div class="modal-body"></div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.BootButton', array(
            'type' => 'primary',
            'label' => Yii::t('app', 'Yes'),
            'url' => '#',
            'htmlOptions' => array('class' => 'confirmation-yes', 'data-dismiss' => 'modal'),
        )); ?>
        <?php $this->widget('bootstrap.widgets.BootButton', array(
            'label' => Yii::t('app', 'No'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss' => 'modal'),
        )); ?>
    </div>
    <?php $this->endWidget(); ?>

    <header>
        <?
        $this->widget(Yii::localExtension('bootstrap', 'widgets.BootNavbar'),
            array(
                'fixed' => true,
                'brand' => '',
                'brandUrl' => '/admin',
                'collapse' => true,
                'items' => array(array('class' => 'bootstrap.widgets.BootMenu', 'items' => Yii::app()->admin->componentsMenu())),
                'htmlOptions' => array('class' => 'mainMenu'),
            )
        );
        //require '_userInfo.php'; // Информация о текущем пользователе перенесена в меню
        ?>

    </header>

    <div class="container root-container" style="position:relative;">
        <?php
        // мультиязычность
        if (Yii::app()->hasComponent('lang')) {
            ?>
            <div id="langChange" style="margin: 18px 0 0 0;">
                <div class="btn-group">
                    <?php
                    if (is_array(Yii::app()->lang->languages) && count(Yii::app()->lang->languages) > 1) {
                        foreach (Yii::app()->lang->languages as $key => $value) {
                            echo CHtml::tag('a', array('href' => '/language/set/' . $key, 'class' => 'btn' . (Yii::app()->language == $key ? ' active' : '')), $value['long']);
                        }
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>
        <?php echo $content; ?>
    </div>

    <footer>
        <div class='container'>
            <div class="brand"></div>
            <p class="copyright">
                <?= Yii::t('admin', 'All right reserved.'); ?><br/>
                Powered by <a href="http://alekseev-koshevy.net" target="_blank">Alekseev & Koshevy CMS</a>.
            </p>
        </div>
    </footer>

</div>

</body>
</html>