<?php

/**
 * Вывод списка компонентов, сгрупированных по пакетам. Без пагинации.
 * @param $components array
 */
$this->renderPartial('components/_header');


echo CHtml::openTag("selection", array("id"=>"applicationComponents", "class"=>"row"));

$prevousPackage = NULL;
$mod = 0;
$modWidth = 12;
$modStep = 3;
foreach($components as $component)
{
	// следующая группа (пакет)
	if(!($mod%$modStep)){
		if($mod) echo CHtml::closeTag("selection");
		echo CHtml::openTag("selection", array("class"=>"row-fluid"));
	}
	
	$mod++;

	$this->renderPartial('components/_component', compact("component"));	
}

if($mod) echo CHtml::closeTag("selection");

echo CHtml::closeTag("selection");
