<?

/**
 * Вывод одного компонента.
 * @param $component array
 */

$notExists = ($component->exists || $component->default)?NULL:" notExists";
$error = ($component->error)?" error":NULL;
$active = ($component->active)?" active":NULL;
$title = ($title = $component->title)?"$title ({$component->alias})"
	: ($component->class_name?$component->class_name:Yii::label($component->alias, $component->type));


// компонент
?>
<div id="<?=$this->_componentHtmlId($component);?>" class="item well span4 <?=$notExists?><?=$error?><?=$active?>">

	<h3 class="attribute title"><?=$title?></h3>
	<div class="attribute path"><strong><?= $component->getAttributeLabel("path") . ": " ?></strong><?= $component->path?></div>

	<? if($component->description){ ?>
		<div class="attribute description"><?=$component->description?></div>
	<? } ?>

	<div class="attribute type"><strong><?= $component->getAttributeLabel("type") . ": "?></strong><?= $component->type?></div>
	<div class="attribute class_name"><strong><?= $component->getAttributeLabel("class_name") . ": " ?></strong><?= $component->class_name?></div>
	<div class="attribute class_name"><strong><?= $component->getAttributeLabel("vendor") . ": " ?></strong><?= (($vendor=$component->vendor)?$vendor:Yii::t("admin", "unknown"))?></div>

	<? if($component->package){ ?>
		<div class="attribute package"><strong><?= $component->getAttributeLabel("package") . ": " ?></strong><?= $component->package?></div>
	<? } ?>

	<!-- Связи с другими компонентами -->
	<? if(sizeof($component->requirements)){ ?>
		<div class="attribute reqierements">
			<strong><?= $component->getAttributeLabel("reqirements") . ": "; ?></strong>
				<? foreach($component->requirements as $requirement){
						$this->widget('bootstrap.widgets.BootLabel', array(
							'type'=>$requirement->exists?($requirement->active?"success":"default"):"important",
							'label'=>($title = $requirement->title)?"$title ({$requirement->alias})"
								: Yii::label($requirement->alias, $requirement->type),
						));
                        echo " ";
					}
				?>
		</div>
	<? } ?>


	<? if(!$error && !$notExists){ ?>

	<!-- Включен ли компонент -->
	<div class="stateButtons"> 
		<?
			$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
				'buttonType'=>'link',
				'type'=>$component->active?"success":NULL,
				'label'=>$component->active?Yii::t("admin", "Enabled"):Yii::t("admin", "Disabled"),
				'loadingText'=>Yii::t("app", "loading") . "...",
				'toggle'=>true,
				'icon'=>'icon-off' . ($component->active?' icon-white':NULL),
				"active"=>$component->active,
				'htmlOptions'=>array(
					"href"=>"javascript:void;",
					"id"=>"preload{$component->id}",
					"class"=>"ajaxControl enabled",
					"action"=>$this->createUrl("enabled", array("id"=>$component->id, "value"=>":value")),
				),
			));
		?>	

		<? if($component->type == "component"){ ?>
			<!-- Загружается ли компонент при загрузке -->
			<?
				$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
					'buttonType'=>'link',
					'type'=>$component->preload?"info":NULL,
					'label'=>$component->preload?Yii::t("admin", "Preload"):Yii::t("admin", "Easy loading"),
					'loadingText'=>Yii::t("app", "loading") . "...",
					'toggle'=>true,
					"active"=>$component->preload,
					'htmlOptions'=>array(
						"href"=>"javascript:void;",
						"id"=>"preload{$component->id}",
						"class"=>"ajaxControl preload".($component->active?NULL:" disabled"),
						"action"=>$this->createUrl("preload", array("id"=>$component->id, "value"=>":value")),
						'data-title'=>Yii::t('admin', 'Preload components'),
						'data-content'=>Yii::t('admin', 'Application will create this component at start, if turned on. Ingoring, when component disabled.'),
						'rel'=>'popover'
					),
				));
			?>	

		<? } ?>
		

		<!-- Редактирование аргументов -->
		<?
			$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'Arguments',
				'loadingText'=>Yii::t("app", "loading") . "...",
				'icon'=>'icon-wrench icon-white',
				'htmlOptions'=>array(
					"href"=>"javascript:void;",
					"class"=>"ajaxControl arguments",
					"action"=>$this->createUrl("arguments", array("id"=>$component->id)),
					'data-title'=>Yii::t('admin', 'Edit arguments'),
					'data-content'=>Yii::t('admin', 'You can edit init arguments of application {type}.', array('{type}'=>$component->type)),
					'rel'=>'popover'
				),
			));
		?>

	</div>
	
	<? } ?>


	<?
	if($error){
		echo "<br>";
		$this->widget('bootstrap.widgets.BootLabel', array(
			'type'=>"important",
			'label'=>$component->error,
		));
	}
	?>


	<!-- Компонент не существует -->
	<? if($notExists){ ?>
		<div class="attribute notExists"><?= Yii::t("admin", "Component not exists!"); ?></div>
	<? } ?>


</div>
