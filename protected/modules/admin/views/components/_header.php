<div><br/><br/><?

?><h1><? echo Yii::t('admin', 'Application components and modiles management.'); ?></h1><?
?>	
<strong><?=Yii::t('admin', 'Warning!'); ?></strong>
<?=Yii::t('admin', 'Incorrect changes of the app configuration components may fail!'); ?>
</p>
<br />
<?
echo CHtml::link(Yii::t('admin', 'Reload components information'), $this->createUrl('reload'),
	array
	(
		'class'=>'reload btn btn-primary btn-large',
		'data-title'=>Yii::t('admin', 'Searching for components changes'),
		'data-content'=>Yii::t('admin', 'Initiate it when you add/delete/change system components and modules.'),
		'rel'=>'popover'
	)
);

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
	'buttonType'=>'link',
	'size'=>'large',
	'type'=>'success',
	'icon'=>'icon-ok icon-white',
	'label'=>Yii::t("admin", "Approve"),
	'loadingText'=>Yii::t("app", "approving components") . "...",
	'htmlOptions'=>array
	(
		"href"=>"javascript:void;",
		'action'=>$this->createUrl('approve'),
		'data-title'=>Yii::t('admin', 'Write configuration'),
		'data-content'=>Yii::t('admin', 'Write configuration into native Yii configuration files. Approve configuration to apply changes.'),
		'rel'=>'popover'
	)
));

?>
</div>
<br/><br/>
<?

if(is_array(Yii::app()->admin->errors))
	foreach(Yii::app()->admin->errors as $error){
		Yii::app()->user->setFlash('error', "<strong>".Yii::t('admin', 'Error!')."</strong> $error.");
		$this->widget('bootstrap.widgets.BootAlert');}
