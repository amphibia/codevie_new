<?

/**
 * @title Administration system component
 * @description { Iinitialize system services, incapsulate components manegement functions. }
 * @preload no
 * @vendor koshevy
 * @package admin
 */
class AdminComponent extends ExtCoreApplicationComponent
{

    public $blankComponent = array();

    /**
     * @param array список конфигурационных файлов,
     * применяемых при сопоставлении данных
     *
     * ПРИМЕЧАНИЕ: preload должен располагаться в самом конце,
     * т.к. к моменту расположения идентификаторы компонентов уже
     * должны быть известны
     */
    public $configFiles = array();

    /**
     * @param array Записи в конфигах по умолчанию. Применяется в случае
     * отсутствия заполненных аргументов.
     * Данные для модулей и компонентов хранятся отдельно. Можно указывать
     * готовый текст  или данные как есть (переменные, массивы и т.п.).
     */
    public $default = array();


    public $dictionary = array(
        'actions' => array(
            'index' => 'Application components management',
            'reload' => 'Reload components information',
            'clearcache' => 'Reload with clear cache',
        )
    );

    public $dictionaryCache = array(
        'actions' => array(
            'cache' => 'Flush cache',
            'assets' => 'Flush assets',
        )
    );


    /**
     * @param array ошибки, возникшие при работе с конфигурацией.
     */
    public $errors = array();

    /**
     * @param array массив ссылок на элементы списка $_components,
     * осуществляемых по алиасу компонента (разделён по типу компонент)
     */
    public $aliases = array();

    /**
     * Срок жизни кэша в рамках создания административной системы.
     * @var integer
     */
    public $cacheExpire = 86400;

    /**
     * @param array список всех найденных компонентов,
     * в качестве ключа используется имя класса
     */
    protected $_components = array();


    public function init()
    {
        Yii::import('admin.services.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::app()->registerService('components', 'AdmComponentsService', 'admin',
            array(
                'title' => Yii::t('admin', 'Application components'),
                'menuGroup' => 'options',
                //'actions' => array('index', 'reload'),
                // значения в словаре указываются без перевода
                'dictionary' => $this->dictionary
            ));
        Yii::app()->registerService('cache', 'AdmFlushCacheService', 'admin',
            array(
                'title' => Yii::t('admin', 'Cache and assets'),
                'menuGroup' => 'cache',
                //'actions' => array('index', 'reload'),
                // значения в словаре указываются без перевода
                'dictionary' => $this->dictionaryCache
            ));
    }

    /**
     * Создание вложенного массива меню с ссылками на админиcтративные сервисы.
     * @return array
     */
    public function componentsMenu()
    {
        $items = array();
        $serviceCheck = null;
        foreach (Yii::app()->getServices('admin') as $serviceName) {
            $data = Yii::app()->getServiceData($serviceName);

            // предполагается, что заголовки будут указываться уже с учётом
            // интернационализации
            $title = $data['title'] ? $data['title'] : $serviceName;
            $menuGroup = isset($data['menuGroup']) ? $data['menuGroup'] :
                Yii::t('admin', 'other') . "/$title";

            $menuLevel = & $items;

            foreach (explode('/', $data['menuGroup']) as $menuGroup) {
                $menuGroup = Yii::t('admin', $menuGroup);
                if (!isset($menuLevel[$menuGroup])) {
                    $menuLevel[$menuGroup] = array();
                    $menuLevel[$menuGroup]['items'] = array();
                }
                $menuLevel[$menuGroup]['label'] = $menuGroup;
                $menuLevel = & $menuLevel[$menuGroup]['items'];
            }


            // Divider
            if ($serviceCheck !== null && $serviceCheck != $serviceName && count($menuLevel)) {
                $menuLevel[] = '---';
            }
            $serviceCheck = $serviceName;

            $dictionary = isset($data['dictionary']) ? $data['dictionary'] : array();

            // для экономии ресурсов на сбор данных по действиям сервисов,
            // рекомендуется указывать их в прикреплённых данных
            if (isset($data['actions'])) {
                foreach ($data['actions'] as $action) {
                    die($action);
                    if ($action == 'error') continue;

                    $active = (Yii::app()->controller->id == $serviceName) &&
                    ($action == Yii::app()->controller->lastActionID)
                        ? true : false;

                    $menuLevel[$serviceName . "_" . $action] = array(
                        'label' => Yii::label($action, 'actions', true, $dictionary),
                        // не используется "Service::createUrl", чтобы сэкономить
                        // ресурсы
                        'url' => array("/admin/services/{$serviceName}", 'tag' => $action),
                        'active' => $active,
                    );
                }
            } // анализ структуры сервиса по умолчанию
            else {
                $service = Yii::app()->getService($serviceName);
                $actions = $service->getActions();

                if (!sizeof($dictionary)) $dictionary = isset($service->dictionary)
                    ? $service->dictionary : array();

                foreach ($actions as $action) {
                    if ($action->id == 'error') continue;

                    if ($action->hasRequieredArguments || $action->isAjax())
                        continue;

                    $active = (Yii::app()->controller->id == $serviceName) &&
                    ($action->id == Yii::app()->controller->lastActionID)
                        ? true : false;
                    $menuLevel[$serviceName . "_" . $action->id] = array('label' => '');
                    $icon = '';
                    if ($action->id == 'index') {
                        $icon = 'list';
                    } elseif ($action->id == 'create') {
                        $icon = 'plus';
                    } elseif ($action->id == 'form') {
                        $icon = 'pencil';
                    } elseif ($action->id == 'reload') {
                        $icon = 'refresh';
                    } elseif ($action->id == 'clearcache') {
                        $icon = 'repeat';
                    }

                    $menuLevel[$serviceName . "_" . $action->id] = array(
                        'label' => Yii::label($action->id, 'actions', true, $dictionary),
                        // не используется "Service::createUrl", чтобы сэкономить
                        // ресурсы
                        'url' => array("/admin/services/{$serviceName}", 'tag' => $action->id),
                        'active' => $active,
                        'icon' => $icon,
                    );
                }
            }
        }

        // Информация о текущем пользователе
        if (Yii::app()->hasComponent('users')) {
            $username = null;
            if (Yii::app()->user->isGuest) {
                $username = Yii::t('admin', 'Guest user');
            } else if (!isset(Yii::app()->user->model)) {
                $username = Yii::t('admin', 'User not found');
            } else {
                $username = Yii::app()->user->model->username;
            }

            $items[$username] = array(
                'label' => $username,
                'icon' => 'user',
                'items' => array(
                    'profile' => array(
                        'label' => Yii::t('admin', 'User profile'),
                        'url' => Yii::app()->createUrl('/admin/services/users/update/' . Yii::app()->user->model->id),
                        'active' => false,
                        'icon' => 'edit',
                    ),
                    'logout' => array(
                        'label' => Yii::t('admin', 'Logout'),
                        'url' => Yii::app()->users->logoutUrl,
                        'active' => false,
                        'icon' => 'off',
                    ),
                )
            );
        }

        //CVarDumper::dump($items, 4, 1);exit();
        return $items;
    }


    /**
     * Поиск компонентов системы: потомки классов CApplicationComponent
     * и CWebModule. Метода извлекает информацию, содержащуюся в PHP-DOC формате
     * при каждом файле, обрабатывает её, записывает результаты в базу.
     *
     * Примечание: чтобы система смогла обнаружить файл, он должен быть именован
     * по стандартным соглашениям YII для модулей и компонентов.
     * @return void
     */
    public function scanApplicationComponents()
    {
        $appPath = Yii::getPathOfAlias('application');
        $files = CFileHelper::findFiles($appPath, array(
            'fileTypes' => array('php'), 'exclude' => array('config')));

        $fileNameRule = "/^(?P<path>.*[^\w](?P<className>(?P<name>\w+)(?P<type>Component|Module)).php)$/";


        foreach ($files as $fileName) {
            if (!preg_match($fileNameRule, $fileName, $attributes))
                continue;

            $attributes = array(
                "path" => $attributes["path"], "className" => $attributes["className"],
                "name" => $attributes["name"], "type" => strtolower($attributes["type"])
            );

            // чтение дополнительных данных из файла
            if ($phpDocAttributes = $this->_parsePhpFile(
                $attributes["path"], $attributes["className"])
            )
                $attributes = array_merge($attributes, $phpDocAttributes);
            else {
                $attributes = array_merge($attributes, $this->blankComponent);
                $attributes["error"] = Yii::t('admin', 'File parse error');
            }

            $this->_components[$attributes["className"]] = $attributes;
        }

        $this->_merge();
        $this->_save();
    }

    /**
     * Разбор внутренней структуры файла; проверка соответствия имени файла
     * и имени класса, извлечение аттрибутов, указанных в PHPDOC, аргументов класса.
     * @param string $filepath
     * @param string $className
     * @return array извлечённые аттрибуты или NULL в случае ошибки
     */
    protected function _parsePhpFile($filepath, $className)
    {
        // обработка файла - извлечение PHPDOC шапки и атрибутов
        $text = file_get_contents($filepath);
        $phpFileRule = "=^<\?(?P<phpTag>php)?.*?(/\*\*(?P<phpDocHeader>.*?)\*/).*?" .
            "class\s+(?P<className>\w+)\s*(?>extends\s+(?P<parentClass>\w+))?\s+" .
            "\{(?P<classBody>.*)}.*$=s";

        if (!preg_match_all($phpFileRule, $text, $fileMatches) || current($fileMatches["className"]) != $className)
            return NULL;

        extract($fileMatches);
        $attributes = $this->blankComponent;

        // извлечени PHPDOC - аттрибутов
        if (is_array($phpDocHeader) && sizeof($phpDocHeader))
            foreach ($phpDocHeader as $doc) {
                if (preg_match_all("/@(?P<name>\w+)\\s*(?P<value>(?>{.*?\})|(?>\s+[^\n\{]*))/s",
                    $doc, $docMatches, PREG_SET_ORDER)
                )

                    foreach ($docMatches as $attribute)
                        $attributes[$attribute["name"]] = preg_replace("/(?>\n\s*\*\s+)/", " ", trim(trim($attribute["value"], "[{}]")));
            }

        // информация об аргументах компонента, содержащихся
        // в теле класса
        if (is_array($classBody) && sizeof($classBody)) {
            /** TODO: на данный момент аргументы, инициализированные непустым массивом игнорируются */
            $classBody = current($classBody);
            if (preg_match_all('/public\s+\$(?P<name>\w+)(?>\s*=\s*(?P<value>(?>".*?")|(?>\'.*?\')|(?>\w+(?>\s*\(\s*\))?));)?/',
                $classBody, $classArguments, PREG_SET_ORDER)
            )
                foreach ($classArguments as $name => $value)
                    if (isset($value["value"]))
                        $attributes["arguments"][$value["name"]] = $value["value"];
        }

        return $attributes;
    }


    /**
     * Сопоставление данных сканирования с данными
     * в конфигурационных файлах.
     * @return void
     */
    protected function _merge()
    {
        // синхронизация данных с конфигом
        foreach ($this->configFiles as $type => $file)
            $this->_applyConfigFile($type);
    }

    /**
     * Сопоставление с файлом конфигурации.
     * @param string $section раздел конфигурации, к которому относится файл (component/module/preload)
     * @return void
     */
    protected function _applyConfigFile($section)
    {
        $filename = Yii::getPathOfAlias('application') . "/config/" . $this->configFiles[$section];
        $config = require $filename;

        if (!is_array($config))
            $this->errors[] = Yii::t("admin", 'Invalid configuration "{section}" section. Ignored.',
                array("{section}" => $section));

        // сверка с конфигом: какие компоненты загружаются автоматически
        // (для компонентов, с указанным атрибутом "@preload no" игнорируется)
        if ($section == "preload") {
            foreach ($config as $alias) {
                $copmonents = $this->aliases["component"];

                if (isset($copmonents[$alias])) {
                    $copmonents[$alias]["preload"] = "yes";
                } else $this->errors[] = Yii::t("admin",
                    'Error ocurred when configuration "preload" section parsed: component "{component}" not found',
                    array("{component}" => $alias));
            }

            return;
        }


        foreach ($config as $alias => $item) {
            // значение по умолчанию может задаваться прямым текстом для вставки в конфиг,
            // либо значением, которое будет превращено в текст
            if (isset($this->default[$section][$alias])) {
                $defaultValue = is_array($defaultValue = $this->default[$section][$alias]) ?
                    $this->_recourseArguments($alias, $defaultValue, 1, false) : $defaultValue;
            } else $defaultValue = NULL;

            if (isset($item["class"])) {
                if ($path = Yii::getPathOfAlias($item["class"])) $path .= ".php";
                $className = preg_replace("/\w+\./", "", $item["class"]);

                // компонент не был найден сканированием файлов
                if (!isset($this->_components[$className])) {
                    $this->_components[$className] = $this->blankComponent;
                    $this->_components[$className]["className"] = $className;
                    $this->_components[$className]["path"] = $path;
                    $this->_components[$className]["exists"] = file_exists($path);
                } else {
                    // проверка, что у одинаковых классов одинаковые пути
                    if ($path != $this->_components[$className]["path"])
                        $this->errors[] = Yii::t('admin', 'Ambigious class information - class {className} found at {path} and use path {configPath} in config.');
                }

                // применение атрибутов из конфига
                if (isset($this->_components[$className]["arguments"]))
                    $this->_components[$className]["arguments"] = array_merge(
                        $this->_components[$className]["arguments"], $item);
                else
                    $this->_components[$className]["arguments"] = $item;

                // создание ссылки по алису
                if (isset($this->aliases[$section][$alias]))
                    $this->errors[] = Yii::t('admin', 'Alias ({section} id) must be unique.',
                        array("{section}" => $section));
                else {
                    $this->_components[$className]["default"] = $defaultValue;
                    $this->_components[$className]["alias"] = $alias;
                    $this->aliases[$section][$alias] = & $this->_components[$className];
                }

                $this->_components[$className]["type"] = $section;
                $this->_components[$className]["active"] = true;
            } else {
                // класс не указан
                if (!$defaultValue)
                    $this->errors[] = Yii::t('admin', 'Can`t find minimal information about {alias} {section}. Ignored.',
                        array("{section}" => $section, "{alias}" => $alias));
                else {
                    // гипотетическое имя класса
                    $className = "C" . ucfirst($alias) . ucfirst($section);
                    $this->_components[$className] = $this->blankComponent;
                    $this->_components[$className]["alias"] = $alias;
                    $this->_components[$className]["default"] = $defaultValue;
                    $this->aliases[$section][$alias] = & $this->_components[$className];
                    $this->_components[$className]["path"] = NULL;
                    $this->_components[$className]["className"] = $className;
                    $this->_components[$className]["type"] = $section;
                    $this->_components[$className]["active"] = true;
                }
            }
        }
    }

    /**
     * Сохранение в таблицу компонент.
     * @return void
     */
    public function _save()
    {
        foreach ($this->_components as $component) {
            // превращение пути в псевдоним
            $component["path"] = str_replace(Yii::getPathOfAlias('application'), 'application', $component["path"]);
            $component["path"] = str_replace(Yii::getPathOfAlias('system'), 'system', $component["path"]);
            $component["path"] = str_replace(DIRECTORY_SEPARATOR, '.', $component["path"]);
            $component["path"] = str_replace(".php", '', $component["path"]);

            if ($component["path"] && !$component["default"]) {
                if (!is_array($component['arguments']))
                    $component['arguments'] = array();
                $component['arguments']['class'] = $component["path"];
            }


            // обработка зависимостей
            $itemRule = '(?P<type>component|module|package)\s*:\s*(?P<alias>\w+)';
            if (!isset($component["requirements"])) $component["requirements"] = NULL;
            if (preg_match_all("/(?>[\,\s]*$itemRule)/", $component["requirements"], $req, PREG_SET_ORDER)) {
                $component["requirements"] = array();
                foreach ($req as $requirement) $component["requirements"][] = array(
                    "type" => $requirement["type"], "alias" => $requirement["alias"]);
            }

            // внесение данных в базу
            $component["class_name"] = $component["className"];
            if (!$component["alias"]) $component["alias"] = strtolower(preg_replace("/(Component|Module)/", "", $component["className"]));
            $newComponent = Component::updateComponent($component);
            if (sizeof($newComponent->errors))
                foreach ($newComponent->errors as $field => $errorGroup)
                    foreach ($errorGroup as $error) $this->errors[] = "{$field}: $error";
        }

    }

    /**
     * Очистка таблицы компонент.
     * @return void
     */
    public function clear()
    {
        $tableName = Component::model()->tableName();
        Yii::app()->db->createCommand("TRUNCATE TABLE $tableName")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE {{app_relations}}")->execute();
    }


    /**
     * сохранение из базы в конфиг
     */
    public function approve()
    {
        foreach ($this->configFiles as $section => $file) {
            $file = fopen(Yii::getPathOfAlias('application') . "/config/$file", "w");
            fwrite($file, "<?php\n\nreturn array(\n");

            if ($section == 'preload') {
                $preloads = Component::model()->findAllByAttributes(
                    array('type' => 'component', 'active' => 1, 'preload' => '1'));

                $preloadsResult = array();
                foreach ($preloads as $component)
                    if ($component->exists || $component->default)
                        $preloadsResult[] = "'$component->alias'";

                fwrite($file, implode(", ", $preloadsResult));
            } else {
                foreach (Component::model()->findAllByAttributes(array('type' => $section, 'active' => 1)) as $component) {
                    $component->default = trim($component->default, ",");
                    $default = "\n\t'{$component->alias}' => {$component->default},\n";
                    if ($component->exists) {
                        $component->arguments;
                        if (!(sizeof($component->arguments) &&
                            $statement = $this->argumentsToText($component))
                        )
                            $statement = $default;

                        fwrite($file, $statement);
                    } else fwrite($file, $default);
                }
            }

            fwrite($file, "\n);");
            fclose($file);
        }
    }

    /**
     * Преврещение цепочки аругментов (значения, масивы) в PHP-код для изменения
     * конфигурационных файлов.
     * @param Component $component
     * @return string
     */
    public function argumentsToText($component)
    {
        if (is_array($component->arguments)) {
            return $this->_recourseArguments($component->alias, $component->arguments);
        } else return NULL;
    }

    /**
     * Подуровень аргументов (используется функцией argumentsToText).
     * @param type $name имя переменной/ключа
     * @param type $value значение
     * @param type $level уровень глубины (для формирования отступов)
     * @param type $useAlias требуется ли выводить имя переменной и оператор присваивания
     * @return string
     */
    protected function _recourseArguments($name, $value, $level = 1, $useAlias = true)
    {
        $tab = $level ? str_repeat("\t", $level) : NULL;
        $result = ($useAlias && !is_numeric($name)) ? "\n{$tab}'{$name}' => " : NULL;
        if (!$result && $useAlias) $result = "\n{$tab}";

        if (is_array($value)) {
            $result .= "array(";
            foreach ($value as $subName => $subValue) $result .= $this->_recourseArguments(
                $subName, $subValue, $level + 1);
            $result .= "\n{$tab})";
        } else if (!$value) $result .= (is_null($value)) ? " NULL" : " false";
        else if (preg_match("/^(array\s*\(\s*\))|(null)|(false)$/", strtolower($value))) $result .= $value;
        else $result .= "'" . addslashes(trim(trim(preg_replace("/[\n\r]/", "\\n", $value), "['\"]"))) . "'";

        return $result ? "$result," : NULL;
    }

}
 
 