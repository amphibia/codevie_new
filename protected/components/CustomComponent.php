<?php

/**
 * @title Custom application components
 * @description { Initialize conjuncture of application. }
 * @preload yes
 * @vendor koshevy
 */
class CustomComponent extends CApplicationComponent
{

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);
        Yii::import('application.services.*');
        Yii::app()->registerService('feedback', 'FeedbackAdminService', 'admin',
            array('title' => 'Feedback email options', 'menuGroup' => 'application',
                'dictionary' => array('actions' => array('form' => 'Feedback email options'))
            )
        );
    }

    /**
     * Обращение к разделу главной страницы.
     */
    public function getMainPageCategory(){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getMainPageCategory();
    }

    /**
     * Обращение к необходимым разделам
     * @param array|string $categories Массив ключей категорий, либо 1 значение
     * @return StructCategory|array из StructCategory
     */
    public function getCategories($categories){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getCategories($categories);
    }

    /**
     * Заголовок страницы.
     * @return string
     */
    public function getPageTitle()
    {
        // Формирование заголовка страницы
        $title = Yii::t('custom', 'app_name');
        if (($currentInstance = Yii::app()->struct->currentPage) ||
            ($currentInstance = Yii::app()->struct->currentCategory)
        ) {
            $title = "{$title} &ndash; {$currentInstance->title}";
        } else {
            $controllerName = Yii::app()->controller->id;
            $actionName = Yii::app()->controller->action->id;
            if ($controllerName == 'site' && $actionName == 'index') {
                // Main page
                $mainPageCategory = Yii::app()->custom->getMainPageCategory();
                $title = "{$title} &ndash; {$mainPageCategory->title}";
            } else {
                $title2 = Yii::t('titles', ucfirst($controllerName) . ' ' . $actionName);
                $title = "{$title} &ndash; {$title2}";
            }
        }

        return $title;
    }
}
