<?php

class m290515_140400_search_field extends CDbMigration
{
    public function up()
    {
        $this->addColumn('struct_category', 'in_search', 'tinyint(1)');
        $this->createIndex('in_search', 'struct_category', 'in_search');
        if (Yii::app()->hasComponent('lang') && count(Yii::app()->lang->languages) > 1) {
            foreach (Yii::app()->lang->languages as $key => $value) {
                $tableName = 'struct_category_' . $key;
                if (($this->getDbConnection()->getSchema()->getTable($tableName)) !== null) {
                    $this->addColumn($tableName, 'in_search', 'tinyint(1)');
                    $this->createIndex('in_search', $tableName, 'in_search');
                }
            }
        }

        $this->addColumn('struct_page', 'in_search', 'tinyint(1)');
        $this->createIndex('in_search', 'struct_page', 'in_search');
        if (Yii::app()->hasComponent('lang') && count(Yii::app()->lang->languages) > 1) {
            foreach (Yii::app()->lang->languages as $key => $value) {
                $tableName = 'struct_page_' . $key;
                if (($this->getDbConnection()->getSchema()->getTable($tableName)) !== null) {
                    $this->addColumn($tableName, 'in_search', 'tinyint(1)');
                    $this->createIndex('in_search', $tableName, 'in_search');
                }
            }
        }
    }

    public function down()
    {
        $this->dropColumn('struct_category', 'in_search');
        $this->dropIndex('in_search', 'struct_category');
        if (Yii::app()->hasComponent('lang') && count(Yii::app()->lang->languages) > 1) {
            foreach (Yii::app()->lang->languages as $key => $value) {
                $tableName = 'struct_category_' . $key;
                if (($this->getDbConnection()->getSchema()->getTable($tableName)) !== null) {
                    $this->dropColumn($tableName, 'in_search');
                    $this->dropIndex('in_search', $tableName);
                }
            }
        }

        $this->dropColumn('struct_page', 'in_search');
        $this->dropIndex('in_search', 'struct_page');
        if (Yii::app()->hasComponent('lang') && count(Yii::app()->lang->languages) > 1) {
            foreach (Yii::app()->lang->languages as $key => $value) {
                $tableName = 'struct_page_' . $key;
                if (($this->getDbConnection()->getSchema()->getTable($tableName)) !== null) {
                    $this->dropColumn($tableName, 'in_search');
                    $this->dropIndex('in_search', $tableName);
                }
            }
        }
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}