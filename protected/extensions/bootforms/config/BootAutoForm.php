<?php

return array(

    /**
     * Обязательный параметр - исходная форма.
     * @var CForm
     */
    'form'  => NULL,

    /**
     * Требуется ли использовать опцию "не возвращаться назад".
     * @var boolean
     */
    'repeatActionOption'  => true,
    
    /**
     * Класс строки с кнопками.
     * @var string
     */
    'buttonsRowClass'  => 'form-actions',

	/**
	 * @var Тип формы (смотрите константы Bootstrap.BootActiveForm).
	 */
	'type'  => 'horizontal',

	/**
	 * @var boolean отображение ошибок.
	 */
	'inlineErrors'  => true,

    /**
     * Порядок вывода полей.
     * @var array
     */
    'fieldsOrder'  => array(),

    /**
     * URL страницы, на которую требуется вернуть форму по сохранении.
     * @var string
     */
    'referer'  => NULL,

    /**
     * Форма собирает данные о недастоющих элементах в любом случае.
     * @var boolean
     */
    'autoElements'  => false,

    /**
     * Методы (веджета BootActiveForm), соответствующие типам элементов.
     * Столбец 'items' говорит использует ли элемент множественные значения.
     * @var array 
     */
    'typesMethods'  => array(
        'text'=> array('textFieldRow', 'items'=>false),
        'select' => array('dropDownListRow', 'items'=>true),
        'dropdownlist' => array('dropDownListRow', 'items'=>true),
        //'checkBoxList' => array('checkBoxList', 'items'=>true),
        'file' => array('fileFieldRow', 'items'=>false),
        'textarea' => array('textAreaRow', 'items'=>false),
        'uneditable' => array('uneditableRow', 'items'=>false),
        'checkbox' => array('checkBoxRow', 'items'=>false),
        'checkboxlist' => array('checkBoxListRow', 'items'=>true),
        'checkboxlistinline' => array('checkBoxListInlineRow', 'items'=>true),
        'radiobutton' => array('radioButtonRow', 'items'=>false),
        'radiobuttonlist' => array('radioButtonListRow', 'items'=>true),
        /** @todo: $captchaOptions для BootActiveForm::captchaRow никак не передаются */
        'captcha' => array('captchaRow', 'items'=>false),

		'date' => array('datepickerRow', 'items'=>false,
			'attributes'=>array(
				'prepend'=>'<i class="icon-calendar"></i>',
				'options' => array('format' => strtolower(Yii::app()->locale->getDateFormat('short')), 'weekStart' => 1),
			)
		),

		'dateRange' => array('dateRangeRow', 'items'=>false,
			'attributes'=>array(
				'prepend'=>'<i class="icon-calendar"></i>',
				'options' => array('format' => strtolower(Yii::app()->locale->getDateFormat('short')), 'weekStart' => 1),
			)
		),

		'redactor' => array('redactorRow', 'items'=>false, 'attributes'=>array('prepend'=>'<i class="icon-calendar"></i>')),
		'wysihtml5' => array('html5EditorRow', 'items'=>false),
		'toggle' => array('toggleButtonRow', 'items'=>false),
		
    ),

	/**
	 * Стандартные элементы (объект CFormInputElement).
	 * @var array
	 */
	'standartElements'  => array(
		'url', 'email', 'password', 'number', 'range', 'date',
	),

    /**
     * Типы соответствующих кнопок.
     * @var array
     */
    'buttonTypes'  => array(
        'save' => 'primary',
        'send' => 'primary',
        'create' => 'primary',
        'add' => 'primary',
        'cancel' => 'danger',
        'remove' => 'danger',
        'delete' => 'danger',
    ),

    /**
     * Размеры соответствующих кнопок.
     * @var array
     */
    'buttonSizes' => array
	(
        'save' => 'large',
        'send' => 'large',
        'create' => 'large',
        'add' => 'large',
        'cancel' => 'large',
        'remove' => 'large',
        'delete' => 'large',
	),
	
    /**
     * Иконки соответствующих кнопок.
     * @var type 
     */
    'buttonIcons'  => array(
        'save' => 'ok white',
        'send' => 'ok white',
        'create' => 'ok white',
        'add' => 'ok white',
        'reset' => 'remove',
        'cancel' => 'remove white',
        'remove' => 'remove white',
        'delete' => 'remove white',
    ),

    /**
     * Типы элементов, использующиеся для реляционных атрибутов.
     * @var array 
     */
    'relationElementTypes'  => array(
        CActiveRecord::BELONGS_TO => 'dropdownlist',
        CActiveRecord::HAS_ONE => 'dropdownlist',
        CActiveRecord::HAS_MANY => 'checkboxlist',
        CActiveRecord::MANY_MANY => 'checkboxlist'
    ),

    /**
     * Классы по умолчанию для соответствующих элементов.
     * @var array
     */
    'defaultClasses'  => array(
        'textarea' => 'span6',
        'text' => 'span4',
        'dropdownlist' => 'span3',
    ),

    /**
     * Поля, используемые для вывода названий и заголовков (в порядке приоритета)
     * @var array
     */
    'titleFields'  => array(
        'title', 'name', 'value', 'description', 'alias'
    ),

);
