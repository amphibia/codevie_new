<?php

Yii::import('application.extensions.image.Image');

/**
 * @title PHP Mailer Component
 * @description { Advanced mail sending. }
 * @preload no
 * @vendor koshevy
 * @package phpmailer
 *
 * Компонент-надстройка для PHPMailer
 * (https://code.google.com/a/apache-extras.org/p/phpmailer/).
 *
 * Позволяет конфигурировать аккаунты, использовать представления,
 * парсит изображения в шаблонах, автоматически добавляя их в аттачменты.
 */
class MailerComponent extends ExtCoreApplicationComponent
{
    /**
     * Список аккаунтов с настройками для PHPMailer.
     * array('koshevy@gmail.com'=>array(...))
     *
     * В каждом аккаунте могут быть следующие настройки:
     * [type] => smtp|mail|sendmail
     * [hml] => true|false
     * [view] => '//mail/price' - используемое представление
     * [attachments] => привязанные файлы
     * [options] => array(...) - настройки PHPMailer.
     *
     * @var array
     */
    public $accounts = array();

    /**
     * @var PHPMailer
     */
    protected $_phpMailer = NULL;

    /**
     * Обращение к PHPMailer-объекту.
     * @return PHPMailer
     */
    public function getPhpMailer(){
        return $this->_phpMailer;
    }

    public function init(){
        Yii::loadExtConfig($this, __FILE__);
        return true;
    }


    /**
     * Отправка сообщения.
     * @param array $data - данные отправки:
     * to, cc, bcc, - адреса получателей (строка или массив)
     * subject - тема письма
     * content - содержимое (текст или массив значений, если используется представление)
     *
     * @param string $account_id - ID аккаунта из конфига
     * @param array $confing - дополнительные настройки (или все настройки,
     * если аккаунт не указан).
     */
    public function sendMessage($data, $account_id, $confing = array())
    {
        $mailerConfig = array_merge(
            $account_id?$this->accounts[$account_id]:array(), $confing);

        Yii::import(Yii::localExtension('mailer').'.lib.*');

        $this->_phpMailer = new PHPMailer();
        if(isset($mailerConfig['options']))
            foreach($mailerConfig['options'] as $option => $value){
                $this->_phpMailer->$option = $value;}

        // кому
        if(isset($data['to'])){
            if(!is_array($data['to'])) $data['to'] = explode (',', $data['to']);
            foreach($data['to'] as $to) $this->_phpMailer->AddAddress(trim($to));
        }
        if(isset($data['cc'])){
            if(!is_array($data['cc'])) $data['cc'] = explode (',', $data['cc']);
            foreach($data['cc'] as $cc) $this->_phpMailer->AddBCC(trim($cc));
        }
        if(isset($data['bcc'])){
            if(!is_array($data['bcc'])) $data['bcc'] = explode (',', $data['bcc']);
            foreach($data['bcc'] as $bсс) $this->_phpMailer->AddBCC(trim($bсс));
        }

        // тема письма
        if(isset($data['subject']))
            $this->_phpMailer->Subject =  $data['subject'];

        // тип отправки
        if(isset($mailerConfig['type'])){
            $mailerConfig['type'] = strtolower($mailerConfig['type']);
            if($mailerConfig['type'] == 'smtp'){
                $this->_phpMailer->IsSMTP(true);
                $this->_phpMailer->SMTPAuth = true;
            }
            if($mailerConfig['type'] == 'mail') $this->_phpMailer->IsMail(true);
            if($mailerConfig['type'] == 'sendmail') $this->_phpMailer->IsSendmail(true);
        }

        if(isset($mailerConfig['html']) && $mailerConfig['html'])
            $this->_phpMailer->IsHTML(true);

        // текст
        if(isset($mailerConfig['view']) && $mailerConfig['view'])
            $text = Yii::app()->controller->renderPartial(
                $mailerConfig['view'], $data, true);
        else $text = $data['content'];

        $this->_parseImages($text);
        $this->_phpMailer->Body = $text;

        // привязанные файлы
        $attachments = array_merge(
            (isset($mailerConfig['attachments'])?$mailerConfig['attachments']:array()),
            (isset($data['attachments'])?$data['attachments']:array())
        );

        foreach($attachments as $name => $adress)
            $this->_phpMailer->AddAttachment($adress, $name);

        return $this->_phpMailer->Send();
    }

    /**
     * Поиск изображений и добавление их в привязанные файлы.
     * @param string $text
     */
    protected function _parseImages(&$text)
    {
        $_phpMailer = $this->_phpMailer;
        $text = preg_replace_callback('/\<img.*?src="(?P<src>[^"]*)"/',
            function($matches) use($_phpMailer){
                static $count = 0;
                $name = "image_$count"; $count++;

                $imagePath = trim($matches['src']);
                $imagePath = preg_replace("/(http:\/)?\/".$_SERVER['HTTP_HOST']."/",
                    $_SERVER['DOCUMENT_ROOT'], trim($matches['src']), 1);

                // Предупреждение о том что файл не существует.
                // Необходимо чтобы диагностировать ошибку в верстке макета.
                if(!is_file($imagePath)) throw new CException(
                    Yii::t('users', 'File in message body not found on local server!'));

                if(function_exists('mime_content_type'))
                    $mimeType = mime_content_type($imagePath);
                else
                {
                    $pathInfo = pathinfo($imagePath);
                    $mimeType = 'image/'.$pathInfo['extension'];
                }

                $_phpMailer->AddEmbeddedImage($imagePath, $name, $name, 'base64', $mimeType);
                return "<img src=\"cid:$name\" ";
            }, $text);

    }

}

?>
