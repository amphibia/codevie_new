<?php

/**
 * @package vote
 * @author slaik
 *
 * Валидатор для проверки полей модели, к которым привязывается VoteFormElement.
 * Необходимо использовать их в паре.
 *
 */
class VoteValidator extends CValidator
{
    public $skipOnError = true;

    /**
     * ID компонента управления закачками (на случай, если компонент будет
     * добавлен под другим ID).
     * @var type
     */
    public $uploadsComponentID = 'upload';

    /**
     * Минимальное количество изображений.
     * @var int
     */
    public $min = NULL;

    /**
     * Максимальное количество изображений.
     * @var int
     */
    public $max = NULL;

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        $validator = & $this;
        if (!is_array($object->$attribute)) {
            $object->$attribute = array();
        }

        if (isset($_POST['Vote_answer'])) {
            foreach ($_POST['Vote_answer'] as $key => $answer) {
                if (!$answer) {
                    unset($_POST['Vote_answer'][$key]);
                    unset($_POST['Vote_votes'][$key]);
                }
            }
            $object->$attribute = array_combine($_POST['Vote_answer'], $_POST['Vote_votes']);
        }


        if (isset($_POST['Vote_votes'])) {
            foreach ($_POST['Vote_votes'] as $key => $votes) {
                if (!$votes) {
                    $_POST['Vote_votes'][$key] = 0;
                } elseif (!is_numeric($votes)) {
                    $object->addError($attribute, Yii::t('vote', 'Number of votes must be numeric'));
                    return false;
                }
            }
            $object->$attribute = array_combine($_POST['Vote_answer'], $_POST['Vote_votes']);
        }


        //CVarDumper::dump($_POST,2,1);


    }

}
