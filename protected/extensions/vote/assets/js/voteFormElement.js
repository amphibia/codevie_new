$(document).ready(function () {
    $(".voteFormElement button.add").click(function () {
        parent = $(this).parent();
        objectToClone = parent.find('.control-group.to_clone').clone();
        objectToClone.removeClass('to_clone').find('input').each(function () {
            $(this).removeAttr('disabled');
        });
        objectToClone.removeClass('to_clone').insertBefore($(this)).show();

        $('[rel="tooltip"]').tooltip({trigger: 'hover'});
        return false;
    });


    $(".voteFormElement button.delete").live('click', function () {
        var parent = $(this).parent();

        var voteDeleted = $(this).closest('.voteFormElement')
            .find('.voteDeleted.to_clone').clone();
        var parentClone = parent.clone();

        voteDeleted.removeClass('to_clone').
            find('.cancel').click(function () {
                parentClone.find('.tooltip').remove();
                voteDeleted.replaceWith(parentClone);
                $('[rel="tooltip"]').tooltip({trigger: 'hover'});
            });

        parent.replaceWith(voteDeleted.show());

        return false;
    });

    $(".voteFormElement").sortable({
        handle: ".move",
        placeholder: "votePlaceholder well",
        cancel: 'button'
    });
});