<?php
if (count($this->category->pages)) {
    ?>
    <div <?= CHtml::renderAttributes($this->htmlOptions) ?>>
        <?php
        if ($this->title) {
            echo CHtml::tag('h2', array('class' => 'widgetTitle'), $this->title);
        }

        foreach ($this->category->pages as $vote) {
            $voteData = $vote->{$this->field};
            // Проверка проголосовал ли пользователь
            $tpl = '_unVotedElement';
            if (isset(Yii::app()->request->cookies['vote_' . $vote->id])) {
                $tpl = '_votedElement';
            }
            $this->render($tpl, compact('vote', 'voteData'));
        }
        ?>
    </div>
<?php
}
?>