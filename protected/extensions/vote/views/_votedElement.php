<?php
echo CHtml::openTag('div', array('class' => 'voteWidget_vote results'));
echo CHtml::tag('h3', array('class' => 'voteWidget_answer'), $vote->title);
echo CHtml::openTag('div', array('class' => 'voteWidget_answers'));
$totalVotes = 0;
foreach ($voteData as $answer => $votes) {
    $totalVotes += $votes;
}
foreach ($voteData as $answer => $votes) {
    $percent = 0;
    $percent = round($votes * 100 / $totalVotes);
    echo CHtml::openTag('div', array('class' => 'answerItem'));
    echo CHtml::tag('span', array('class' => 'percent'), $percent . '%');
    echo CHtml::tag('span', array('class' => 'votes'), $votes);
    echo CHtml::tag('span', array('class' => 'answer'), $answer);
    echo CHtml::closeTag('div');
}
echo CHtml::tag('div', array('class' => 'totalVotes'), $totalVotes . ' ' . Yii::t('vote', 'answer|answers', $totalVotes));
echo CHtml::closeTag('div');
echo CHtml::closeTag('div');