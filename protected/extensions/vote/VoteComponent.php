<?php

/**
 * @title Vote component
 * @description { Component for voting widget }
 * @preload yes
 * @vendor slaik
 * @package vote
 * @requirements{}
 *
 * Компонент, содержащий регистрацию контроллера VoteController
 *
 */
class VoteComponent extends ExtCoreApplicationComponent
{
    public $salt = '/_%_VOTE_SALT_DKSISD81313111';

    public function init()
    {
        Yii::app()->controllerMap['vote'] = array(
            'class' => Yii::localExtension(basename(__DIR__), 'VoteController'),
        );

        return true;
    }
}