<?php

class VoteController extends ExtController
{

    /**
     * Название поля, к которому прикреплено голосование
     * @var string
     */
    public $field = null;

    public function actionIndex()
    {
        ExtCoreJson::error('wrong request');
        ExtCoreJson::response();
    }

    public function actionVote()
    {
        // Проверка параметров
        $hash = Yii::app()->request->getPost('hash');
        $voteID = Yii::app()->request->getPost('voteID');
        $this->field = Yii::app()->request->getPost('field');
        $answer = Yii::app()->request->getPost('answer');
        if (!$voteID) {
            ExtCoreJson::error('Not set voteID');
            ExtCoreJson::response();
        }
        if (!$this->field) {
            ExtCoreJson::error('Not set field');
            ExtCoreJson::response();
        }
        // Проверка совпадения хеша
        if ($hash != hash('sha256', $voteID . $this->field . Yii::app()->vote->salt)) {
            ExtCoreJson::error('The hash is invalid. Please reload the page!');
            ExtCoreJson::response();
        }
        $vote = StructPage::model()->findByPk($voteID);
        if (!$vote) {
            ExtCoreJson::error(Yii::t('vote', 'Vote #{voteID} not found!', array('{voteID}' => $voteID)));
            ExtCoreJson::response();
        }
        if ($answer === null) {
            ExtCoreJson::error(Yii::t('vote', 'Please select an answer!'));
            ExtCoreJson::response();
        }

        $voteData = $vote->{$this->field};

        // Процедура голосования
        if (!isset(Yii::app()->request->cookies['vote_' . $voteID])) {
            if (!$vote->issetCustomField($this->field)) {
                ExtCoreJson::error('There is no "' . $this->field . '" field.');
                ExtCoreJson::response();
            }
            // Пишем голос в поле
            if (is_array($voteData)) {
                $answers = array_keys($voteData);
                $answer = $answers[$answer];
                $voteData[$answer]++;
                $vote->{$this->field} = $voteData;
            } else {
                ExtCoreJson::error('The data in the "' . $this->field . '" is not array.');
                ExtCoreJson::response();
            }
            $vote->save(false);

            // Сохраняем в куки что человек проголосовал в этом голосовании
            $cookie = new CHttpCookie('vote_' . $voteID, 1);
            $cookie->expire = time() + 10000000000;
            Yii::app()->request->cookies['vote_' . $voteID] = $cookie;
        } else {
            ExtCoreJson::error(Yii::t('vote', 'You have already voted!'));
            ExtCoreJson::response();
        }

        // Отображение результата
        $this->render('_votedElement', compact('vote', 'voteData'));

    }
}