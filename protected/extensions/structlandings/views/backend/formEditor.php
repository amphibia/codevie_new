<? $value = &$this->value ?>

<? require __DIR__ . '/_formTeaser.php'; ?>

<div id="<?= $this->id ?>"
     class="struct-form-editor<? if(isset($this->model->errors[$this->attribute])){ ?> open<? } ?>"
     default-fields="<?= $this->value["defaultFields"]?'true':'false'?>"
     data-max-fields="<?= $this->maxFieldsNumber ?>">

    <div class="form-options">
        <div class="control-group">
            <label for="<?= $attrPrefix ?>[title]"><?= Yii::t('structlandings', 'Заголовок формы') ?></label>
            <input  placeholder="<?= Yii::t('structlandings', 'Без заголовка') ?>"
                    class="span6"
                    maxlength="255"
                    data-attr="title"
                    name="<?= $attrPrefix ?>[title]" type="text"
                    value="<?= $this->value['title'] ?>"
                />

            <?php $this->_showErrorDetails('title'); ?>
        </div>

        <div class="control-group">
            <label for="<?= $attrPrefix ?>[description]"><?= Yii::t('structlandings', 'Предисловие к форме') ?></label>
            <textarea
                    placeholder="<?= Yii::t('structlandings', 'Без предисловия') ?>"
                    class="span6"
                    maxlength="512"
                    data-attr="description"
                    name="<?= $attrPrefix ?>[description]"
                    type="text"
                ><?= $this->value['description'] ?></textarea>

            <?php $this->_showErrorDetails('description'); ?>
        </div>

        <div class="control-group">
            <label for="<?= $attrPrefix ?>[theme]"><?= Yii::t('structlandings', 'Тема письма') ?></label>
            <input  placeholder="<?= Yii::t('structlandings', 'Тема по-умолчанию') ?>"
                    class="span6"
                    maxlength="255"
                    data-attr="theme"
                    name="<?= $attrPrefix ?>[theme]"
                    type="text"
                    value="<?= $this->value['theme'] ?>"
                />

            <?php $this->_showErrorDetails('theme'); ?>
        </div>
        <div class="hint-bottom">
            <p><?= Yii::t('structlandings', 'Тема по умолчанию настраивается при разработке сайта и ее смена не предполагается; обычно, это что-то доволно общее, вроде "Отправка с формы".'); ?></p>
            <p><?= Yii::t('structlandings', 'Указывайте тему для каждого типа формы, чтобы ориентироваться в потоке заявок.'); ?></p>
        </div>

        <div class="control-group">
            <label for="<?= $attrPrefix ?>[buttonTitle]"><?= Yii::t('structlandings', 'Текст кнопки отправки формы') ?></label>
            <input  placeholder="<?= Yii::t('structlandings', 'Текст по-умолчанию') ?>"
                    class="span6"
                    maxlength="255"
                    data-attr="theme"
                    name="<?= $attrPrefix ?>[buttonTitle]"
                    type="text"
                    value="<?= $this->value['buttonTitle'] ?>"
                />

            <?php $this->_showErrorDetails('buttonTitle'); ?>
        </div>

        <div class="control-group">
            <label for="<?= $attrPrefix ?>[email][]"><?= Yii::t('structlandings', 'Получатели заявок (эл. адреса):') ?></label>
            <div class="repeat-field-blank">
                <input  placeholder="<?= Yii::t('structlandings', 'Введите нового адресата') ?>"
                        class="span3"
                        maxlength="128"
                        data-attr="email"
                        name="<?= $attrPrefix ?>[email][]"
                        type="email"
                    />
            </div>
            <?php if(sizeof($this->value['email'])): ?>
                <?php foreach($this->value['email'] as $index => $email): ?>
                    <div class="repeat-field">
                        <input  placeholder="<?= Yii::t('structlandings', 'Введите нового адресата') ?>"
                                class="span3"
                                maxlength="128"
                                data-attr="email"
                                name="<?= $attrPrefix ?>[email][]"
                                type="email"
                                value="<?php echo $email ?>"
                            />
                    </div>
                    <?php $this->_showErrorDetails('email', $index); ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="hint-bottom">
            <p><?= Yii::t('structlandings', 'По умолчанию, заявки с формы отправляются на эл. адреса, указанные в Настройки/Отправка писем.'); ?></p>
        </div>

        <input type="hidden" name="<?= $attrPrefix ?>[defaultFields]" class="default-fields-state" value="<?= $this->value["defaultFields"]?'1':'0'?>" />
        <? require __DIR__ . "/_fieldsEditor.php"; ?>

    </div>

    <div class="less">
        <a><?= Yii::t('structlandings', 'Свернуть настройки полей формы') ?></a>
    </div>

    <br/>

</div>

<?
Yii::app()->clientScript->registerScript("formEditorInit__{$this->id}", <<<EOT
    // инициализация JS-плагина для конкретного виджета
    // (предполагается, что виджетов может быть много)
    $(document).ready(function(){
        $('#{$this->id}').structFormEmailField_init();
    });
EOT
    , CClientScript::POS_READY
);
