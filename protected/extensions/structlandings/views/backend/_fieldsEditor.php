<div class="control-group">
    <div class="well span6">
        <h2 class="sectionHeader"><?= Yii::t('structlandings', 'Настройка полей формы') ?></h2>

        <div class="no-fields">
            <h3>
                <?= Yii::t('structlandings', 'Нет полей / набор полей по умолчанию:'); ?>
            </h3>
            <dl>
                <? foreach($this->value['fields'] as $field): ?>
                    <dt><?= $field['title'] ?>:</dt>
                    <dd><?= $presetsList[$field['preset']] ?></dd>
                <? endforeach ?>
            </dl>

            <button class="edit-default-fields btn btn-success btn-large" type="button">
                <i class="icon-th-list icon-white"></i>
                <?= Yii::t('structlandings', 'Изменить список полей') ?>
            </button>

        </div>

        <div class="fields-editor">
            <ul>
                <li class="deleted alert bill">
                    <?= Yii::t('structlandings', 'Поле удалено.') ?>
                    <a class="restore"><?= Yii::t('structlandings', 'Восстановить') ?></a>
                </li>
                <?
                // заготовка для динамического создания новых полей
                $this->_echoFieldEditorItem(NULL, NULL, $presetsList, $attrPrefix, true);

                // вывод элементов, соотвтетствующих полям
                foreach($this->value['fields'] as $key => $field){
                    $this->_echoFieldEditorItem($key, $field, $presetsList, $attrPrefix);
                }
                ?>
            </ul>
            <div class="no-fields-in-list alert">
                <i class="icon-info-sign"></i>
                <strong><?= Yii::t('structlandings', 'Обратите внимание!') ?></strong>
                <div>
                    <?= Yii::t('structlandings', 'В списке полей не осталось ни одного поля. После сохранения данных, настройки полей будут сброшены и будет применяться конфигурация полей формы по-умолчанию.') ?>
                </div>
            </div>
            <button class="add-field-controll btn btn-success" type="button">
                <i class="icon-plus-sign icon-white"></i>
                <?= Yii::t('structlandings', 'Добавить поле') ?>
            </button>
            <button class="default-fields-controll btn btn-danger" type="button">
                <i class="icon-ban-circle icon-white"></i>
                <?= Yii::t('structlandings', 'Настройки полей по-умолчанию') ?>
            </button>
        </div>
    </div>
</div>