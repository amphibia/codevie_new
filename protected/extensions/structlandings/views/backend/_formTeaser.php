<? if(!isset($this->model->errors[$this->attribute])): ?>
    <div class="struct-form-teaser alert alert-success" rel="<?= $this->id ?>">
        <div class="illustration"></div>
        <dl>
            <dt><?= Yii::t('structlandings', 'Заголовок формы:') ?></dt>
            <dd data-attr="title" data-empty-value="<?= $emptyValue = Yii::t('structlandings', 'Без заголовка') ?>">
                <?= $value['title'] ? $value['title'] : $emptyValue ?>
            </dd>

            <dt><?= Yii::t('structlandings', 'Предисловие к форме:') ?></dt>
            <dd data-attr="description" data-empty-value="<?= $emptyValue = Yii::t('structlandings', 'Без предисловия') ?>">
                <?= $value['description'] ? $value['description'] : $emptyValue ?>
            </dd>

            <dt><?= Yii::t('structlandings', 'Тема письма:') ?></dt>
            <dd data-attr="theme" data-empty-value="<?= $emptyValue = Yii::t('structlandings', 'Тема по-умолчанию') ?>">
                <?= $value['theme'] ? $value['theme'] : $emptyValue ?>
            </dd>

            <dt><?= Yii::t('structlandings', 'Получатели заявок (эл. адреса):') ?></dt>
            <? $emptyValue = Yii::t('structlandings', 'По-умолчанию'); ?>
            <dd data-attr="email" data-empty-value="<?= htmlspecialchars($emptyValue) ?>">
                <?
                echo ($value['email'] && sizeof($value['email']))
                    ? implode(', ', $value['email'])
                    : $emptyValue
                ?>
            </dd>
        </dl>
        <div class="more">
            <a><?= Yii::t('structlandings', 'Подробнее / Редактировать') ?></a>
        </div>
    </div>
<? endif ?>