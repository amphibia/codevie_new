<?php

/**
 * @package structlandings
 * @author koshevy
 * 
 * Валидатор данных от редактора полей.
 *
 * Работает как промежуточное звено, получающее данные от формы,
 * в том виде, в котором удобнее организовать в HTML-виде.
 *
 * Далее, приводит их формат данных, описанный в хэлпере FormEditorHelper,
 * и проверяет в соответствии с правилами модели LandingForm.
 *
 */
class FormDataValidator extends CValidator
{
    /**
     * Информация об ошибках в данных, полученных от виджета редактора форм.
     *
     * Формат информации об ошибке:
     *
     * сгруппированы по типу ошибки (title, description, theme, email, fieldData)
     * внутри группы ключ означает индекс элемента, которому соответствует ошибка (например [2=>[], 6=>[]]
     *
     * @var array
     */
    public $errorInfo = array(
        'title' => array(),
        'description' => array(),
        'theme' => array(),
        'email' => array(),
        'fieldData' => array(),
        'buttonTitle' => array(),
    );


    /**
     * Максимальная длина описания заголовка.
     * @var int
     */
    public $maxTitleLength = 72;

    /**
     * Максимальная длина описания формы.
     * @var int
     */
    public $maxDescriptionLength = 512;

    /**
     * Максимальная длина поля "заголовок" в настройках полей.
     * @var int
     */
    public $maxFieldTitleLength = 256;

    /**
     * Максимальная длина поля "аргументы" в настройках полей.
     * @var int
     */
    public $maxFieldArgumentsLength = 255;


    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        $this->_normalizePostData($object, $attribute);

        $data = $object->$attribute;

        // длина заголовка
        if($data['title'] && (mb_strlen($data['title'], 'utf8') > $this->maxTitleLength))
            $this->_addErrorDetails('title', Yii::t(
                'structlandings',
                'Длина заголовка формы не должна превышать {maxLength} символа',
                array('{maxLength}' => $this->maxTitleLength)
            ));

        // длина описания
        if($data['description'] && ((mb_strlen($data['description']) > $this->maxDescriptionLength)))
            $this->_addErrorDetails('description', Yii::t(
                'structlandings',
                'Длина описания формы не должна превышать {maxLength} символов',
                array('{maxLength}' => $this->maxDescriptionLength)
            ));

        // проверка эл. адресов
        if(isset($data['email']) && $data['email']){
            if(!is_array($data['email'])){
                $object->addError($attribute, Yii::t(
                    'structlandings',
                    'Данные об эл. адресах имеют неверный формат: возможно произошел сбой. Обновите страницу, и попробуйте еще раз.'
                ));

                return;
            }
            else
            {
                // проверка всех указанных электронных адресов
                foreach($data['email'] as $index => $email)
                {
                    if(!preg_match('/^([\w\d\-_]+\.?)+@([\w\d\-_]+\.?)+(\.\w{2,10})?$/', $email))
                        $this->_addErrorDetails('email', Yii::t(
                            'structlandings',
                            'Неправильный формат эл. адреса.'
                        ), $index);

                    if(mb_strlen($data['title']) > $this->maxDescriptionLength)
                        $this->_addErrorDetails('email', Yii::t(
                            'structlandings',
                            'Слишком длинный электронный адрес.'
                        ), $index);
                }
            }
        }

        // проверка полей
        if(!$data['defaultFields'])
        {
            $presets = FormEditorHelper::getRulePresets();
            $index = 0; // поскольку массив ассоциативный, индекс считается отдельно
            foreach($data['fields'] as $field)
            {
                // наличие заголовка
                if(!$field['title'])
                    $this->_addErrorDetails('fieldData', Yii::t(
                        'structlandings',
                        'Обязательно введите название поля'
                    ), $index);

                // максимальная длина заголовка
                if(mb_strlen($field['title']) > $this->maxFieldTitleLength)
                    $this->_addErrorDetails('fieldData', Yii::t(
                        'structlandings',
                        'Длина названия не должна превышать {maxLength} символа',
                        array('{maxLength}' => $this->maxFieldTitleLength)
                    ), $index);

                // максимальная длина поля "аргументы"
                if(mb_strlen($field['title']) > $this->maxFieldArgumentsLength)
                    $this->_addErrorDetails('fieldData', Yii::t(
                        'structlandings',
                        'Длина поля с аргументами не должна быть длинее {maxLength} символов',
                        array('{maxLength}' => $this->maxFieldArgumentsLength)
                    ), $index);

                // проверка пресета
                if(!isset($presets[$field['preset']]))
                    $this->errorInfo[] = array(
                        'message' => Yii::t(
                                'structlandings',
                                'Указан неизвестный пресет'
                            ),
                        'attribute' => 'fieldData',
                        'index' => $index
                    );

                $index++;
            }
        }

        $object->$attribute = $data;

        foreach($this->errorInfo as $attr){
            if(sizeof($attr))
            {
                $object->addError($attribute, Yii::t('custom',
                    'Некоторые из настроек были указаны неверно'
                ));

                break;
            }
        }
    }


    /**
     * Добавление развернутой информации об ошибке внутри виджета.
     * @param $attribute
     * @param $message
     * @param int $index
     */
    protected function _addErrorDetails($attribute, $message, $index = 0)
    {
        if(!isset($this->errorInfo[$attribute][$index]))
            $this->errorInfo[$attribute][$index] = array();

        $this->errorInfo[$attribute][$index][] = $message;
    }

    /**
     * Получение данных из формата, удобного для POST и превращение их в корректный вид
     * (описываемый в хэлпере  FormEditorHelper и используемого при хранении моделью LandingForm).
     *
     * Данные массива "fields" приходят в несгрупированных массивов, соответствующих полям,
     * с приставкой "fields" (например, "fieldTitle", "fieldRequiredMessage").
     *
     * @param $object
     * @param $attribute
     * @return null
     */
    protected function _normalizePostData($object, $attribute)
    {
        Yii::import(Yii::localExtension('structlandings', 'lib.*'));

        $postData = $object->$attribute;
        $dataBlank = FormEditorHelper::getFormDataBlank();
        $completeData = array();

        // нормализация обычных данных (кроме данных полей)
        foreach(array_keys($dataBlank) as $formOption){
            if($formOption != 'fields')
                $completeData[$formOption] = $postData[$formOption];
        }

        // Нормализация данных с полями (данные из POST, начинающиеся
        // с префекса "field" будут сгруппированы в поле "fields")
        $completeData['fields'] = array();
        $completeData['defaultFields'] = (bool)$completeData['defaultFields'];

        // и наоборот, если стоит пометка "использовать поля по-умолчанию",
        // данные о пришедших полях игнорируются
        if(!$completeData['defaultFields'])
        foreach(array_keys($dataBlank['fields'][0]) as $fieldOption){
            $postKey = "field".ucfirst($fieldOption);
            if(isset($postData[$postKey]) && is_array($postData[$postKey])){
                foreach($postData[$postKey] as $index => $value){
                    if(!isset($completeData['fields']["field_{$index}"]))
                        $completeData['fields']["field_{$index}"] = array();

                    $completeData['fields']["field_{$index}"][$fieldOption] = $value;
                }
            }
        }

        FormEditorHelper::normalizeData($completeData);

        $object->$attribute = $completeData;
    }

    /**
     * Вызов функции проверки на стороне клиента.
     * @param CModel $object
     * @param string $attribute
     * @return string|void
     */
    //public function clientValidateAttribute($object,$attribute)
    //{
    //    return "console.log(value, messages, attribute)";
    //}
}
