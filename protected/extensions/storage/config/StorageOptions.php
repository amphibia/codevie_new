<?php

/**
 * @var array Конфиг закачки.
 */    
return array
(
    // ID контроллера для доступа к хранилищу
    // Примечание: также, ID контроллера нужно изменить вручную далее в свойствах
    // 'rules' и 'urlParseRules'!
    "controllerID" => "storage",

    
    // директория - хранилище изображений
    "storageDir" => "/uploads",
    
    // директория для кэша
    "cacheDir" => "/_cache",

    // поддериктория для конкретных задачи/пользователя 
    "subdir"    => Yii::app()->hasComponent('user')
    	? ((Yii::app()->user->isGuest) ? "common" : 'user_' . Yii::app()->user->id)
    	: 'common',


	// правило корректного имени файла
	// @var string
	'fileNameRule' => ($fileNameRule = '[a-zA-Z0-9_]{1,64}\.(\w{3,5})(\|\-?\d{1,3}\|\-?\d{1,3}\|\d{1,3}\|\d{1,3}\|[a-zA-Z0-9]{8,96})?'),

	// правила извлечения аргументов из коммандной строки
	"rules" => array
	(
		//'storage/<method:\w+><args:.*>' => 'storage/index',
		'storage' => 'storage/index',
		'storage/<method:\w+>/<context:\w+>/<preset:\w+>/<filename:'.$fileNameRule.'>' => 'storage/<method>',
		'storage/<method:\w+>/<temp:_temp>/<context:\w+>/<preset:\w+>/<filename:'.$fileNameRule.'>' => 'storage/<method>',

		'storage/<method:\w+>/<context:\w+>/w<width:\d{1,4}>/<filename:'.$fileNameRule.'>' => 'storage/<method>',
		'storage/<method:\w+>/<context:\w+>/h<height:\d{1,4}>/<filename:'.$fileNameRule.'>' => 'storage/<method>',
		'storage/<method:\w+>/<context:\w+>/w<width:\d{1,4}>/h<height:\d{1,4}>/<filename:'.$fileNameRule.'>' => 'storage/<method>',
		'storage/<method:\w+>/<context:\w+>/x<x:\d{1,4}>/y<y:\d{1,4}>/w<width:\d{1,4}>/h<height:\d{1,4}>/<filename:'.$fileNameRule.'>' => 'storage/<method>',
	),

	// отложенна функция, определяющая, можно ли обрабатывать данные в соответствии
	// с аргументами вне зависимости от пресетов
	'forcePreset' => function(){ return false; },

	// голые настройки
	"blankPreset" => array("width" => NULL, "height" => NULL, "x" => NULL, "y" => NULL, "side" => NULL),

    // водяной знак (если не нужен - делается FALSE)
    "watermark" => array
    (
        /** @TODO: функция наложения водяных знаков не реализована */
        
        // файл водяного знака
        $_SERVER["DOCUMENT_ROOT"]."/images/watermark.png",

        // пресеты, к которым применяется (если нет таких, оставьте array)
        "presets" => array("default", "newsGallery")
    ),

    // пресеты изменения файлов
    "presets" => array
    (
		// здесь только стандартные размеры
		// дополнительные размеры указывайте в общем конфиге или динамически -
		// в подключаемых зависимых конпонентов
		'default' => array(),
		'original' => array(),
		'avatar' => array('width' => 26, 'height' => 26),
		'avatarLarge' => array('width' => 42, 'height' => 42),
		'uploadPreview' => array('width' => 96, 'height' => 96),
		'cropPreview' => array('side' => 450),
		'_1to2_small' => array('width' => 96, 'height' => 192),
		'_2to1_small' => array('width' => 192, 'height' => 96),
		'_long_small' => array('width' => 230, 'height' => 96),
    ),

    // правила интерпретации оригинальных URI файла
    // Примечание: 'STORAGE_DIR' в правилах будет заменено на storageComponent::$storageDir,
    // на '/' на DIRECTORY_SEPARATOR.
    'urlParseRules' => array(
        'rule' => '@^(?>www\.)?(?P<host>'.str_replace('www.', '', $_SERVER['HTTP_HOST']).')?(?P<storageDir>STORAGE_DIR)?(?>/(?P<tmpDir>_temp))?/(?P<context>\w+)/(?P<filename>'.$fileNameRule.')$@',
		/*'rule' => '@^(?P<host>'.$_SERVER['HTTP_HOST'].')?(?P<storageDir>STORAGE_DIR)?(?>/(?P<tmpDir>_temp))?/(?P<context>\w+)/(?P<filename>.+\.\w{2,5})$@'*/

    ),

	// псевдонимы используемого хоста
	'hostAliases' => array(),

	/**
	 * Соль для формирования подписи.
	 * @var string
	 */
	'signatureSalt' => NULL,

	/**
	 * @var Регульярное выражение для извлечения аргументнов кадрирования из
	 * сохраненного имени файла.
	 */
	'attributesParseReg' => '/^(?P<fileName>.*)\|(?P<x>\-?\d{1,3})\|(?P<y>\-?\d{1,3})\|(?P<width>\d{1,3})\|(?P<height>\d{1,3})\|(?P<signature>[a-zA-Z0-9]{8,96})$/'

);
