<?php

/**
 *  Контроллер обращения к хранилищу изображений.
 */
class StorageController extends ExtController
{
    /**
     * Компонент, с которым работает контроллер.
     * @var storageComponent
     */
    public $storage = NULL;

    protected $_useAutoArguments = false;

    /**
     * Файлы в локальном хранилище.
     */
    public function actionLocal(
        $context = NULL, $preset = NULL,$filename = NULL,
        $width = NULL, $height = NULL, $x = NULL, $y = NULL, $temp = NULL)
	{
		$period = 3600*72;
		$lastModifiedTime = floor(time()/$period) * $period;
		$expiresTime = ceil(time()/$period) * $period;

		header("Cache-control: public, max-age={$period}, s-maxage={$period}, no-transform");
		header("Pragma: public");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModifiedTime) . " GMT");
		header("Last-Expires: " . gmdate("D, d M Y H:i:s", $expiresTime) . " GMT");

		
        if($temp) $temp = "/$temp";
        $arguments = get_defined_vars();

		// запрос может содержать указание нужного формата,
		// либо параметры
		if($preset)
		{
			// обращение к требуемому формату
			if(isset($this->storage->presets[$preset])){
				$arguments = array_merge($this->storage->blankPreset, $arguments);
				$arguments = array_merge($arguments, $this->storage->presets[$arguments["preset"]] );
			} else throw new CHttpException(404);
		}
		else

		// проверка существования пресета с запрашиваемыми настройками
		if(!$preset = $this->storage->presetExists($arguments)) throw new CHttpException(404);

		if($preset === true) $presetDirectory = (($x&&$y)?"x$x/y$y/":NULL)."w$width/h$height";
		else $presetDirectory = $preset;

		// относительные адреса файлов
		$filename = $this->storage->parseOptionsFileName($filename);
		$filePathOrginal = "{$this->storage->storageDir}{$temp}/{$context}/{$filename}";

		// учет настроек пользовательского кадрирования
		if(isset($_POST['UploadsFormElement']) && isset($_POST['UploadsFormElement'][$optionsKey = md5($filename)]))
		{
			// формирования слепка настроек
			$optionsSumm = NULL;
			foreach($_POST['UploadsFormElement'][$optionsKey] as $key => $value)
				if($value) $optionsSumm .= $key.$value;

			if($optionsSumm)
			{
				$optionsSumm = md5($optionsSumm);
				$presetDirectory .= "/$optionsSumm";

				$this->storage->userCropOptions = $_POST['UploadsFormElement'][$optionsKey];
			}
		}

		$filePathCache = "{$this->storage->cacheDir}{$temp}/{$context}/{$presetDirectory}/{$filename}";

		// требуется файл без предобработки: перекидывает напрямую на файл
        if($preset === "original"){
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: $filePathOrginal");
            exit;
        }
        // требуется файл с предобработкой: кэш найден
        else if(is_file($_SERVER["DOCUMENT_ROOT"].$filePathCache)){
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: $filePathCache");
            exit;
        }
        // требуется файл с предобработкой: кэша нет
        else $this->storage->createCacheLocal(
            $filePathOrginal, $filePathCache, $arguments, $preset);
    }

    
    /**
     * Индекс запрещён
     */
    public function actionIndex(){
        throw new CHttpException(403,
            Yii::t("storage", "Storage is forbidden."));
    }


}