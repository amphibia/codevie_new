<?php
Yii::import(Yii::localExtension('mtreeview', '*'));

/**
 * @author koshevy
 * @package mtreeview
 * 
 * Дерево с сортировкой (за основу взято свободное расширение Yii MTreeView).
 * Т.к. виджет имеет свойство 'isSortable', которое может быть отключено,
 * предпочтительней использовать этот виджет даже когда сортировка не нужна.
 *
 * Для осуществления перемещения элемента (со стороны сервера), в клиентскую
 * часть отправляется data('moveItemUrl'), привязанное к корневому элементу дерева.
 * 
 * 
 * Пример использования:
 * $this->widget(Yii::localExtension('mtreeview', 'MTreeViewSortable', __FILE__),array(
 *     'model' => $newsCategory,
 * ));
 * 
 * Чтобы реализовать обратную связь, т.е. сохранение позиции элемента после
 * перетаскивания, нужно добавить к используемому контроллеру действие actionMoveItem
 * (роут действия можно изменить в свойстве $itemMoveAction).
 * Действие принимает из поста аргументы id, beforeID, afterID, appendTo из POST.
 * В сервисе можно указывать прямо в действии:
 * actionMoveItem($id, $beforeID = NULL, $afterID = NULL, $appendTo = NULL).
 * 
 * Значение, указаноое в $itemMoveAction доступно со стороны клиента в  data('moveItemUrl'),
 * привязанном к корневому элементу.
 * 
 * В виджете можно настроить динамическую перерисовку при изменениях (@see $repaintOnChanges).
 * 
 * Более подробно о настрйках смотрите в документации к свойствам класса.
 * Описание базового функционала MTreeView (активное DB-дерево) смотрите тут:
 * http://www.yiiframework.com/extension/mtreeview/.
 */
class MTreeViewSortable extends MTreeView
{
    /**
     * Включена ли DRAG-N-DROP - сортировка элементов.
     * @var boolean
     */
    public $isSortable = NULL;

    /**
     * Роут для формирования ссылки на действие, перемещающее запись внутри дерева.
     * Передаёт следующие аргументы: id, beforeID, afterID, appendTo.
     * 
     * Аргументы передаются в POST. Если действие-приёмщк находится в сервисе,
     * можно указывать их в аргументах действия вида:
     * actionMoveItem($id, $beforeID = NULL, $afterID = NULL, $appendTo = NULL).
     * @var string
     */
    public $itemMoveAction = NULL;

    /**
     * Условие и параметры отсортировки.
     * Осно
     * По умолчанию - выборка некорневых элементов (только для Nested-моделей).
     * @var array
     */
    public $conditions = array();

    /**
     * Рабочая модель.
     * @var CModel
     */
    public $model = NULL;

    /**
     * ID записи-предка (не имеет значения для Nested-записей).
     * @var string
     */
    public $parentIdField = NULL;

    /**
     * Используемые поля.
     * @var string
     */
    public $fields = array();

    /**
     * Шаблон формирования надписи.
     * @var string
     */
    public $template = NULL;

    /**
     * Свойство, помещающее, что при обновлении структуры дерева требуется
     * перерисовать его.
     * 
     * При перерисовке отправляеся запрос по текущему URL, при этом в POST
     * отправляется 'MTreeViewRepaint'. Если используется динамическая перерисовка,
     * в месте использования виджета необходимо вставит проверку и, в случае
     * обнаружения параметра $_POST['MTreeViewRepaint'], выводить только виджет.
     * @var boolean
     */
    public $repaintOnChanges = NULL;

    /**
     * Скорость анимации (fast/slow).
     * @var mixed
     */
    public $animated = NULL;


    public $assets = array();



    protected $_id = NULL;

    public function init()
    {
        // загрузка конфига по умолчанию
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);
        
        // автоопределение Nested Sets
        if($this->model instanceof ExtCoreNestedAR){
             $this->hierModel = 'nestedSet';

             $this->fields['lft'] = $this->parentIdField;
             $this->fields['lft'] = $this->model->leftAttribute;
             $this->fields['rgt'] = $this->model->rightAttribute;
        }
        else $this->hierModel = 'adjacency';

        $this->_id = "Tree_".get_class($this->model);
        $this->cookieId = 'MTreeViewSortable_'.$this->_id;
        $this->table = $this->model->tableName();

        $this->htmlOptions['id'] = $this->_id;
        if(!isset($this->htmlOptions['class'])) $this->htmlOptions['class'] = 'treeview';
        else $this->htmlOptions['class'] .= ' treeview';
        if($this->isSortable) $this->htmlOptions['class'] .= ' sortable';

        // включение динамической перерисовки
        if($this->repaintOnChanges) $this->htmlOptions['dynamic_repaint'] = 'true';

        $this->_initJsData();
        parent::init();
		

    }
    
    /**
     * Отправка данных на сторону клиента.
     */
    public function _initJsData()
    {
        // создаётся URL действия перемещения элемента (без параметров)
        $params = (Yii::app()->controller instanceof ExtCoreService) ? false : array();
        $moveItemUrl = Yii::app()->controller->createUrl($this->itemMoveAction, $params);
        Yii::app()->clientScript->registerScript('moveItemUrl',
            "$('#{$this->_id}').data('moveItemUrl','$moveItemUrl');", CClientScript::POS_READY);
    }


}
