/**
 * @author koshevy
 * @package mtreeview
 * Клиентская часть для виджета MTreeViewSortable.
 * Реализует DRAG-N-DROP перетаскивание элементов дерева.
 * 
 * События (привязываются к корневому элементу дерева):
 * 'selectItems' - при выборе элементов;
 * 'update' - при соверёшнном перетаскивании.
 * 
 * Чтобы изменить работу дерева, переопределите следующие методы корневого элемента:
 * 'start' - при начале перетаскивания;
 * 'update' - при выполненных изменениях.
 * 
 */

/**
 * Инициализация Drag-n-Drop перетаскивания для всех неинициализированных таблиц.
 * Вызывается при инициализации и перерисовке деревьев.
 */
function initSortable()
{
    // инциализаци я Drag-n-Drop перетаскивания
    $(".treeview.sortable:not(.initialized)").nestedSortable({
        delay:700,
        disableNesting: 'no-nest',
        cancel: '.no-drag',
        forcePlaceholderSize: true,
        handle: 'span',
        helper:    'clone',
        items: 'li',
        maxLevels: 12,
        opacity: .6,
        placeholder:'list-placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> span',
        listType: 'ul',
        start:function(){$('.treeview.sortable li').removeClass('ui-selected')},
        update:function(evt, ui)
        {
            ui.item.addClass('ui-selected');

            var $prev = ui.item.prev();
            var $next = ui.item.next();
            var $parent = ui.item.parent().parent();

            // AJAX-функция перемещения элемента
            var moveItemUrl = $(this).closest('.treeview.sortable').data('moveItemUrl');
            var args = {id:ui.item.attr('id'), beforeID:0, afterID:0, appendTo:0};

            $(this).repaintElements();

            // отправка запроса
            if($next.length){args.beforeID = $next.attr('id');}
            else if($prev.length){args.afterID = $prev.attr('id');}
            else if($parent.length){args.appendTo = $parent.attr('id');}

            var $target = $(this);

            $(this).request(moveItemUrl, args, null, function(){
                $target.repaint();

                // события для внедрения в процесс
                $target.closest('.treeview.sortable').trigger('selectItems');
                $target.closest('.treeview.sortable').trigger('update');
            });

        }
    }).addClass('initialized');
}

/* 
 * Переопределение функционала CTreeView для работы с динамическим изменением
 * содержимого.
 */
;(function($)
{
	// переопределение метода из TreeView, делающее привязку collapsable/expandable
    // по $.live()
	$.extend($.fn, {

        repaintElements: function()
        {
            // удаление ненужных +/- (свернуть/развернуть)
            var li = $('.treeview.sortable ul:not(:has(li))').parent();
                li.find('.hitarea').remove();
                li.removeClass('collapsable');
                li.removeClass('expandable');
                li.removeClass('lastCollapsable');

            // добавление +/- в созданные ветки
            $('.treeview.sortable li:has(ul:has(li)):not(.collapsable):not(.expandable)')
                .addClass('collapsable')
                .prepend('<div class="hitarea collapsable-hitarea"></div>')
                .find();

            // перерисовка направляющих линий
            $('.treeview.sortable li').removeClass('last');
            $('.treeview.sortable, .treeview.sortable ul').each(function(){
                $(this).find('li:last').addClass('last');
            });
        },

        /**
         * Загрузка предыдущего состояния дерева (открытые, закрытые элементы)
         * из кукисов.
         */
        loadCookiesState: function()
        {
            var $tree = $($(this)[0]).closest('.treeview.sortable'),
                collapsableIds = $.cookie($tree.attr('id') + '_collapsable'),
				selectedIds = $.cookie($tree.attr('id') + '_selected');

            // Дерево сайта открывается в первый раз для этого браузера.
            if(collapsableIds === null){
                // открывает все группы
                $tree.find('[type="group"]').each(function(){
                    $(this).closest('li').find('>.hitarea').click();
                });

                // открывает первый элемент, если он не открыт (например, если не группа)
                var $firstHitarea = $tree.find('li').first().find('.hitarea');
                if($firstHitarea.hasClass('expandable-hitarea'))
                    $firstHitarea.click();
            }
            else if(collapsableIds){
                var ids = collapsableIds.split(',');
                var selector = 'ol#u';
                for(var id in ids){
                    selector += ', li#'+ids[id]+'>.hitarea'; }
                $(selector).click();
            }

			$('.ui-selected').removeClass('ui-selected');
            if(selectedIds){
                var ids = selectedIds.split(',');
                var selector = 'ol#u';
                for(var id in ids){
                    selector += ', li#'+ids[id]; }
                $(selector).addClass('ui-selected');
            }
			
			$(this).closest('.treeview.sortable').trigger('selectItems');
        },

        /**
         * Сохранение состояния дерева (открытые/закрытые элементы) в кукисы. 
         **/
        saveCookiesState: function()
        {
			var $tree = $($(this)[0]).closest('.treeview.sortable'),
				collapsableIds = Array(),
				selectedIds = Array();

			$('.treeview.sortable .collapsable[id]').each(function(){
				collapsableIds.push(this.id);});

			$('.treeview.sortable [id].ui-selected').each(function(){
				selectedIds.push(this.id);});

            $.cookie($tree.attr('id') + '_collapsable', collapsableIds);
			$.cookie($tree.attr('id') + '_selected', selectedIds);
            return false;
        },

		applyClasses: function(settings, toggler)
        {
            var CLASSES = $.treeview.classes;
            
			// TODO use event delegation
			this.filter(":has(>ul):not(:has(>a))").find(">span").unbind("click.treeview").bind("click.treeview", function(event) {
                if(!event.ctrlKey && !event.metaKey){
                    // don't handle click events on children, eg. checkboxes
                    if ( this == event.target )
                        toggler.apply($(this).next());
                }
			}).add( $("a", this) ).hoverClass();


			if (!settings.prerendered) {
				// handle closed ones first
				this.filter(":has(>ul:hidden)")
						.addClass(CLASSES.expandable)
						.replaceClass(CLASSES.last, CLASSES.lastExpandable);

				// handle open ones
				this.not(":has(>ul:hidden)")
						.addClass(CLASSES.collapsable)
						.replaceClass(CLASSES.last, CLASSES.lastCollapsable);

	            // create hitarea if not present
				var hitarea = this.find("div." + CLASSES.hitarea);
				if (!hitarea.length)
					hitarea = this.prepend("<div class=\"" + CLASSES.hitarea + "\"/>").find("div." + CLASSES.hitarea);
				hitarea.removeClass().addClass(CLASSES.hitarea).each(function() {
					var classes = "";
					$.each($(this).parent().attr("class").split(" "), function() {
						classes += this + "-hitarea ";
					});
					$(this).addClass( classes );
				})
			}

			// apply event to hitarea
			$("div." + CLASSES.hitarea).live('click', function(){
				toggler.apply(this);
				$(this).saveCookiesState();
				return false;
			});


            // открытые в прошлый раз элементы (из COOKIE)
            $(this).loadCookiesState();
		},
        
        /**
         * Перерисовка (вызывается при изменении структуры дерева или свойств,
         * изменяющих логику отображения дерева).
         * @return bolean
         */
        repaint:function()
        {            
            // сохранение выделенных ID
            var ids = Array();
            $(this).find('.ui-selected').each(function(){
                ids.push($(this).attr('id'));
            });
            
            if(!$(this).attr('dynamic_repaint')) return false;
            $(this).addClass('clientContent');
            $(this).addClass('clientContentReplace');
            
            var $targetParent = $(this).parent(),
                moveItemUrl = $(this).data('moveItemUrl'); // привязанный к элементу URL
			/*
            $(this).request('', {'MTreeViewRepaint':true}, null, function()
            {
                var $target = $targetParent.find('.treeview.sortable:not(.initialized)');
                $target.loadCookiesState();

                initSortable();
                $target.repaintElements();

                // восстановление выделенных элементов
                for(index in ids){
                    $target.find("#"+ids[index]).addClass('ui-selected');
                }
                
                $target.data('moveItemUrl', moveItemUrl);

            });
			*/

            return true;
        }
	});

})(jQuery);


$(document).ready( function()
{
    initSortable();

    //  снятие выделения
    $('.treeview.sortable').click(function(evt){
		if(!$(evt.target).hasClass('treeview')) return true
        var $activeTrees = $('.treeview.sortable:has(.ui-selected)');
        if($(evt.target).hasClass($.treeview.classes.hitarea)) return true;
        if(!evt.ctrlKey && !evt.metaKey)
            $('.treeview.sortable li').removeClass('ui-selected');
        $activeTrees.trigger('selectItems');
		$(this).saveCookiesState();
    });

	// выделение элементов дерева
	$('.treeview.sortable li').live('click', function(evt){
		var $targetLi = $(evt.target).closest('[id]');
		if(!$(evt.target).is('a') && !$(evt.target).parent().is('a') && ($targetLi.attr('id') == $(this).attr('id')))
		{
			if(!evt.ctrlKey && !evt.metaKey){
				$('.treeview.sortable li').removeClass('ui-selected');
				$(this).addClass('ui-selected');
			}
			else if($(this).hasClass('ui-selected'))
				$(this).removeClass('ui-selected');
			else $(this).addClass('ui-selected');

			$(this).closest('.treeview.sortable').trigger('selectItems');
		}
		$(this).saveCookiesState();
	});
	
	//  перемещение курсора в дереве  и открытие элемента с клавиатуры
	$(document).keydown(function(evt){
		
		// enter
		if(evt.keyCode == 13){
			var $selectedItem = $('.treeview li[id].ui-selected:last');
			if($selectedItem.length){
				if(!evt.metaKey) $selectedItem.dblclick();
				else{
					var url = $selectedItem.find('a').attr('href');
					if(url) location.href = url;
				}
			}
		}

		// вверх
		if(evt.keyCode == 38){
			var $selectedItem = $('.treeview li[id].ui-selected:first');

			if($selectedItem.length){
				var $prev = $selectedItem.prev('.treeview li[id]')
				var $sub = $prev.find('li[id]:last');
				if(!$prev.hasClass('expandable') && $sub.length) $prev = $sub;
				if(!$prev.length){
					$prev = $selectedItem.parent().closest('li[id]');
				}

				if($prev.length){
					$('.treeview li[id].ui-selected').removeClass('ui-selected');
					$prev.addClass('ui-selected');
					$prev.closest('.treeview.sortable').trigger('selectItems');
					$prev.saveCookiesState();
					return false;
				}
			}
		}

		// вниз
		if(evt.keyCode == 40){
			var $selectedItem = $('.treeview li[id].ui-selected:last');

			if($selectedItem.length){
				var $next = $selectedItem.next('.treeview li[id]')
				var $sub = $selectedItem.find('li[id]:first');
				if(!$selectedItem.hasClass('expandable') && $sub.length) $next = $sub;
				if(!$next.length){
					$next = $selectedItem.parent().closest('li[id]').next('li[id]');
				}

				if($next.length){
					$('.treeview li[id].ui-selected').removeClass('ui-selected');
					$next.addClass('ui-selected');
					$next.closest('.treeview.sortable').trigger('selectItems');
					$next.saveCookiesState();
					return false;
				}
			}
		}
		
		// вправо (разворачивание)
		if(evt.keyCode == 39){
			var $selectedItem = $('.treeview li[id].ui-selected .expandable-hitarea');
			if($selectedItem.length){
				$selectedItem.click();
			}
		}
		
		// влево (сворачивание)
		if(evt.keyCode == 37){
			var $selectedItem = $('.treeview li[id].ui-selected .collapsable-hitarea');
			if($selectedItem.length){
				$selectedItem.click();
				$('.treeview .expandable-hitarea li[id].ui-selected ').removeClass('ui-selected');
			}
		}
	});

});



