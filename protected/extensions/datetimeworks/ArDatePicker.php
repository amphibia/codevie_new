<?php

$bootstrapPath = Yii::getPathOfAlias(Yii::localExtension('bootstrap'));
Yii::setPathOfAlias('bootstrap', $bootstrapPath);
Yii::import('bootstrap.widgets.*');

/**
 * @package datetimeworks
 * @author koshevy
 * @requirments bootstrap
 *
 * Datepicker-виджет, автоматически превращающий TIMESTAMP в соответствующее значение.
 * Базируется на Bootstrap Datepicker.
 */
class ArDatePicker extends TbDatePicker
{
    /**
     * Тип формата даты ('short', 'medium', 'long', 'full').
     * Конкретный формат берется с Yii::app()->locale->getDateFormat($dateWidth)
     * @var string
     */
    public $dateWidth = 'short';

    /**
     * Возможно ли оставить поле пустым.
     * @var bool
     */
    public $allowEmptyDate = false;


    public function init()
    {
        /**
         * @todo форматы CDateFormatter и JQueryUI datepicker, за исключением
         * "short" несоместимы. Необходимо сделать конвертор.
         */

        $value = $this->value or $value = $this->model->{$this->attribute};

        if((is_null($value) || ($value == '')) && !$this->allowEmptyDate)
            $value = time();

        $this->options['format'] = strtolower(Yii::app()->locale->getDateFormat($this->dateWidth));

        if($value){
            $value = Yii::app()->dateFormatter->formatDateTime($value, $this->dateWidth, NULL);
            $this->value = ($this->model->{$this->attribute} = $value);
        }

        parent::init();
    }

}
