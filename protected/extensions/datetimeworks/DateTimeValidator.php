<?php

/**
 * @package datetimeworks
 * @author koshevy
 *
 * Валидатор для проверки даты и времени, имеющей стандартное представление
 * и хранящеся в UNIX TIMESTAMP.
 */
class DateTimeValidator extends CValidator
{
    /**
     * Начало допустимого временного диапазона (UNIX TIME).
     * @var int
     */
    public $min = NULL;

    /**
     * Конец допустимого временного диапазона (UNIX TIME).
     * @var int
     */
    public $max = NULL;

    /**
     * Возможно ли оставить поле пустым.
     * @var bool
     */
    public $allowEmptyDate = false;

    /**
     * Поле, которое задается как первая дата (ниже нее не спускается).
     * @var string
     */
    public $startDateAttribute = NULL;


    /**
     * Приведение значения к общему виду (TIMESTAMP)
     * @param $value
     * @return int
     */
    protected function _parseValue($value)
    {
        // если значение не TIMESTAMP, оно конвертируется
        if(!ctype_digit($value))
        {
            // перебр всех форматов
            $timestamp = NULL;
            foreach(array('short', 'medium', 'long', 'full') as $dateWidth)
            {
                $timestamp = CDateTimeParser::parse(
                    $value,
                    Yii::app()->locale->getDateFormat($dateWidth)
                );

                // найден
                if($timestamp) break;
            }

            return $timestamp;
        }
        else return $value;
    }

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        // это сфера компетенции валидатора "required"
        if(!$object->$attribute) return;

        if($this->allowEmptyDate && (is_null($object->$attribute) || ($object->$attribute == "")))
            return;

        // если значение не TIMESTAMP, оно конвертируется
        $timestamp = $this->_parseValue($object->$attribute);

        // проверка корректности конвертированной даты
        if(!$timestamp){
            $object->addError($attribute, Yii::t('datetimeworks', 'Unknown date format'));
            return;
        }

        // минимальная дата
        $min = ($this->startDateAttribute && ($this->min < $object->{$this->startDateAttribute}))
            ? $object->{$this->startDateAttribute} : $this->min;
        $min = $this->_parseValue($min);

        if($min && ($timestamp < $min)){
            $timestamp = $min;
            /*
			$minDate = Yii::app()->dateFormatter->formatDateTime($min, 'full', NULL);
			$object->addError($attribute,
				Yii::t('datetimeworks', 'Minimal date is: {minDate}',
					array('{minDate}'=>$minDate)
			));

			return;
            */
        }

        // максимальная дата
        if($this->max && ($timestamp > $this->max)){
            $timestamp = $this->max;
            /*
			$maxDate = Yii::app()->dateFormatter->formatDateTime($this->max, 'full', 'full');
			$object->addError($attribute,
				Yii::t('datetimeworks', 'Maximal date is {maxDate}',
					array('{minDate}'=>$maxDate)
			));

			return;
            */
        }

        // непосредственно перед сохранением будет выполнено превращение
        // значения в UNIX TIMESTAMP
        $object->onBeforeSave = function(CModelEvent $event) use($attribute, $timestamp){
            $event->sender->$attribute = $timestamp;
        };
    }

}
