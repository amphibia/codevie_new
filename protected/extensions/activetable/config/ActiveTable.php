<?php

return array
(
    /**
     * Идентификатор таблицы.
     * Если указывается NULL, проставляется автоматически.
     * @var string
     */
    'id' => NULL,

    /**
     * Представления, с которыми работает виджет (при необходимости, можно
     * поменять менять).
     * @var array
     */
    'views' => array
    (
        "main"      => "main",
        "_form"     => "_form",
        "_table"    => "_tabe"
    ),

    /**
     * Общий роут, который применяется для постраничногоо вывода и сортировки.
     * ПРИМЕЧАНИЕ: для кнопок используется другая система создания ссылок.
     * @var string
     */
    'route' => NULL,

    /**
     * Параметры для формирования URL.
     * @var array
     */
    'params' => array(),

    /**
     * Использование альтернативного перелистывания (перелистывание с помощью
     * mousewheel, активные границы).
     * Работает только при включенных ActiveTable::$useAjax и ActiveTable::$usePagination.
     * @var type 
     */
    'useAlternativePagination' => true,
    
    /**
     * Использовать ли AJAX для этой таблицы
     * @var boolean
     */
    'useAjax' => true,

    /**
     * Элемент на странице, от контекста которого происходят AJAX-запросы.
     * Если указано NULL, используется контейнер таблицы.
     * @var string
     */
    'ajaxField' => NULL,
    
    /**
     * Модель, с которой работает таблица.
     * @var ActiveRecord
     */
    'model' => NULL,

    /**
     * Подписи к обычным полям (по умолчанию задаются в модели).
     * Также здесь могут указываться подписи к дополнительным полям.
     * 
     * @var array
     */
    'labels' => array(),

    /**
     * Массив с конверторами-предобработчиками данных перед выводом.
     * Конвертор назначается для каждого поля (например:
     * $preConvertors["id"] = function($fieldName, $fieldValue, $row, $isHeader = FALSE){ ... }).
     * 
     * @var array
     */
    'preConvertors' => array
    (
        // обработка данных из UNIXTIMPESTAMP
        "date_created" => $dateConvertor = function($fieldName, $fieldValue, $row, $isHeader = false)
        {
            if(!is_numeric($fieldValue)) return $fieldValue;
    
            if(!$isHeader)
                 return date(Yii::app()->params["dateFormatShort"], (int)$fieldValue);
            else return $fieldValue;
        },

        "date_changed" => $dateConvertor
    ),
    
    /**
     * Дополнительные поля. Для формирования значений используйте $preConvertors.
     * @var array
     */
    'additionalFields' => array(),
    
    /**
     * Поля, которые будут использоваться.
     * Если NULL, будут использоваться все поля.
     * 
     * Может использоваться вместе с $useFields, применятся в первую очередь.
     * $ingnoreFields работает по остаточному принципу.
     * @var array 
     */
    'useFields' => NULL,
    
    /**
     * Поля, которые будут игнорироваться.
     * Если NULL, будут использоваться все поля.
     * 
     * Может использоваться вместе с $useFields, который применятся в первую очередь.
     * В этом случае, работает по остаточному принципу.
     * @var array 
     */
    'ignoreFields' => NULL,

    /**
     * Порядок указания полей (включая дополнительные поля и поля с управляющими
     * кнопками). Если свойство не установлено, используется стандартный порядок:
     * требуемые плоя модели в естественном порядке, дополнительные поля, поля
     * управляющих элементов.
     * 
     * Примечание: при использовании данного свойства, $useFields и $ignoreFields
     * игнорируются.
     * 
     * @var array 
     */
    'fieldsOrder' => NULL,

    /**
     * Массив с настройками для кнопок.
     * Совпадает с форматом BootstrapButton, за исключением того, что вместо 'url'
     * указывается 'route'.
     * 
     * 'route' применяется для createUrl():
     * нужно указывать array(<route>, 'arg_name1', 'arg_name2'...).
     * Если NULL, применяется createUrl($name, array('id'=>row[id])).
     * 
     * @var array
     */
    'buttons' => array(),

    /**
     * Свойства элементов управления по умолчанию.
     * @var type 
     */
    'defualtButtonOptions' => array
    (
        //"label" => NULL,

        /**
         * Для createUrl(), нужно указывать array(<route>, 'arg_name1', 'arg_name2'...),
         * т.е. сначала действие, а затем перечисление имён параметров, которые
         * будут подставляться из атрибутов строки.
         * Если NULL, применяется createUrl($name, array('id'=>row[id])).
         */
        //"route" => array()
    ),

    /**
     * Использование заголовка.
     * @var boolean
     */
    'useHeader' => true,

    /**
     * Кнопки, которые должны отобразиться в хэдере.
     * Свойства полностью совместимы с BootstrapButton, за исключением того,
     * что в качестве 'url' принимаtтся 'route' - шаблон пути.
     * 
     * Параметр 'route' указывается следующим образом:
     * array(<route>, $arg1, $arg2...).
     * @var array 
     */
	'headerButtons' => array(),

    /**
     * Использовать форму поиска или нет.
     * @var boolean
     */
    'useSearch' => true,
            
    /**
     * Название индекса в $_POST, в котором хранится запрос на поиск по
     * своему содержимому (отсортировка).
     * Виджет сам достаёт переменную и обрабатывает.
     * @var type 
     */
    'searchVar' => 'search',
    
    /**
     * Значение поля поиска.
     * @var string 
     */
    'search' => NULL,
    
    /**
     * Функция поиска.
     * Аргументы функции: $model, $search
     * 
     * @var callback
     */
    'searchFunction' => function(ActiveRecord $model, $search)
	{
		$conditions = array();
		$params = array();

		foreach($model->attributeNames() as $attribute)
		{
			$paramName = ":param_{$attribute}";
			$conditions[] = "(t.$attribute LIKE $paramName)";
			$params[$paramName] = '%'.addcslashes($search, '%').'%';
		}

		$model->dbCriteria->mergeWith(array(
			'condition' => implode($conditions, ' OR '),
			'params' => $params
		));
	},
    /**
     * Попись к форме поиска.
     * @var string
     */
    'searchLabel' => Yii::t('ActiveTable', 'Search'),

    /**
     * Фильтр по умолчанию.
     * @var string
     */
    'defaultScope' => 'reset',
            
    /**
     * Функции - условия для отсортировки (по фильтрам).
     * Формат: $scopes["published"] => function($scope){ ... }
     * 
     * Параметры функции: $model, $scopeValue.
     * 
     * Задача функции - настроить отсортировку в $model.
     * 
     * 
     * ПРИМЕЧАНИЕ: имя "reset" будет игнорироваться, т.к. значение "reset" для
     * $this->scope зарезервировано для сброса фильтра.
     * 
     * @var array (of callback)
     */
    'scopes' => array(),
            
    /**
     * Переменная в $_GET, хранящая значение текущего фильтра.
     * @var string
     */
    'scopeVar' => 'scope',

    /**
     * Переменная в $_GET, хранящая значение текущего фильтра.
     * @var string
     */
    'scopeValueVar' => 'scopeValue',

    /**
     * Подписи к фильтрам.
     * @var array
     */
    'scopeLabels' => array(),

    /**
     * Описания фильтров.
     * @var array
     */
    'scopeDescriptions' => array(),
            
    /**
     * Текущий фильтр отсортировки.
     * Значение "reset" означает, что фильтр сброшен.
     * 
     * @var string
     */
    'scope' => 'reset',

    /**
     * Дополнительный параметр, который позволяет указывать значение фильтра
     * для $scopesFunction. Например, если идёт отсортировка "по алфавиту",
     * параметр может передавать "А-Б".
     * 
     * ПРИМЕЧАНИЕ: пока нигде не используется.
     * @var string 
     */
    'scopeValue' => NULL,

    /**
     * Поле сортировки.
     * @var string 
     */
    'sortField' => 'id',

    /**
     * Имя параметра, в котором хранится поле сортировки.
     * @var string 
     */
    'sortVar' => NULL,

    /**
     * Направление сортировки (ASC/DESC).
     * @var string
     */
    'sortDirection' => 'asc',

    /**
     * Выводить или не выводить постраничную листалку.
     * @var boolean
     */
    'usePagination' => true,

    /**
     * Настройки элемента пагинации.
     * @var array
     */
    'pagerOptions' => array(
        'header' => false,
        'maxButtonCount' => 5
    ),
            
    /**
     * Количество элементов на одной странице.
     * Можно указывать напрямую, но здесль указывать нагляднее - все настройки
     * в одном месте.
     * @var integer
     */
    'pageSize' => NULL,
			
			
	/**
	 * Использовать выделение строк и групповые операции над этими строками.
	 * @var boolean
	 */
	'useGroupOperations' => true,

	/**
	 * Роут действия, по которому будет автоматически подгружаться панель
	 * управления групповыми операциями. В качестве аргумента будет передаваться
	 * массив с ID  выделенных элементов.
	 * 
	 * Параметр должен называться $ids - для соместимости с клиентской частью
	 * ActiveTable.
	 * 
	 * @var string
	 */
	'groupOpeartionsBarRoute' => 'groupOperationsBar',


    /**
     * Тэг таблицы.
     * @var string
     */
    'tableTag' => 'table',

    /**
     * Тэг, оборачивающий заголовок таблицы.
     * Может быть не указанным.
     * @var string
     */
    'tableHeadTag' => 'thead',

    /**
     * Тэг, оборачивающий тело таблицы.
     * Может быть не указанным.
     * @var string 
     */
    'tableBodyTag' => 'tbody',
            
    /**
     * Тэг строки.
     * @var string
     */
    'rowTag' => 'tr',

    /**
     * Тэг ячейки.
     * @var string
     */
    'cellTag' => 'td',

    /**
     * HTML - атрибуты различных элементов таблицы.
     * Например $this->htmlAttributes["row"] = array("valign"=>"top");
     * 
     * @var array
     */
    'htmlAttributes' => array
    (
        "container"         => array(),
        
        "table"             => array("cellspacing"=>0, "cellpadding"=>0),
        "row"               => array(),
        "headRow"           => array(),
        "cell"              => array("valign"=>"top"),
        
        // дополнительные поля
        "additionalField"   => array(),

        // настройка формы фильтров по таблице
        "form"          => array("method" => "POST"),
        "searchBlock"   => array(),
        "searchField"   => array(),
        "searchSubmit"  => array(),
        "scopeButton"   => array(),
        "scopeValue"    => array(),
        
        "info"          => array(),
    ),

    /**
     * Классы для различных элементов таблицы.
     * Например $this->classes["headRow"] = array("row", "headRow");
     * 
     * @var array
     */
    'classes' => array
    (
        "container"         => array("ActiveTable", "clientContent" ),
        
        "table"             => array(
            "table", "clientContentReplace", "clientContent"
        ),

        "row"               => array("bodyRow"),
        "headRow"           => array("headRow"),
		"cell"              => array("cell"),

        // дополнительные поля
        "additionalField"   => array("additionalField"),

        // настройка формы фильтров по таблице
        "form"          => array('form-search', 'form', 'well'),
        "searchField"   => array('input-medium'),
        "searchBlock"   => array("searchBar"),
        "searchSubmit"  => array("searchSubmit"),
        "scopeButton"   => array("scopeButton"),
        "scopeValue"    => array("scopeValue"),

        // панель информации
        "info"          => array("info"),
    ),

    /**
     * Опубликованные ассетсы для этого виджета.
     * @var string
     */
    'assets' => array
	(
        'js' => array('ActiveTable.js', 'mousewheel.plugin.js'),
        'css' => array('ActiveTable.css'),
    ),


);