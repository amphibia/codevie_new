<?php

/**
 * @var array Конфиг закачки.
 */
return array
(
    /**
     * Разрешённые MIME-типы документов (используется, если включено расширение
     * "Fileinfo").
     * @var array
     */
    'allowedDocumentMime' => array(
        'text/plain',
        'text/xml',
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'video/x-flv',
        'video/mp4',
        'video/webm',
        'video/quicktime',
        'video/x-msvideo',
        'video/x-ms-wmv',
    ),

    /**
     * Разрешённые MIME-типы изображений (используется, если включено расширение
     * "Fileinfo").
     * @var array
     */
    'allowedImageMime' => array('image/jpeg', 'image/pjpeg', 'image/jpg', 'image/gif', 'image/png'),


    "maxFileNameLentgh" => 255,

    // обязательно для заполнения!
    "acceptedHosts" => array($_SERVER['HTTP_HOST']),

    "storageHost" => $_SERVER['HTTP_HOST'],
    "storageRoot" => $_SERVER['DOCUMENT_ROOT'],

    // атрибуты INPUT[FILE]-элемента, используемого
    // виджетом закачки
    "formElementAttributes" => array("type" => "file", "multiple" => "multiple"),

    // ID контроллера, обрабатывающего закачки
    "controllerID" => "upload",

    // ID компонента - хранилища
    "storageComponentID" => "storage",

    // переименовывание файла при загрузке
    "fileRenameFunction" => function ($fileName) {
            $newName = date("Y_d_m__h_i_s__") . substr(microtime(1), -3);
            $newName = preg_replace('/[^\w]/', '_', $newName);
            return preg_replace("/^(.*)(\.[a-zA-Z]{2,6}){1}$/", "$newName$2", $fileName);
        },

    // представления для информеров
    'informersViews' => array(
        'main' => 'main',
        'disabled' => 'informers/disabled',
        'start' => 'informers/start',
        'success' => 'informers/success',
        'fail' => 'informers/fail',
        'deleted' => 'informers/deleted',
        'options' => 'options',
    ),

    /**
     * Сообщение об ошибке MIME. Будет использоваться и на клиентской стороне.
     * @var string
     */
    'mimeErrorMessage' => Yii::t('upload', 'MIME type error')

);
