/**
 * @autor koshevy
 * @title uploadsFormElement
 * @package imagesupload
 * @descr
 * 
 * Плагин - множественная закачка.
 * Клиентская часть для Yii-виджета @see UploadFormElement.
 * Плагин идёт в комплекте с серверной частью - контроллер @see ImagesUploadController.
 * 
 * Также, некоторые рутинные операции, не включенные в плагин описаны в прилагающемся
 * скрипте @see uploadsRoutine.js.
 * 
 * Работает с FF и Webkit. С IE не тестировался, для Opera нужно написать
 * костыль (вместо XHR-отправки, отдавать файл в скрытой форме). Вероятно,
 * для работы с IE придётся использовать тот же костыль, что и для Opera.
 *
 * UPDATE: В новых версиях Opera работает закачка, но проблемы с 'progress-bar'.
 *
 * Информеры подгружаются со стороны сервера (адреса указаны в в @see uploadServerIntefaces).
 * Указывать адреса не требуется - они указываются при заргузке виджета со стороны
 * сервера.
 *
 * Максимальное и минимальное количество прикрепляемых изображений указываются
 * в аттрибутах 'min' и 'max' в INPUT[FILE]-элементе.
 * Менять эти свойства в клиентской части, также, нет смысла, т.к. они задаются в валидаторе
 * @see ImageValidator, включенным в комплект расширения.
 *
 * Все необходимые настрйки указываются в конфигурационном файле @see StorageOptions,
 * идумщем в комплекте расширения; либо в конфигурации расширения storage,
 * от которого напрямую зависит расширение imagesupload.
 *
 * Во время изменений состояния (добавление/удаление информера/получение результата)
 * вызывается функция update(), которая генерирует событие 'onUploadElementUpdate'.
 *
 * @todo: Проверить косяки со статусом загрузки (Opera, Safari) - событие отлавливается,
 * процент нормально высчитывается, но не удаётся обратиться к информеру.
 * @todo: Проверить работоспособность в IE.
 * 
 * @events События указаны в порядке вызова:
 * onBeforeUpload
 * onRegisterFile
 * onSendFile
 * onUploadStart
 * onUploadProgres
 * onUploadComplete
 * onUploadSuccess
 * onUploadFail
 * onUploadElementUpdate
 */
jQuery.fn.extend({

    /**
     * @var array Адреса интерфейсов со стороны сервера, используемые
     * при закачке. Указывать не требуется - задаются серверной стороной.
     */
    uploadServerIntefaces:{},

    /**
     * Системные сообщения системы загрузки.
     * @var array
     **/
    uploadFormElementMessages:{
        error: 'Error occured',
        limitExceded: 'You can`t attach more images for this field',
        statusErrors:{
            400: 'Bad Request',
            403:'Forbidden',
            404:'Not Found',
            405:'Method Not Allowed',
            406:'Not Acceptable',
            408:'Request Timeout',
            410:'Gone',
            412:'Precondition Failed',
            413:'Request Entity Too Large',
            414:'Request-URI Too Large',
            415:'Unsupported Media Type',
            423:'Locked',
            429:'Too Many Requests',
            431:'Request Header Fields Too Large',
            434:'Requested host unavailable',
            456:'Unrecoverable Error',
            500:'Internal Server Error',
            503:'Service Unavailable',
            505:'HTTP Version Not Supported',
            507:'Insufficient Storage',
            508:'Loop Detected',
            509:'Bandwidth Limit Exceeded'
        }
    },

    /**
     * Действия, происходящие при обновлении состояния - добавлении новой закачки,
     * удалиении файла/информера.
     */
    update: function(){
        $(this)._isMaxLimitExceeded();
        $(this).trigger('onUploadElementUpdate');
    },

	/**
	 * Удаление информера по нажатию кнопки "удалить".
	 * В качестве this передаётся сама кнопка, а не FILE-элемент.
	 */
    removeInformer: function()
    {
		var $informer = $(this).parents('.uploadInformer'),
			$file = $informer.parents('.imagesUploads').find('input.imagesUpload');

		// удаление информера уже загруженного файла
		if($informer.hasClass('uploadInformerSuccess'))
		{
			var $billet = $(this).closest('.imagesUploads')
					.find('.uploadInformerDeleted.billet').clone(),
				$informerClone = $informer.clone();

			$billet.attr('data-js-mime', 'text/xml');
			$billet.removeClass('billet');
			$billet.find('.cancel').click(function(){
				$billet.replaceWith($informerClone);
				$file._isMaxLimitExceeded();
			});

			$informer.replaceWith($billet);
			$file._isMaxLimitExceeded();
		}

		// удаление информера загружаемого файла, или информера о неудачной попытке
		else
		{
			$informer.remove();
			$file.update();
		}
    },

    /**
     * Функция - оболочка отправки файлов на указанный интерфейс.
     * Функцию можно переопределить, непосредственная закачка происходит в
     * функции _uploadFiles.
     * 
     * Работает только с INPUT[TYPE=FILE].
     *  
     * @param string receiverUrl отправка файла на указанный интерфейс
     * @return boolean произошла отправка или нет
     */
    uploadFiles: function(receiverUrl)
    {   
        // адрес по умолчанию
        if(!receiverUrl) receiverUrl = $(this).uploadServerIntefaces.upload;
        
        // внутренняя функция непосредственной отправки
        return $(this)._uploadFiles(receiverUrl);
    },

     /**
     * Внутренняя функция отправки файлов на сервер. Вызывается из uploadFiles.
     *  
     * @param string receiverUrl отправка файла на указанный интерфейс
     * @return boolean произошла отправка или нет
     */   
    _uploadFiles: function(receiverUrl)
    {
        // непосредственно элемент INPUT:FILE
        var fileInput = this.context;

        $(fileInput).trigger('onBeforeUpload', receiverUrl);

        // обработка списка
        for(var fileIndex in fileInput.files)
        {
            // пропуск служебной информации
            if(typeof fileInput.files[fileIndex] != "object") continue;
            
            // данные файла из очереди
            var fileData = $(this)._fileData(fileInput.files[fileIndex]);

            // Проверка типа файла. Данные берутся со стороны сервера, откуда
			// они отправляются через data-привязку к элементу закачки.
			var mimeTypes = $(fileInput).data('MIME_TYPES');
			if($.inArray(fileData.type, mimeTypes) == -1)
			{				
				$(this)._error($(fileInput).data('MIME_TYPE_ERROR'), fileData);
				return;
			}
			
			// Проверка уникальности указанного MIME для конкретного поля.
			if($(fileInput).attr('differentfiletypes') == 1)
			{
				var similarItems = $(fileInput).closest('.controlElement')
					.find('.uploadInformerSuccess[data-js-mime="'+fileData.type+'"]').length;

				if(similarItems)
				{
					$(this)._error($(fileInput).data('DUPLICATE_TYPE_ERROR'), fileData);
					return;
				}
			}
            

            // отправка файла
            $(this)._sendFile(receiverUrl, fileInput.files[fileIndex], fileData);
        }
        
        // все файлы отправлены корректно
        return true;

    },


    /**
     * @var array ссылки на использованные в закачках XMLHttpRequest-объекты.
     */
    _xhrs: [null],

    /**
     * @var array данные об отправленных файлах.
     */
    _filesData: [null],

    /* первый эелемент - NULL, чтобы ID=0 никуда не ссылался*/


    /**
     * @var integer Количество неподгруженных информеров (о начале загрузки).
     * Необходимо для отслеживания состояния и предотвращения ситуации, когда
     * файл хагружается быстрее, чем происходит загрузка информера (если она
     * совпала со спадом скорости).
     */
    _informerWaiting: [],


    /**
     * Отправляет файл из очереди. Вызывается из uploadFiles.
     * @param string receiverUrl отправка файла на указанный интерфейс
     * @return boolean произошла отправка или нет
     */
    _sendFile: function(receiverUrl, fileItem, fileData)
    {           
        /**
         * @todo Нужно дописать обходной путь, на случай, если браузер
         * коряво работает с отправкой файлов через XHR. */ 

        var xhr = $(this)._registerFile(receiverUrl, fileData);
        if(!xhr) return;

        // отправка данных
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("X-File-Name", encodeURIComponent(fileData.name));
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
		xhr.setRequestHeader("Token-Name", $(document).data('ajaxTokenName'));
		xhr.setRequestHeader("Token-Value", $(document).data('ajaxTokenValue'));
		xhr.send(fileItem);

        $(this).trigger('onSendFile',
            {receiverUrl:receiverUrl, fileItem:fileItem, fileData:fileData, xhr:xhr});
    },

    _registerFile: function(receiverUrl, fileData)
    {
        if($(this)._isMaxLimitExceeded()){
            $(this)._error($(this).uploadFormElementMessages.limitExceded, fileData);
            return false;
        }
        
        
        // идентификатор новой ветки закачки
        var uploadID = $(this)._filesData.length;

        var xhr = new XMLHttpRequest();

        $(this)._xhrs.push(xhr);
        $(this)._filesData.push(fileData);


        // контекст для передачи в событие
        var elementContext = this;

        // стек ожидающих информеров начала закгрузкм (пока не создастся,
        // выводить информер о завершении нет смысла)
        $(this)._informerWaiting.push(true)
        

        xhr.open("POST", receiverUrl, true);

        // вызывается обработчик начала закачки
        $(this).uploadStart.apply(elementContext, [uploadID, fileData, xhr]);

        // отслеживание прогреса закачки
        xhr.upload.onprogress = function(eventInfo){
            $(elementContext).uploadProgress.apply(elementContext,
                [uploadID, fileData, eventInfo, xhr] );
        }

        xhr.onreadystatechange=function(eventInfo)
        {
            // завершение закачки
            if (xhr.readyState==4)
            {
                // ставит в очередь
                $(elementContext).queue(function()
                {
                    $(elementContext)._uploadComplete.apply(
                        elementContext,
                        [uploadID, fileData, eventInfo, xhr]
                    );
                        
                    $(elementContext).dequeue();
                });
            }
        }

        $(this).trigger('onRegisterFile',
            {receiverUrl:receiverUrl, fileData:fileData, xhr:xhr, uploadID:uploadID});

        return xhr;
    },
    
    /**
     * Проверка, не приевысило ли количество загрузок максимально допустимое
     * количество прикреплённых файлов для конкретного поля.
     * @return boolean лимит исчерпан или нет
     **/
    _isMaxLimitExceeded: function()
    {
        var $resultsArea = $(this).parent().find('.resultsArea');
        var filesCount = $resultsArea.find('.uploadInformerSuccess, .uploadInformerStart').length;

        // общее количество файлов, с учётом уже отправленных на загрузку
        var activeCount = $(this)._informerWaiting.length + filesCount;


        var max;
        if((max = $(this).attr('max')) && (activeCount >= max))
		{
            $(this).hide();
            $(this).parent().find('.uploadNew').slideUp(200);
			$(this).parent().find('.uploadInformerDeleted .cancel').fadeOut(200);
        }
        else
		{
			$(this).show();

			// Проверка, чтобы нельзя было восстановить элемент с MIME-типом, уже
			// имеющимся в списке (если установлено свойство differentfiletypes).
			if(this.attr('differentfiletypes') == '1')
			{
				$('.uploadInformerDeleted').each(function()
				{
					if($(this).hasClass('billet')) return;

					var mimeType = $(this).attr('data-js-mime'),
						selector = '.uploadInformerSuccess[data-js-mime="'+mimeType+'"]';

					if($resultsArea.find(selector).length)
						 $(this).find('.cancel').fadeOut(200);
					else $(this).find('.cancel').fadeIn(200);
				});
			}
			else
			{
				$(this).parent().find('.uploadNew').slideDown(200);
				$(this).parent().find('.uploadInformerDeleted .cancel').fadeIn(200);
			}
        }

        return (activeCount>=$(this).attr('max')) ? true : false;
    },


    /**
     * Данные о файле, с учётом межбраузерных отличий.
     * @var File fileItem элемент Filelist-списка у INPUT:FILE.
     **/
    _fileData: function(fileItem)
    {
        // свойства файла
        var fileData =
        {
            name: (fileItem.name ?
                fileItem.name : fileItem.fileName),
            
            size: (fileItem.size ?
                fileItem.size : fileItem.fileSize),
            
            type: (fileItem.type ?
                fileItem.type : fileItem.fileType),
            
            // также добавляются данные о поле формы,
            // к которому привязан файл
            attribute: $(this).attr("attribute")
            
        };


        return fileData;
    },


    /**
     * Обращение к информеру закачки (начато/успешно/неудачно)
     * @param integer uploadID ID операции закачки
     * @return object JQuery-ссылка на объект
     */
    _getInformer: function(uploadID)
    {
        return $(this).parent().find('.resultsArea .uploadInformer[upload_id='+uploadID+']'); 
    },


    /* поэтапные действия элемента */

    /**
     * Обработка начала закачки.
     * @var integer uploadID уникальный идентификатор (порядковый номер) закачки.
     * @var object fileData данные отправляемого файла
     * @var XMLHttpRequest обслуживающий запрос
     **/
    uploadStart: function(uploadID, fileData, xhr)
    {   
        var context = this;

        // создание информера закачки
        $('.resultsArea', $(this).parent()).request
        (
            $(this).uploadServerIntefaces.informerUploading,
            {id:uploadID, fileData:fileData}, false,
            
            // действия по завершении запроса
            function()
            {
                // обработка для кнопки "прервать закачку"
                var informer = $(context)._getInformer(uploadID);
                $(".abortUpload", informer).click(function()
                {
                    informer.remove();
                    xhr.abort();
                    $(context).update();
                });

                // информер загрузился, загрузка снята с учёта
                if($(context)._informerWaiting.length)
                    $(context)._informerWaiting.shift();
                
                $(context).update();
            }
        );

        // Прикрепление к информеру ссылки на XMLHttpRequest - объект,
        // обслуживающий запрос, чтобы в любой момент можно было
        // остановитьь закачку.
        // Также прикепляются данные отправляемого файла.
        $(this)._getInformer(uploadID).data("xhr", xhr);
        $(this)._getInformer(uploadID).data("fileData", fileData);

        $(this).trigger('onUploadStart',
            {uploadID:uploadID, fileData:fileData, xhr:xhr});
    },

    /**
     * Оповещение о продвижении закачки.
     * @var integer uploadID уникальный идентификатор (порядковый номер) закачки.
     * @var object fileData данные отправляемого файла
     * @var object eventInfo
     * @var XMLHttpRequest обслуживающий запрос
     **/
    uploadProgress: function(uploadID, fileData, eventInfo, xhr)
    {
        var informer = $(this)._getInformer(uploadID);
        var value = Math.round((eventInfo.loaded ? eventInfo.loaded:eventInfo.position) / (eventInfo.total ? eventInfo.total : eventInfo.totalSize) *100);

        $(".progress .bar", informer).width(value+'%');

        $(this).trigger('onUploadProgres',
            {value:value, uploadID:uploadID, fileData:fileData, eventInfo:eventInfo, xhr:xhr});
    },

    /**
     * Обрабатывание ответа от сервера.
     * 
     * Эту функцию лчше не переопределять, т.к. она вызывает из себя
     * uploadComplete и uploadFail. Для переопределения есть uploadComplete.
     **/
    _uploadComplete: function(uploadID, fileData, eventInfo, xhr)
    {
        var context = this;

        
        // Обработка запроса до того, как подгрузились информеры может привести
        // к ситуации, когда информеры о завершении закачки начинаюты выводиться
        // раньше, чем выведены информеры о начале закачке.
        // 
        // Поэтому, если есть активная подгрузка информеров, обработка
        // завершения закачки откладывается.
        if($(context)._informerWaiting.length){       
            setTimeout( function(){
                    $(context)._uploadComplete(uploadID,
                    fileData, eventInfo, xhr); 
            },  500);
            return;
        }

        $(this).uploadComplete.apply(this,[uploadID, fileData, eventInfo, xhr] );
        if(Math.round(parseInt(xhr.status)/100) == 2) // 2xx-ответ
             $(this).uploadSuccess.apply(this, [uploadID, fileData, eventInfo, xhr] );
        else $(this).uploadFail.apply(this, [uploadID, fileData, eventInfo, xhr] );

    },

    /**
     * Обработка завершения закачки (при любом исходе).
     * @var integer uploadID уникальный идентификатор (порядковый номер) закачки.
     * @var object fileData данные отправляемого файла
     * @var object eventInfo
     * @var XMLHttpRequest обслуживающий запрос
     **/
    uploadComplete: function(uploadID, fileData, eventInfo, xhr)
    {
        $(this).trigger('onUploadComplete',
            {uploadID:uploadID, fileData:fileData, eventInfo:eventInfo, xhr:xhr});
    },

    /**
     * Обработка успешного завершения закачки.
     * @var integer uploadID уникальный идентификатор (порядковый номер) закачки.
     * @var object fileData данные отправляемого файла
     * @var object eventInfo
     * @var XMLHttpRequest обслуживающий запрос
     **/
    uploadSuccess: function(uploadID, fileData, eventInfo, xhr)
    {
		var result, context = this;

		try{
			result = JSON.parse(eventInfo.target.responseText);
		}
		catch(err){
			$(this)._error('Server-side JSON parse error');
			$(this).uploadFail(uploadID, fileData, eventInfo, xhr);
			return;
		}

        // замена информера на информер "успешная закачка"
        $(this)._getInformer(uploadID).request
        (
            $(this).uploadServerIntefaces.informerSuccess,
            {id:uploadID, fileData:fileData, filename:result.filename}, false,

            // действия по завершении запроса
            function()
			{
				// пометка о MIME, определяемом браузером
				$(context)._getInformer(uploadID).attr('data-js-mime', fileData.type);
				
				$(".deleteItem").click(function(){$(this).removeInformer();});

				$(this).trigger('onUploadSuccess', {
					uploadID:uploadID, fileData:fileData,
					eventInfo:eventInfo, xhr:xhr
				});

				$(context).update();
            }
        );
    },

    /**
     * Обработка неудачного завершения закачки.
     * @var integer uploadID уникальный идентификатор (порядковый номер) закачки.
     * @var object fileData данные отправляемого файла
     * @var object eventInfo
     * @var XMLHttpRequest обслуживающий запрос
     **/
    uploadFail: function(uploadID, fileData, eventInfo, xhr)
    {
        var context = this;
        var informer = $(this)._getInformer(uploadID);

        // замена информера на информер "закачка не удалась"
        informer.request
        (
            $(this).uploadServerIntefaces.informerFail,
            {id:uploadID, fileData:fileData}, false,

            // действия по завершении запроса
            function()
            {                
                // обработка для кнопки "удалить информер"
                var informer = $(context)._getInformer(uploadID);
                $(".deleteItem", informer).click(function(){informer.remove();});
                
                try{
                    var result = JSON.parse(eventInfo.target.responseText);
                    $(this)._error(result.message, fileData);
                } catch(e){
                    //console.log(xhr.status);
                    if(xhr.status in $(this).uploadFormElementMessages.statusErrors) {
                        $(this)._error($(this).uploadFormElementMessages.statusErrors[xhr.status], fileData);
                    }
                    else{
                        $(this)._error($(this).uploadFormElementMessages.error, fileData);
                    }
                }
                finally {}
                
                $(this).trigger('onUploadFail', {uploadID:uploadID,
                    fileData:fileData, eventInfo:eventInfo, xhr:xhr});
                $(context).update();
            }
        );
    },

	/**
	 * Инициализация формы редактирования изображения.
	 */
	initOptionsForm: function($informer)
	{
		var $area = $(this).closest('.imagesUploads'),
			$billet = $area.find('.imagesUploads_options.billet').clone();

		$billet.removeClass('billet').hide();
		$billet.find('.original img')
			.attr('src', $informer.attr('data-preview-url'));
		$informer.append($billet);

		// блок с предпросмотром изображения
		var	$image = $informer.find('.imagesUploads_options .original img'),
			$preview = $informer.find('.thumbs .preview .image'),
			$optionsDialog = $informer.find('.imagesUploads_options'),

			$jCrop = null;

		$informer.addClass('noScroll');
		$informer.find('.imagesUploads_options').slideDown(200);

		if($informer.attr('data-file-type') != 'image')
			$informer.find('.forImages').remove();
		if($optionsDialog.find('.loadable').length)
			$optionsDialog.addClass('loading');

		// действия кнопки "применение настроек""
		$informer.find('.saveOptions').click(function()
		{
				var	title = $informer.find('[name="title"]').val(),
					description = $informer.find('[name="description"]').val();

				if($informer.attr('data-file-type') == 'image')
				{
					var	$width = $informer.find('[name="width"]'),
						$height = $informer.find('[name="height"]'),
						$x = $informer.find('#x'), $y = $informer.find('#y'),
						width = $width.length ? parseInt($width.val()) : '',
						height = $height.length ? parseInt($height.val()) : '',
						x = $x ? parseInt($x.html()) : '',
						y = $y ? parseInt($y.html()) : '';

					if(!width || !height
						|| (height<10) || (width<10) || (height>500) || (width>500)
						|| (x>100) || (y>100))
					{
						$informer.find('.error').show();
						return;
					}

					$informer.find('.error').hide();
					$informer.find('.inputX').val(x);
					$informer.find('.inputY').val(y);
					$informer.find('.inputWidth').val(width);
					$informer.find('.inputHeight').val(height);
				}

				$informer.find('.inputTitle').val(title);
				$informer.find('.inputDescription').val(description);

				$informer.find('.itemOptions').removeClass('active');
				$optionsDialog.slideUp(200);
		});

		// инициализация общих настроек
		var	initTitle = $informer.find('.inputTitle').val(),
			initDescription = $informer.find('.inputDescription').val();
		$optionsDialog.find('[name="title"]').val(initTitle);
		$optionsDialog.find('[name="description"]').val(initDescription);

		// отмена настроек
		$informer.find('.cancelOptions').click(function(){

			$informer.find('.inputX').val(x);
			$informer.find('.inputY').val(y);
			$informer.find('.inputWidth').val(width);
			$informer.find('.inputHeight').val(height);

			$informer.find('.itemOptions').removeClass('active');
			$optionsDialog.slideUp(200);
		});

		// Инициализация JCrop после того, как подгрузится картинка.
		$image.load(function(){
			
				$optionsDialog.removeClass('loading');

				var	imageWidth = $image.width(),
					imageHeight = $image.height(),
					$parent = $image.parent();

				// позиционирование изображения по центру
				$(this).css('left', (($parent.width() - imageWidth)/2-1)+'px');
				$(this).css('top', (($parent.height() - imageHeight)/2-2)+'px');

				$(this).parent().Jcrop({
				onSelect: function(coords)
				{
					// расчеты в процентах
					var	width = Math.round(coords.w/imageWidth*100),
						height = Math.round(coords.h/imageHeight*100),
						x = Math.round((coords.x - (($parent.width()-imageWidth)/2))/imageWidth*100),
						y = Math.round((coords.y - (($parent.height()-imageHeight)/2))/imageHeight*100);

					// вывод текущих координат (в процентах)
					$informer.find('#x').html(x);
					$informer.find('#y').html(y);
					$informer.find('#width').val(width);
					$informer.find('#height').val(height);

					// отношение ширины и высоты по отношению друг к другу
					// (для позиционирования окна предпросомотра)
					var absSide = (width>height) ? width : height,
						absWidth = Math.round(width/absSide*100),
						absHeight = Math.round(height/absSide*100),
						previewWidth = Math.round(imageWidth/100*absWidth),
						previewHeight = Math.round(imageHeight/100*absHeight);

					// окно предпросмотра
					$preview.css('width', previewWidth+'px')
						.css('height', previewHeight+'px')
						.css('left', Math.round(($parent.width()-previewWidth)/2+2)+'px')
						.css('top', Math.round(($parent.height()-previewHeight)/2-1)+'px');

					// определение координат предпросмотра
					var scaleX = Math.round(imageWidth/coords.w*100),
						scaleY = Math.round(imageHeight/coords.h*100),
						scale = (width>height) ? scaleX : scaleY,
						offsetX = Math.round($image.width() / 100 * x / 100 * scale)*-1,
						offsetY = Math.round($image.height() / 100 * y / 100 * scale)*-1;

					// вывод и настройка изображения предпросмотра
					$preview.css('background-image', 'url('+$informer.attr('data-original-url')+')')
						.css('background-size', scaleX+'% '+scaleY+'%')
						.css('background-position', offsetX+'px '+offsetY+'px')
						.css('background-repeat', 'no-repeat');

					$informer.find('input#ratio').removeAttr('disabled');

				} }, function(){
					var	x = ($parent.width() - imageWidth) /2,
						y = ($parent.height() - imageHeight) /2,
						ex = imageWidth+x, ey = imageHeight+y;

					// сброс настроек
					$informer.find('.resetOptions').click(function(){
						$jCrop.setSelect([x,y,ex,ey]);
						$jCrop.setOptions({aspectRatio:0});
						$informer.find('input#ratio').removeAttr('checked');
					});

					// уже сохраненные настройки обрезки
					var	initX = parseInt($informer.find('.inputX').val()),
						initY = parseInt($informer.find('.inputY').val()),
						initWidth = parseInt($informer.find('.inputWidth').val()),
						initHeight = parseInt($informer.find('.inputHeight').val());

					// если указывались настройки ОБРЕЗКИ, они переписывают
					// настроки по умолчанию
					if(initWidth && initHeight){
						x = x + (imageWidth / 100 * initX);
						y = y + (imageHeight / 100 * initY);
						ex = x + (imageWidth / 100 * initWidth);
						ey = y + (imageHeight / 100 * initHeight);
					}
					
					$jCrop = this;
					$jCrop.setSelect([x,y,ex,ey]);
				});
			});

		// фильтр ввода (только цифры)
		$informer.find('#width, #height').keydown(function(evt){
			if(((evt.keyCode < 47) || (evt.keyCode > 57))
				&& (evt.keyCode != 37) && (evt.keyCode != 38) && (evt.keyCode != 40)
				&& (evt.keyCode != 39) && (evt.keyCode != 8) && (evt.keyCode != 13) )
				return false;

			if(evt.keyCode == 13){
				$(this).change(); return false;
			}
		});

		// изменение RATIO
		$informer.find('#width, #height').change(function(){
			var	$width = $informer.find('#width'),
				$height = $informer.find('#height');

			// введены ширина и высота
			if($width.val() && $height.val())
			{
				var widthVal = parseInt($width.val()),
					heightVal = parseInt($height.val()),
					relativeWidth = $image.width()/100*widthVal,
					relativeHeight = $image.height()/100*heightVal,
					$fixedRatio = $informer.find('input#ratio'),
					ratio = relativeWidth / relativeHeight;

				// установка текущих координат
				var coords = $jCrop.tellSelect();
				coords['x2'] = coords['x'] + relativeWidth;
				coords['y2'] = coords['y'] + relativeHeight;
				$jCrop.setSelect([coords['x'], coords['y'], coords['x2'], coords['y2']]);

				// доступность галочки FIXED RATIO
				$fixedRatio.removeAttr('disabled');
				if($fixedRatio.attr('checked'))
					$jCrop.setOptions({aspectRatio:ratio});
				else $jCrop.setOptions({aspectRatio:0});
			}

			else{
				$fixedRatio.attr('disabled', 'disabled');
				$jCrop.setOptions({aspectRatio:0});
			}
		});

		// включение выключение FIXED RATIO
		$informer.find('input#ratio').change(function(){
			$informer.find('#height').change();
		});

		$image.parent().css('width', $informer.find('.imagesUploads_options .original').width()+'px')
	},

    /**
     * Сообщение об ошибке.
     * @param object message сообщение
     * @param object fileData данные файла, с которым произошла ошибка
     */
    _error: function(message, fileData){
        $.client.message('icon-info-sign', message);
    }

});