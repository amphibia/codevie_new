<?
/**
 * @autor koshevy
 * Заготовка для формы редактирования свойств изображения/документа.
 * Клон данной заготовки будет заполнен данными и вставлен в инофрмер
 * файла.
 * 
 * Манипуляции происходят в скрипте управления закачкой - uploadsFormElement.js.
 *
 * Элемент с классом "billet" - заготовка. До тех пор, пока этот класс не будет
 * удален, элемент не будет виден.
 * 
 * Элементы с классом "forImages" применяются только для изображений (для документов
 * не применяются).
 *
 * Класс "loadable" для элементов говорит о том, что при загрузке форма будет
 * ждать загрузки этих жлементов.
 */
?>
<div id="imagesUploads_options_<?= $this->id ?>" class="imagesUploads_options billet">

	<div class="thumbs forImages loadable">
		<div class="original">
			<label><?=Yii::t('UploadsFormElement', 'Original');?></label>
			<img src="" />
		</div>

		<div class="preview">
			<label><?=Yii::t('UploadsFormElement', 'Preview');?></label>
			<div class="image" style="background-color:#ccc;"></div>
		</div>

		<div class="clear"></div>
	</div>

	<div class="actions well">

		<div class="descriptionForm">

			<div class="inputs">
				
				<div>
					<?= Yii::t('app', 'Title'); ?>:<br/>
					<input name="title" type="text" maxlength="255" />
				</div>

				<br/>

				<div>
					<?= Yii::t('app', 'Description'); ?>:<br/>
					<textarea name="description"></textarea>
				</div>
				
			</div>
			
		</div>
		
		<div class="ratioForm forImages">

			x: <span id="x">0</span>% &nbsp; y: <span id="y">0</span>%

			<div class="inputs">
				<?= Yii::t('app', 'width'); ?>:&nbsp;<?
				$this->widget('CMaskedTextField',
					array(
						'name'=>'width',
						'mask'=>'99',
						'placeholder'=>Yii::t('UploadsFormElement', 'Width'),
						'htmlOptions' => array('maxlength' => 2),
					)
				);
				?>&nbsp;%
				&nbsp;
				<?= Yii::t('app', 'height'); ?>:&nbsp;<?
				$this->widget('CMaskedTextField',
					array(
						'name'=>'height',
						'mask'=>'99',
						'placeholder'=>Yii::t('UploadsFormElement', 'Height'),
						'htmlOptions' => array('maxlength' => 2),
					)
				);
				?>&nbsp;%
			</div>

			<label class="fixedRatio">
				<input id="ratio" disabled="disabled" type="checkbox" /> <? echo Yii::t('UploadsFormElement', 'Fixed ratio'); ?>
			</label>
			
		</div>
		
		<br/>

		<?
		// кнопка "Сохранить"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
			array('buttonType'=>'button', 'icon'=>'icon-ok icon-white', 'label'=>Yii::t('app', 'Apply'),
				'htmlOptions'=>array('class'=>'saveOptions'),
				'type'=>'primary',
			)
		);
		
		?>&nbsp<?
		
		// кнопка "Отмена"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
			array('buttonType'=>'button', 'icon'=>'icon-remove', 'label'=>Yii::t('app', 'Cancel'),
				'htmlOptions'=>array('class'=>'cancelOptions'),
			)
		);

		?>&nbsp <?

		// кнопка "Сброс"
		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
			array('buttonType'=>'button', 'icon'=>'icon-refresh', 'label'=>Yii::t('app', 'Reset options'),
				'htmlOptions'=>array('class'=>'resetOptions forImages'),
			)
		);
		?>
	</div>
	
		<div class="error alert alert-error">
			<?= Yii::t('UploadsFormElement', 'Ivalid parametres. You must have correct coords.'); ?>
		</div>
	
</div>