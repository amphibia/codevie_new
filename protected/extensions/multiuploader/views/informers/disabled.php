<? if(defined('YII_DEBUG') && YII_DEBUG){ ?>
<div class="alert alert-error">
    <strong><?= Yii::t('upload', 'Upload not supported!'); ?></strong>
    <?= Yii::t('upload', 'Upload form element not supported, because ImagesUploadComponent disabled.'); ?>
</div>
<? } ?>