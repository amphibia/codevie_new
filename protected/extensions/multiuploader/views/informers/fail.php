<div class="alert alert-error uploadInformer uploadInformerFail clientContent clientContentReplace" upload_id="<?=$fileId?>">

    <button class="close" data-dismiss="alert" type="button">×</button>
    
    <span class="label label-important"><?=Yii::t('upload', 'Upload failed');?></span>
    
    <p>
        <div class="fileName"><?=$fileName?></div>
        <div class="fileSize"><?=round($fileSize/(1024*1024), 2)?> Mb</div>
    </p>
    
</div>