<?php

/**
 * Контроллер, отвечающий за загрузку файлов и вывод информеров. Является серверным 
 * приложению к виджету @see UploadsFormElement.
 * 
 * Ни каких манипуляций с контроллером производить не требуется.
 * Контроллер подключается к приложению компонентом @see ImagesUploadComponent.
 * 
 * Все настройки ноаходятся в свойствах компонентов @see storageComponent
 * и @see ImagesUploadComponent.
 */
class UploadController extends ExtController
{
     /**
     * Загрузка файлов будет отслеживаться отдельно.
     */
    public function getRbacOperation(){
        return 'File upload';
    }

    /**
     * Компонент, осуществляющий закачку.
     * @var ImagesUploadComponent
     */
    public $uploadComponent = NULL;

   /**
    * Действие по умолчанию - загрузка файлов
    */ 
	public function actionIndex()
	{
        ExtCoreJson::$use = true;

        $actionResult = $this->uploadComponent->loadFromXHR() or
        $actionResult = $this->uploadComponent->loadFromForm();
        if(!$actionResult) throw new CHttpException(400, 'No input file');

        // содержимое принято
        header("HTTP/1.1 202 Accepted");

        // вывод сообщения, мол всё нормально
        ExtCoreJson::$data = array(
            "code"     => "202",
            "message"  => "Upload succes.",
            "filename" => "{$this->uploadComponent->storageHost}{$this->uploadComponent->filePath}",
        );
        ExtCoreJson::response();
	}    


    /**
     * AJAX - подгрузка информера статуса закачки.
     * @param type $informerType тип информера(start|success|fail)
     */
    public function actionInformers($informerType = NULL)
    {
        if(!isset($this->uploadComponent->informersViews[$informerType]))
            throw new CHttpException(400, "Request is out of context.");

        // имя файла, уже закачанного yf cthdth (для информера "success")
        if($informerType == "success"){
            $srcFileName = isset($_POST["filename"]) ? $_POST["filename"] : NULL;
            $srcPathInfo = parse_url("http://$srcFileName");
            if(!isset($srcPathInfo['host']) || !in_array($srcPathInfo['host'],
                $this->uploadComponent->acceptedHosts))
                    throw new ExtCoreFishyError(400, Yii::t('upload', "Not accepted file host."));
        }
            else $srcFileName = NULL;

        $this->renderPartial(
            $this->uploadComponent->informersViews[$informerType],
            array(
                "storage" => $this->uploadComponent->storage,
                "newFileName" => $srcFileName,
                "fileId" => (int)$_POST["id"],
                "fileName" => $_POST["fileData"]["name"],
                "fileSize" => (int)$_POST["fileData"]["size"],
                "attribute" => htmlspecialchars($_POST["fileData"]["attribute"]),
            )
        );

    }


}