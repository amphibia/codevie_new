<?
/**
 * @author koshevy
 * Заголовок календаря.
 * Выводит элементы управления - список выборамесяца, года, перемотка в предыдущий
 * и следующий месяц.
 */
?>
<div <?= CHtml::renderAttributes($this->_elementHtmlOptions('calendarHeader')); ?>>
<?
	echo $this->_prevMonthLink();
	echo $this->_nextMonthLink();
	
	?>
	<div class="selects">
		<div class="selectMonth"><?= $this->_monthsList(); ?></div>
		<div class="selectYear"><?= $this->_yearsList(); ?></div>
		<div class="clear"></div>
	</div>
	<?

	echo CHtml::tag('div', array('class'=>'clear'), '');
?>
</div>