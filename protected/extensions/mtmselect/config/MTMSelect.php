<?php

/**
 * @package mtmselect
 * @author koshevy
 * Конфигурация по умолчанию для виджета MTMSelect.
 */
return array
(
    /**
     * Атрибут модели, к которому привязывается поле.
     * @var string
     */
    'attribute' => NULL,

    /**
     * Поле которое используется в модели как название (используется, если формой
     * не было произведено извлечение ID и заголовка).
     * @var string
     */
    'titleField' => 'title',

    /**
     * Модель, из которой происходит выборка.
     * @var ActiveRecord
     */
    'model' => NULL,
    
    /**
     * Класс валидатора, с которым работает виджет (по умолчанию - ManyToManyValidator).
     * Виджет берёт информацию о минимальном и максимальном количестве требуемых
     * элементов из валидатора.
     * @var type 
     */
    'validatorClass' => 'ManyToManyValidator',

    /**
     * Валидатор, привязанный к атрибуту этого виджета.
     * @var CValidator
     */
    'validator' => NULL,

    /**
     * Элементы, отображающиеся в меню выбора.
     * Каждый элемент передаётся по формату  array{'id'=><ID>, 'title'=><TITLE>, 'image'=><url/class>).
     * @var array
     */
    'items' => NULL,

    /**
     * Адрес JSON-интерфейса, с которого подгружаются данные о новых страницах.
     * Аргументы интерфейса: {page, search}.
     * 
     * Внимание: не забудьте фильтровать данные в интерфейсах.
     * @var string
     */
    'route' => NULL,

    /**
     * Размер страницы.
     * @var integer
     */
    'pageSize' => 15,

    /**
     * Символ - отступ при отображении вложенных списков
     * @var string
     */
    'tabSymbol' => '<span class="tabSymbol">&nbsp;</span>',

    /**
     * Шаг отступа при отображении вложенных списков
     * @var string
     */
    'tabSize' => 3,

    /**
     * @var array
     */
    'assets' => array(
        'js' => array('mousewheel.plugin.js', 'mtmselect.js'),
        'css' => array('mtmselect.css'),
    ),

    /**
     * Используемые виджетом представления.
     * @var array
     */
    'views' => array(
        'main'=>'main',
        'item'=>'item',
        'menuItem'=>'menuItem',
        'noItems'=>'noItems'
    ),

    /**
     * Каждый элемент может повторяться в списке не более одного раза.
     * Если свойства задано, элементы меню, уже находящиеся в списке выбранных,
     * становятся неактивными.
     * @var int
     */
    'distinct' => true,
    
    /**
     * Используется ли сортировка выбранных элементов (смена порядка между собой).
     * 
     * ПРИМЕЧАНИЕ: для того, чтобы запоминалась позиция элементов, необходимо
     * специальным образом указать тип сортировки для MTM-отношения в основной
     * модели.
     * 
     * @example
     * .
     * .
     * 'categories' => array(self::MANY_MANY, 'NewsCategory',
     *     'news_news_category(news_id, category_id)',
     *     'together' => true, 'order'=>'categories_categories.id ASC'
     * )
     * // 'categories_categories' - это автоматический псевдоним Yii для
     * // промежуточной таблицы
     * .
     * Т.е. необходимо, чтобы сортировка происходила в соответствии с порядком
     * создания записей в промежуточной теблице. Соответственно, в ней должно
     * быть поле-первичный ключ.
     * 
     * @var booelan
     */
    "sortable" => true,
    
    /**
     * Скорость выполнения эффектов (листание меню).
     * @var int
     */
    'menuSlideDuration' => 400,

    /**
     * Заголовок во всплывающей подсказке.
     * @var type 
     */
    'hintTitle' => Yii::t('mtmselect', 'Change more elements from list'),

    /**
     * Текст во всплывающей подсказке.
     * @var type 
     */
    'hintText' => Yii::t(
        'mtmselect', 'Click it, and select one or more items from list. <p><br>'.
        'You can <i>find items</i> by part of name with search tool, builted-in menu.</p>'
    ),

    /**
     * Подпись на кнопке добавления.
     * @var type 
     */
    'buttonLabel' => Yii::t('app', 'Add'),
    
    /**
     * Сообщение, выводящееся в случае отстутствия элементов для выбора.
     * @var string
     */
    'noItemsMessage' => Yii::t('mtmselect', '<strong>No items!</strong><br/>Please add one or more items to related list.'),
);

?>
