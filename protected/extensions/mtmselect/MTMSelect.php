<?php

/**
 * @package mtmselect
 * @author koshevy
 * Many to many selector.
 * Выборка нескольких элементов из связанных моделей ManyToMany.
 * 
 * Привязывается к реляционным аргументам (описанным в Model::relations() как
 * MANY_TO_MANY).
 * 
 * @example
 * Чтобы развернуть элемент формы с этим виджетом, необходимо указать в настройках
 * формы по образцу:
 * 'elements' => array
 * (
 *     'categories'=>array('type'=>Yii::localExtension('mtmselect', 'MTMSelect'),
 *         'route'=>'CategoriesMTMPage'),
 * .
 * .
 * 
 * В параметре 'route' указывается роут действия, по которому будет обращаться
 * виджет для подгрузки данных (постраничный вывод). Если не указывать этот
 * аргумент, разбивка по страницам не будет производится. При этом, страница
 * будет обрезана по превышении @see MTMSelect::MAX_LIST_SIZE.
 * 
 * Действие подгрузки страницы должно иметь следующие аргументы:
 * ($pageNumber, $pageSize, $search = NULL). Имена аргументов могут быть изменены,
 * но порядок должен сохранятся для совместимости.
 * 
 * Виджет имеет стандартное, для элементов с перечислением данных, свойство
 * @see MTMSelect::$items, в котором перечисляются элементы для выбора
 * вида array(id=>title, id1=>title1...). Но если поле не указано, виджет сам
 * анализирует соответствующее отношение модели. Кроме того, если проедполагается
 * работа с большими списками и постраничным выводом, не рекомендуется указывать
 * атрибуты заранее.
 * 
 * В контроллере нужно только вызвать специально подготовленный хелпер
 * @see MTMSelect::getPage($model, $relationName, $pageNumber, $pageSize, $titleField = 'title', $search = NULL),
 * передав ему в аргументах модель,имя отношения, к которому привязан атрибут
 * (проще говоря, атрибут модели для этого виджета) и данные из запроса.
 * Возмоно потребуется указать $titleField - имя атрибута-названия.
 * 
 * Пример рабочего действия:
 * public function actionCategoriesMTMPage($pageNumber, $pageSize, $search = NULL){
 *     Yii::import(Yii::localExtension('MTMSelect', '*'));
 *     MTMSelect::getPage($this->model, 'categories', $pageNumber, $pageSize, 'title', $search);
 * }
 * 
 * Далее, для корректной работы формы при сохранении, назначьте для атирбута, ослуживаемого
 * виджетом, валидатор @see ManyToManyValidator. В нём можно указать ограничение
 * по количеству прикремляемых элементов - минимальное и максимальное количество.
 * 
 * Пример использования валидатора:
 * public function rules()
 * {
 *     return array(
 *         array('categories', Yii::localExtension('mtmselect', 'ManyToManyValidator'), 'min'=>'1', 'max'=>32),
 * .
 * .
 * .
 */
class MTMSelect extends CInputWidget
{
    /**
     * Максимальное количество элементов, выводящихся непостраничным списком.
     */
    const MAX_LIST_SIZE = 255;

    const CONTAINER_HTML_CLASS = 'mtmselect';
    const MENU_HTML_CLASS = 'mtm-menu dropdown-menu';
    
    /**
     * Атрибут модели, к которому привязывается поле.
     * @var string
     */
    public $attribute = NULL;
    
    /**
     * Поле которое используется в модели как название (используется, если формой
     * не было произведено извлечение ID и заголовка).
     * @var string
     */
    public $titleField = NULL;

    /**
     * Модель, из которой происходит выборка.
     * @var ActiveRecord
     */
    public $model = NULL;
    
    /**
     * Класс валидатора, с которым работает виджет (по умолчанию - ManyToManyValidator).
     * Виджет берёт информацию о минимальном и максимальном количестве требуемых
     * элементов из валидатора.
     * @var type 
     */
    public $validatorClass = NULL;

    /**
     * Валидатор, привязанный к атрибуту этого виджета.
     * @var CValidator
     */
    public $validator = NULL;

    /**
     * Элементы, отображающиеся в меню выбора.
     * Каждый элемент передаётся по формату  array{'id'=><ID>, 'title'=><TITLE>, 'image'=><url/class>).
     * @var array
     */
    public $items = NULL;

    /**
     * Класс связанной модели, из которой берутся данные.
     * Может быть указана в ручную, либо анализируется автоматически.
     * @var string
     */
    public $relationClassName = NULL;

    /**
     * Адрес JSON-интерфейса, с которого подгружаются данные о новых страницах.
     * Аргументы интерфейса: {page, search}.
     * 
     * Внимание: не забудьте фильтровать данные в интерфейсах.
     * @var string
     */
    public $route = NULL;

    /**
     * Размер страницы.
     * @var integer
     */
    public $pageSize = NULL;

    /**
     * Символ - отступ при отображении вложенных списков
     * @var string
     */
    public $tabSymbol = NULL;

    /**
     * Шаг отступа при отображении вложенных списков
     * @var string
     */
    public $tabSize = NULL;

    /**
     * @var array
     */
    public $assets = array();

    /**
     * Используемые виджетом представления.
     * @var array
     */
    public $views = array();

    /**
     * Каждый элемент может повторяться в списке не более одного раза.
     * Если свойства задано, элементы меню, уже находящиеся в списке выбранных,
     * становятся неактивными.
     * @var booelan
     */
    public $distinct = NULL;

    /**
     * Используется ли сортировка выбранных элементов (смена порядка между собой).
     * 
     * ПРИМЕЧАНИЕ: для того, чтобы запоминалась позиция элементов, необходимо
     * специальным образом указать тип сортировки для MTM-отношения в основной
     * модели.
     * 
     * @example
     * .
     * .
     * 'categories' => array(self::MANY_MANY, 'NewsCategory',
     *     'news_news_category(news_id, category_id)',
     *     'together' => true, 'order'=>'categories_categories.id ASC'
     * )
     * // 'categories_categories' - это автоматический псевдоним Yii для
     * // промежуточной таблицы
     * .
     * Т.е. необходимо, чтобы сортировка происходила в соответствии с порядком
     * создания записей в промежуточной теблице. Соответственно, в ней должно
     * быть поле-первичный ключ.
     * 
     * @var booelan
     */
    public $sortable = NULL;

    /**
     * Скорость выполнения эффектов (листание меню).
     * @var int
     */
    public $menuSlideDuration = NULL;

    /**
     * Путь действия, по которому происходит обращение для полного обновления
     * дерева. Может понадобиться, если элементы древа имеют сложные шаблоны
     * отображения названий и динамически меняют свойство.
     * @var string
     */
    public $ajaxReloadRoute = NULL;


    /**
     * Заголовок во всплывающей подсказке.
     * @var type 
     */
    public $hintTitle = NULL;

    /**
     * Текст во всплывающей подсказке.
     * @var type 
     */
    public $hintText = NULL;

    /**
     * Подпись на кнопке добавления.
     * @var type 
     */
    public $buttonLabel = NULL;
    
    /**
     * Сообщение, выводящееся в случае отстутствия элементов для выбора.
     * @var string
     */
    public $noItemsMessage = NULL;
    
    /**
     * Заранее подгружаемый список элементов, которые потребуется вывести
     * в списке выделенных.
     * 
     * Отдельно выбираются элементы, если в значении атрибута модели (к которому
     * привязывается виджет) передаются не модели, а ID выделенных элементов,
     * при этом подгрузка осуществляется постранично, и нужных элементов нет
     * в CInputWidget::$items.
     * 
     * Если в списке передаются модели, не используется.
     * Если не используется постраничный вывод, ссылается на $items.
     * 
     * ПРИМЕЧАНИЕ: Если нужен непосредственный список значений (может быть либо
     * списком модели, либо списком ID, используйте $this->value).
     * 
     * @var array
     */
    protected $_selectedItems = array();

    
    /**
     * Количество страниц.
     * @var int
     */
    protected $_maxPage = NULL;

    protected $_containerHtmlOptions = array();
    
    protected $_menuHtmlOptions = array();
    
    
    /**
     * Встроенные скрипты, вшиваемые в страинцу.
     * Применяются для отправки динамических данных на сторону клиента.
     */
    protected function _attachInlineScripts()
    {
        // шаблоны выбранных элементов и элементов меню хранятся в
        // $('$this->htmlOptions['id']').data('MTMSelect_item_template') и
        // $(document).data('MTMSelect_menuItem_template')
        $tmArguments = array('id'=>':id', 'title'=>':title');
        Yii::app()->clientScript->registerScript(
            "MTMSelect_item_template_{$this->htmlOptions['id']}",
            "$('#{$this->htmlOptions['id']}').data('MTMSelect_item_template', '".
            preg_replace('/[\\n\\r]/', '', $this->render($this->views['item'], $tmArguments, true))."');",
            CClientScript::POS_END
        );
        Yii::app()->clientScript->registerScript(
            "MTMSelect_menuItem_template_{$this->htmlOptions['id']}",
            "$('#{$this->htmlOptions['id']}').data('MTMSelect_menuItem_template', '".
            preg_replace('/[\\n\\r]/', '', $this->render($this->views['menuItem'], $tmArguments, true))."');",
            CClientScript::POS_END
        );
        
        // шаблон пути для обращения к новой странице и/или поиску
        $ajaxUri = rtrim(Yii::app()->controller->createUrl(
            $this->route, array(":pageNumber", ":pageSize", ":search")), '/');
        Yii::app()->clientScript->registerScript("MTMSelect_ajax_uri_{$this->htmlOptions['id']}",
            "$('#{$this->htmlOptions['id']}').data('MTMSelect_ajax_uri', '$ajaxUri');");
    }

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

		static $elementsCounter = 0;
        $this->htmlOptions['id'] = get_class($this)."_".
			($this->attribute ? $this->attribute : $elementsCounter++);
        $this->_attachInlineScripts();

        // данные для анализа внешних связей
        if(!$this->items)
        {
            if(!$this->relationClassName)
            {
                $relations = $this->model->relations();
                if($relations[$this->attribute][0] != CActiveRecord::MANY_MANY)
                    throw new CException(Yii::t('mtmselect', 'This widget can be used for MANY TO MANY relations only.'));
                    $class = $this->relationClassName = $relations[$this->attribute][1];
            }
            else $class = $this->relationClassName;
        }

        // значение можно указать, а можно взять динамически
        if(is_null($this->value)) $this->value = $this->model->{$this->attribute};

        // самостоятельная загрузка данных из модели: анализируются связи модели
        // (должны использваться MANY TO MANY)
        if(!$this->items)
        {
			$this->items = array();
			$models = $class::model();
			$models->dbCriteria->limit = $this->pageSize;

			// приблуды для NestedSets
			if($models instanceof ExtCoreNestedAR)
			{
				$models->dbCriteria->addCondition($models->levelAttribute.'>1');
				$models->dbCriteria->order = $models->leftAttribute.' ASC';

				//$element->items = array();
				foreach($models->findAll() as $item){
					$this->items[$item->primaryKey] = str_repeat($this->tabSymbol,
						$this->tabSize*($item->{$item->levelAttribute}-2)).$item->{$this->titleField};
				}
			}
			else
				foreach($models->findAll() as $item)
					$this->items[$item->primaryKey] =  $item->{$this->titleField};
        }

        // если не используется параметр ROUTE (т.е. подгрузка на месте), то
        // постраничный вывод не используется тоже
        if(!$this->route){
            $this->pageSize = self::MAX_LIST_SIZE;
            $this->_maxPage = 0;

            // все нужные для отображения в списке выбранных есть в списке для
            // создания меню
            $this->_selectedItems = &$this->items;
        }
        else
        {
            $this->_maxPage = ceil($class::model()->count / $this->pageSize);
            if($this->_maxPage){
                
                // обращение к базе для поиска выводимых в списке выделенных
                // элементов (т.к. в списке отображаемых могут быть отображены
                // не все)
                $this->_selectedItems = array();
                if(is_array($this->value)) foreach($this->value as $value){
                    if($value instanceof Model) continue;
                    $this->_selectedItems[] = $value;
                }
                $selectedItems = $class::model()->findAllByPk($this->_selectedItems);
                $this->_selectedItems = array();
                foreach($selectedItems as $item){
                    $this->_selectedItems[$item->primaryKey] = $item->{$this->titleField};
                }
            }
            else $this->_selectedItems = &$this->items;
			
			if($this->_maxPage) $this->_maxPage = $this->_maxPage-1;
        }
		
        // поиск валидатора
        foreach($this->model->getValidators($this->attribute) as $validator)
            if(get_class($validator) == $this->validatorClass){
                $this->validator = $validator; break; }

        // подготовка HTML-атрибутов выводимых элементов
        $this->_prepareHtmlOptions();
    }

    /**
     * Хелпер для выдачи страницы, запрашиваемой виджетом.
     * Используется в контроллерах, привязываемых к виджету.
     * 
     * Выдаёт результат в JSON-формате, который интерпретируется клиентской
     * частью виджета для формирования новой подгружаемой страницы.
     * 
     * Требуется только указать модель, имя MANY_TO_MANY-отношения.
     * 
     * ПРИМЕЧАНИЕ: следует отметить, что действие контроллера, для совместимости
     * с виджетом, должно принимать параметры в следующем порядке:
     * ($pageNumber, $pageSize, $search = NULL).
     * 
     * Поддерживается NestedSets.
     * @todo осттупы для NestedSet не рисутся - трабл, недоделка. Надо сделать.
     * 
     * 
     * @example
     * Пример действия, использующего этот хелпер:
     * .
     * .
     * public function actionCategoriesMTMPage($pageNumber, $pageSize, $search = NULL){
     *     Yii::import(Yii::localExtension('MTMSelect', '*'));
     *     MTMSelect::getPage($this->model, 'categories', $pageNumber, $pageSize, 'title', $search);
     * }
     * .
     * .
     * 
     * @param ActiveRecord $model модель, из которой будут браться данные
     * @param string $relationName имя MANY_TO_MANY отношения или имя класса
     * @param int $pageNumber
     * @param int $pageSize
     * @param string $titleField поле, используемое для подписи
     * @param string $search поисковый запрос
     */
    public static function getPage($model, $relationName, $pageNumber, $pageSize, $titleField = 'title', $search = NULL)
    {
        ExtCoreJson::$use = true;

        $relations =  $model->relations();
        if(!isset($relations[$relationName]))
             $modelClass = $relationName;
        else $modelClass = $relations[$relationName][1];

        $model = $modelClass::model();

		if($model instanceof ExtCoreNestedAR){
			$model->dbCriteria->addCondition('t.'.$model->levelAttribute.">'1'");
			$model->dbCriteria->order = $model->leftAttribute;
		}

        if($search) $model->dbCriteria->addSearchCondition($titleField, $search);
        $model->activeDataProvider->pagination->itemCount = $model->count;
        $model->activeDataProvider->pagination->currentPage = $pageNumber;
        $model->activeDataProvider->pagination->pageSize = $pageSize;

		// пустышка виджета для загрузки конфига
		Yii::loadExtConfig($mtm = new self, __FILE__);

        $items = array();
        foreach($model->activeDataProvider->data as $item)
		{

			$tabsCount = ($item instanceof ExtCoreNestedAR)
				? $item->{$item->levelAttribute}-2 : 0;

			$items[] = array(
				'id' => $item->primaryKey,
				'title' => ($mtm->tabSize ? str_repeat($mtm->tabSymbol, $mtm->tabSize*$tabsCount) : NULL) .
					$item->$titleField,
				'img' => NULL
			);
        }

        ExtCoreJson::$data = array(
            'items' => $items,
            'maxPage' => ($maxPage=ceil($model->count / $pageSize))?$maxPage-1:$maxPage
        );
        ExtCoreJson::response();
    }

    /**
     *  Подготовка  HTML-свойств для выводимых элементов.
     */
    protected function _prepareHtmlOptions()
    {
        // свойства главного контейнера
        $this->_containerHtmlOptions = $this->htmlOptions;
        $this->_containerHtmlOptions['distinct'] = $this->distinct;
        $this->_containerHtmlOptions['duration'] = $this->menuSlideDuration;
        if(!isset($this->_containerHtmlOptions['class'])) $this->_containerHtmlOptions['class'] = '';
        $this->_containerHtmlOptions['class'] .= ' '.self::CONTAINER_HTML_CLASS;
        if($this->sortable) $this->_containerHtmlOptions['class'] .= ' sortable';

        // свойства меню выбора
        $this->_menuHtmlOptions = array(
            'class' => self::MENU_HTML_CLASS,
            'page-number' => 0,
            'page-size' => $this->pageSize,
            'max-page' => $this->_maxPage
        );

        if($this->validator){
            $this->_containerHtmlOptions['max'] = $this->validator->max;
            $this->_containerHtmlOptions['min'] = $this->validator->min;
        }
    }

    public function run(){
        if(sizeof($this->items))$this->render($this->views['main']);
        else $this->render($this->views['noItems']);
    }
}

?>
