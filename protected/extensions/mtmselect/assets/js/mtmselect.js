/**
 * @package mtmselect
 * @author koshevy
 * @descr
 * Клиент-приложение для виджета Yii MTMSelect (Many To Many Select).
 * Не требуется никаких дополнительных действий.
 * 
 * Требует для своей работы благин client.js.
 * 
 * Добавляет к элементам JQuery метод @see changePage(direction).
 * 
 * Все необходимые настройки берёт с серверной стороны.
 * Смотрите представление 'main' к виджету, реализующее плагин.
 * 
 * @events
 * OnMTMSelectUpdate - вызывается при любых обновлениях (листание страниц меню,
 * поиск, выбор элемента, удаление элемента из списка). Не очищайте исторю события,
 * т.к. к нему привязаны родные обработки плагина.
 * OnMTMSelectAdd - вызывается при выборе элемента.
 * OnMTMSelectRemove - вызывается при удалении элемента из списка.
 * 
 * @todo: проверялся только в Chrome и FF.
 **/
jQuery.fn.extend({
    
    /**
     * Смена страницы в меню.
     * Метод сам берёт значение поля поиска и вставляет в запрос.

     * @param direction - направление:
     * 'prev' - предыдущая страница;
     * 'next' - следующая;
     * 'search'- применение поискового фильтра.
     */
	changePage: function(direction){
        var $menu = $(this),
            $owner = $(this).closest('.mtmselect'),
            pageNumber = parseInt($menu.attr('page-number')),
            pageSize = $menu.attr('page-size'),
            maxPage = $menu.attr('max-page'),
            search = $menu.find("#search").val(),
            uri = $owner.data('MTMSelect_ajax_uri'),
            template = $owner.data('MTMSelect_menuItem_template');

        // если предыдущее листание не завершилось
        if($menu.find('.tmp-wrapper').length || $(this).hasClass('clientBusy'))
            return false;

        if(direction == 'prev'){
            if(pageNumber<1) return;pageNumber--;}
        else if(direction == 'next'){
            if(pageNumber>=maxPage) return;pageNumber++;}
        else if(direction == 'search'){pageNumber = 0;}
        else return;
        
        // сразу указывается, чтобы во время загрузки не грузануть лишнего
        $menu.attr('page-number', pageNumber);

        uri = uri.replace(/:pageNumber/g, pageNumber);
        uri = uri.replace(/:pageSize/g, pageSize);
        uri = uri.replace(/:search/g, search);


		var fixedWidth = $($menu.find('.mtm-menu-item').get(0)).width();

        // помечает старые элементы как 'old-items' и оборачивает во временный контейнер
        // с фиксированной шириной
        if($menu.find('.mtm-menu-item').length)
             $menu.find('.mtm-menu-item').addClass('old-items').wrapAll('<li style="width:'+fixedWidth+'px" class="tmp-wrapper"></li>');
        else $menu.find('.up.divider').after('<li style="width:'+fixedWidth+'px" class="tmp-wrapper"></li>');
        var $tmpWrapper = $menu.find('.tmp-wrapper');
            $tmpWrapper.css('height', $tmpWrapper.height()+'px')
            .css('position', 'relative').css('overflow', 'hidden');

        // оборачивание всех старых в ещё один отдельный контейнер,
        // и такая же процедура с новыми
        $tmpWrapper.find('.old-items').wrapAll('<li style="width:'+fixedWidth+'px" class="old-items-wrapper"></li>');
        if(direction == 'next'){
             $tmpWrapper.prepend('<li style="width:'+fixedWidth+'px" class="new-items-wrapper"></li>');}
        else $tmpWrapper.append('<li style="width:'+fixedWidth+'px" class="new-items-wrapper"></li>');

        var $oldWrapper = $tmpWrapper.find('.old-items-wrapper');
        var $newWrapper = $tmpWrapper.find('.new-items-wrapper');
        
        if(direction == 'next'){
            $oldWrapper.css('top', "0px").css('position', 'absolute');
            $newWrapper.css('top', $oldWrapper.height()+"px").css('position', 'absolute');            
        }else{
            $oldWrapper.css('top', "0px").css('position', 'absolute');
            $newWrapper.css('top', "-"+$oldWrapper.height()+"px").css('position', 'absolute');
        }

        $(this).request(uri, {}, function(response){
            for(var i in response['items']){
                if(!response['items'][i]['id']) continue;
                var item = template;

                item = item.replace(/:id/g, response['items'][i]['id']);
                item = item.replace(/:title/g, response['items'][i]['title']);
                item = item.replace(/:image/g, response['items'][i]['image']);


                $newWrapper.append(item);
            }

            var maxPage = response['maxPage'];
            $menu.attr('max-page', maxPage);
            if(pageNumber>=maxPage) pageNumber = maxPage;

            // ещё раз указывается - утверждённое
            $menu.attr('page-number', pageNumber);

            var hTmpOld = $oldWrapper.height();
            var hTmpNew = $newWrapper.height();

            var afterSlide = function(){
                $menu.find('.oldItemsWrapper,.old-items').remove();
                $tmpWrapper.replaceWith($newWrapper.html());
                $menu.trigger('OnMTMSelectUpdate');
                $menu.css('height', $menu.height()-(hTmpOld-hTmpNew));
            };
            $menu.css('height', $menu.height()+'px');

            if((direction == 'prev')){
                $newWrapper.animate({top:"0px"},$menu.attr('duration'), afterSlide);
                $oldWrapper.animate({top:$newWrapper.height()+"px"}, $menu.attr('duration'));
            }
            else{
                $newWrapper.animate({top:"0px"},$menu.attr('duration'), afterSlide);
                $oldWrapper.animate({top:"-"+$oldWrapper.height()+"px"}, $menu.attr('duration'));
            }

        });
    }
});

function initMTMSelect()
{	
    // действия при обновлении списка
    $('.mtmselect').bind('OnMTMSelectUpdate', function(){
        if($.popover) $('[rel="popover"]').popover();
        if($.tooltip) $('[rel="tooltip"]').tooltip();

        $('.mtmselect .mtm-menu-item.disabled').removeClass('disabled');
        $('.mtmselect').each(function(){
            var $owner = $(this);
            if($owner.attr('distinct')){
                $(this).find('.mtmitem').each(function(){
                    $owner.find('.mtm-menu-item#'+$(this).attr('id')).addClass('disabled');
                });
            }
        });

        var $owner = $(this).closest('.mtmselect'),
            $menu = $owner.find('.mtm-menu');

        if($menu.attr('page-number')<1)
            $menu.find('.prev').addClass('disabled');
        else
            $menu.find('.prev').removeClass('disabled');
        if($menu.attr('page-number')>=$menu.attr('max-page'))
            $menu.find('.next').addClass('disabled');
        else
            $menu.find('.next').removeClass('disabled');

        $('.mtm-menu-item').unbind('click');
        $('.mtm-menu-item.disabled').click(function(){return false;});

        // выбор нового элемента в меню
        $('.mtmselect .mtm-menu-item a').unbind('click');
        $('.mtmselect .mtm-menu-item a').click( function(evt){

            var id = $(this).closest('.mtm-menu-item').attr('id'),
                title = $(this).html(),
                $owner = $(this).closest('.mtmselect'),
                template = $owner.data('MTMSelect_item_template');

            // такая уже есть (для distinct="1" нельзя)
            if($owner.attr('distinct'))
                if($owner.find('.items [id="'+id+'"]').length)
                    return false;


            template = template.replace(/:id/g, id);
            template = template.replace(/:title/g, title);
            template = $(template);

            $owner.find('.items').append(template);
            $(this).closest('.mtmselect').trigger('OnMTMSelectAdd');
            $(this).closest('.mtmselect').trigger('OnMTMSelectUpdate');

            return false;

        });
        
        // максимальное и минимальное количество элементов
        var maxItemsCount = $(this).attr('max'),
            minItemsCount = $(this).attr('min'),
            $addButton = $(this).find('.add');
        if($(this).find('.mtmitem').length >= maxItemsCount){
             $addButton.closest('.open').removeClass('open');
             $addButton.hide();
        }
        else $addButton.show();
        
    });

    $('.mtmselect').trigger('OnMTMSelectUpdate');
    
    $('.mtmselect.sortable .items').sortable({
        placeholder:'placeholder'
    });
    
    // удаление выбранного элемента из списка
    $('.mtmselect .remove-item').live('click', function(){
        $(this).closest('.mtmitem').fadeOut(500, function(){
			var $controlElement = $(this).closest('.mtmselect');
			$(this).remove();
            $controlElement.trigger('OnMTMSelectRemove');
            $controlElement.trigger('OnMTMSelectUpdate');
        });
        $('.popover.fade, .tooltip.fade').remove();
    });

    // кнопка добавления элемента
    if($.dropdown) $('.mtmselect .add').dropdown();
	else
	{
		$('.mtmselect .add').click(function(){
			$(this).parent().toggleClass('open');
		});
	}

    // листание меню
    $('.mtmselect .prev').click(function(){
        $(this).closest('.mtm-menu').changePage('prev');
        return false;
    });
    $('.mtmselect .next').click(function(){
        $(this).closest('.mtm-menu').changePage('next');
        return false;
    });

    $('.mtmselect .mtm-menu').click(function(evt){
        var $target = $(evt.target);
        if( $target.hasClass('mtm-menu') ||
            $target.hasClass('divider') )
            return false;

    })
    $('.mtmselect .searchBlock').click(function(){return false;})
    $('.mtmselect .searchBlock input').change(function(){
        $(this).attr('value', $(this).val());
        $(this).closest('.mtm-menu').changePage('search');
    })
    $('.mtmselect .searchBlock input').keydown(function(evt){
        if(evt.keyCode == 13){$(this).change();return false;}
    });


    // обработка движения колеса мыши в право и лево (для таблиц с соответствующим режимом)
	/**
    $('.mtmselect .mtm-menu').mousewheel(function(event, delta, deltaX, deltaY) {
        if(Math.abs(deltaY) < (Math.abs(deltaX)*3)) return false;
        if(!$(this).hasClass('clientBusy')){
            if(deltaY < 0) $(this).changePage('next');
            else $(this).changePage('prev');
            return false;
        }
    });
	*/

}

$(document).ready(function(){ initMTMSelect(); });

