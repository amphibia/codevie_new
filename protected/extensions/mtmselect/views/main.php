<?
/**
 * @package mtmselect
 * @author koshevy
 * @desc
 * Представление для вывода выбранного элемента в виджете MTMSelect, а также
 * для создания шаблона нового элемента (будет применятся при создании элемента
 * в клиентском скрипте).
 * 
 * Используются следующие параметры:
 * @param int $id - идентификатор записи (указывайте ':id' при создании шаблона)
 * @param string $title - подпись (указывайте ':title' при создании шаблона)
 * @param string $image - иллюстрация (@todo: изображение пока не поддерживается)
 * 
 */

echo CHtml::openTag('div', $this->_containerHtmlOptions);
?>

    <div class="items"><?
        if(is_array($this->value))
		{
			// данные могут придти в виде array([id]=>title...) или array(model, model...),
			// в зависимости от предобраоток модели и формы
			foreach($this->value as $item => $value){
				if($item instanceof Model)
					 $this->render($this->views['item'], array('id'=>$item->primaryKey, 'title'=>$value->{$this->titleField}));
				else{
					$this->render($this->views['item'], array('id'=>$value, 'title'=>$this->_selectedItems[$value]));
				}
			}
		}
    ?></div><?
    ?><div>
        <button class="add btn dropdown-toggle" <? 
        if($this->validator && $this->validator->max && (sizeof($this->value)>=$this->validator->max))
            echo 'style="display:none;"';
        ?> type="button" data-toggle="button" <?
        ?> rel="popover" data-title="<?= $this->hintTitle ?>" <?
        ?> data-content="<?= $this->hintText ?>">
            <span class="icon-plus"></span>
            <?= $this->buttonLabel; ?> <span class="caret"></span>
        </button>
        
        <ul <?= CHtml::renderAttributes($this->_menuHtmlOptions); ?>>

            <div class="searchBlock input-append">
                <div class="loading"><?= Yii::t('app', 'loading'); ?>...</div>
                <input id="search" class="clientDisabled input-medium" type="text" placeholder="<?
                    echo Yii::t('mtmselect', 'Search');
                ?>" /><button class="btn" name="yt0" type="button">
                    <span class="icon-search"></span>
                </button>
            </div>

            <li class="up prev pager-element"><a href="javascript:;"><span class="icon icon-chevron-up"></span></a></li>
            <li class="up divider"></li>

            <?
                foreach($this->items as $id => $title)
                    $this->render($this->views['menuItem'], compact('id', 'title'));
            ?>

            <li class="down divider"></li>
            <li class="down next pager-element"><a href="javascript:;"><span class="icon icon-chevron-down"></span></a></li>
        </ul>

    </div>
<?
echo CHtml::closeTag('div');