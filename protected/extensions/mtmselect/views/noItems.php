<?php
/**
 * @package mtmselect
 * @author koshevy
 * @desc
 * 
 * Представление сообщения об отсутствии наименований для выбора, которое выводится
 * вместо управляющих элементов если связанная модель пуста.
 */
?>

<div class="alert alert-block alert-warning fade in">
    <p><?= $this->noItemsMessage ?></p>
</div>

