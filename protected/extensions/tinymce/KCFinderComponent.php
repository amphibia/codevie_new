<?php

/**
 * @title KCFinder uploads and exploring
 * @description { Itegrate KCFinder plugin to used TinyMCE plugins. Use storage directories, if it enabled. }
 * @preload yes
 * @vendor koshevy
 * @package tinymce
 * 
 * Компонент, подключающий KCFinder к TinyMCE (при условии, что в конфиге к
 * TinyMCE editorConfig['file_browser_callback'] не указан другой обработчик).
 * 
 * Также, подключает к controllerMap контроллер KCFinderController, который
 * осуществляет доступ к серверной части KCFinder.
 * 
 * Если в приложении есть компонент storageComponent, интегрируется с ним,
 * используя его директорию.
 */
class KCFinderComponent extends ExtCoreApplicationComponent
{
    /**
     * Директория - хранилище изображений.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    public $imagesDir = NULL;

    /**
     * Поддериктория для конкретных задачи/пользователя.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    public $subdir = NULL;

    /**
     * ID контроллера - KCFinder.
     * @return string 
     */
    public $controllerID = NULL;

    /**
     * Класс контроллера - KCFinder.
     * @return string 
     */
    public $controllerClass = NULL;    
    
    /**
     * ID компонента - хранилища. Если находит этот компонент в системе, берёт всю
     * информаци. с него.
     * @var string
     */
    public $storageComponentID = NULL;

    /**
     * Имя js-функции, обрабатывающей комманду TinyMCE, открывающей окно загрузки
     * (file_browser_callback).
     * @var string
     */
    public $jsBrowseFunction = NULL;

    /**
     * Хранилище, с которым работает компонент.
     * @var storageComponent
     */
    protected $_storage = NULL;

    /**
     * @var array
     */
    public $assets = array();


	public function init()
	{
		// загрузка конфига по умолчанию
		Yii::loadExtConfig($this, __FILE__);
		Yii::applyAssets($this, __FILE__);

		if($this->storageComponentID && Yii::app()->hasComponent($this->storageComponentID))
		{
			$this->_storage = Yii::app()->{$this->storageComponentID};
			$this->imagesDir = $this->_storage->storageDir;
			$this->subdir = $this->_storage->getContext();
		}

		Yii::app()->controllerMap[$this->controllerID] = array(
			'class' => Yii::localExtension(basename(__DIR__), $this->controllerClass),
			'ownerComponent' => &$this
		);

		return parent::init();
	}
}

?>
