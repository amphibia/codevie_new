<?php

/**
 * @author Slaik
 * Widget TinyMCE Javascript HTML WYSIWYG editor
 *
 * Данные, отправляемые на клиентскую часть (через jQuery data()):
 * 'TinyMCE_filesBrowseTitle' (привязано к document) - TinyMCE::$filesBrowseTitle 
 * 'tinymce_forceAction' (привязано к оригинальному input) - TinyMCE::$forceSaveAction
 * 'tinymce_forceActionMessage' (привязано к оригинальному input) - TinyMCE::$forceSaveMessage
 * 'tinymce_rootContainerSelector' (привязано к оригинальному input) - TinyMCE::$rootContainerSelector
 */
class TinyMCE extends CInputWidget
{

    /**
     * Конфигурация для TinyMCE - плагина.
     * @var array
     */
    public $editorConfig = array();

    /**
     * Стили внешней части, подключаемые для предпросмотра результата.
     * Устанавливает свойство 'content_css'.
     * @var array
     */
    public $faceCssFiles = array();

    /**
     * Свой стиль для редактора.
     * @var string
     */
    public $editorCss = NULL;

    /**
     * Свой стиль для всплывающих окон.
     * @var string
     */
    public $popupCss = NULL;

    /**
     * Метод прямого сохранения текста (в активном контроллере).
     *
     * По умолчанию:
     * actionSetAttributes($id, Array $values, $returnMessage = true, Array $customMessage = NULL).
     * Порядок работы сохраняется.
     * @var string
     */
    public $forceSaveAction = NULL;

    /**
     *  Сообщение, возвращаемое при прямом сохранении.
     * @var string
     */
    public $forceSaveMessage = NULL;

    /**
     * Заголовок окна подгрузки файлов.
     * 
     * ПРИМЕЧАНИЕ: если используется несколько элементов TinyMCE в одной форме,
     * то на заголовок, заданный для последнего из них будет применятся для
     * всех - на стороне клиента значение задаётся только один раз.
     * @var string
     */
    public $filesBrowseTitle = NULL;

    /**
     * ID компонента приложения, который обеспечивает загрузку файлов из TinyMCE.
     * Компонент должен иметь свойство $jsBrowseFunction (JS-функция, реализующей
     * поддержку загрузки) - именно оно используется при конфигурированиии виджета.
     * @var string 
     */
    public $fileUploaderID = NULL;

    /**
     * Свойство, используемое клиентской частью: селектор элемента, являющегося
     * корневым контейнером для редактора, на всю площадь которого будет развёрнут
     * редактор (в режиме full-screen, в обычном режиме применяется элемент
     * по селектору из $ownContainerSelector).
     * 
     * ПРИМЕЧАНИЕ: CSS-свойство элемента 'position' будет установлено в 'relative',
     * и если промежуточный родитель тоже будет задан как 'relative', возникнет
     * сбой при разворачивании. Если же селектор не будет указан, либо будет
     * указан неверно, редактор развернётся на всю страницу.
     * @var string
     */
    public $rootContainerSelector = NULL;

    /**
     * Свойство, используемое клиентской частью: селектор элемента, являющегося
     * корневым контейнером для редактора, на всю площадь которого будет развёрнут
     * редактор в обычном положении (для режима full-screen смотрите $rootContainerSelector).
     * 
     * ПРИМЕЧАНИЕ: CSS-свойство элемента 'position' будет установлено в 'relative',
     * и если промежуточный родитель тоже будет задан как 'relative', возникнет
     * сбой при разворачивании. Если же селектор не будет указан, либо будет
     * указан неверно, редактор развернётся на всю страницу.
     * @var string
     */
    public $ownContainerSelector = NULL;

    /**
     * @var array
     */
    public $assets = array();

    /**
     * @var array
     */
    public $views = array();


    public function init()
    {
        // загрузка конфига по умолчанию
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        $this->_initAssets();
        $this->_prepareToDirectSave();

        $this->editorConfig['body_id'] = $id = get_class($this->model)."_{$this->attribute}";
        $this->editorConfig['execcommand_callback'] = 'TinyMCE_execHandle';
        
        // подключение KCFinder
        if(Yii::app()->hasComponent($this->fileUploaderID))
            $this->editorConfig['file_browser_callback'] = Yii::app()->{$this->fileUploaderID}->jsBrowseFunction;
        // отправка данных на сторону клиента
        $script = "\n\ntinyMCE.init({mode:'exact', elements:'$id'," .
                  "script_url: '{$this->assets['directory']}/js/tiny_mce.js',".
                  "language: '".(Yii::app()->language == 'kz' || Yii::app()->language == 'kk' ? 'ru' : Yii::app()->language)."'," .
                  trim(json_encode($this->editorConfig), '[]{}') . "});\n\n";
        $script .= "$(document).data('TinyMCE_filesBrowseTitle', '".addslashes($this->filesBrowseTitle)."');"; 
        Yii::app()->clientScript->registerScript("tinymce_$id", $script);
        
        // селекторы занимаемых областей
        Yii::app()->clientScript->registerScript("tinymce_rootContainerSelector_$id",
            '$(\'[name="'.(get_class($this->model)."[{$this->attribute}]").'"]\').'.
            'data("tinymce_rootContainerSelector", "'.$this->rootContainerSelector.'");');
        Yii::app()->clientScript->registerScript("tinymce_ownContainerSelector_$id",
            '$(\'[name="'.(get_class($this->model)."[{$this->attribute}]").'"]\').'.
            'data("tinymce_ownContainerSelector", "'.$this->ownContainerSelector.'");');
    }

    /**
     * Подготовка настроек для прямого сохранения.
     */
    protected function _prepareToDirectSave()
    {
        $this->htmlOptions['_id'] = $this->model->id;
        $this->htmlOptions['_attribute'] = $this->attribute;
        
        // обратная связь при сохранении напрямую
        if($this->forceSaveAction && !$this->model->isNewRecord)
        {
            $this->editorConfig['theme_advanced_buttons1'] = 'save,'
                .$this->editorConfig['theme_advanced_buttons1'];

            $url = Yii::app()->controller->createUrl($this->forceSaveAction, false);
            Yii::app()->clientScript->registerScript("tinymce_forceAction_{$this->attribute}_{$this->model->id}",
                '$(\'[name="'.(get_class($this->model)."[{$this->attribute}]").'"]\').'.
                'data("tinymce_forceAction", "'.$url.'");', CClientScript::POS_READY);
			Yii::app()->clientScript->registerScript("tinymce_forceActionMessage_{$this->attribute}_{$this->model->id}",
				'$(\'[name="'.(get_class($this->model)."[{$this->attribute}]").'"]\').'.
				'data("tinymce_forceActionMessage", "'.addslashes($this->forceSaveMessage).'");', CClientScript::POS_READY);
        }
    }

    /**
     * Подготовка дополнительных внешних ассетсов.
     */
    protected function _initAssets()
    {
        $this->editorConfig['content_css'] = implode(',', $this->faceCssFiles);

        if($this->editorCss){
            $this->editorConfig['editor_css'] = ((substr($this->editorCss, 0, 1) != '/')
                ? $this->assets['directory']:NULL) ."/css/{$this->editorCss}"; }

		if($this->popupCss){
			$this->editorConfig['popup_css'] = ((substr($this->popupCss, 0, 1) != '/')
				? $this->assets['directory']:NULL) ."/css/{$this->popupCss}"; }
    }


    public function run() {
        if(!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = NULL;
        $this->htmlOptions['class'] .= ' tinymce';
     
        $this->render($this->views['main']);
    }

}
