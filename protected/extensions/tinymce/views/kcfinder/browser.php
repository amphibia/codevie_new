<script src="/<?=Yii::app()->controller->id?>/joiner" type="text/javascript"></script>
<script src="/js/client.js"></script>
<script type="text/javascript">
<?php IF (isset($browser->opener['TinyMCE']) && $browser->opener['TinyMCE']): ?>
<script src="<?php echo $browser->config['_tinyMCEPath'] ?>/tiny_mce_popup.js" type="text/javascript"></script>
<?php ENDIF ?>
browser.version = "<?php echo browser::VERSION ?>";
browser.support.chromeFrame = <?php echo (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), " chromeframe") !== false) ? "true" : "false" ?>;
browser.support.zip = <?php echo (class_exists('ZipArchive') && !$browser->config['denyZipDownload']) ? "true" : "false" ?>;
browser.support.check4Update = <?php echo ((!isset($browser->config['denyUpdateCheck']) || !$browser->config['denyUpdateCheck']) && (ini_get("allow_url_fopen") || function_exists("http_get") || function_exists("curl_init") || function_exists('socket_create'))) ? "true" : "false" ?>;
browser.lang = "<?php echo text::jsValue($browser->lang) ?>";
browser.type = "<?php echo text::jsValue($browser->type) ?>";
browser.theme = "<?php echo text::jsValue($browser->config['theme']) ?>";
browser.access = <?php echo json_encode($browser->config['access']) ?>;
browser.dir = "<?php echo text::jsValue($browser->session['dir']) ?>";
browser.uploadURL = "<?php echo text::jsValue($browser->config['uploadURL']) ?>";
browser.thumbsURL = browser.uploadURL + "/<?php echo text::jsValue($browser->config['thumbsDir']) ?>";
<?php IF (isset($browser->get['opener']) && strlen($browser->get['opener'])): ?>
browser.opener.name = "<?php echo text::jsValue($browser->get['opener']) ?>";
<?php ENDIF ?>
<?php IF (isset($browser->opener['CKEditor']['funcNum']) && preg_match('/^\d+$/', $browser->opener['CKEditor']['funcNum'])): ?>
browser.opener.CKEditor = {};
browser.opener.CKEditor.funcNum = <?php echo $browser->opener['CKEditor']['funcNum'] ?>;
<?php ENDIF ?>
<?php IF (isset($browser->opener['TinyMCE']) && $browser->opener['TinyMCE']): ?>
browser.opener.TinyMCE = true;
<?php ENDIF ?>
browser.cms = "<?php echo text::jsValue($browser->cms) ?>";
_.kuki.domain = "<?php echo text::jsValue($browser->config['cookieDomain']) ?>";
_.kuki.path = "<?php echo text::jsValue($browser->config['cookiePath']) ?>";
_.kuki.prefix = "<?php echo text::jsValue($browser->config['cookiePrefix']) ?>";
$(document).ready(function() {
    browser.resize();
	browser.init();
    $('#all').css('visibility', 'visible');
});
$(window).resize(browser.resize);
</script>

<style>
$mtime = @filemtime(__FILE__);
if ($mtime) httpCache::checkMTime($mtime);
$browser = new browser();
$config = $browser->config;
ob_start();

?>
html, body {
    overflow: hidden;
}

body, form, th, td {
    margin: 0;
    padding: 0;
}

a {
    cursor:pointer;
}

* {
    font-family: Tahoma, Verdana, Arial, sans-serif;
    font-size: 11px;
}

table {
    border-collapse: collapse;
}

#all {
    vvisibility: hidden;
}

#left {
    float: left;
    display: block;
    width: 25%;
}

#right {
    float: left;
    display: block;
    width: 75%;
}

#settings {
    display: none;
    padding: 0;
    float: left;
    width: 100%;
}

#settings > div {
    float: left;
}

#folders {
    padding: 5px;
    overflow: auto;
}

#toolbar {
    padding: 5px;
}

#files {
    padding: 5px;
    overflow: auto;
}

#status {
    padding: 5px;
    float: left;
    overflow: hidden;
}

#fileinfo {
    float: left;
}

#clipboard div {
    width: 16px;
    height: 16px;
}

.folders {
    margin-left: 16px;
}

div.file {
    overflow-x: hidden;
    width: <?php echo $config['thumbWidth'] ?>px;
    float: left;
    text-align: center;
    cursor: default;
    white-space: nowrap;
}

div.file .thumb {
    width: <?php echo $config['thumbWidth'] ?>px;
    height: <?php echo $config['thumbHeight'] ?>px;
    background: no-repeat center center;
}

#files table {
    width: 100%;
}

tr.file {
    cursor: default;
}

tr.file > td {
    white-space: nowrap;
}

tr.file > td.name {
    background-repeat: no-repeat;
    background-position: left center;
    padding-left: 20px;
    width: 100%;
}

tr.file > td.time,
tr.file > td.size {
    text-align: right;
}

#toolbar {
    cursor: default;
    white-space: nowrap;
}

#toolbar a {
    padding-left: 20px;
    text-decoration: none;
    background: no-repeat left center;
}

#toolbar a:hover, a[href="#upload"].uploadHover {
    color: #000;
}

#upload {
    position: absolute;
    overflow: hidden;
    opacity: 0;
    filter: alpha(opacity:0);
}

#upload input {
    cursor: pointer;
}

#uploadResponse {
    display: none;
}

span.brace {
    padding-left: 11px;
    cursor: default;
}

span.brace.opened, span.brace.closed {
    cursor: pointer;
}

#shadow {
    position: absolute;
    top: 0;
    left: 0;
    display: none;
    background: #000;
    z-index: 100;
    opacity: 0.7;
    filter: alpha(opacity:50);
}

#dialog, #clipboard, #alert {
    position: absolute;
    display: none;
    z-index: 101;
    cursor: default;
}

#dialog .box, #alert {
    max-width: 350px;
}

#alert {
    z-index: 102;
}

#alert div.message {
    overflow-y: auto;
    overflow-x: hidden;
}

#clipboard {
    z-index: 99;
}

#loading {
    display: none;
    float: right;
}

.menu {
    background: #888;
    white-space: nowrap;
}

.menu a {
    display: block;
}

.menu .list {
    max-height: 0;
    overflow-y: auto;
    overflow-x: hidden;
    white-space: nowrap;
}

.file .access, .file .hasThumb {
    display: none;
}

#dialog img {
    cursor: pointer;
}

#resizer {
    position: absolute;
    z-index: 98;
    top: 0;
    background: #000;
    opacity: 0;
    filter: alpha(opacity:0);
}
</style>

<script type="text/javascript">
$('body').noContext();
</script>
<div id="resizer"></div>
<div id="shadow"></div>
<div id="dialog"></div>
<div id="alert"></div>
<div id="clipboard"></div>
<div id="all">
<div id="left">
    <div id="folders"></div>
</div>
<div id="right">
    <div id="toolbar">
        <div>
        <a href="kcact:upload"><?php echo $browser->label("Upload") ?></a>
        <a href="kcact:refresh"><?php echo $browser->label("Refresh") ?></a>
        <a href="kcact:settings"><?php echo $browser->label("Settings") ?></a>
        <div id="loading"></div>
        </div>
    </div>
    <div id="settings">

    <div>
    <fieldset summary="view" id="view">
    <legend><?php echo $browser->label("View:") ?></legend>
        <input id="viewThumbs" type="radio" name="view" value="thumbs" /></th>
        <label for="viewThumbs">&nbsp;<?php echo $browser->label("Thumbnails") ?></label>
        <input id="viewList" type="radio" name="view" value="list" />
        <label for="viewList">&nbsp;<?php echo $browser->label("List") ?></label>
    </fieldset>
    </div>

    <div>
    <fieldset summary="show" id="show">
    <legend><?php echo $browser->label("Show:") ?></legend>
            <input id="showName" type="checkbox" name="name" />&nbsp;<label for="showName">&nbsp;<?php echo $browser->label("Name") ?></label>
            <input id="showSize" type="checkbox" name="size" />&nbsp;<label for="showSize">&nbsp;<?php echo $browser->label("Size") ?></label>
            <input id="showTime" type="checkbox" name="time" />&nbsp;<label for="showTime">&nbsp;<?php echo $browser->label("Date") ?></label>
    </fieldset>
    </div>

    <div>
    <fieldset summary="order" id="order">
    <legend><?php echo $browser->label("Order by:") ?></legend>
        <input id="sortName" type="radio" name="sort" value="name" /></th>
        <label for="sortName">&nbsp;<?php echo $browser->label("Name") ?></label>
        <input id="sortType" type="radio" name="sort" value="type" />
        <label for="sortType">&nbsp;<?php echo $browser->label("Type") ?></label>
        <input id="sortSize" type="radio" name="sort" value="size" />
        <label for="sortSize">&nbsp;<?php echo $browser->label("Size") ?></label>
        <input id="sortTime" type="radio" name="sort" value="date" />
        <label for="sortTime">&nbsp;<?php echo $browser->label("Date") ?></label>
        <input id="sortOrder" type="checkbox" name="desc" />
        <label for="sortOrder">&nbsp;<?php echo $browser->label("Descending") ?></label>
    </fieldset>
    </div>

    </div>
    <div id="files">
        <div id="content"></div>
    </div>
</div>
<div id="status"><span id="fileinfo">&nbsp;</span></div>
</div>

