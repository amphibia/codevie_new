<?php

return array
(
    /**
     * Заголовок окна.
     * @var string
     */
    'title' => Yii::t('kcfinder', 'Browse file'),


    'layout' => Yii::localExtension('tinymce', 'views.kcfinder.layouts.browser'),
    
    'assets' => array(
        'core' => array('jquery'),

        'js' => array(
            'kcfinder/jquery.rightClick.js',
            'kcfinder/jquery.drag.js',
            'kcfinder/helper.js',
            'kcfinder/browser/0bject.js',
            'kcfinder/browser/clipboard.js',
            'kcfinder/browser/dropUpload.js',
            'kcfinder/browser/files.js',
            'kcfinder/browser/folders.js',
            'kcfinder/browser/init.js',
            'kcfinder/browser/misc.js',
            'kcfinder/browser/settings.js',
            'kcfinder/browser/toolbar.js',
            'kcfinder/browser/oxygen_theme_init.js',
            'tiny_mce_popup.js',
        ),
        
        'css' => array('kcfinder/oxygen_theme_style.css')
    ),
);

?>
