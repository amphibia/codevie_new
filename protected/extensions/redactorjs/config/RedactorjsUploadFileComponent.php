<?php

return array
(
    /**
     * Директория - хранилище изображений.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    'imagesDir' => NULL,

    /**
     * Поддериктория для конкретных задачи/пользователя.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    'subdir' => NULL,
    
    /**
     * ID контроллера - KCFinder.
     * @return string 
     */
    'controllerID' => 'fileUpload',

    /**
     * Класс контроллера - KCFinder.
     * @return string 
     */
    'controllerClass' => 'RedactorjsUploadFileController',

);

?>
