<?php

/**
 * @title RedactorjsImageUpload
 * @description { Регистрирует контроллер RedactorjsImageUploadController }
 * @preload yes
 * @vendor vitaszanoza
 * @package tinymce
 *
 */

class RedactorjsUploadFileComponent extends ExtCoreApplicationComponent
{
	/**
	 * Директория - хранилище изображений.
	 * Если приложением используется компонент storageComponent,
	 * свойство будет переопределено в соответствии с его настройками.
	 *
	 * @var string
	 */
	public $imagesDir = NULL;

	/**
	 * Поддериктория для конкретных задачи/пользователя.
	 * Если приложением используется компонент storageComponent,
	 * свойство будет переопределено в соответствии с его настройками.
	 *
	 * @var string
	 */
	public $subdir = NULL;

	/**
	 * ID компонента - хранилища. Если находит этот компонент в системе, берёт всю
	 * информаци. с него.
	 * @var string
	 */
	public $storageComponentID = NULL;


	/**
	 * Имя js-функции, обрабатывающей комманду TinyMCE, открывающей окно загрузки
	 * (file_browser_callback).
	 * @var string
	 */
	public $jsBrowseFunction = NULL;

	/**
	 * @var array
	 */
	public $assets = array();

	/**
	 * ID контроллера - KCFinder.
	 * @return string
	 */
	public $controllerID = NULL;

	/**
	 * Класс контроллера - KCFinder.
	 * @return string
	 */
	public $controllerClass = NULL;


	public function init()
	{
		// загрузка конфига по умолчанию
		Yii::loadExtConfig($this, __FILE__);
		Yii::applyAssets($this, __FILE__);


		Yii::app()->controllerMap[$this->controllerID] = array(
			'class' => Yii::localExtension(basename(__DIR__), $this->controllerClass),
		);

		return parent::init();
	}

	/**
	 * Возвращает расширение файла
	 * @return string
	 */
	public function getType($mimeType){
		$type = null;
		switch ($mimeType){
			case 'image/png': $type='png'; break;
			case 'image/jpg': $type='jpg'; break;
			case 'image/gif': $type='gif'; break;
			case 'image/jpeg': $type='jpeg'; break;
			case 'image/pjpeg': $type='pjpeg'; break;
			case 'application/zip': $type='zip'; break;
			case 'application/x-rar-compressed': $type='rar'; break;
			case 'application/octet-stream': $type='rar'; break;
			case 'application/pdf': $type='pdf'; break;
			case 'image/vnd.adobe.photoshop': $type='psd'; break;
			case 'application/msword': $type='doc'; break;
			case 'application/rtf': $type='rtf'; break;
			default: $type=null; break;
		}
		return $type;
	}
}

?>
