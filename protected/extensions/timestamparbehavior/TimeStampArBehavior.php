<?php

/**
 * TimeStampArBehavior class file.
 *
 * @author Alex Slaik <alexslaik@gmail.com>
 * @version 0.2
 */

/**
 * TimeStampArBehavior работает с timestamp данными в ActiveRecord
 *
 * При сохранении переводит в timestamp,
 * при выводе формирует короткую дату в формате 
 * текущей локали
 * 
 */
class TimeStampArBehavior extends CActiveRecordBehavior {

	public $fields = array( );
	public $dateWidth = 'short'; // width of the date pattern. It can be 'full', 'long', 'medium' and 'short'. If null, it means the date portion will NOT appear in the formatting result
	public $timeWidth = null; // width of the time pattern. It can be 'full', 'long', 'medium' and 'short'. If null, it means the time portion will NOT appear in the formatting result

	public function beforeValidate( $event ) {
		// Преобразовываем даты в timestamp
		foreach ( $this->fields as $field_name ) {
			if ( $event->sender->$field_name && !ctype_digit( $event->sender->$field_name ) ) {
				$event->sender->$field_name = CDateTimeParser::parse( $event->sender->$field_name, Yii::app()->locale->getDateFormat( $this->dateWidth ) );
			}
		}
		parent::beforeValidate( $event );
		return true;
	}

	public function afterValidate( $event ) {
		// Если есть ошибки в форме - вернуть полю первоначальный вид
		if ( $event->sender->errors ) {
			foreach ( $this->fields as $field_name ) {
				if ( $event->sender->$field_name && ctype_digit( $event->sender->$field_name ) ) {
					$event->sender->$field_name = Yii::app()->dateFormatter->formatDateTime( $event->sender->$field_name, $this->dateWidth, $this->timeWidth );
				}
			}
		}
		parent::afterValidate( $event );
		return true;
	}

	public function afterFind( $event ) {
		foreach ( $this->fields as $field_name ) {
			if ( ctype_digit( $event->sender->$field_name ) ) {
				$event->sender->$field_name = Yii::app()->dateFormatter->formatDateTime( $event->sender->$field_name, $this->dateWidth, $this->timeWidth );
			}
		}
		parent::afterFind( $event );
		return true;
	}

	public function afterSave( $event ) {
		foreach ( $this->fields as $field_name ) {
			if ( ctype_digit( $event->sender->$field_name ) ) {
				$event->sender->$field_name = Yii::app()->dateFormatter->formatDateTime( $event->sender->$field_name, $this->dateWidth, $this->timeWidth );
			}
		}
		parent::afterSave( $event );
		return true;
	}

}
