<?php

/**
 * 
 * Хелпер для работы с JSON (при условии работы JS-клиента, поддерживающего
 * формат комманд.
 * 
 * @todo:хелпер в разработке.
 * 
 * POST-параметры, используемые хелпером начинаются с префикса "json_".
 * 
 */
class ExtCoreJson extends CComponent
{
    /**
     * Данные, возвращаемые с запросом.
     * @var array
     */
    public static $data = array('result'=>1);

    /**
     * Свойство, которое будет означать, что системные данные по умолчанию
     * (например вывод сообщений об ошибках) должен происходить в JSON.
     * @var boolean 
     */
    public static $use = false;

    /**
     * Массив с командами для клиента.
     * @var array
     */
    protected static $_commands = array();

    
    /**
     * Использовался ли ExtCoreJson для передачи данных или команд.
     * @return boolean
     */
    public static function isUsed(){
        return self::$use || (sizeof(self::$_commands) || (sizeof(self::$data)>1)
            || (self::$data['result']!=1));
    }

    public static function clearCommands(){ self::$_commands = array();}

    /**
     * Добавление комманды - запроса на подтверждение.
     * @param string $message Сообщение с запросом.
     */
    public static function confirm($message = NULL)
    {
        if(is_null($message)) $message = Yii::t('extCore', 'A you sure?');
        
        // парметр, говорящий о том, что пользователь дал подтверждение
        $confirm = isset($_POST["json_confirm"])
            ? (bool)$_POST["json_confirm"] : false;

        if(!$confirm){
            self::$_commands["confirmation"] = $message;
            self::response();
        }
    }

	/**
	 * Комманда клиенту о необходимости выоплнить вход.
	 * @param string $message
	 */
	public static function auth_error($message = NULL)
	{
		$message or $message = Yii::app()->user->loginRequiredAjaxResponse;
		self::error($message);
		self::$_commands['auth_required'] = true;
	}

    public static function message($message = NULL){
		if(!isset(self::$_commands["message"]))
			self::$_commands["message"] = array();
		self::$_commands["message"][] = $message; }

    public static function error($message = NULL){
        self:: $data['result'] = 0; // пометка, что была ошибка
		if(!isset(self::$_commands["error"]))
			self::$_commands["error"] = array();
		self::$_commands["error"][] = $message; }

	public static function replace_element($element, $value){
		if(!isset(self::$_commands["replace_element"]))
			self::$_commands["replace_element"] = array();
		self::$_commands["replace_element"][] = compact('element', 'value'); }

    /**
     * Ответ JSON-комманд.
	 * 
	 * Аргумент $_POST["json_operation_id"] помечает, что текущее обращение является
	 * логическим продолжением указанной операции.
     */
    public static function response(){
        if(isset($_POST["json_operation_id"]))
            self::$data["json_operation_id"] = intval($_POST["json_operation_id"]);

        //header("Content-Type: text/json");
        header('Content-type: text/json; charset=utf-8');
		echo json_encode(array_merge(self::$data, array("__commands" => self::$_commands)));
        exit;
    }
}

?>
