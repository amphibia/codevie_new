<?php


/**
 * Хелпер с встроенными по умолчанию конверторами компонента OutputRegion
 * (menu, pagination, calendar, table и т.п.).
 */
class OutputHelpers extends CApplicationComponent
{	
	
	/**
	 * @var базовые конверторы данных для OutputRegions
	 */
	protected static $_baseConvertors = array
	(
		"menu"       => array("OutputHelpers", "convertorMenu"),
		"calendar"   => array("OutputHelpers", "convertorCalendar"),
		"pagination" => array("OutputHelpers", "convertorPagination"),
		"table"      => NULL
	);


	public static function convertorMenu($region, $data)
	{
		// класс HTML-списка
		$data["htmlOptions"] = array("class"=>"menu ".$region->name);
        $data = array_merge_recursive($data, Yii::app()->params["menu"]);
  

		// вывод стандартного виджета меню
		if(Yii::app()->controller)
			Yii::app()->controller->widget('zii.widgets.CMenu', $data);

	}
    
 	public static function convertorPagination($region, $data)
	{
		// вывод стандартного виджета меню
		if(Yii::app()->controller)
			Yii::app()->controller->widget('CLinkPager',
            array
            (
                "cssFile"        => '/css/pagination.css',
                "header"         => '',
                "pages"          => $data,
                "maxButtonCount" => Yii::app()->params["paginationButtonCount"],
            )
        );

	}


    public static function convertorCalendar($region, $data)
	{
        Yii::app()->controller->renderPartial("//calendar", $data);
	}


	/** @TODO: должны быть возможность добавлять базовые конверторы к $_baseConvertors */

	/**
	 * для некоторых регионов есть предопределённые
	 * конверторы, привязанных к имени (например, menu, pagination и т.д.,
	 * вне зависимости от того, какой регион для них родительский
	 * @param name string имя региона
	 * @return callback функция-конвертор, или NULL, если к указанному имени
	 * нет привязанных конверторов
	 */
	public static function defaultConvertor($name)
	{
		if(array_key_exists($name, self::$_baseConvertors))
			return self::$_baseConvertors[$name];
		
		return NULL;
	}


}


