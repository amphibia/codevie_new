<?php
/**
 * Надстройка для работы c адресами в приложении.
 */
class ExtCoreUrlManager extends CUrlManager
{
    /**
     * Новая функция формирования URL-ов:
     * вместо /controller/action/arg1/value1/arg2/value2 генерит
     * /controller/action/value1/value2
     * 
     * @param string $route the controller and the action (e.g. article/read)
     * @param array $params list of GET parameters (name=>value). Both the
     * name and value will be URL-encoded. If the name is '#', the corresponding
     * value will be treated as an anchor and will be appended at the end of
     * the URL.
     * @param string $ampersand
     *
     * @param ExtCoreController контроллер, настройки которого используются при формировании URL.
     *
     * @return string формированный URL
     */
    public function createUrl($route, $params = array(), $ampersand='&', ExtCoreController $controller = NULL)
    {
        $url = NULL;
        $controller or $controller = Yii::app()->controller;

        // Автоматическое начало роута (модуль/контроллер/действие) - в случае,
        // если роут не задан.
        if(!$route){
            if($module = Yii::app()->getActiveModule()) $url .= "/". $module->name;
            if($controller) $url .= "/" . $controller->action->id;
        } else $url = "/".trim($route,'/');


        if(sizeof($params))
        {
            $reservedArguments =  array();
            if(isset($controller->reservedArguments))
            	foreach($controller->reservedArguments as $name => $rule)
            	{
            		if(isset($params[$name])){
            			$reservedArguments[$name] = $params[$name];
						unset($params[$name]);
					}
            	}

            foreach($params as $param){
                if(!$param) continue;
                if(is_array($param)){
                     if(sizeof($param)) $url .= "/" . implode("/", $param);
				}
                else $url .= "/$param";
            }
            
            /**
             * @TODO
             * Необходимо указать в документации, что перед указаним номера страницы
             * в URL ставится слово "page" (это правило относится и к интерфейсам
             * сервисов, которые имеют свою, особую, схему формирования URL).
             */
            if(sizeof($reservedArguments))
            	foreach($reservedArguments as $key => $value)
            		$url .= "/$key/$value";
        }


        // если не получается сгенерить URL, обращается
        // к функции предка
        //if(!$url) return parent::createUrl($route, $params, $ampersand);

        return $url ."/";
    }
    
    /**
	 * Переопределяет интерпритацию аргументов из строки запроса.
     * Требуется просто составить список, сопостовление с аргументами будет
     * происходить на уровне контроллера.
	 * @param string $pathInfo path info
	 */
	public function parsePathInfo($pathInfo){
        $pathInfo = trim($pathInfo, '/');
		if(!$pathInfo) return;
        else $_GET = explode("/", $pathInfo);
	}

}