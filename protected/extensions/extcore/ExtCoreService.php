<?php

/*
 * Класс сервиса.
 * Содержит в себе необходимы данные для развёртывания сервисных систем. 
 *
 * Пример развёрнутой сервисной системы - административная система сайта.
 * Она собирается из зарегистрированных сервисов группы "admin",
 * подключенными к системе, не соприкосаясь с ними напрямую.
 */
class ExtCoreService extends ExtCoreExtController
{
	/**
	 * @param array словарь заголовков действий
	 */
	public $dictionary = array();


    public function __construct($id = NULL, $module=null){
		parent::__construct($id, $module);
        Yii::loadExtConfig($this, __FILE__);
    }

    public function getRbacOperation(){
        return "Services";
    }

	/**
	 * @var mixed данные этого сервиса (что угодно: группа меню, заголовок,
     * ключевое слово и т.д.)
	 */
	public $data = array();
    
    private $_filterChain = array();

	/**
	 * Действия этого сервиса (как встроенные, так и вынесенные в отдельный класс).
	 * @return array массив обеъктов ExCoreAction и ExtCoreInlineAction
	 */
	public function getActions()
	{
		static $actions = NULL;

		if(!$actions){
			$serviceMethods = get_class_methods(get_class($this));

			// встроенные методы
			$inlineActions = array();
			foreach($serviceMethods as $method)
				if(preg_match("/^action([A-Z][a-zA-Z0-9_]{0,24})$/", $method, $matches)){
					$actionName = strtolower($matches[1]);
					$inlineActions[$actionName] = new ExtCoreServiceInlineAction($this, $actionName);
				}

			// внешние методы
			$foreignActions = array();
			foreach($this->actions() as $actionID => $actionAlias){
				
                /** @TODO переопределить $this->createActionFromMap **/
				
                // CopyPasted from /yii/framework/web/CController.php at line 410
				$action = $this->createActionFromMap($this->actions(), $actionID, $actionID);

                if($action!==null && !method_exists($action,'run'))
					throw new CException(Yii::t('yii', 'Action class {class} must implement the "run" method.',
						array('{class}'=>get_class($action))));

				$foreignActions[$actionID] = $action;
			}
			
			$actions = array_merge($foreignActions, $inlineActions);
		}

		return $actions;
	}

    /**
     * Создание ссылки на действие сервиса в контексте используемого модуля.
     * @param string $route
     * @param array $params (если указать false, не производтся проверка параметров)
     * @param string $ampersand
     * @return string 
     */
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
		if(preg_match("/^\w{1,64}$/", $route))
			$url = Yii::app()->extCoreServiceUrl($this->id, $route ? $route : $this->action->id, $params);
		else $url = parent::createUrl($route, $params, $ampersand, $this);

		return $url;
    }

    /**
     * Проверка, не является ли действие AJAX
     * @param CAction $action
     * @return boolean
     */
    public function isAjaxAction(CAction $action)
    {
        if(!isset($this->_filterChain[$action->id]))
            $this->_filterChain[$action->id] = CFilterChain::create($this, $action, $this->filters());
        
        foreach($this->_filterChain[$action->id] as $filter)
            if($filter->name == 'ajaxOnly') return true;

        return false;
    }

    
    /**
     * 
     * @param array $arguments аргументы обращения к интерфейсу
     * @param boolean $beforeRun подготовка перед запуском действия
     * @return array ассоциативный массив с аргументами, соответсвующий аргументам действия
     */
    public function prepareArguments($actionID, Array $arguments, $beforeRun = false)
	{
        if(!isset($this->actions[$actionID]))
			$this->missingAction($actionID);

		$action = $this->actions[$actionID];
		$reqArguments = array_keys($action->arguments);

        // учитываются аргументы, приходящие из POST
        $arguments = array_merge($arguments, array_intersect_key($_POST, array_fill_keys($reqArguments, NULL)));        
		$preparedArguments = sizeof($reqArguments) ? array_combine($reqArguments, array_fill(0, sizeof($reqArguments), NULL)) : array();

        // исключение для указание номера страницы:
        // аргументы вида page/\d+ обрабатываются как указание номера страницы
        foreach($this->reservedArguments as $name => $rule){
        	if((false !== $pageArgPosition = array_search($name, $arguments))
            && is_numeric($pageArgPosition))
        	{
            	if(isset($arguments[$pageArgPosition+1])
            		&& !isset($arguments[$name])
            		&& preg_match($rule, $arguments[$pageArgPosition+1]))
        	    {
    	            $arguments[$name] = $arguments[$pageArgPosition+1];
	                unset($arguments[$pageArgPosition+1], $arguments[$pageArgPosition]);
            	}
        	}
        }

		foreach($arguments as $name => $value)
		{   
			if(!sizeof($reqArguments))
				throw new CHttpException(404,Yii::t('extCore','The action "{action}" was unnecessary arguments.',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));

			// безымянный аргумент - встаёт в порядке очерёдности 
			if(is_numeric($name)){
				$position = array_shift($reqArguments);
				$preparedArguments[$position] = $value;
			}

			// именованный аргумент (например, "page") - имеет приоритет
			// на одноимённую позицию в списке требуемых аргументов
			else
			{
                $position = NULL;

				if(!in_array($name, $reqArguments))
					throw new CHttpException(404,Yii::t('extCore','The action "{action}" was unnecessary argument "{argument}".',
						array('{action}'=>$actionID==''?$this->defaultAction:$actionID, '{argument}'=>$name)));

				unset($reqArguments[array_search("$name", $reqArguments)]);
				$preparedArguments[$name] = $value;
			}
            
            
            $index = !is_numeric($name) ? $name : $position;

            // массивы передаётся в JSON
            /** @todo: непонятная ситуация с появлением кавычек при выключенном magic_quotes,
             * из-за чего JSON_DECODE может не срабатывать или работать неправильно */
            if($action->arguments[$index]['isArray']){
                $preparedArguments[$index] = $beforeRun
                ? (is_array($value) ? $value : json_decode(stripslashes($value), true))
                : (is_array($value) ? urlencode(json_encode($value)) : $value);
            }
		}

		foreach($reqArguments as $index => $argumentName)
			if($action->arguments[$argumentName]["isOptional"]) unset($reqArguments[$index]);
            
		if(sizeof($reqArguments))
			throw new CHttpException(404,Yii::t('extCore','Not been sent the required arguments ({arguments}).',
				array('{arguments}'=>implode(", ", array_values($reqArguments)))));


		return $preparedArguments;
    }

    public function run($actionID){
        Yii::app()->raiseEvent('onBeginControllerAction', new CEvent($this));
        return CController::run($actionID);
        Yii::app()->raiseEvent('onEndControllerAction', new CEvent($this));
    }


    /**
     * Обращение к действию (не путать с CAction::runWithParams).
     * @param string actionID идентификатор (имя) интерфейса
     * @param array $arguments аргументы, передаваемые интерфейсу
     */
    public function runWithParams($actionID, Array $arguments = array()){
		$_GET = $this->prepareArguments($actionID, $arguments, true);
        $this->lastActionID = $actionID;
        $this->run($actionID);
    }


}