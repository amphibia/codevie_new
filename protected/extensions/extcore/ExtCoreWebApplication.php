<?php

/*
 * Расширение приложения.
 * Часть методов CModule переопределена один в один, т.к. потребовалось
 * переопределить обращение к компоненту, чтобы при создании сохранять в нём ID
 * в приложении, а т.к. CModule::$_components и CModule::$_componentConfig
 * определены как "private", то пришлось переопределять и связанные методы.
 */
class ExtCoreWebApplication extends CWebApplication
{
    /** @todo: Нужно пройтись по событиям приложения, проверить прикрепляемые
     * аргументы, задокументировать */
    
    // добавление нового
    public function onRegisterService(CEvent $event){}

    // отслеживание запуска сонтроллеров
	public function onActionRun(CEvent $event){}
	public function onBeginControllerAction(CEvent $event){}
	public function onEndControllerAction(CEvent $event){}
    
    // отслеживание сохранений AR-моделей
	public function onARModelBeforeSave(CEvent $event){}
	public function onARModelAfterSave(CEvent $event){}


	public $assets = array();

    protected $_components=array();
    protected $_componentConfig=array();

	/**
	 * @var array массив стандартных подписей названий (на понятном человеческом языке),
	 * соответствующих названиям полей, действий, методов и т.д.
	 */
	public $dictionary = array();


	protected function init()
	{
		// для случаев, когда код подтверждения можно передать только в заголовках
		// (например, при отправке файла)
		if(	isset($_SERVER['HTTP_TOKEN_NAME'])
			&& isset($_SERVER['HTTP_TOKEN_VALUE'])
			&& ($_SERVER['HTTP_TOKEN_NAME'] == Yii::app()->request->csrfTokenName)
		)
			$_POST[$_SERVER['HTTP_TOKEN_NAME']] = $_SERVER['HTTP_TOKEN_VALUE'];

		parent::init();
        Yii::applyAssets($this, __FILE__);
        
        // добавление CSRF-токена к ajax-запросам
		Yii::app()->clientScript->registerScript('ajaxToken',
			"if($) if($.client) $(document).ready(function(){\$.client.config.ajaxOptions.data".
			"['{$this->request->csrfTokenName}']='{$this->request->csrfToken}'});",
			CClientScript::POS_HEAD
		);

		Yii::app()->clientScript->registerScript('ajaxTokenName',
			"if($) $(document).ready(function(){\$(document).data('ajaxTokenName', '{$this->request->csrfTokenName}')});",
			CClientScript::POS_HEAD
		);

		Yii::app()->clientScript->registerScript('ajaxTokenValue',
			"if($) $(document).ready(function(){\$(document).data('ajaxTokenValue', '{$this->request->csrfToken}')});",
			CClientScript::POS_HEAD
		);

	}


	/**
	 * @var array массив c сервисами, разбитыми по группам	
	 */
	protected $_servicesGrouped = array();

	/**
	 * @var array массив ссылок на сервисы (для прямого обращения);
	 * хранит ссылки на все сервисы без разделения на группы
	 */
	protected $_services = array();

    
    /**
     * @var array данные привязанные к сервисам (обычно: title, description,
     * menuGroup). Привязанные данные позволят не инициировать сервисы в индексах,
     * чтобы обратиться к данным. Это поможет избежать больших трат производительности
     * в больших сервисных системах. 
     */
    protected $_serviceData = array();
    
	/**
	 * Обращение к сервисам определённой группы.
	 *
	 * @param string $group группа сервисов
	 * @return array массив сервисов
	 */
	public function getServices($group)
	{
        // перед отдачей, создаются объекты неинициализрованных сервисов
        // (объекты создаются при первом обращении, до этого хранятся имена классов)
        if(isset($this->_servicesGrouped[$group]))
             return array_keys($this->_servicesGrouped[$group]);
        else return NULL;
    }

	/**
	 * Обращение к сервису по идентефикатору.
	 *
	 * @param string $id идентификатор сервиса
	 * @return Service найденный сервис
	 */
    public function getService($id)
    {
        if(isset($this->_services[$id])){
            // если объект ещё не создан
            if(!is_object($this->_services[$id]))
                $this->_services[$id] = new $this->_services[$id]($id);

			return $this->_services[$id];
        }

		else throw new CHttpException(400,
			Yii::t('extCore', 'Service "{service}" not found',
				array("{service}"=>$id)));
    }

    /**
     * Обращение к данным, привязанным к сервису.
     * @param int $id
     * @return mixed | NULL
     */
    public function getServiceData($id){
        return isset($this->_serviceData[$id])
            ? $this->_serviceData[$id] : NULL;
    }

	/**
	 * Регистрация сервиса. Регистрируется класс сервиса, по которому при первом
     * обращении создаётся объект сервиса.
	 *
	 * @param string $service  класс сервиса
     * @param string $group группа сервиса (например, admin)
	 */
	public function registerService($serviceID, $serviceClass, $group = "background", $data = NULL)
	{
        // проверка существования класса
        if(!class_exists($serviceClass))
			throw new CException(
				Yii::t('app', 'Service "{serviceClass}" not found.',
					array('{serviceClass}' => $serviceClass))
            );

		// создание группы
		if(!isset($this->_servicesGrouped[$group]))
			$this->_servicesGrouped[$group] = array();

		// идентификтор должен быть уникальным
		if(isset($this->_services[$serviceID]))
			throw new CException(
                Yii::t('app', 'Service class name must be unique')
            );

		// ссылка на сервис в массиве, разделёном на группы
		$this->_servicesGrouped[$group][$serviceID] = $serviceClass;

        // ссылка на сервис в общей таблицы
        $this->_services[$serviceID] =
        &$this->_servicesGrouped[$group][$serviceID];

        if($data) $this->_serviceData[$serviceID] = $data;
	}

    /**
     * Сссылка на интерфейс сервиса.
     * @param mixed $service сервис (потомок Service) или ID сервиса 
     * @param string $interface интерфейс, к которому будет происходить обращение
     * @param array $arguments массив аргументов к сервису (по порядку, или
     * ключ => значение); если указать false, проверка не будет производиться
     * @return string URI (/example/service/..) обращения к сервису с указанными
     * параметрами
     */
    public function extCoreServiceUrl($serviceID, $actionID = "index", $arguments = array())
    {
        $actionID = strtolower($actionID);

        $service = $this->getService($serviceID);
		$actions = $service->getActions();

        // проверка существования действия
        if(!isset($actions[$actionID]))
            throw new CException( Yii::t("extCore",
                "Cant find action (service:{service}, action:{actionID})",
				array("{service}"=>get_class($service), "{actionID}"=>$actionID)
            ) );

        // проверка, насколько отправляемые аргументы соответствуют аргументам;
        // проверка ссылок, ещё во время создания, поможет отследить вероятные
        // ошибки без надобности прощёлкивать каждый элемент
		if($arguments !== false)
             $arguments = $service->prepareArguments($actionID, $arguments);
        else $arguments = array();

        // формирование ссылки
        $moduleName = $this->getActiveModule() or $moduleName = NULL;
        $route = ($moduleName?"/{$moduleName->id}":NULL) . "/services/{$serviceID}/{$actionID}";
        $url = Yii::app()->urlManager->createUrl($route, $arguments, '&', $service);
        
        return $url;
    }

    /**
     * Обращение к активному модулю
     * @return ExtCoreModule активный модуль
     */
    public function getActiveModule(){
        return ExtCoreModule::getActiveModule();
    }

	/**
	 * Checks whether the named component exists.
	 * @param string $id application component ID
	 * @return boolean whether the named application component exists (including both loaded and disabled.)
	 */
	public function hasComponent($id)
	{
		return isset($this->_components[$id]) || isset($this->_componentConfig[$id]);
	}
    
	/**
	 * Sets the application components.
	 *
	 * When a configuration is used to specify a component, it should consist of
	 * the component's initial property values (name-value pairs). Additionally,
	 * a component can be enabled (default) or disabled by specifying the 'enabled' value
	 * in the configuration.
	 *
	 * If a configuration is specified with an ID that is the same as an existing
	 * component or configuration, the existing one will be replaced silently.
	 *
	 * The following is the configuration for two components:
	 * <pre>
	 * array(
	 *     'db'=>array(
	 *         'class'=>'CDbConnection',
	 *         'connectionString'=>'sqlite:path/to/file.db',
	 *     ),
	 *     'cache'=>array(
	 *         'class'=>'CDbCache',
	 *         'connectionID'=>'db',
	 *         'enabled'=>!YII_DEBUG,  // enable caching in non-debug mode
	 *     ),
	 * )
	 * </pre>
	 *
	 * @param array $components application components(id=>component configuration or instances)
	 * @param boolean $merge whether to merge the new component configuration with the existing one.
	 * Defaults to true, meaning the previously registered component configuration of the same ID
	 * will be merged with the new configuration. If false, the existing configuration will be replaced completely.
	 */

	public function setComponents($components,$merge=true)
	{
		foreach($components as $id=>$component)
		{
			if($component instanceof IApplicationComponent)
				$this->setComponent($id,$component);
			else if(isset($this->_componentConfig[$id]) && $merge)
				$this->_componentConfig[$id]=CMap::mergeArray($this->_componentConfig[$id],$component);
			else
				$this->_componentConfig[$id]=$component;
		}
	}
    
    /**
     * Добавление компонента к приложению (также, компоненты ExtCoreApplicationComponent
     * бдут хранить свой ID в системе)
     * @param string $id
     * @param string $component 
     */
    public function setComponent($id,$component,$merge=true)
    {
        if($component===null)
            unset($this->_components[$id]);
        else
        {
            $this->_components[$id]=$component;
            if(isset($component->id)) $component->id = $id;
            if(!$component->getIsInitialized())
                $component->init();
        }
    }

    /**
     * Обращение к компоненту. В реализации класса ExtCoreWebApplication функция
     * изменена таким образом, чтобы компоненты ExtCoreApplicationComponent
     * хранили собственные идентификаторы в приложениии.
     * @param string $id
     * @param boolean $createIfNull
     * @return CApplicationComponent
     */
    public function getComponent($id,$createIfNull=true)
    {
		if(isset($this->_components[$id])){
			return $this->_components[$id];}
		else if(isset($this->_componentConfig[$id]) && $createIfNull)
		{
			$config=$this->_componentConfig[$id];
			if(!isset($config['enabled']) || $config['enabled'])
			{
				Yii::trace("Loading \"$id\" application component",'system.CModule');
				unset($config['enabled']);
				$component=Yii::createComponent($config);
                if($component instanceof ExtCoreApplicationComponent)
                    $component->id = $id;
				$component->init();
				return $this->_components[$id]=$component;
			}
		}
    }

	/**
	 * Returns the application components.
	 * @param boolean $loadedOnly whether to return the loaded components only. If this is set false,
	 * then all components specified in the configuration will be returned, whether they are loaded or not.
	 * Loaded components will be returned as objects, while unloaded components as configuration arrays.
	 * This parameter has been available since version 1.1.3.
	 * @return array the application components (indexed by their IDs)
	 */
	public function getComponents($loadedOnly=true){
		if($loadedOnly)
			 return $this->_components;
		else return array_merge($this->_componentConfig, $this->_components);
	}


    public function displayError($code,$message,$file,$line)
    {
        if(Yii::app()->request->isAjaxRequest){
            if(ExtCoreJson::isUsed()){
                ExtCoreJson::message($message);
                ExtCoreJson::$data['result'] = 0;
                ExtCoreJson::$data['message'] = $message;
                ExtCoreJson::$data['code'] = $code;
                ExtCoreJson::$data['errorCode'] = $code;
                ExtCoreJson::response();
            }
        }
        
        parent::displayError($code,$message,$file,$line);
    }

}