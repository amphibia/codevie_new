<?php



class ExtCoreOutput extends CList
{

    // события
    public function onBeforeRender($event){}
    public function onAfterRender($event){}

	protected $_name	= NULL;
    protected $_parent	= NULL;
	protected $_convertor = NULL;


    /**
     * Требуется ли обрамлять содержимое региона тегами с именами и параметрами.
     * @var boolean
     */
    public $wrapTags = true;

    /**
     * Текущая глубина просчёта.
     * @var integer 
     */
    public $depth = 0;


    /**
     * Вложенные регионы.
     * @var array
     */
	protected $_regions = array();

    /**
     * @var boolean был ли просчитан этот регион 
     */
    protected $_wasRendered = false;


	public function __construct($name = 'root', $parent = NULL){
        if($parent) $parent__ = $parent->name;
        else $parent__ = NULL;

		parent::__construct();
		$this->_name = $name;
		$this->_parent = $parent;
		self::$_items[] = &$this;

        // по завершении действия производится вывод
        if(!$this->parent){
            Yii::app()->attachEventHandler('onEndControllerAction', function(){
                Yii::app()->output->render(); Yii::app()->output->clear(); });
        }
	}

    public function init(){}


    /**
     * таблица с данными, отправленными на вывод
     * @var array
     */
	protected static $_items = array();


	public static function getById($name){
		return self::$_items[$name]; }

	public static function getUsedIDs(){
		return array_keys(self::$_items); }


	/**
	 * @return string имя элемента
	 */
	public function getName(){
        return $this->_name; }

	/**
	 * @return ExtCoreOutput  родительский элемент
	 */
	public function getParent(){
        return $this->_parent; }


	/**
	 * обращение к регионам элемента
	 * @return array массив с перечислением областей
	 * (подуровней элемента)
	 */
	public function regions(){
		return $this->_regions; }


	/**
	 * обращение к конкретной области
	 * @param id string идентификатор области
	 **/
	public function region($name){
		$regions = $this->regions();
		if($regions) if(array_key_exists($name, $regions) )
			return $regions[$name];

		return $this->addRegion($name);
	}


	// регистрация вложенного региона
	protected function _register($item){
		if(array_key_exists($item->name, $this->_regions))
			throw new CException("Region with same name already exists in thes region.");

		$this->_regions[$item->_name] = $item;
	}

	// добавление вложенного региона
	public function addRegion($name){
		$region = new self($name, $this);
		$this->append($region);
		$this->_register($region);

		return $region;
	}

	// добавление вложенного региона в начало
	public function prependRegion($name){
		$region = new self($name, $this);
		$this->prepend($region);
		$this->_register($region);

		return $region;
	}


    
    // переопределение родительских функций
	public function prepend($item){ $this->insertAt(0, $item); return $this; }
	public function append($item){ $this[] = $item; return $this; }
    public function clear(){ parent::clear(); return $this; }


	/**
     * Привязка конвертора.
     * 
     * @param callback $callback функция или (замыкание)обычная или анонимная)
     * @return ExtCoreOutput ссылка на свой объект (для продолжения цепочки операторов)
     */
	public function setConvertor($callback){
		if(!is_callable($callback))
			throw new CException("Convertor must be callable.");

		$this->_convertor = $callback;
        return $this;
	}

	/**
     * обращение к конвертору региона
     * @return callback  
     */
	public function getConvertor()
	{
		// обращение к своему конвертору
		if(is_callable($this->_convertor))
			return $this->_convertor;

		// конвертору предка
		if($this->_parent instanceof ExtCoreOutput)
			if($parentConvertor = $this->_parent->getConvertor())
				return $parentConvertor;

		// или конвертору по умолчанию по имени элемента
		if($default = OutputHelpers::defaultConvertor($this->name))
			 return $default;
		else return NULL;
	
	}

    
	/**
     * Просчёт региона и его вложений (рекурсивно).
     */
	public function render()
	{
        
        $eventArgument = new CEvent($this);
        $this->raiseEvent('onBeforeRender', $eventArgument);

        $this->_wasRendered = true;
		$convertor = $this->getConvertor();        
        
        // края региона
        //if($this->parent && $this->wrapTags) echo CHtml::openTag('div',
        //    array('region'=>$this->_name, 'parent'=>$this->parent->name));

        if($this->parent) Yii::app()->output->depth++;
		foreach($this as $key => $item)
		{

			// просчёт вложенного региона
			if($item instanceof ExtCoreOutput){
				$item->render();
				continue;
			}

			if(is_callable($item)){
				if($result = call_user_func($item)){
					if($convertor) echo call_user_func($convertor, $result);
					else echo $result; }
            }
            else
            {
				if($convertor){
                    echo call_user_func_array($convertor, array($this, $item));
                }
                else{
					// необработанные данные
					if(is_array($item) || is_object($item)){
						if(defined(YII_DEBUG))
                            if(YII_DEBUG) CVarDumper::dump($item); }

					else echo($item);
				}
			}
        }

        if($this->parent) Yii::app()->output->depth--;


        //if($this->parent && $this->wrapTags) echo CHtml::closeTag('div');
        $this->raiseEvent('onAfterRender', $eventArgument);

        return $this;
	}

}


