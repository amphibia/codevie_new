<?php $this->beginWidget(
		Yii::localExtension('bootstrap', 'widgets.BootModal'),
		array(
			'id'=>'deleteConfirmationModal',
			'autoOpen'=>true,
			'events' => array('hide'=>'function(){history.back();}')
		)
		
	);
?>
 
<div class="modal-header">
    <h4><?= Yii::t('extcore', 'Delete items')?></h4>
</div>
 
<div class="modal-body">
    <p><?= $message ?></p>
</div>
 
<div class="modal-footer">
	<?
		$deleteUrl = $this->createUrl('delete', array(
			'id' => $id,
			'confirmation' => '1'
		));
	?>
	<form action="<?= $deleteUrl ?>" method="POST">
		<input type="hidden" name="returnUrl" value="<?= isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:$this->createUrl('index') ?>" />
		<?php $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
				'buttonType' => 'submit',
				'type'=>'danger',
				'label'=>Yii::t('extcore', 'Delete item(s)'),
				'url' => $this->createUrl('delete', array(
					'id'=>$id,
					'confirmation'=>'1',
					'returnUrl'=>  '"'. urlencode(isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:$this->createUrl('index')).'"'
					)
				)
			)
		);
		?>

		<?php $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
			'label'=>Yii::t('extcore', 'Cancel'),
			'url'=>'#',
			'htmlOptions'=>array('data-dismiss'=>'modal'),
		)); ?>
	</form>
</div>
 
<?php $this->endWidget(); ?>