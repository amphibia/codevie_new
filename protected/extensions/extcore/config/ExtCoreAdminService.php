<?php

return array
(
    /**
     * Конфигурация формы создания.
     * @var array
     */
    'createForm' => array(),

    /**
     * Конфигурация формы редактирования.
     * @var array
     */
    'updateForm' => array(),


    // свойства формы создания
    'createForm' => array
    (
        'showErrorSummary'  => true,

        'buttons'=>array(
            'save' => array('type' => 'submit', 'label' => Yii::t('admin', 'Save')),
            /** @todo Хорошая идея, но учитывая обилие сложных виджетов,
             * для её реализации потребуется пришивать к ним клиент-функцию
             * очистки, срабатываеющей на сабмит формы. */
            //'reset' => array('type' => 'reset', 'label' => Yii::t('admin', 'Reset form'))
        ),
    ),


    /**
     * Поля, отображаемые в таблице индекса (actionIndex).
     * @var array
     */
    'indexTableOptions' => array
    (
        'fieldsOrder' => array('id', 'title', 'update'),
        'route' => 'index',
        'scopes' => array('published'=>function($model, $scopeValue){}, 'unpublished'=>function(){}),
        //'defaultScope' => 'all',
        'buttons' => array(
            'update' => array(
                'htmlOptions' => array(
                    'rel'=>'tooltip',
                    'title' => Yii::t('extcore', 'Item manipulations'),
                ),
                //'type'=>'primary',
                'buttons' => array(
                    array(
                        'label' => ' ',
                        'icon'=>'icon-pencil',
                        'items' => array(
                            'update' => array('label'=>Yii::t('app', 'Open in edit form')),
                            'delete' => array('label'=>Yii::t('app', 'Delete item')),
                        )
                    )
                )
            )
        ),
        'pageSize' => 50,
        'sortVar' => 'orderBy',
		
		/**
		 * Кнопки, которые должны отобразиться в хэдере.
		 * Свойства полностью совместимы с BootstrapButton, за исключением того,
		 * что в качестве 'url' принимаtтся 'route' - шаблон пути.
		 * 
		 * Параметр 'route' указывается следующим образом:
		 * array(<route>, $arg1, $arg2...).
		 * @var array 
		 */
		'headerButtons' => array(
			array('buttonType'=>'link', 'icon'=>'icon-plus', 'label'=>Yii::t('admin', 'Add'), 'route'=>array('create')),
		),
    ),

    /**
     * Адреса представлений, используемых сервисом.
     * @var array 
     */
    'views' => array(
        'create' => 'create',
        'update' => 'update',
        'index' => 'index'
    ),

    /**
     * Текст запроса подтверждения на удаление элементов.
     * @var string
     */
    'deleteConfirmationMessage' => 'Do you wish delete elements {elements}?',

    /**
     * Текст сообщения об успешном удалении элемента.
     * @var string
     */
    'deleteSuccessMessage' => 'Element #{id} was deleted.',

	/**
     * Текст сообщения о неудачном удалении элемента.
     * @var string
     */
    'deleteFailMessage' => 'Error: element #{id} could not be deleted.',

);