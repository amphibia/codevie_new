<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ExtCoreExtController extends ExtCoreController
{
    /**
     * @var boolean использование автоподстановки аргументов в функцию
     * (переопределено в этом классе)
     * @see run
     */
    protected $_useAutoArguments = true;
    
    private $_viewPath;


    public function getRbacOperation(){
        return "Extension controllers";
    }

    public function getViewPath(){
        if($this->_viewPath===null){
            $class=new ReflectionClass(get_class($this));
            $this->_viewPath=dirname($class->getFileName()).DIRECTORY_SEPARATOR.'views';
        }
        return $this->_viewPath;
    }

    public function setViewPath($value){
        $this->_viewPath=$value;
    }


}