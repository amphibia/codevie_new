<?php

return array
(
    "views" => array('form' => '/services/feedbackAdminForm',),
    "modelClass" => 'FeedbackOptionsForm',

    "formConfig" => array(

        'elements' => array(
            'from_email' => array('type' => 'text', 'class' => 'span3'),
            '<div class="control-group hint-bottom">' .
            '<div class="controls">' . Yii::t('forms', 'Email of sender (system account).') . '</div>' .
            '</div>',

            'to_email' => array('type' => 'text', 'class' => 'span3'),
            '<div class="control-group hint-bottom">' .
            '<div class="controls">' . Yii::t('forms', 'Email - receiver.') . '</div>' .
            '</div>',

            '<br/>',

            'bcc' => array('type' => 'text', 'class' => 'span4',
                'placeholder' => Yii::t('users', 'No copies')),
            'type' => array('type' => 'dropdownlist', 'class' => 'span4',
                'items' => array(
                    'mail' => Yii::t('forms', 'PHP native mail function'),
                    'smtp' => Yii::t('forms', 'SMTP email account'),
                )),
            '<div class="accountData">',
            'username' => array('type' => 'text', 'class' => 'span2'),
            'host' => array('type' => 'text', 'class' => 'span3'),
            'port' => array('type' => 'text', 'class' => 'span1'),
            'password' => array('type' => 'password', 'class' => 'span2',
                'placeholder' => Yii::t('users', 'Enter new password...')
            ),

            'auth' => array(
                'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                'attributes' => array(
                    'enabledLabel' => Yii::t('struct', 'On'),
                    'disabledLabel' => '',
                    'enabledStyle' => 'success',
                )
            ),
            'secure' => array('type' => 'default'),

            'debug' => array(
                'type' => Yii::localExtension('bootstrap', 'widgets.TbToggleButton'),
                'attributes' => array(
                    'enabledLabel' => Yii::t('struct', 'On'),
                    'disabledLabel' => '',
                    'enabledStyle' => 'success',
                )
            ),

            '</div>',

            '<br/>',
        ),

        'buttons' => array(
            'save' => array(
                'label' => Yii::t('users', 'Save changes'),
                'type' => 'submit',
                'icon' => 'icon-check',
            )
        )
    ),
);