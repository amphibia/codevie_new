<?php

return array(
	'MIME type error'			=> 'MIME type error',
	'Error occured'			=> 'Error occured',
	'You can`t attach more images for this field'			=> 'You can`t attach more images for this field',
	'Attribute for this field must have "{validator}" validator!'			=> 'Attribute for this field must have "{validator}" validator!',
	'The specified file type is already loaded.'			=> 'The specified file type is already loaded.',
	'Element deleted.'			=> 'Element deleted.',
	'Cancel'			=> 'Cancel',
	'Upload started'			=> 'Upload started',
	'Image'			=> 'Image',
);