<?php

return array(
	'Comments: {count}'			=> 'Коментариев: {count}',
	'No comments'			=> 'Коментариев нет',
	'Send'			=> 'Комментировать',
	'Add comment'			=> 'Добавить комментарий',
	'to answer'			=> 'ответить',
	'answer'			=> 'ответить',
	'Last comments'			=> 'Последние комментарии',
	'to news'			=> 'к новости',
	'CommentsWidget require CommentsAgentComponent, installed as "commentsAgent".'			=> 'CommentsWidget require CommentsAgentComponent, installed as "commentsAgent".',
	'CommentsWidget require CommentsAgentComponent, installed as "commentsagent".'			=> 'CommentsWidget require CommentsAgentComponent, installed as "commentsagent".',
	'Can`t create root comment item!'			=> 'Can`t create root comment item!',
	'Answer'			=> 'Answer',
	'The maximum level of nesting is reached.'			=> 'The maximum level of nesting is reached.',
	'More comments'			=> 'More comments',
	'Comment sent'			=> 'Comment sent',
);