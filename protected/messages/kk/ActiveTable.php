<?php

return array(
	'Search' => 'Поиск',
	'Position of page end' => 'Текущая позиция (по концу страницы)',

	'This element indicate position of page end in summary content.<br/><br/>Current position is {position}%.<br/><br/><i>Click to place of element for jumping to needest position!</i>' =>
	'Индикатор, показывающий положение текущей страницы (по концу страницы) среди всех страниц.<br/><br/>Текущее положение: {position}%.<br/><br/><i>Нажмите для быстрого перехода к соответствующей позиции!</i>',

	'Page {page}. Showed {itemCount} in {totalCount}.' => 'Страница {page}. Показано {itemCount} из {totalCount}.',
	'Sort field by field "{field}"' => 'Отсортировать по полю "{field}"',
	'Click again to turn off this scope.' => 'Нажмите еще раз, чтобы выключить фильтр.',
	'No items' => 'Ничего нет',
	'Use {ctrl}<strong>←</strong> &nbsp;and&nbsp; {ctrl}<strong>→</strong>' => 'Используйте {ctrl}<strong>←</strong> &nbsp;и&nbsp; {ctrl}<strong>→</strong>',
);