<?php

return array(
	'<strong>Cant find operations!</strong><br/>Check your RBAC system.'			=> '<strong>Не найдено ни одной операции!</strong><br/>Убедитесь, что RBAC-система (модуль users) установлена и работает правильно.',
	'Change more elements from list'			=> 'Выберите/изментие больше элементов из списка',
	'Click it, and select one or more items from list. <p><br>You can <i>find items</i> by part of name with search tool, builted-in menu.</p>'			=> 'Нажмите, и выберите один или несколько элементов из меню. Вы можете воспользоваться поиском, встроенным в меню для поиска элементов.',
	'<strong>No items!</strong><br/>Please add one or more items to related list.'			=> 'Связанные данные не найдены.',
	'Remove element from list'			=> 'Убрать элемент из списка',
	'Search'			=> 'Поиск',
	'<strong>Categories!</strong><br/>Please add one or more categories of site content.'			=> '<strong>Сайт не имеет ни одного раздела!</strong><br/>Пожалуйста, создайте хотя бы один раздел.',
	'No tags'			=> 'No tags',
);