<?php

return array(
	'User role update'			=> 'User role update',
	'User roles management'			=> 'User roles management',
	'Element #{id} saved.'			=> 'Element #{id} saved.',
	'Save'			=> 'Save',
	'Element #{id} created. For editing element, go to {edit form}.'			=> 'Element #{id} created. For editing element, go to {edit form}.',
	'User role create'			=> 'User role create',
);