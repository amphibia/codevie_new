<?php

return array(
	'Add new answer'			=> 'Добавить новый вариант ответа',
	'Answer'			=> 'Ответ',
	'Number of votes'			=> 'Количество голосов',
	'Remove answer'			=> 'Удалить вариант ответа',
	'Move answer'			=> 'Переместить вариант ответа',
	'Number of votes must be numeric'			=> 'Количество голосов должно быть числовым',
	'WidgetTitle'			=> 'Голосование',
	'Vote now'			=> 'Ответить',
	'Vote #{voteID} not found!'			=> 'Голосование с ID {voteID} не найдено!',
	'Please select an answer!'			=> 'Пожалуйста, выберите ответ!',
	'You have already voted!'			=> 'Вы уже проголосовали!',
	'answer|answers'			=> 'ответ|ответа|ответов',
);