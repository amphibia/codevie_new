<?php

return array
(
	'templateRange' => array
	(
		'default' => 'Default content page',
		'mainpage' => 'Main page',
		'about' => 'О компании',
		'bestsellers' => 'Bestsellers',
		'sale' => 'Акции',
		'customer_day' => 'Клиентские дни',
		'club' => 'Stars Club',
		'brands' => 'Бренды',
		'contacts' => 'Контакты',
	)
	
);
