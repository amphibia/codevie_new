<?php

return array
(
	'layout' => '//layouts/structLayout',

	'views' => array
	(
		'category' => '/category',
		'page' => '/page',
	),
);