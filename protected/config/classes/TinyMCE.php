<?php

return array
(
    /**
     * Стили внешней части, подключаемые для предпросмотра результата.
     * Устанавливает свойство 'content_css'.
     * @var array
     */
    'faceCssFiles' => array(
        '/css/custom.css?rand=' . rand(),
        '/css/editor.css?rand=' . rand(),
    ),
);
