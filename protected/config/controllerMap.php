<?php

/**
 * В этом файле описываются контроллеры используемых расширений.
 */

return array(
    
    /** 
     * Расширение для закачки и выбора изображений из форм.
     */
    'imagesupload'  => array(
        'class'     => 'ext.imagesuploader.ExtImagesUploadController'
    ),
    
    /**
     * Расширение для работы с авторизацией.
     */
    'auth'          => array(
        'class'     => 'ext.authext.AuthExtController'
    ),
    
    /**
     * Расширение для работы с EAuth авторизацией.
     */
    'eauth'          => array(
        'class'     => 'ext.eauth.EAuthController'
    ),
    
);