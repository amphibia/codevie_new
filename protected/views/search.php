<?php
$search = (isset($_REQUEST['search']) ? $_REQUEST['search'] : NULL);
?>
<div class="searchResults">

    <!-- Форма поиска -->
    <form method="GET" id="searchPanel" action="/search">
        <?=
        CHtml::textField('search', $search, array(
            'placeholder' => Yii::t('custom', 'Search at site...'),
            'id' => 'searchField'
        ));
        ?>
    </form>

    <div class="results">
        <?php
        if (count($this->results)) {
            echo CHtml::tag('h2', array(), Yii::t('custom', 'Found by "{search}" query', array('{search}' => $search)) . ': ' . count($this->results));
            foreach ($this->results as $item) {
                if (($item instanceof StructPage) && $item->category && ($item->category->alias == 'main'))
                    continue;

                echo CHtml::openTag('div', array('class' => 'item'));

                echo CHtml::tag(
                    'h4', array(),
                    CHtml::link($item->title, $item->createUrl())
                );

                if (isset($item->short_text) && $item->short_text)
                    echo CHtml::tag('p', array(), strip_tags($item->short_text));

                if (isset($item->description) && $item->description)
                    echo CHtml::tag('p', array(), nl2br($item->description));

                echo CHtml::closeTag('div');
            }
        } else {
            echo CHtml::tag('div', array('class' => 'alert alert-info'), Yii::t('custom', 'Ничего не найдено'));
        }
        ?>
    </div>

</div>