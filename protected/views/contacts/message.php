<html>
<body>
<?php
if (isset($content['verifyCode'])) {
    unset($content['verifyCode']);
}
foreach ($content as $key => $val) {
    if (!$val) {
        continue;
    }
    echo '<p>' . $this->model->generateAttributeLabel($key) . ': <strong>' . nl2br($val) . '</strong></p>';
}
?>
<p>IP: <strong><?= $_SERVER['REMOTE_ADDR'] ?></strong></p>
</body>
</html>