<div class="row">
<?php

    // редактируемый текст
    if(Yii::app()->struct->currentCategory)
        echo Yii::app()->struct->currentCategory->full_text;

    if(!$this->_sent)
    {
        // вывод формы
        $this->widget(Yii::localExtension('bootforms', 'BootAutoForm'), array(
            'form' => $form,
			'type'=>'vertical',
            'autoElements' => true,
            'repeatActionOption' => false,
        ));
    }
    else{
        ?><h3><?= Yii::t('rennie', 'Your information has been sent. Thank you!'); ?></h3><?
    }
?>
</div>
