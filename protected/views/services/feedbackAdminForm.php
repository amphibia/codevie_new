<div class="page-header">
<h1><?= Yii::t('users', 'Email notifications setup'); ?></h1><br/>
<?

// кнопка "в управление пользователями"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('users', 'Users management'),
    'url'=>Yii::app()->getService('users')->createUrl('index'),
));

?>
</div>

<?php $this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert')); ?>

<h3>Настройка email-аккаунта для отправки писем от имени сайта</h3>

<p style="max-width:500px">
Здесь указываются параметры аккаунт элекронной почты, от имени которого
осуществляется отправка писем с заявками на кредит и резюме соискателя.
</p>

<br/>

<?
// вывод автоформы
$this->widget(Yii::localExtension('bootforms', 'BootAutoForm'),
	array(
		'form' => $form,
		'autoElements' => true,
		'repeatActionOption' => false,
	)
);
?>