<?php

// Главная категория шаблона.
$mainCategory = ($category->parent->type == "group")
    ? $category : $category->parent;


// *** HTML-содержимое раздела ***
?>
    <h1 id="categoryTitle"><?= $category->title ?></h1>
<?= $category->full_text ?>
<?php
if (!$this->_sent) {
    echo $form->render();
} else {
    ?>
    <div class="successfulMessage"><?= Yii::t('forms', 'Your message has been sent.'); ?></div><?
}
