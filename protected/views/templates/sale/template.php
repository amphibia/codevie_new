<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text discount "Скидка"
 * @param pages text date "Дата"
 * @param pages document doc "Условия"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
?>

<!-- .content -->
<main class="content">
    <div class="page-sale">
        <div class="container">
            <h2 class="page-title"><?= $category->title ?></h2>
            <div class="sale-items">
                <?php foreach($category->pages as $page): ?>
                <div class="sale-item">
                    <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                        <div class="sale-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                    <?php endif ?>
                    <div class="column-container">
                        <div class="col-left">
                            <div class="sale-text"> 
                                <b><?= $page->title ?></b>
                                <p><?= nl2br($page->short_text) ?></p>
                                <?php if ($file = Yii::app()->storage->decodeFiles($page->doc)): ?>
                                <a href="http://<?= $file[0]['fileName'] ?>" class="link-more"><?= $file[0]['title']; ?></a>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-right">
                            <div class="sale-number">
                                <strong>
                                    <?= $page->discount ?>
                                </strong>
                               <div class="date"><?= $page->date ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:initial;left:530px;top:50px;bottom:initial;"> <img src="/dist/images/icons-sale/i-1.svg" alt=""> </div>
            <div class="point" style="right:initial;left:780px;top:210px;bottom:initial;"> <img src="/dist/images/icons-sale/i-2.svg" alt=""> </div>
            <div class="point" style="right:-135px;left:initial;top:60px;bottom:initial;"> <img src="/dist/images/icons-sale/i-3.svg" alt=""> </div>
            <div class="point" style="right:-5px;left:initial;top:570px;bottom:initial;"> <img src="/dist/images/icons-sale/i-4.svg" alt=""> </div>
            <div class="point" style="right:-65px;left:initial;top:840px;bottom:initial;"> <img src="/dist/images/icons-sale/i-5.svg" alt=""> </div>
            <div class="point" style="right:120px;left:initial;top:initial;bottom:-300px;"> <img src="/dist/images/icons-sale/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-140px;top:initial;bottom:120px;"> <img src="/dist/images/icons-sale/i-7.svg" alt=""> </div>
            <div class="point" style="right:initial;left:45px;top:550px;bottom:initial;"> <img src="/dist/images/icons-sale/i-8.svg" alt=""> </div>
            <div class="point" style="right:initial;left:5px;top:300px;bottom:initial;"> <img src="/dist/images/icons-sale/i-9.svg" alt=""> </div>
        </div>

        <div class="social-links">
            <div class="container">
                <div class="social-links-title">
                    <h2>Поделиться:</h2>
                </div>
                <div class="likely">
                    <div class="twitter">Твитнуть</div>
                </div>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="vkontakte">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="telegram">Отправить</div>
                </div>
                <div class="likely">
                    <div class="whatsapp">Отправить</div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->
