<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
*
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword html
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

$pages = $category->pages;
$colums = array_chunk($pages, 7);
?>

<!-- .content -->
<main class="content">
    <!--  -->
    <div class="page-brands">
        <div class="container">
            <h2 class="page-title"><?= $category->title ?></h2>
            <div class="brands-container">
                <?php foreach($colums as $pages): ?>
                <div class="col brand-item">
                    <ul>
                        <?php foreach($pages as $page): ?>
                        <li><a href="<?= $page->createUrl() ?>" class="brands-popup"><?= $page->title ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:-65px;left:initial;top:150px;bottom:initial;"> <img src="images/icons-about/i-1.svg" alt=""> </div>
            <div class="point" style="right:-190px;left:initial;top:initial;bottom:-200px;"> <img src="images/icons-about/i-2.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-155px;top:initial;bottom:-200px;"> <img src="images/icons-about/i-4.svg" alt=""> </div>
            <div class="point" style="right:initial;left:20px;top:initial;bottom:200px;"> <img src="images/icons-about/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-30px;top:400px;bottom:initial;"> <img src="images/icons-about/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:310px;top:180px;bottom:initial;"> <img src="images/icons-about/i-7.svg" alt=""> </div>
            <div class="point" style="right:335px;left:initial;top:-120px;bottom:initial;"> <img src="images/icons-about/i-8.svg" alt=""> </div>
        </div>
        <div class="social-links">
            <div class="container">
                <div class="social-links-title">
                    <h2>Поделиться:</h2>
                </div>
                <div class="likely">
                    <div class="twitter">Твитнуть</div>
                </div>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="vkontakte">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="telegram">Отправить</div>
                </div>
                <div class="likely">
                    <div class="whatsapp">Отправить</div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->
