<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text city "Город"
 * @param pages text name "Название центра"
 * @param pages datepicker date_display "Дата публикации"

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
?>

<!-- .content -->
<main class="content">
    <div class="page-customer-day">
        <div class="container">
            <h2 class="page-title"><?= $category->short_text ?></h2>
            <div class="customer-items">
                <?php foreach($category->pages as $page): ?>
                <div class="customer-item">
                    <div class="item">
                        <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                            <div class="day-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                        <?php endif ?>
                    </div>
                    <div class="item">
                        <div class="day-text"> 
                            <b><?= $page->title ?></b>
                            <p><?= nl2br($page->short_text) ?></p> 
                            <!--a href="<?= $page->createUrl() ?>"><?= Yii::t('custom', 'Перейти в раздел') ?></a-->
                        </div>
                    </div>
                    <div class="item">
                        <div class="day-data"> <strong><span><?= Yii::app()->dateFormatter->format("d", $page->date_display) ?></span><?= Yii::app()->dateFormatter->format("MMMM", $page->date_display) ?></strong>
                            <h3><?= $page->city ?></h3>
                            <b><?= $page->name ?></b>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:345px;left:initial;top:-100px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-1.svg" alt=""> </div>
            <div class="point" style="right:-65px;left:initial;top:150px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-2.svg" alt=""> </div>
            <div class="point" style="right:-225px;left:initial;top:585px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-3.svg" alt=""> </div>
            <div class="point" style="right:-45px;left:initial;top:1085px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-4.svg" alt=""> </div>
            <div class="point" style="right:505px;left:initial;top:initial;bottom:-125px;"> <img src="/dist/images/icons-customer-day/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:425px;top:initial;bottom:-20px;"> <img src="/dist/images/icons-customer-day/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-75px;top:initial;bottom:-40px;"> <img src="/dist/images/icons-customer-day/i-7.svg" alt=""> </div>
            <div class="point" style="right:initial;left:5px;top:initial;bottom:700px;"> <img src="/dist/images/icons-customer-day/i-8.svg" alt=""> </div>
            <div class="point" style="right:initial;left:25px;top:400px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-9.svg" alt=""> </div>
            <div class="point" style="right:initial;left:310px;top:240px;bottom:initial;"> <img src="/dist/images/icons-customer-day/i-10.svg" alt=""> </div>
        </div>
        <div class="social-links">
            <div class="container">
                <div class="social-links-title">
                    <h2>Поделиться:</h2>
                </div>
                <div class="likely">
                    <div class="twitter">Твитнуть</div>
                </div>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="vkontakte">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="telegram">Отправить</div>
                </div>
                <div class="likely">
                    <div class="whatsapp">Отправить</div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->
