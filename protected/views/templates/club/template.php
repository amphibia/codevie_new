<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children foreword_required off
 * @option children foreword_length_min none
 * @option children foreword_length_max none
 * @option children content none
 * @option children content_required off
 * @option children content_length_min none
 * @option children content_length_max none
 * @param children text title1 "Заголовок 1"
 * @param children tiny_mce text1 "Текст 1"
 * @param children text title2 "Заголовок 2"
 * @param children tiny_mce text2 "Текст 2"
 * @param children text title3 "Заголовок 3"
 * @param children tiny_mce text3 "Текст 3"
 * @param children single_image img "Иллюстрация к статье"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
?>

<!-- .content -->
<main class="content">
    <div class="page-club">
        <div class="container">
            <h2 class="page-title"><?= $category->short_text ?></h2>
        </div>

        <div class="club-tabs">
            <div class="container">
                <ul class="nav-tabs" role="tablist">
                    <?php $n=0; foreach($category->children as $children): ?>
                    <li<?= ($n==0)?' class="active"':'' ?>><a href="#tab_<?= $children->alias ?>" data-toggle="tab"><?= $children->title ?></a></li>
                    <?php $n++; endforeach ?>
                </ul>
            </div>

            <div class="tab-content">
                <?php $n=0; foreach($category->children as $children): ?>
                <div class="tab-pane<?= ($n==0)?' active':'' ?>" id="tab_<?= $children->alias ?>">
                    <div class="container">
                        <div class="club-info">
                            <?php if($children->title1): ?>
                            <div class="club-info-item"> <b><?= $children->title1 ?></b>
                                <p><?= $children->text1 ?></p>
                            </div>
                            <?php endif ?>
                            <?php if($children->title1): ?>
                            <div class="club-info-item"> <b><?= $children->title2 ?></b>
                                <p><?= $children->text2 ?></p>
                            </div>
                            <?php endif ?>
                            <?php if($children->title1): ?>
                            <div class="club-info-item"> <b><?= $children->title3 ?></b>
                                <p><?= $children->text3 ?></p>
                            </div>
                            <?php endif ?>
                        </div>
                    </div>
                   
                    <?php if ($image = Yii::app()->storage->decodeImages($children->img)): ?>
                        <div class="club-img"><img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""></div>
                    <?php endif ?>
                </div>
                <?php $n++; endforeach ?>
            </div>
        </div>
            
        <div class="points-container">
            <div class="point" style="right:340px;left:initial;top:-100px;bottom:initial;"> <img src="/dist/images/icons-stars-club/i-1.svg" alt=""> </div>
            <div class="point" style="right:-65px;left:initial;top:130px;bottom:initial;"> <img src="/dist/images/icons-stars-club/i-2.svg" alt=""> </div>
            <div class="point" style="right:-205px;left:initial;top:initial;bottom:120px;"> <img src="/dist/images/icons-stars-club/i-3.svg" alt=""> </div>
            <div class="point" style="right:initial;left:15px;top:initial;bottom:30px;"> <img src="/dist/images/icons-stars-club/i-4.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-60px;top:320px;bottom:initial;"> <img src="/dist/images/icons-stars-club/i-5.svg" alt=""> </div>
        </div>

        <div class="social-links">
            <div class="container">
                <div class="social-links-title">
                    <h2>Поделиться:</h2>
                </div>
                <div class="likely">
                    <div class="twitter">Твитнуть</div>
                </div>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="vkontakte">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="telegram">Отправить</div>
                </div>
                <div class="likely">
                    <div class="whatsapp">Отправить</div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->
