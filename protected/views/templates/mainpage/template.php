<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self content none
 *
 * @param self full_textarea code_head "Код в HEAD"
 * @param self full_textarea code_before_end_body "Код в конце BODY"
 * @param self text facebook "facebook"
 * @param self text instagramm "instagramm"
 * @param self text tokenInstagramm "tokenInstagramm"
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children content none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages content none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text link "Ссылка"
 * @param pages dropdownlist bg_color "Цвет фона" "black,white"
 * @param pages dropdownlist type_slider "Тип слайдера" "type1,type2"

 */
?>

<!-- .content -->
<main class="content">
    <div class="main-promo">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach($category->pages as $page): ?>
                    <?php if($page->type_slider == 'type1' || $page->type_slider == ''): ?>
                        <div class="swiper-slide slide-<?= $page->bg_color ?>-them">
                            <div class="container">
                                <div class="column-container clearfix">
                                    <div class="col-left">
                                        <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                                            <div class="slide-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" /> </div>
                                        <?php endif ?>
                                    </div>
                                    <div class="col-right">
                                        <div class="slide-text"> <b><?= $page->title ?></b>
                                            <p><?= nl2br($page->short_text) ?></p> <a href="<?= $page->link ?>" class="link-more"><?= Yii::t('custom', 'Подробнее') ?></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="swiper-slide slide-classic-them">
                            <div class="container">
                                <div class="slide-items-title">
                                    <b><?= $page->title ?></b>
                                </div>
                                <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                                    <div class="slide-img">
                                        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
            <div class="main-promo-controls">
                <div class="main-promo-btn btn-prev"><span class="arrow"></span></div>
                <div class="main-promo-btn btn-next"><span class="arrow"></span></div>
            </div>
        </div>
    </div>
    <div class="main-customer-day">
        <?php
        $categoryCustomerDay = Yii::app()->custom->getCategories('customer_day');
        ?>
        <div class="container">
            <h2 class="page-title"><?= $categoryCustomerDay->short_text ?></h2>
            <?php foreach($categoryCustomerDay->pages as $page): ?>
            <div class="customer-item">
                <div class="item">
                    <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                        <div class="day-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                    <?php endif ?>
                 </div>
                <div class="item">
                    <div class="day-text">
                        <b><?= $page->title ?></b>
                        <p><?= nl2br($page->short_text) ?></p>
                    </div>
                </div>
                <div class="item">
                    <div class="day-data"> <strong><span><?= Yii::app()->dateFormatter->format("d", $page->date_display) ?></span><?= Yii::app()->dateFormatter->format("MMMM", $page->date_display) ?></strong>
                        <h3><?= $page->city ?></h3>
                    </div>
                </div>
            </div>
            <?php break; endforeach ?>
            <a href="<?= $categoryCustomerDay->createUrl() ?>" class="link-more"><?= Yii::t('custom', 'Перейти в раздел') ?></a>
        </div>
        <div class="points-container">
            <div class="point" style="right:265px;left:initial;top:-170px;bottom:initial;"> <img src="/dist/images/main-sale-icons/i-1.svg" alt=""> </div>
            <div class="point" style="right:-65px;left:initial;top:130px;bottom:initial;"> <img src="/dist/images/main-sale-icons/i-2.svg" alt=""> </div>
            <div class="point" style="right:-225px;left:initial;top:380px;bottom:initial;"> <img src="/dist/images/main-sale-icons/i-3.svg" alt=""> </div>
            <div class="point" style="right:155px;left:initial;top:initial;bottom:60px;"> <img src="/dist/images/main-sale-icons/i-4.svg" alt=""> </div>
            <div class="point" style="right:525px;left:initial;top:initial;bottom:-45px;"> <img src="/dist/images/main-sale-icons/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:430px;top:initial;bottom:45px;"> <img src="/dist/images/main-sale-icons/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:15px;top:initial;bottom:25px;"> <img src="/dist/images/main-sale-icons/i-7.svg" alt=""> </div>
            <div class="point" style="right:initial;left:35px;top:285px;bottom:initial;"> <img src="/dist/images/main-sale-icons/i-8.svg" alt=""> </div>
            <div class="point" style="right:initial;left:310px;top:145px;bottom:initial;"> <img src="/dist/images/main-sale-icons/i-9.svg" alt=""> </div>
        </div>
    </div>
    <div class="main-bestsellers">
        <?php
        $categoryBestsellers = Yii::app()->custom->getCategories('bestsellers');
        ?>
        <div class="container">
            <h2 class="page-title"><?= $categoryBestsellers->title ?></h2>
            <div class="main-best-tabs">
                <ul class="nav-tabs" role="tablist">
                    <?php $n=0; foreach($categoryBestsellers->children as $children): ?>
                        <li<?= ($n == 0)?' class="active"':'' ?>> <a href="#tab_<?= $children->alias ?>" data-toggle="tab"><?= $children->title ?></a> </li>
                    <?php $n++; endforeach ?>
                </ul>
                <div class="tab-content clearfix">
                    <?php $n=0; foreach($categoryBestsellers->children as $children): ?>
                    <div class="tab-pane<?= ($n == 0)?' active':'' ?>" id="tab_<?= $children->alias ?>">
                        <div class="best-in-carousel">
                            <?php $nPage=0; foreach($children->pages as $page): ?>
                                <div class="best-item">
                                    <div class="block-in">
                                        <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                                            <div class="best-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                                        <?php endif ?>
                                        <div class="best-title">
                                            <b><?= $page->title ?></b>
                                        </div>
                                        <div class="best-text">
                                            <p><?= $page->short_text ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if($nPage > 2) break;
                                $nPage++;
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <?php $n++; endforeach ?>
                </div>
            </div>
            <a href="<?= $categoryBestsellers->createUrl() ?>" class="link-more"><?= Yii::t('custom', 'Перейти в раздел') ?></a>
        </div>
        <div class="points-container">
            <div class="point" style="right:initial;left:500px;top:-35px;bottom:initial;"> <img src="/dist/images/main-best-icons/i-1.svg" alt=""> </div>
            <div class="point" style="right:initial;left:745px;top:135px;bottom:initial;"> <img src="/dist/images/main-best-icons/i-2.svg" alt=""> </div>
            <div class="point" style="right:-135px;left:initial;top:-20px;bottom:initial;"> <img src="/dist/images/main-best-icons/i-3.svg" alt=""> </div>
            <div class="point" style="right:-5px;left:initial;top:540px;bottom:initial;"> <img src="/dist/images/main-best-icons/i-4.svg" alt=""> </div>
            <div class="point" style="right:575px;left:initial;top:initial;bottom:-55px;"> <img src="/dist/images/main-best-icons/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-155px;top:initial;bottom:-205px;"> <img src="/dist/images/main-best-icons/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:45px;top:initial;bottom:225px;"> <img src="/dist/images/main-best-icons/i-7.svg" alt=""> </div>
            <div class="point" style="right:initial;left:15px;top:260px;bottom:initial;"> <img src="/dist/images/main-best-icons/i-8.svg" alt=""> </div>
            <div class="point" style="right:-55px;left:initial;top:initial;bottom:-25px;"> <img src="/dist/images/main-best-icons/i-10.svg" alt=""> </div>
        </div>
    </div>
    <div class="instagramm-widget">
        <div class="container">
            <div class="insta-top">
                <h2 class="page-title"><?= Yii::t('custom', 'Code de Vie В INSTAGRAM') ?></h2> <a href="<?= $category->instagramm ?>" class="link-more" target="_blank"><?= Yii::t('custom', 'ВСЯ ЛЕНТА') ?></a> </div>
        </div>
        <div class="insta-carousel">
            <div class="container">
                <div class="swiper-container">
                    <div class="swiper-wrapper" id="instafeed" data-accessToken="<?= $category->tokenInstagramm ?>"></div>
                </div>
            </div>
            <div class="insta-controls">
                <div class="insta-btn btn-prev"><span class="arrow"></span></div>
                <div class="insta-btn btn-next"><span class="arrow"></span></div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->