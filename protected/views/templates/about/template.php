<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery on
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content html
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img "Иллюстрация к статье"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
?>

<!-- .content -->
<main class="content">
    <div class="page-about">
        <div class="about-information">
            <div class="container">
                <h2 class="page-title"><?= $category->title ?></h2>
                <div class="about-img">
                    <div class="back-img"></div>
                    <div class="item-img">
                        <?php if ($image = Yii::app()->storage->decodeImages($category->img)): ?>
                            <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                        <?php endif ?>
                    </div>
                </div>
                <div class="about-text"> <b><?= nl2br($category->short_text) ?></b>
                    <?= $category->full_text ?>
                </div>
            </div>
        </div>
        <div class="about-widgets">
            <div class="container">
                <?php if ($images = Yii::app()->storage->decodeImages($category->gallery)): ?>
                    <?php foreach($images as $img): ?>
                        <?php
                        $img['description'] =  explode('|', $img['description']);
                        ?>
                        <a href="<?= (isSet($img['description'][1]))? $img['description'][1]:'#' ?>">
                        <div class="widget">
                            <div class="widget-img"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt=""> </div>
                            <div class="widget-text"> <b><?= $img['title'] ?></b>
                                <p><?= $img['description'][0] ?></p>
                            </div>
                        </div>
                        </a>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:-65px;left:initial;top:150px;bottom:initial;"> <img src="/dist/images/icons-about/i-1.svg" alt=""> </div>
            <div class="point" style="right:-190px;left:initial;top:500px;bottom:initial;"> <img src="/dist/images/icons-about/i-2.svg" alt=""> </div>
            <div class="point" style="right:-45px;left:initial;top:930px;bottom:initial;"> <img src="/dist/images/icons-about/i-3.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-155px;top:initial;bottom:200px;"> <img src="/dist/images/icons-about/i-4.svg" alt=""> </div>
            <div class="point" style="right:initial;left:40px;top:initial;bottom:600px;"> <img src="/dist/images/icons-about/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:30px;top:320px;bottom:initial;"> <img src="/dist/images/icons-about/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:310px;top:210px;bottom:initial;"> <img src="/dist/images/icons-about/i-7.svg" alt=""> </div>
            <div class="point" style="right:335px;left:initial;top:-120px;bottom:initial;"> <img src="/dist/images/icons-about/i-8.svg" alt=""> </div>
        </div>
    </div>
</main>
<!-- end .content -->
