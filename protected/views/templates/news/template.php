<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image image "Иллюстрация к новости"
 * @param pages datepicker date_display "Дата публикации"
 */
?>

<? define('PAGE_SIZE', 9); ?>

<?
$newsCategoryUrl = $category->createUrl();
$dataProvider = $this->model->getActiveDataProvider();
$dataProvider->pagination->pageSize = PAGE_SIZE;
$dataProvider->pagination->route = $newsCategoryUrl;
?>
<div class="news">

    <? if ($dataProvider->totalItemCount): ?>

        <? foreach ($dataProvider->data as $index => $newsItem): ?>
            <div class="newsItem">
                <div class="img">
                    <?
                    if ($newsImage = Yii::app()->storage->decodeImages($newsItem->image)) {
                        $newImageUrl = Yii::app()->storage->createUrl($newsImage[0], '_2to1_small');
                        echo CHtml::tag('a', array('class' => 'image-container', 'href' => "$newsCategoryUrl/{$newsItem->alias}"),
                            CHtml::image($newImageUrl, $newsImage[0]['title'])
                        );
                    }
                    ?>
                </div>
            <span
                class="publish-date"><?= Yii::app()->dateFormatter->formatDateTime($newsItem->date_display, 'long', NULL); ?></span>
                <a href="<?= "$newsCategoryUrl/{$newsItem->alias}" ?>" class="link"><?= $newsItem->title ?></a>

                <p><?= nl2br($newsItem->short_text) ?></p>
            </div>
        <? endforeach ?>

    <? endif ?>

    <?
    $this->widget('CLinkPager', array(
        'cssFile' => false,
        'pages' => $dataProvider->pagination,
        'maxButtonCount' => 7,
        'htmlOptions' => array('class' => 'pagination'),
        'selectedPageCssClass' => 'active',
        'hiddenPageCssClass' => 'disabled',
        'firstPageCssClass' => 'hidden',
        'lastPageCssClass' => 'hidden',

        'prevPageLabel' => Yii::t('custom', '← Назад'),
        'nextPageLabel' => Yii::t('custom', 'Вперед →'),
        'lastPageLabel' => '',
        'firstPageLabel' => '',

        'header' => '',
    ));
    ?>
    <div class="clearfix"></div>

</div>