<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 * @param self form_editor form_data "Настройки формы отправки"
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children foreword_required off
 * @option children foreword_length_min none
 * @option children foreword_length_max none
 * @option children content none
 * @option children content_required off
 * @option children content_length_min none
 * @option children content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery off
 * @option pages foreword html
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages text gps_lat "Координата метки на карте (GPS Lat)"
 * @param pages text gps_lng "Координата метки на карте (GPS Lng)"
 * @param pages text gps_zoom "GPS Zoom"
 * @param pages single_image marker_icon "Иконка на карте"
 * @param pages single_image img "Иллюстрация к статье"
 */

?>

<!-- .content -->
<main class="content">
    <div class="page-contacts">
        <div class="container">
            <h2 class="page-title"><?= $category->title ?></h2>
            <div class="contacts-tabs">
                <!-- Основная навигация по городам -->
                <ul class="nav-tabs" role="tablist">
                    <?php $n=0; foreach($category->childrenTech as $children): ?>
                    <li<?= ($n == 0)?' class="active"':'' ?>> <a href="#tab_<?= $children->alias ?>" data-toggle="tab" class="up-links"><?= $children->title ?></a> </li>
                    <?php $n++; endforeach ?>
                 </ul>
                <!-- конец основной навигации -->
                <!-- родитель контента всех городов -->
                <div class="tab-content">

                    <?php $n=0; foreach($category->childrenTech as $children): ?>
                    <div class="tab-pane tab-pane-up<?= ($n == 0)?' active':'' ?>" id="tab_<?= $children->alias ?>">

                        <ul class="nav-tabs-inside" role="tablist">
                            <?php $n2=0; foreach($children->pages as $page): ?>
                            <li<?= ($n2 == 0)?' class="active"':'' ?>> <a href="#tab_<?= $children->alias ?>_<?= $page->alias ?>" data-toggle="tab"><?= $page->title ?></a> </li>
                            <?php $n2++; endforeach ?>
                        </ul>

                       <div class="tab-content-inside" id="jsMapTabs">
                           <?php $n2=0; foreach($children->pages as $page): ?>
                            <div class="tab-pane tab-pane-down<?= ($n2 == 0)?' active':'' ?>" id="tab_<?= $children->alias ?>_<?= $page->alias ?>">
                                <!-- блок с адресом и картой -->
                                <div class="contact-item">
                                    <!-- текст адресса -->
                                    <div class="adres">
                                        <p><?= $page->short_text ?></p>
                                    </div>
                                    <!-- end -->
                                    <!-- картинка магазина -->
                                    <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                                        <div class="place-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                                    <?php endif ?>
                                    <!-- end -->
                                    <!-- карта -->
                                    <div class="map-container">
                                        <?php
                                        $imgUrl = '/images/map-marker.svg';
                                        if ($image = Yii::app()->storage->decodeImages($page->marker_icon)) $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
                                        ?>
                                        <div class="map jsMap" data-latitude="<?= $page->gps_lat ?>" data-longitude="<?= $page->gps_lng ?>" data-marker="<?= $imgUrl ?>"> </div>
                                    </div>
                                    <!-- end -->
                                </div>
                                <!-- end -->
                            </div>
                            <?php $n2++; endforeach ?>
                        </div>

                    </div>
                    <?php $n++; endforeach ?>
                </div>
                <!-- Конец контента всех городов -->
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:315px;left:initial;top:-80px;bottom:initial;"> <img src="/dist/images/icons-contacts/i-1.svg" alt=""> </div>
            <div class="point" style="right:-65px;left:initial;top:130px;bottom:initial;"> <img src="/dist/images/icons-contacts/i-2.svg" alt=""> </div>
            <div class="point" style="right:-225px;left:initial;top:initial;bottom:-80px;"> <img src="/dist/images/icons-contacts/i-3.svg" alt=""> </div>
            <div class="point" style="right:initial;left:340px;top:initial;bottom:80px;"> <img src="/dist/images/icons-contacts/i-4.svg" alt=""> </div>
            <div class="point" style="right:initial;left:25px;top:initial;bottom:240px;"> <img src="/dist/images/icons-contacts/i-5.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-60px;top:370px;bottom:initial;"> <img src="/dist/images/icons-contacts/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:620px;top:180px;bottom:initial;"> <img src="/dist/images/icons-contacts/i-7.svg" alt=""> </div>
        </div>
    </div>
</main>
<!-- end .content -->