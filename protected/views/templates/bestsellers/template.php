<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
?>

<!-- .content -->
<main class="content">
    <div class="page-bestsellers">
        <div class="container">
            <h2 class="page-title"><?= $category->parent->title ?></h2>
            <div class="bestsellers-tabs">
                <ul class="nav-tabs" role="tablist">
                    <?php foreach($category->parent->children as $children): ?>
                    <li<?= ($category->id == $children->id)?' class="active"':'' ?>> <a href="#tab_<?= $children->alias ?>" data-toggle="tab"><?= $children->title ?></a> </li>
                    <?php endforeach ?>
                </ul>
                <div class="tab-content clearfix">
                    <?php $n=0; foreach($category->parent->children as $children): ?>
                    <div class="tab-pane<?= ($n ==0)?' active':'' ?>" id="tab_<?= $children->alias ?>">
                        <?php foreach($children->pages as $page): ?>
                        <div class="best-item">
                            <div class="block-in">
                                <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                                    <div class="best-img"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                                <?php endif ?>
                                <div class="best-title">
                                     <b><?= $page->title ?></b>
                                </div>
                                <div class="best-text">
                                    <p><?= $page->short_text ?></p>
                                    <?= $page->full_text ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                    <?php $n++; endforeach ?>
                </div>
            </div>
        </div>
        <div class="points-container">
            <div class="point" style="right:initial;left:493px;top:50px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-1.svg" alt=""> </div>
            <div class="point" style="right:initial;left:750px;top:230px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-2.svg" alt=""> </div>
            <div class="point" style="right:-130px;left:initial;top:65px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-3.svg" alt=""> </div>
            <div class="point" style="right:0px;left:initial;top:880px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-4.svg" alt=""> </div>
            <div class="point" style="right:-60px;left:initial;top:initial;bottom:80px;"> <img src="/dist/images/icons-bestsellers/i-5.svg" alt=""> </div>
            <div class="point" style="right:130px;left:initial;top:initial;bottom:-200px;"> <img src="/dist/images/icons-bestsellers/i-6.svg" alt=""> </div>
            <div class="point" style="right:initial;left:345px;top:initial;bottom:-50px;"> <img src="/dist/images/icons-bestsellers/i-7.svg" alt=""> </div>
            <div class="point" style="right:initial;left:50px;top:620px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-8.svg" alt=""> </div>
            <div class="point" style="right:initial;left:15px;top:315px;bottom:initial;"> <img src="/dist/images/icons-bestsellers/i-9.svg" alt=""> </div>
            <div class="point" style="right:initial;left:-100px;top:initial;bottom:240px;"> <img src="/dist/images/icons-bestsellers/i-10.svg" alt=""> </div>
        </div>
        <div class="social-links">
            <div class="container">
                <div class="social-links-title">
                    <h2>Поделиться:</h2>
                </div>
                <div class="likely">
                    <div class="twitter">Твитнуть</div>
                </div>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="vkontakte">Поделиться</div>
                </div>
                <div class="likely">
                    <div class="telegram">Отправить</div>
                </div>
                <div class="likely">
                    <div class="whatsapp">Отправить</div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end .content -->