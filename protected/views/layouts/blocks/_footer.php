</div>
<!-- .footer -->
<footer class="footer">
    <div class="container">
        <div class="col">
            <div class="copyright">
                <p><?= Yii::t('custom', '2017 Codevie. Все права защищены') ?></p>
            </div>
        </div>
        <div class="col">
            <div class="social">
                <p><?= Yii::t('custom', 'Мы в соц. сетях:') ?></p>
                <div class="social-links">
                    <a href="<?= $mainPageCategory->facebook ?>" target="_blank"></a>
                    <a href="<?= $mainPageCategory->instagramm ?>" target="_blank"></a>
                </div>
            </div>
        </div>
        <!-- <div class="col">
            <div class="made-in">
                <p><?= Yii::t('custom', 'Сделано в') ?><a href="http://amphibia.kz/">Amphibia</a></p>
            </div>
        </div> -->
    </div>
</footer>
<!-- end .footer -->
</div>
<!-- end .wrapper -->
<div class="m-overlay"></div>
<?= $mainPageCategory->code_before_end_body ?>
</body>

</html>