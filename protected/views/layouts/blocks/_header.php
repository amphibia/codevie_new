<?php
$categories = Yii::app()->custom->getCategories(array('mainMenu'));

// Атрибуты тега body
$bodyAttributes = array();
if (isset($currentCategory)) {
    if(Yii::app()->struct->currentTemplate)
    {
        $bodyAttributes['data-template'] = Yii::app()->struct->currentTemplate;
    }
    else{
        $bodyAttributes['data-template'] = 'mainpage';
    }
}

Yii::app()->clientScript->scriptMap = array('jquery.js' => false);
//Yii::app()->clientScript->registerScriptFile('/js/jquery-1.11.2.min.js');

Yii::app()->clientScript->registerCssFile('/dist/styles/vendor.min.css');
Yii::app()->clientScript->registerCssFile('/dist/styles/main.min.css');

//Yii::app()->clientScript->registerScriptFile('/js/forms.js');
Yii::app()->clientScript->registerScriptFile('http://maps.google.com/maps/api/js?key=AIzaSyCMj2rfyStL44OtZzxpnLCI8JiJs-dSvkA');
//Yii::app()->clientScript->registerScriptFile('/dist/scripts/vendor.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('/dist/scripts/main.min.js', CClientScript::POS_END);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script src="/dist/scripts/vendor.min.js"></script>

    <!-- IE SUPPORT -->
    <!--[if lte IE 9]>
    <script src="/js/libs/jquery.placeholder.min.js"></script>
    <script>$().ready(function(){ $('[placeholder]').placeholder(); });</script>
    <![endif]-->

    <meta charset="utf-8" />
    <?php
    if (
        (($currentCategoryOrPage = Yii::app()->struct->currentPage) || ($currentCategoryOrPage = Yii::app()->struct->currentCategory))
        && $currentCategoryOrPage->issetCustomField('_seo_keywords') && $currentCategoryOrPage->_seo_keywords
        && $currentCategoryOrPage->issetCustomField('_seo_description') && $currentCategoryOrPage->_seo_description
    ):
        ?>
        <meta name="keywords" content="<?= $currentCategoryOrPage->_seo_keywords ?>" />
        <meta name="description" content="<?= $currentCategoryOrPage->_seo_description ?>" />
    <?php elseIf(
        $mainPageCategory
        && $mainPageCategory->issetCustomField('_seo_keywords') && $mainPageCategory->_seo_keywords
        && $mainPageCategory->issetCustomField('_seo_description') && $mainPageCategory->_seo_description
    ): ?>
        <meta name="keywords" content="<?= $mainPageCategory->_seo_keywords ?>" />
        <meta name="description" content="<?= $mainPageCategory->_seo_description ?>" />
    <?php endIf; ?>

    <title><?= Yii::app()->custom->getPageTitle() ?></title>

    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="theme-color" content="#000000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <?= $mainPageCategory->code_head ?>
</head>

<body <?= CHtml::renderAttributes($bodyAttributes) ?>>

<!--[if lt IE 10]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser.
    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->

<!-- .wrapper -->
<div class="wrapper">
    <div class="wrapper-in">
        <!-- .header -->
        <header class="header">
            <div class="container">
                <div class="logo"> 
                        <a href="/">
                            <img src="../images/logo.svg">
                            <small>Сеть магазинов косметики и парфюмерии</small>
                        </a> 
                </div>
                <!-- nav-main -->
                <nav class="nav-main">
                    <div class="container">
                        <?
                        $this->widget('application.modules.struct.StructMenu', array(
                            'showRoot' => false,
                            'depth' => 1,
                            'parentCategory' => $categories['mainMenu'] ? $categories['mainMenu']->id : null,
                            'htmlAttributes' => array('class' => 'list-inline'),
                        ));
                        ?>
                    </div>
                </nav>
                <!-- end nav-main -->
                <div class="m-nav-btn">
                    <span></span>
                </div>
            </div>
        </header>
        <!-- end .header -->