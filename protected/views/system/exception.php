<?

// Определение, не является ли этот пользователь "super_user".
// Для этого, должен быть установлен модуль "users".
$superUser = false;
if(	Yii::app()->hasComponent('users')
	&& isset(Yii::app()->user->model)
	&& isset(Yii::app()->user->model->role)
	&& Yii::app()->user->model->role->super_user)
	$superUser = true;

if($superUser || Yii::app()->params['showErrorsForAll'])
	 require '_debug.php';
else require 'error.php';

?>