<?php

/**
 * @title {Language change controller}
 * @desc {Language change controller}
 */
class LanguageController extends Controller
{
    public $_useAutoArguments = true;

    public function actionSet($value)
    {
        if (array_key_exists($value, Yii::app()->lang->languages)) {
            Yii::app()->lang->setLanguage($value);
        }

        $redirectUri = '/';
        if (isset($_SERVER['HTTP_REFERER'])) {
            $pathinfo = parse_url($_SERVER['HTTP_REFERER']);
            if ($pathinfo['host'] == $_SERVER['HTTP_HOST'])
                $redirectUri = $_SERVER['HTTP_REFERER'];
        }
        Yii::app()->request->redirect($redirectUri);
    }
}