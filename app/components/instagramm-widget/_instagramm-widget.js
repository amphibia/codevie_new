(function(){
	
	var instagramWidgetFeed = new Instafeed({
		get: 'user',
		userId: 4037558200,
		accessToken: $('#instafeed').attr("data-accessToken"),
		resolution: 'low_resolution',
		template: 
				'<div class="swiper-slide">' +
					'<a href="{{link}}" target="_blank">' + 
						'<img src="{{image}}">' +
					'</a>' +
				'</div>',
		after: function(){
			instagramSwiper = new Swiper('.instagramm-widget .swiper-container', {
				
				simulateTouch: false,
				spaceBetween: 20,
				loop: true,
				autoplay: 3000,
				prevButton: '.insta-btn.btn-prev',
				nextButton: '.insta-btn.btn-next',
				breakpoints: {
					1920: {
						slidesPerView: 6,
						centeredSlides: false
					},
					1400: {
						slidesPerView: 4,
						centeredSlides: false
					},
					1200: {
						slidesPerView: 3,
						centeredSlides: false
					},
					1024: {
						slidesPerView: 'auto',
						centeredSlides: true
					}
				}
			})
		}
	});

	if($('#instafeed').length)
		instagramWidgetFeed.run();
		
})();