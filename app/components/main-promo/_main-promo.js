(function(){
	var contentCarousel = new Swiper('.main-promo .swiper-container', {
		prevButton: '.main-promo-btn.btn-prev',
		nextButton: '.main-promo-btn.btn-next',
		loop: true,
		speed: 1200,
		autoplay: 5000,
		effect: 'fade'
	})
})();