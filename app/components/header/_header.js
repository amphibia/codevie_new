(function(){
	/* 
		w - width 
		h - header
		top - Window scroll position of top
	*/
   	var w =  $(window).width();
   	var h = $('.header');
   	if (w >= 1025) {
   		$(window).scroll(function() {
   			var top = $(window).scrollTop();
   			if (top >= 50) {
   				h.addClass('init_scroll');
   			} else {
   				h.removeClass('init_scroll');
   			}
   			if (top > 850) {
   				h.addClass('show');
   			} else {
   				h.removeClass('show');
   			}
   		});
   	}
})();