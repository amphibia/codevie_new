(function(){
	var initiMap = function(){
		var tabs = $('.tab-content').find('.tab-pane-up.active').find('#jsMapTabs');
		tabs.find('.jsMap').each(function(i,elem){
	        var div         = $(elem);
	        var latitude    = div.attr('data-latitude');
	        var longitude   = div.attr('data-longitude');
	        var icon        = div.attr('data-marker');
	        var latLng      = new google.maps.LatLng(latitude, longitude);
	        var tab         = div.parents('.tab-pane-down').attr('id');
	        var mapStyle 	= [{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"saturation":"0"},{"color":"#444444"}]},{"featureType":"administrative.country","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f7f7f7"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":"44"},{"visibility":"simplified"},{"color":"#ff6e6c"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#342a3f"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"53"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.rail","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f2d9d9"},{"visibility":"on"}]}];
	        var map = new google.maps.Map(div[0], {
	            zoom:       15,
	            center:     latLng,
	            fullscreenControl: true,
	            styles: mapStyle,
	            mapTypeId:  google.maps.MapTypeId.ROADMAP
	        });
	        var marker = new google.maps.Marker({
	            icon:       icon,
	            position:   latLng,
	            map:        map,
	            flat:       true,
	            optimized:  true,
	            draggable:  false
	        });
	        var resize = function(){
	            var center = map.getCenter();
	            google.maps.event.trigger(map,'resize');
	            map.setCenter(center);
	        };
	        google.maps.event.addDomListener(window,'resize', function(){
	            resize();
	        });
	        $('[data-toggle="tab"]').filter('[href="#'+ tab +'"]').on('shown.bs.tab', function(e){
	            resize();
	        });
	    });
	};
	initiMap();
	$('a.up-links').on('shown.bs.tab', function () {
		initiMap();
	});
    
})();