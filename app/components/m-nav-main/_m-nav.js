(function(){

	var btn = $('.m-nav-btn');
	var nav = $('.nav-main');
	var over = $('.m-overlay');
	var body = $('body');

	btn.on('click', function(){
		if ($(this).hasClass('open')) {
			btn.removeClass('open');
			nav.removeClass('open');
			over.removeClass('open');
			body.removeClass('open');
		} else {
			btn.addClass('open');
			nav.addClass('open');
			over.addClass('open');
			body.addClass('open');
		}
	});
	over.on('click', function(){
		btn.removeClass('open');
		nav.removeClass('open');
		over.removeClass('open');
		body.removeClass('open');
	});
	
})();