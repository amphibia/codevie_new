(function(){
	if ($(window).width() <= 1024) {
		var $main = $('.main-best-tabs');
		$main.find('.tab-content').find('.tab-pane').each(function(i,elem){
			var _self = $(elem);
			var $slider = _self.find('.best-in-carousel');
			$slider.flickity({
				// contain: true
				// wrapAround: true
			});
		});
		$main.find('[data-toggle="tab"]').on('shown.bs.tab', function () {
			$('.best-in-carousel').flickity('resize');
		});
	}
	// if ($(window).width() > 1025) {
	// 	equalheight = function(container){
	// 		var currentTallest = 0,
	// 			currentRowStart = 0,
	// 			rowDivs = new Array(),
	// 			$el,
	// 			topPosition = 0;
	// 		$(container).each(function() {

	// 			$el = $(this);
	// 			$($el).height('auto')
	// 			topPostion = $el.position().top;

	// 			if (currentRowStart != topPostion) {
	// 				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	// 					rowDivs[currentDiv].height(currentTallest);
	// 				}
	// 				rowDivs.length = 0; // empty the array
	// 				currentRowStart = topPostion;
	// 				currentTallest = $el.height();
	// 				rowDivs.push($el);
	// 			} else {
	// 				rowDivs.push($el);
	// 				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	// 			}
	// 			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	// 				rowDivs[currentDiv].height(currentTallest);
	// 			}
	// 		});
	// 	}

	// 	equalheight('.best-item');

	// 	$(window).resize(function(){
	// 		equalheight('.best-item');
	// 	});
	// }
})();