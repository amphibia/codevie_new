<?php
define('MICRO_TIME', microtime(true));
header("Content-Type: text/html;charset=UTF-8");

error_reporting(E_ALL);
date_default_timezone_set('Asia/Almaty');

// change the following paths if necessary
//$yiiBase=dirname(__FILE__).'/../yii_core/framework/yii.php';
$yiiBase = dirname(__FILE__) . '/../yii/YiiBase.php';
$yii = dirname(__FILE__) . '/protected/extensions/extcore/ExtCoreYii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);
Yii::$classMap['ExtCoreWebApplication'] = "protected/extensions/extcore/ExtCoreWebApplication.php";
Yii::createApplication('ExtCoreWebApplication', $config)->run();
//Yii::createWebApplication($config)->run();