## Getting started

- Install dependencies: `npm install --global gulp-cli bower`
- Run `npm install` to install dev dependencies
- Run `gulp serve` to preview and watch for changes
- Run `gulp` to build your webapp for production

## App structure

```
app
|- fonts
|- images
|- scripts
|- styles
|- views
|- components
```
